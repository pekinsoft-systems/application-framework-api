# Application Framework API



## Java/Swing Desktop Application Development Made Easy

The *Application Framework API* has the lofty goal of making the development of Java/Swing desktop applications easy, to the point where all a developer needs to do is concentrate on the business logic of the application.

The way we have accomplished this is by making sure that all of the wiring and plumbing that is required by a desktop application is already in place and ready to use. For example, if one were to simply subclass our `com.pekinsoft.framework.PlatformApplication` class and create a `*.resources.CustomApplication.properties` resource bundle to brand their application, they will already have a main window (with *pseudo*-docking enabled), an options dialog, an about dialog, a main menu bar, and toolbars. The menu bar and toolbars are all generated automatically by seeking out providers of the `com.pekinsoft.api.ActionBean` abstract class. The `ActionBean` class defines the methods required for the *Framework* to locate and install both menu and/or toolbar actions. `ActionBean` also has no requirements that a subclass implement or override any of its methods. The only reason for needing to extend `ActionBean` is to define the custom resources for the menu items and/or toolbar buttons that need to be created.

All of the branding takes place within a simple resource bundle file that has the same name as the `Application` subclass, and is located in a subpackage of that class, named "`resources`". Change out the various resource keys with the values that make sense for your custom application. Once you have done that, all it takes to launch your (albeit *blank* at this point) application is:

```java
public class MyApplication extends PlatformApplication {

    public static void main(String... args) {
        Application.launch(MyApplication.class, args);
    }

}
```

With that single line of code, you will have a main window showing on the desktop, will have a menu bar and toolbar, an options dialog, and an about screen. If you also defined, at a minimum, the following resources, your application will be branded to you:

```
Application.title = My Cool Application
Application.name = MyApp
Application.id = myapp
Application.vendor = My Rockin' Software
Application.vendorId = Rockin Software
Application.icon = logo.png
Application.homepage = https://my.rockinsoftware.com/
```

To have all of this work as just described, your project structure would need to be:

```
+-> src/
    +-> com
        +-> rockinsoftware
            +-> my
                +-> resources
                |   +-> MyApplication.properties
                |   +-> logo.png
                +-> MyApplication.java
```

With the above defined, when you run your application, you will see the following:

![`MyApplication` Running with About Dialog](images/MyApplication-Running.png?ref_type=heads)

![`MyApplication` Running with Options Dialog](images/MyApplication-Running-with-Options-Dialog.png?ref_type=heads)

To make your application useful, you would then lay out the rest of your required package structure, build your child windows (using the `javax.swing.JInternalFrame` for the docking windows), and code your business-logic. No need to worry about all of the details that make the components communicate, just add classes that are interested in listening for property changes as change listeners to the `ApplicationContext`, and they will be notified of the changes.

## Authors and acknowledgment
The *Application Framework API* is being developed and maintained by Pekin<sup>SOFT</sup> Systems and Sean Carrick (the Pekin<sup>SOFT</sup> founder).

## License
The *Application Framework API* is licensed under the GNU General Public License.

## Project status
Workable, but only if the `SingleFrameApplication` subclass overrides the `startup` method and provides their own main window at this time (APR 16, 2024), due to the rest of the *platform* application still being built and tested.
