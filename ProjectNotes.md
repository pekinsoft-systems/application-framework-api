# Application Framework Project Notes
These notes are being created to remember items that may need to be fixed or
changed within the Application Framework as I test it with a project.

## Changes/Fixes Needed
- **COMPLETE**: Need to modify `View` and `FrameView` to take an `ApplicationContext`, instead
  of the `Application` that they currently take for a parameter. *This is because
  the entirety of the Framework is accessible via the `ApplicationContext`, so 
  the `Application` is really not needed.* 
  
  Typically, the `Application` is only used to get an instance of the 
  `ApplicationContext` anyway. Therefore, there is no need to be passing the
  `Application` itself around.
  
  **May want to check and change other classes in the Framework as well.**
- **COMPLETE**: Fix the `Application.initRootPaneContainer` method to provide a decent default
  window size.
- **COMPLETE**: need to move the `resources` packages into 
  `main/resources/${package.path}/resources`.
----
- **COMPLETE**: `StorageLocations`: only offers `CONFIG_DIR`, `DATA_DIR`, `ERROR_DIR`, 
`FILE_SYSTEM`, and `LOG_DIR`. We need to also offer `APP_HOME` and `USER_HOME`. 
Possibly for 2.5.2 update.
- **COMPLETE**: Add the class `ResourceUtils` that provides a method for copying
template and other files out of the JAr and onto the hard drive. This class was
primarily created for use by the `Database` class for database table generation
and inserting default data, but can be used by any class that needs this functionality.

For 2.5.6 Update:

- **COMPLETE**: Change `StatusMessage` to require an `Level` instead of a `java.util.Level`, so that no casting
  is required.
- **COMPLETE**: Add the following methods to `ApplicationContext`:
    - `getWindowManager` to allow for getting the default `WindowManager` implementation.
    - `getDockingManager` to allow for getting the default `DockingManager` implementation.
    - `getStatusDisplayer` to allow for getting the default `StatusDisplayer` implementation.
    - `getProgressHandler` to allow for getting the default `ProgressHandler` implementation.

  Each of these getters should be implemented as follows (using `getWindowManager` as an example):

    ```java
    public WindowManager getWindowManager() {
        if (getApplication().getMainView() instanceof WindowManager wm) {
            return wm;
        } else {
            Lookup<WindowManager> lkp = Lookups.getDefault();
            return Lookup.getDefault().lookup(WindowManager.class);
        }
    }

    ```

    This should cover all instances where the `mainView` property of the `Application` implements either the `WindowManager` or `DockingManager` interface. For the `StatusDisplayer` and `ProgressHandler`, the methods should be implemented as (using `StatusDisplayer` as an example):

    ```java
    public StatusDisplayer getStatusDisplayer() {
        Component component = getApplication().getMainView().getStatusBar();
        if (component instanceof StatusDisplayer sd) {
            return sd;
        } else {
            Lookup<StatusDisplayer> lkp = Lookups.getDefault();
            return Lookup.getDefault().lookup(StatusDisplayer.class);
        }
    }
    ```
- **COMPLETE**: `ToolBarGenerator` is not creating `javax.swing.ButtonGroup`s properly because the `MenuGenerator` writes the generated ID value back to the `ActionX`. Therefore, need to add an explanation to the `MenuGenerator` class JavaDoc that `MenuGenerator` needs to be called first in an `Application`, then `ToolBarGenerator` can be called. When the `ActionX.GROUP` value is pulled from the `ActionX` in `MenuGenerator`, the original value needs to be stored in a ***package-private*** `String` field that can then be accessed from the `ToolBarGenerator` class when the `ActionX.GROUP` is being checked for creating `JToggleButton`s.

  Currently, the `JToggleButton`s get created in `ToolBarGenerator`, but they do not act as part of a `ButtonGroup`. In other words, both `JToggleButton`s are able to be selected at the same time. This has got to be a result of not having the `ActionX.GROUP` *name* available, just the key that is generated in `MenuGenerator`. Therefore, if we are able to access the original group **name** from the `MenuGenerator`, we may get properly functioning `JToggleButton`s for the toolbars that are generated.
