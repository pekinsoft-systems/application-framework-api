# Simple Wizard API
Rethinking the Wizard API to simplify it down. The current Wizard API, while nice and comprehensive, is a bit overly complex to use. Being made up of thirty (30) classes, it does a lot of neat things, but creates a bit of a mess when attempting to use it for a simple wizard.

Therefore, I would like to create a new Simple Wizard API which allows for branching to different steps in an easier fashion than the current Wizard API does. This will allow for users to easily skip steps in which they have no interest or for which they have no use. In doing this, we will want to define the following classes:

- `Wizard`: The primary class by which developers create and display a Wizard to the user. This class will contain static methods for showing a Wizard, but they will need to return the Wizard instance so that the Wizard's exit status can be determined, such as whether it was finished or cancelled.
- `WizardController`: The class that controls the Wizard and its data, allowing for the Wizard to flow through its steps based upon user selections.
- `WizardDialog`: The window that will display the Wizard to the user. This class will use a couple of supporting classes to make up the entirety of the window.
- `NavigationPanel`: This class will handle the navigation of the Wizard, by providing buttons for moving back and forward, finishing, and cancelling the Wizard. The buttons' enablement will need to be determined by the current state of the Wizard and the step that is currently being displayed to the user.
- `SidePanel`: This class will be a `JPanel` implementation that handles custom painting that shows the list of steps that make up the Wizard, as well as which step the user is currently on. This will simply provide feedback to the user. The panel itself will default to using a background color that is in contrast to the text color, but will have the ability to use a background image as well. The text color for the panel will need to be determined based on either the background color or primary color of the background image in order to contrast nicely and be readily visible.
- `WizardFinisher`: This will be an interface that the developer may implement and pass the implementing class to the `Wizard`'s show method to allow for performing the actions or computations for which the Wizard is designed to gather the information from the user.
- `WizardStep`: This is an abstract `JPanel` class which is to be implemented to supply the UI that gathers the data from the user.
- `NavigationMode`: An enumeration that may be set by the `WizardStep`s to the `WizardController` so that it may enable/disable the navigation buttons properly.

## Wizard Class
The `Wizard` class will need to have static methods to show a Wizard. Each of these static methods should return the `Wizard` instance, which may then be used to determine the finishing status of the Wizard, such as that is was finished or cancelled. This will allow the implementing application to take appropriate steps based on this status.

The methods that should be available are:

- `Wizard.showWizard(ApplicationContext, String, WizardFinisher, WizardStep...)`: This method will take in the related `ApplicationContext`, the title of the Wizard, the class that should be called to process the data, and an open-ended array of `WizardStep` UI components to gather the information from the user.
- `Wizard.showWizard(ApplicationContext, String, WizardFinisher, Runnable, WizardStep...)`: This method will take everything that the first method takes, adding a `Runnable` to use as a callback in order to create a blocking Wizard. This method is useful for setting up an `Application` on its first run and keeping the rest of the application code from running until the Wizard has properly completed.

As necessary, we can add more of these static methods as we find need.

## NavigationMode Enumeration
The `NavigationMode` enumeration will define the available methods of navigation base upon the status of the current step (individually) and the state of the Wizard (as a whole). The defined constants should be:

- `MODE_CAN_CONTINUE`: Allows for forward-only navigation. Used when more data is required in order to finish the Wizard.
- `MODE_CAN_CONTINUE_OR_FINISH`: Allows for forward navigation and finishing the Wizard. Used when remaining data is optional and the required data has been collected so the Wizard can finish.
- `MODE_CAN_FINISH`: Allows the Wizard to be finished. Used when there is no more data to be collected and the Wizard can be finished.

What needs to be determined with this is whether we should use an `enum` or make these constant integer values in the `Wizard` or `WizardController` class. Either way, these will be needed to make sure that the navigation system works as expected.
