/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.pekinsoft.validation.builtin.stringvalidation;

import com.pekinsoft.validation.Problems;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;


/**
 * Does not allow a string to terminate with a particular character
 *
 * @author Tim Boudreau
 */
final class MayNotEndWithValidator extends StringValidator {
    
    private final ResourceMap rm;
    private final char c;
    
    public MayNotEndWithValidator(char c) {
        this.c = c;
        rm = Application.getInstance().getContext().getResourceMap(getClass());
    }

    @Override
    public void validate(Problems problems, String compName, String model) {
        if (model != null && model.charAt(model.length() - 1) == c) {
            problems.add(rm.getString("MAY_NOT_END_WITH", compName, new String(new char[] { c })));
        }
    }

}
