/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework
 *  Class      :   ValueCannotMatchString.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 26, 2022
 *  Modified   :   Dec 26, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 26, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.validation.builtin.stringvalidation;

import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.validation.Problems;
import com.pekinsoft.validation.Severity;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.3
 * @since 1.0
 */
public class ValueCannotMatchString extends StringValidator {
    
    private final ResourceMap resourceMap;
    private String notMatch;
    
    public ValueCannotMatchString (String notMatch) {
        resourceMap = Application
                .getInstance()
                .getContext()
                .getResourceMap(getClass());
        this.notMatch = notMatch;
    }

    @Override
    public void validate(Problems problems, String compName, String model) {
        if (model.equals(notMatch)) {
            problems.add(resourceMap.getString("string.match.not.allowed",
                    model, notMatch), 
                    Severity.WARNING);
        }
    }

}
