/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework Application API
 *  Class      :   USStateCanadianProvinceValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 1, 2022
 *  Modified   :   Dec 1, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 1, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.validation.builtin.stringvalidation;

import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.validation.Problems;
import com.pekinsoft.validation.Severity;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.3
 * @since 1.0
 */
public class USStateCanadianProvinceValidator extends StringValidator {
    
    private final ResourceMap rm;
    
    public USStateCanadianProvinceValidator () {
        rm = Application.getInstance().getContext().getResourceMap(getClass());
    }

    @Override
    public void validate(Problems problems, String compName, String model) {
        String msg = null;
        if (model.isEmpty()) {
            msg = rm.getString("MSG_MAY_NOT_BE_EMPTY");
            problems.add(msg, Severity.FATAL);
        }
        if (model.length() > 2 || model.length() < 2) {
            msg = rm.getString("MSG_NOT_A_TWO_LETTER_ABBREVIATION", model);
            problems.add(msg, Severity.FATAL);
        }
        if (!isValidStateOrProvince(model)) {
            msg = rm.getString("MSG_NOT_A_VALID_US_STATE_OR_CANADIAN_PROVINCE", model);
            problems.add(msg, Severity.FATAL);
        }
    }
    
    private boolean isValidStateOrProvince(String model) {
        for (String p : provinces) {
            if (model.toUpperCase().equals(p)) return true;
        }
        for (String s : states) {
            if (model.toUpperCase().equals(s)) return true;
        }
        
        return false;
    }
    
    private final String[] provinces = new String[] {
        "AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT"
    };
    private final String[] states = new String[] {
        "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", 
        "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", 
        "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", 
        "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA",
        "WA", "WV", "WI", "WY"
    };

}
