/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework
 *  Class      :   IsADateValidator.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 30, 2022
 *  Modified   :   Dec 30, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 30, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.validation.builtin.stringvalidation;

import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.List;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.validation.Problems;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.3
 * @since 1.0
 */
public class IsADateValidator extends StringValidator {
    
    private final ResourceMap resourceMap;
    private final List<DateTimeFormatter> formats = new ArrayList<>();
    
    public IsADateValidator () {
        resourceMap = Application.getInstance().getContext().getResourceMap(getClass());
        formats.add(DateTimeFormatter.BASIC_ISO_DATE.withResolverStyle(ResolverStyle.SMART));
        formats.add(DateTimeFormatter.ISO_DATE.withResolverStyle(ResolverStyle.SMART));
        formats.add(DateTimeFormatter.ISO_LOCAL_DATE.withResolverStyle(ResolverStyle.SMART));
        formats.add(DateTimeFormatter.ISO_OFFSET_DATE.withResolverStyle(ResolverStyle.SMART));
        formats.add(DateTimeFormatter.ISO_ORDINAL_DATE.withResolverStyle(ResolverStyle.SMART));
    }

    @Override
    public void validate(Problems problems, String compName, String model) {
        boolean valid = true;   // Assume a valid date is provided.
        
        if (model == null || model.trim().isEmpty()) {
            problems.add(resourceMap.getString("null.input"));
            return;
        }
        
        for (DateTimeFormatter df : formats) {
            try {
                df.parse(model);
                valid = true;
                break;
            } catch (DateTimeParseException pe) {
                valid = false;
            }
        }
        if (!valid) {
            problems.add(resourceMap.getString("invalid.date", model));
        }
    }

}
