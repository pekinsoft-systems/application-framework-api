/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License.  When distributing the software, include this License Header
 * Notice in each file and include the License file at
 * nbbuild/licenses/CDDL-GPL-2-CP.  Sun designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copyright 1997-2006 Sun
 * Microsystems, Inc. All Rights Reserved.
 *
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 license, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package com.pekinsoft.validation.builtin.stringvalidation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.validation.Problems;
import com.pekinsoft.validation.Severity;

/**
 *
 * @author Tim Boudreau
 */
final class RequiredPhoneNumberValidator extends StringValidator {
    
    private final ResourceMap rm = Application.getInstance().getContext().getResourceMap(RequiredPhoneNumberValidator.class);
    private Pattern pattern;
    private Matcher matcher;
    
    @Override
    public void validate(Problems problems, String compName, String model) {
        if (model.isEmpty()) {
            String message = rm.getString("MSG_MAY_NOT_BE_EMPTY", model); //NOI18N
            problems.add (message, Severity.WARNING);
        }
        
        if (model.equals("(   )    -    ")) {
            String message = rm.getString("MSG_PHONE_NUMBER_NOT_SET", model); // NOI18N
            problems.add(message, Severity.WARNING);
        }
        
        if (!isValidPhoneNumber(model)) {
            String message = rm.getString("MSG_INVALID_PHONE_NUMBER", model);
            problems.add(message);
        }
    }
    
    private boolean isValidPhoneNumber(String model) {
        return isValidTenDigit(model)
                || isValidDotsOrHyphens(model)
                || isValidParens(model)
                || isValidI18nDialing(model);
    }
    
    private boolean isValidTenDigit(String model) {
        String re = "^\\d{10}$";
        pattern = Pattern.compile(re);
        matcher = pattern.matcher(model);
        return matcher.matches();
    }
    
    private boolean isValidDotsOrHyphens(String model) {
        String re = "^(\\d{3}[- .]?){2}\\d{4}$";
        pattern = Pattern.compile(re);
        matcher = pattern.matcher(model);
        return matcher.matches();
    }
    
    private boolean isValidParens(String model) {
        String re = "^((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";
        pattern = Pattern.compile(re);
        matcher = pattern.matcher(model);
        return matcher.matches();
    }
    
    private boolean isValidI18nDialing(String model) {
        String re = "^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$";
        pattern = Pattern.compile(re);
        matcher = pattern.matcher(model);
        return matcher.matches();
    }
    
}
