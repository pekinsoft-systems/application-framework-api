package com.pekinsoft.validation.builtin.stringvalidation;

import com.pekinsoft.validation.AbstractValidator;


/**
 *
 * @author Tim Boudreau
 */
abstract class StringValidator extends AbstractValidator<String> {
    protected StringValidator() {
        super (String.class);
    }
}
