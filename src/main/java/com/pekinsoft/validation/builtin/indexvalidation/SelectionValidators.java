/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos
 *  Class      :   SelectionValidators.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 18, 2022
 *  Modified   :   Dec 18, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 18, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.validation.builtin.indexvalidation;

import com.pekinsoft.validation.Problems;
import com.pekinsoft.validation.Validator;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.3
 * @since 1.0
 */
public enum SelectionValidators implements Validator<Boolean[]> {

    REQUIRE_SELECTION
    ;
    
    private Validator<Boolean[]> instantiate() {
        Validator<Boolean[]> result;
        switch (this) {
            case REQUIRE_SELECTION:
                result = new ChoiceMustBeMadeValidator();
                break;
            default:
                throw new AssertionError();
        }
        
        return result;
    }

    @Override
    public void validate(Problems problems, String compName, Boolean[] model) {
        instantiate().validate(problems, compName, model);
    }

    @Override
    public Class<Boolean[]> modelType() {
        return Boolean[].class;
    }

}
