/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pekinsoft.validation.spi;

import com.pekinsoft.api.Exceptions;
import com.pekinsoft.lookup.Lookup;
import com.pekinsoft.validation.Problem;
import java.util.Collection;

/**
 *
 * @author Tim Boudreau
 */
public abstract class GlobalValidationCallback {

    private static final Object LOCK = new Object();
    private static GlobalValidationCallback INSTANCE;

    static GlobalValidationCallback getDefault() {
        synchronized (LOCK) {
            if (INSTANCE == null) {
                INSTANCE = new ProxyGlobalCallback();
            }
        }
        return INSTANCE;
    }

    public static GlobalValidationCallback provider() {
        return getDefault();
    }

    public void onValidationTrigger(Object source, Object triggeringEvent) {

    }

    public void onValidationFinished(Object source, Object triggeringEvent) {

    }

    public abstract void onProblem(Object component, Problem problem);

    public abstract void onProblemCleared(Object component, Problem problem);

    private static final class ProxyGlobalCallback extends GlobalValidationCallback {

        private final Exceptions exceptions = (Exceptions) Lookup.getDefault()
                .lookup(Exceptions.class);

        private ProxyGlobalCallback() {

        }

        Collection<? extends GlobalValidationCallback> registered() {
            return (Collection<? extends GlobalValidationCallback>) Lookup
                    .getDefault().lookupAll(GlobalValidationCallback.class);
        }

        @Override
        public void onProblem(Object component, Problem problem) {
            for (GlobalValidationCallback cb : registered()) {
                try {
                    cb.onProblem(component, problem);
                } catch (Exception e) {
                    exceptions.print(e);
                }
            }
        }

        @Override
        public void onProblemCleared(Object component, Problem problem) {
            for (GlobalValidationCallback cb : registered()) {
                try {
                    cb.onProblemCleared(component, problem);
                } catch (Exception e) {
                    exceptions.print(e);
                }
            }
        }
    }
}
