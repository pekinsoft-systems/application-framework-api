/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   WizardController.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 6, 2024
 *  Modified   :   Nov 6, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 6, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.wizard;

import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.utils.DebugLogger;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import javax.swing.JRootPane;

/**
 * The {@code WizardController} controls all aspects of a {@link Wizard},
 * including the navigation state and the data map.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class WizardController implements PropertyChangeListener {

    /**
     * Navigation mode that allows for no forward navigation. This mode should
     * be used whenever there is required data yet to be provided.
     */
    public static final int MODE_NONE = 0;
    /**
     * Navigation mode that allows for forward navigation, but the Wizard is not
     * yet ready to be finished. In other words, there are more steps that have
     * required data to be provided in order for the Wizard to be finished.
     */
    public static final int MODE_CAN_CONTINUE = 1;
    /**
     * Navigation mode that allows for the Wizard to be finished. This mode is
     * used when there are no more steps beyond the current one and all required
     * data has been provided.
     */
    public static final int MODE_CAN_FINISH = 2;
    /**
     * Navigation mode that allows for forward navigation or finishing the
     * Wizard. This mode should be used if all required data has been provided,
     * but there is more optional data that could be collected.
     */
    public static final int MODE_CAN_CONTINUE_OR_FINISH
            = MODE_CAN_CONTINUE | MODE_CAN_FINISH;

    /**
     * Constructs a new {@code WizardController} for a {@link Wizard}.
     *
     * @param dialog   the {@link WizardDialog} that contains the UI
     * @param finisher the class that process the collected data
     * @param steps    the individual {@link WizardStep steps} that collect the
     *                 data
     */
    public WizardController(WizardDialog dialog, WizardFinisher finisher,
            Runnable callback, WizardStep... steps) {
        logger = Application.getInstance().getContext().getLogger(getClass());
        this.steps = List.of(steps);
        this.dialog = dialog;
        this.finisher = finisher;
        this.callback = callback;

        // Set up the first step and add PropertyChangeListener to each step
        for (WizardStep step : steps) {
            step.setController(this);
            step.addPropertyChangeListener(this);
        }

        setCurrentStep(0);
    }

    /**
     * Sets the forward navigation mode to one of the {@code WizardController}
     * constants: {@link #MODE_CAN_CONTINUE}, {@link #MODE_CAN_FINISH},
     * {@link #MODE_CAN_CONTINUE_OR_FINISH}, or {@link #MODE_NONE}.
     *
     * @param navigationMode the navigation mode constant to set
     */
    public void setForwardNavigationMode(int navigationMode) {
        this.navigationMode = navigationMode;
        updateNavigation();
    }

    /**
     * Retrieves the current navigation mode.
     *
     * @return the current mode
     */
    public int getForwardNavigationMode() {
        return navigationMode;
    }

    /**
     * Navigates forward in the {@link Wizard}.
     */
    public void goForward() {
        WizardStep current = steps.get(currentStep);
        if (current.isStepValid() && currentStep < steps.size() - 1) {
            int nextIndex = steps.get(currentStep).getNextStep();
            if (nextIndex >= 0 && nextIndex < steps.size()) {
                current = steps.get(nextIndex);
//                for (Component component : current.getComponents()) {
//                    if (!dataMap.containsKey(component.getName())) {
//                        Object value = null;
//                        switch (component) {
//                            case JPasswordField password -> {
//                                value = password.getPassword();
//                            }
//                            case JTextComponent text -> {
//                                value = text.getText();
//                            }
//                            case JSpinner spinner -> {
//                                value = spinner.getValue();
//                            }
//                            case JCheckBox check -> {
//                                value = check.isSelected();
//                            }
//                            case JRadioButton radio -> {
//                                value = radio.isSelected();
//                            }
//                            case JComboBox combo -> {
//                                value = combo.getSelectedItem();
//                            }
//                            case JList list -> {
//                                value = list.getSelectedValue();
//                            }
//                            case JSlider slider -> {
//                                value = slider.getValue();
//                            }
//                            case JTree tree -> {
//                                value = tree.getSelectionPath();
//                            }
//                            case JTable table -> {
//                                value = table.getSelectedRow();
//                            }
//                            default -> {
//                                value = null;
//                            }
//                        }
//                        if (value != null) {
//                            dataMap.put(component.getName(), value);
//                        }
//                    }
//                }
                setCurrentStep(nextIndex);
            }
        }
    }

    /**
     * Navigates backward in the {@link Wizard}.
     */
    public void goBack() {
        int previousStep = getPreviousStep();
        if (previousStep >= 0) {
            WizardStep current = steps.get(previousStep);
//            for (Component component : current.getComponents()) {
//                if (dataMap.containsKey(component.getName())) {
//                    dataMap.remove(component.getName());
//                }
//            }
            setCurrentStep(previousStep);
        }
    }

    /**
     * Finishes the {@link Wizard}. This method calls the registered
     * {@link WizardFinisher WizardFinisher's}
     * {@link WizardFinisher#finish(java.util.Map) finish} method. It tests for
     * a {@link BackgroundTask} and executes it through the
     * {@link ApplicationContext} if the method returns one.
     */
    public void finish() {
        DebugLogger.trace(logger, "Starting finish");
        cancelled = false;
        WizardStep current = steps.get(currentStep);
        if (current.isStepValid() && current.isComplete()) {
            BackgroundTask<?, ?> finishTask = finisher.finish(dataMap);
            DebugLogger.debug(logger,
                    "Called finisher.finish and received back %s", finishTask);
            if (finishTask != null) {
                DebugLogger.debug(logger, "Received a BackgroundTask, so "
                        + "executing it via the default TaskService.");
                Application.getInstance().getContext()
                        .getTaskService().execute(finishTask);
            }
        }
        dialog.dispose();
        callback.run();
        DebugLogger.trace(logger, "Finished finish");
    }

    /**
     * Determines whether the {@link Wizard} was cancelled by the user.
     *
     * @return {@code true} if it was cancelled; {@code false} if it was
     *         finished
     */
    public boolean isCancelled() {
        return cancelled;
    }

    /**
     * Cancels the {@link Wizard}.
     */
    public void cancel() {
        cancelled = true;
        dialog.dispose();
        callback.run();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        logger.info(String.format("PropertyChangeEvent received for property "
                + "name: %s", evt.getPropertyName()));
        if (evt.getPropertyName() != null) {
            switch (evt.getPropertyName()) {
                case "complete" -> {
                    WizardStep current = steps.get(currentStep);
                    boolean isComplete = (boolean) evt.getNewValue();
                    logger.info("Updating the navigation buttons.");
                    if (current == evt.getSource()) {
                        updateNavigation();
                    }
                }
                case "problem" -> {
                    String problemMessage = (String) evt.getNewValue();
                    logger.info(String.format("Updating the problem label to: "
                            + "%s", problemMessage));
                    dialog.setProblem(problemMessage);
                    updateNavigation();
                }
            }
        }
    }

    /**
     * Convenience method for adding data (key/value pair) to the data map for
     * the {@link Wizard}.
     *
     * @param key   the key to identify the data
     * @param value the value of the data
     */
    public void putMapData(String key, Object value) {
        dataMap.put(key, value);
    }

    /**
     * Convenience method for retrieving a single value from the data map by
     * providing its key.
     *
     * @param key the key for the desired value
     *
     * @return the value identified by the key or {@code null} if not present
     */
    public Object getMapValue(String key) {
        return dataMap.get(key);
    }

    /**
     * Retrieves the entire {@link Map} of data for the {@link Wizard}.
     *
     * @return the data map
     */
    public Map<String, Object> getDataMap() {
        return dataMap;
    }

    private void updateDefaultButton() {
        JRootPane rootPane = dialog.getRootPane();
        if (dialog.isCanFinish() && dialog.isFinishButtonEnabled()) {
            rootPane.setDefaultButton(dialog.getFinishButton());
        } else if (dialog.isCanGoForward() && dialog.isNextButtonEnabled()) {
            rootPane.setDefaultButton(dialog.getNextButton());
        } else {
            rootPane.setDefaultButton(null);
        }
    }

    private void updateNavigation() {
        logger.info("Updating navigation");
        WizardStep current = steps.get(currentStep);
        boolean isComplete = current.isComplete();
        boolean isValid = current.isStepValid();
        boolean canGoNext = navigationMode == MODE_CAN_CONTINUE
                || navigationMode == MODE_CAN_CONTINUE_OR_FINISH;
        boolean canFinish = navigationMode == MODE_CAN_FINISH
                || navigationMode == MODE_CAN_CONTINUE_OR_FINISH;

        logger.info(String.format("updateNavigation: isComplete = %s, isValid "
                + "= %s", isComplete, isValid));
        logger.info(String.format("canGoForward = %s", canGoNext));
        dialog.setCanGoForward(canGoNext);
        logger.info(String.format("dialog.isCanGoForward=%s",
                dialog.isCanGoForward()));
        dialog.revalidate();
        dialog.repaint();

        logger.info(String.format("canFinish = %s", canFinish));
        dialog.setCanFinish(canFinish);

        logger.info(String.format("canGoBack = %s", currentStep > 0));
        dialog.setCanGoBack(currentStep > 0);

        updateDefaultButton();
    }

    private void setCurrentStep(int index) {
        logger.info("Starting setCurrentStep");
        WizardStep currentStep = steps.get(this.currentStep);
        logger.info(String
                .format("Leaving step: %s", currentStep.getStepTitle()));
        currentStep.onExit();

        this.currentStep = index;
        WizardStep nextStep = steps.get(this.currentStep);
        logger.info(String.format("Entering step: %s", nextStep.getStepTitle()));
        nextStep.onEnter();
        nextStep.validateAndNotify();

        // Update WizardDialog and SidePanel to display the current step
        dialog.setCurrentStep(nextStep, this.currentStep);
        dialog.setCanGoBack(this.currentStep > 0);
//        dialog.setCanGoForward(nextStep.isComplete()
//                && this.currentStep < steps.size() - 1);
//        dialog.setCanFinish(this.currentStep == steps.size() - 1
//                && nextStep.isComplete());
        logger.info("Finished setCurrentStep");
    }

    private int getPreviousStep() {
        // Reverse iteration logic or a stack-based approach for branching history
        return currentStep - 1; // Placeholder.
    }

    private final Logger logger;
    private final Map<String, Object> dataMap = new HashMap<>();
    private final List<WizardStep> steps;
    private final WizardDialog dialog;
    private final WizardFinisher finisher;
    private final Runnable callback;

    private int navigationMode = 1;
    private int currentStep = 0;
    private boolean cancelled = false;

}
