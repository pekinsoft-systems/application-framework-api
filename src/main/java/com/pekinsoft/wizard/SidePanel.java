/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   SidePanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 6, 2024
 *  Modified   :   Nov 6, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 6, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.wizard;

import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.utils.ColorUtils;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.UIManager;

/**
 * The {@code SidePanel} is a custom {@link JPanel} implementation that handles
 * custom painting to display a background image or color, and the steps list
 * for the {@link Wizard}. This panel simply provides feedback to the user as
 * to how many steps there are, and which step they are on.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
class SidePanel extends JPanel {

    public SidePanel(List<String> steps) {
        logger.info(String.format("Constructing SidePanel with steps = %s",
                steps));
        setSteps(steps);
        loadBackgroundImage();
        logger.info("Finished constructing SidePanel");
    }

    public void setSteps(List<String> steps) {
        logger.info(String.format("Starting setSteps with steps = ", steps));
        this.steps = steps;
        FontMetrics fm = getFontMetrics(getFont().deriveFont(Font.BOLD, 15.0f));
        int width = fm.stringWidth(resourceMap.getString("sidepanel.title"));
        for (String step : steps) {
            logger.info(String.format("Getting text width of \"%s\"", step));
            width = Math.max(width, fm.stringWidth(step));// + 50);
        }

//        width += 20;
        setSize(new Dimension(width, 250));
        setMinimumSize(new Dimension(width, 0));
        setMaximumSize(new Dimension(width, Integer.MAX_VALUE));
        setPreferredSize(new Dimension(width, 250));
        repaint();
    }

    public void setCurrentStep(int currentStep) {
        this.currentStep = currentStep;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g.create();

        // Antialiasing for smooth text and edges
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        // Draw the backgrouind image or fallback color
        if (backgroundImage != null) {
            g2.drawImage(backgroundImage, 0, 0, getWidth(), getHeight(), this);
        } else {
            Color backgroundColor = UIManager.getColor("Panel.background");
            g2.setColor(backgroundColor != null
                        ? backgroundColor
                        : ColorUtils.LINEN);
            g2.fillRect(0, 0, getWidth(), getHeight());
        }

        drawSteps(g2);
    }

    private void drawSteps(Graphics2D g2) {
        int y = 40; // Start position for drawing text
        int padding = 20;   // Padding between steps

        String panelTitle = resourceMap.getString("sidepanel.title");
        System.err.println("SidePanel Title: " + panelTitle);
        Font oldFont = g2.getFont();
        g2.setFont(oldFont.deriveFont(Font.BOLD, 14.0f));
        Color oldColor = g2.getColor();
        g2.setColor(Color.black);
        g2.drawString(panelTitle, 15, y);
        FontMetrics fm = g2.getFontMetrics(g2.getFont());
        y += fm.getDescent();
        g2.drawLine(15, y, fm.stringWidth(panelTitle) + 15, y);
        g2.setFont(oldFont);
        g2.setColor(oldColor);
        y += padding;

        for (int i = 0; i < steps.size(); i++) {
            String step = steps.get(i);

            if (i == currentStep) {
                // Highlight current step
                g2.setFont(g2.getFont().deriveFont(Font.BOLD));
            } else {
                g2.setFont(g2.getFont().deriveFont(Font.PLAIN));
            }

            Color background = UIManager.getColor("Panel.background");
            Color foreground = UIManager.getColor("Panel.foreground");
            if (foreground == null) {
                foreground = UIManager.getColor("TextField.foreground");
            }
            if (foreground == null) {
                foreground = ColorUtils.isDarkColor(background)
                             ? ColorUtils.FLORAL_WHITE
                             : ColorUtils.SLATE_GRAY;
            }

            g2.setColor(i == currentStep
                        ? ColorUtils.DODGER_BLUE
                        : foreground);
            g2.drawString((i + 1) + ". " + step, 10, y);
            y += padding;
        }
    }

    private void loadBackgroundImage() {
        String imagePath = System.getProperty("framework.wizard.sidePanelImage");

        if (imagePath != null) {
            backgroundImage = loadImage(imagePath);
        }
    }

    private BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace(System.err);
            return null;
        }
    }

    private final ResourceMap resourceMap = Application.getInstance()
            .getContext().getResourceMap(SidePanel.class);
    private final Logger logger = Application.getInstance().getContext()
            .getLogger(SidePanel.class);
    private List<String> steps;
    private int currentStep;
    private BufferedImage backgroundImage;

}
