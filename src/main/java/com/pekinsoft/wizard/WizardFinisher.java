/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   WizardFinisher.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 6, 2024
 *  Modified   :   Nov 6, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 6, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.wizard;

import com.pekinsoft.framework.BackgroundTask;
import java.util.Map;

/**
 * The {@code WizardFinisher} interface provides a custom solution for
 * completing a {@link Wizard}.
 * <p>
 * This interface simply needs to be implemented by a class to provide the logic
 * for the sole {@link #finish(Map) finish} method. The map of the data
 * collected throughout the {@code Wizard} is provided to the method so that
 * actions and computations may take place.
 * <p>
 * The {@code finish} method is setup to return a {@link BackgroundTask}, so
 * that if the computations could take a while to perform, it will be placed
 * onto a background thread to keep the UI responsive to user input. If,
 * however, the actions or computations will not take very long to perform, this
 * method may return {@code null} and simply perform the required work in place.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public interface WizardFinisher {

    /**
     * This method is called from the
     * {@link WizardController WizardController's} {@code finish} method to
     * allow for custom actions and computations to take place.
     * <p>
     * This method may return a {@link BackgroundTask} to have the computations
     * placed onto a background thread if they may take awhile. If the
     * computations will be quick, they can be performed in-place, and
     * {@code null} can be returned.
     *
     * @param dataMap the map of the {@code Wizard}'s data
     *
     * @return a {@code BackgroundTask} or {@code null}
     */
    BackgroundTask<?, ?> finish(Map<String, Object> dataMap);

}
