/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Wizard.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 6, 2024
 *  Modified   :   Nov 6, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 6, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.wizard;

import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.utils.MessageBox;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

/**
 * Encapsulates the logic and state of a Wizard. A Wizard gathers information
 * into a {@link Map}, and then performs some action with that information when
 * the user clicks Finish. To display a Wizard, call one of the
 * {@code show} methods in this class.
 * <p>
 * A Wizard is a series of one or more steps represented by panels in the user
 * interface. Each step is identified by a unique index. Panels are created at
 * the time time they are passed to the {@code show} method. Panels require the
 * data be validated, and added to the Map only when the data is correct.
 * <p>
 * To create a Wizard, you do not implement nor instantiate this class, but
 * rather use the static methods in order to create and show the Wizard's
 * dialog window. The panels that you will use are implementations of the
 * {@link WizardStep}, which is itself a subclass of the
 * {@link javax.swing.JPanel JPanel} component.
 * <p>
 * To create the UI for the {@code WizardStep}s, if designing the project in an
 * IDE with a GUI editor (such as Apache NetBeans), you could simply add a
 * {@code JPanel} form to your project, and change its class definition line:
 * <pre>
 * public class MyWizardStep extends javax.swing.JPanel {
 *     // Class implementation
 * }
 * </pre><p>
 * to:
 * <pre>
 * public class MyWizardStep extends WizardStep {
 *     // Class implementation
 * }
 * </pre><p>
 * By doing it in this way, you will still be able to visually edit the UI
 * layout, while having a subclass of the {@code WizardStep}. Of course, you
 * will also need to implement the abstract methods of {@code WizardStep} in
 * order for your panel to be useful to the {@code Wizard}.
 * <p>
 * Once you have defined the {@code WizardStep}s that will make up your complete
 * Wizard, simply create your Wizard like this:
 * <pre>
 public class ShowMyWizard {

     public static void main(String... args) {
         Wizard.showWizard(Application.getDefault().getContext(),
                 "My Data Wizard", new MyWizardFinisher(),
                 new MyWizardStepOne(), new MyWizardStepTwo(),
                 new MyWizardStepThree(), new MyWizardSummary());
     }
 }
 </pre>
 * <h2>Branching Wizards</h2>
 * To have a Wizard that branches based upon user selections, it is simply a
 * matter of overriding the {@link WizardStep#getNextStep() } method to return
 * the index of the next {@code WizardStep} to use. For example, if a
 * {@code WizardStep} provides a checkbox that will allow the user to skip the
 * next step in a three-step Wizard, you would do so like this:{@snippet lang="java":
 * public class MyBranchingStep extends WizardStep {
 *     // The previous portion of the class setup.
 *
 *     @Override
 *     public int getNextStep() {
 *         if (myCheckBox.isSelected()) {
 *             return 2;
 *         } else {
 *             return 1;
 *         }
 *     }
 *
 *     // The rest of the class implementation.
 * }
 * }
 * <p>
 * In the above example, if the checkbox was installed in the first
 * {@code WizardStep} of the Wizard, when it is selected, the user will skip to
 * the final step of the Wizard, otherwise the user will move sequentially
 * through all steps in the Wizard.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Wizard {

    /**
     * Shows a fully-configured {@code Wizard}, specifying the title,
     * {@link WizardStep WizardSteps}, and {@link WizardFinisher} which is
     * called once the {@code Wizard} completes.
     *
     * @param context  the {@link ApplicationContext} in which this wizard is
     *                 being utilized
     * @param title    the {@code Wizard}'s title
     * @param finisher the class to be called to finish the {@code Wizard}
     * @param steps    the steps that need to be completed
     *
     * @return the constructed {@code Wizard}
     */
    public static Wizard createWizard(ApplicationContext context, String title,
            WizardFinisher finisher, WizardStep... steps) {
        return Wizard.createWizard(context, title, finisher, null, steps);
    }

    /**
     * Shows a fully-configured {@code Wizard}, with a callback method so that
     * {@code Wizard} will block until it is finished.
     *
     * @param context  the {@link ApplicationContext} in which this wizard is
     *                 being utilized
     * @param title    the {@code Wizard}'s title
     * @param finisher the class to be called to finish the {@code Wizard}
     * @param callback the method that should be called upon completion so the
     *                 {@code Wizard} can block execution until complete
     * @param steps    the steps that need to be completed
     *
     * @return the constructed {@code Wizard}
     */
    public static Wizard createWizard(ApplicationContext context, String title,
            WizardFinisher finisher, Runnable callback, WizardStep... steps) {
        return new Wizard(context, title, finisher, callback, steps);
    }

    /**
     * Displays the {@code Wizard}'s dialog window.
     */
    public void showWizard() {
        dialog.setVisible(true);
    }

    public boolean isCancelled() {
        return controller.isCancelled();
    }

    private WizardDialog createWizardDialog() {
        logger.info("Starting createWizardDialog");

        logger.config("Constructing the WizardDialog");
        WizardDialog dialog = new WizardDialog(context, steps);

        logger.config("Constructing the WizardController");
        controller = new WizardController(dialog, finisher, callback, steps);

        logger.config("Setting the WizardController to the WizardDialog");
        dialog.setController(controller);

        for (WizardStep step : steps) {
            logger.config(String.format("Setting the WizardController to the "
                    + "WizardStep \"%s\"", step.getStepTitle()));
            step.setController(controller);
        }

        dialog.setTitle(title);
        dialog.pack();
        dialog.setLocationRelativeTo(WindowManager.getDefault() == null
                                     ? null
                                     : WindowManager.getDefault().getMainFrame());
//        dialog.setDefaultCloseOperation(WizardDialog.DO_NOTHING_ON_CLOSE);
//        dialog.addWindowListener(new CompletionHandler());

        logger.info("Finished createWizardDialog");
        return dialog;
    }

    private Wizard(ApplicationContext context, String title,
            WizardFinisher finisher, Runnable callback, WizardStep... steps) {
        this.context = context;
        this.logger = context.getLogger(getClass());
        this.title = title;
        this.finisher = finisher;
        this.steps = steps;
        this.callback = callback;

        this.dialog = createWizardDialog();
    }

    private class CompletionHandler extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            callback.run();
        }

    }

    private final Logger logger;
    private final ApplicationContext context;
    private final String title;
    private final WizardFinisher finisher;
    private final WizardStep[] steps;
    private Runnable callback = null;
    private final WizardDialog dialog;

    private WizardController controller;

    public static class DefaultWizardFinisher implements WizardFinisher {

        @Override
        public BackgroundTask<?, ?> finish(Map<String, Object> dataMap) {
            ResourceMap resourceMap = Application.getInstance().getContext()
                    .getResourceMap(Wizard.class);
            MessageBox.showInfo(null,
                    resourceMap.getString("default.finisher.message"),
                    resourceMap.getString("default.finisher.title"));

            return null;
        }

    }

}
