/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   WizardStep.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 6, 2024
 *  Modified   :   Nov 6, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 6, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.wizard;

import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Logger;
import java.beans.PropertyChangeListener;
import java.util.Map;
import javax.swing.JPanel;

/**
 * The {@code WizardStep} is an abstract {@link JPanel} subclass that defines
 * the UI for an individual step in a {@link Wizard}.
 * <p>
 * This panel provides the UI for a user to enter data being collected by the
 * {@code Wizard} to which this {@code WizardStep} belongs.
 * <p>
 * The {@code WizardPage} abstract class defaults some values to references to
 * the subclass' {@link ResourceMap}. These defaulted references are to the
 * following resource keys:
 * <dl>
 * <dt><strong>{@code step.title}</strong>:</dt>
 * <dd>This resource defines the localized title for this step. The title is
 * displayed in the list of steps on the {@code Wizard}'s {@link SidePanel} and
 * is shown in <strong>bold</strong> when it is the current step. This value is
 * also displayed at the top of the central panel in the {@link WizardDialog}
 * when the step is entered.</dd>
 * <dt><strong>{@code step.index}</strong>:</dt>
 * <dd>This resource defines the index value for the step. The steps typically
 * are navigated by the {@link WizardController} in numerical order, starting
 * from zero. When the {@code WizardController} calls the {@link #getNextStep()
 * }
 * method, it moves the step to the {@link getStepIndex() } plus one. However,
 * by overriding this method, the subclass can cause the {@code Wizard} to
 * branch to a different step and skip steps in between.</dd>
 * </dl>
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class WizardStep extends JPanel {

    /**
     * Convenience constant that is the same as
     * {@link WizardController#MODE_NONE}
     */
    protected static final int MODE_NONE = WizardController.MODE_NONE;
    /**
     * Convenience constant that is the same as
     * {@link WizardController#MODE_CAN_CONTINUE}
     */
    protected static final int MODE_CAN_CONTINUE = WizardController.MODE_CAN_CONTINUE;
    /**
     * Convenience constant that is the same as
     * {@link WizardController#MODE_CAN_FINISH}
     */
    protected static final int MODE_CAN_FINISH = WizardController.MODE_CAN_FINISH;
    /**
     * Convenience constant that is the same as
     * {@link WizardController#MODE_CAN_CONTINUE_OR_FINISH}
     */
    protected static final int MODE_CAN_CONTINUE_OR_FINISH = WizardController.MODE_CAN_CONTINUE_OR_FINISH;

    /**
     * Constructs a new {@code WizardStep} instance. This constructor is called
     * by subclasses.
     *
     * @param context the {@link ApplicationContext} in which this
     *                {@code WizardStep} is being used
     */
    protected WizardStep(ApplicationContext context) {
        this.context = context;
        logger = context.getLogger(getClass());
    }

    /**
     * Retrieves the title of this {@code WizardStep} for display in the
     * {@link WizardDialog}.
     * <p>
     * This value is displayed both in the {@link SidePanel SidePanel's} list of
     * steps that make up the {@link Wizard}, as well as at the top of the
     * center section of the {@link WizardDialog} when this step is entered.
     * <p>
     * The value returned should be localized.
     * <p>
     * The default implementation returns the value of the resource key named
     * &ldquo;step.title&rdquo;.
     *
     * @return the step's title
     */
    public String getStepTitle() {
        return getResourceMap().getString("step.title");
    }

    /**
     * Retrieves the index value of the current {@code WizardStep}. This value
     * is used by the {@link WizardController} for displaying steps as the
     * {@link Wizard} progresses.
     * <p>
     * The default implementation simply returns the value of the resource key
     * named &ldquo;step.index&rdquo;.
     *
     * @return this step's index value
     */
    public int getStepIndex() {
        return getResourceMap().getInteger("step.index");
    }

    /**
     * Retrieves the index of the next {@code WizardStep} in the {@link Wizard}.
     * <p>
     * This method may be overridden by subclasses that allow for the
     * {@link Wizard} to branch after this step, based upon the user's
     * selections. If the user's selections on this step make the next step or
     * two of the {@code Wizard} not necessary, it can override this method to
     * return the index of the {@code WizardStep} to which the
     * {@link WizardController} should navigate to next.
     * <p>
     * For example:
     * <pre>
     * &#064;Override
     * public int getNextStep() {
     *     if (checkBox.isSelected()) {
     *         return 5;
     *     } else {
     *         return getIndex() + 1;
     *     }
     * }
     * </pre>
     * <p>
     * The default implementation simply returns the value of
     * {@link #getStepIndex() getStepIndex()} plus one.
     *
     * @return the next step's index
     */
    public int getNextStep() {
        return getStepIndex() + 1;
    }

    /**
     * Retrieves the index of the previous {@code WizardStep} in the
     * {@link Wizard}.
     * <p>
     * This method may be overridden by subclasses that allow for the
     * {@code Wizard} to branch prior to this step. If a branching step led the
     * user to this step, and they need to go back, then this method should
     * return the index of the {@code WizardStep} from which it was navigated.
     * The value of the data entry that cause the user to branch to here will
     * need to be checked to determine whether the Wizard had branched to this
     * step.
     * <p>
     * For example:
     * <pre>
     * &#064;Override
     * public int getPreviousStep() {
     *     if ((boolean) getDataEntry("checkBox")) {
     *         return 2;
     *     } else {
     *         return getStepIndex() - 1;
     *     }
     * }
     * </pre>
     * <p>
     * The default implementation simply returns the value of
     * {@link #getStepIndex() getStepIndex()} minus one.
     *
     * @return the previous step's index
     */
    public int getPreviousStep() {
        return getStepIndex() - 1;
    }

    /**
     * Provides a means for {@code WizardStep} setup upon entering into this
     * step via the {@link WizardController}.
     */
    public abstract void onEnter();

    /**
     * Provides a means for {@code WizardStep} tear-down upon completing and
     * exiting this step via the {@link WizardController}.
     */
    public abstract void onExit();

    /**
     * Used by the {@link WizardController} to determine if this step has been
     * fully completed. The {@code WizardController} is registered as a
     * {@link java.beans.PropertyChangeListener PropertyChangeListener} on this
     * property.
     *
     * @return {@code true} if this {@code WizardStep} is complete;
     *         {@code false} otherwise
     */
    public boolean isComplete() {
        return stepCompleted;
    }

    /**
     * Sets whether this {@code WizardStep} is fully completed.
     * <p>
     * This is a bound property.
     *
     * @param stepCompleted {@code true} when this step is fully completed
     */
    public void setComplete(boolean stepCompleted) {
        logger.info(String.format("Setting the stepCompleted property to: %s",
                stepCompleted));
        boolean old = isComplete();
        this.stepCompleted = stepCompleted;
        firePropertyChange("complete", old, isComplete());
    }

    /**
     * Determines whether the data in this {@code WizardStep} is valid and the
     * {@link Wizard} may continue.
     *
     * @return {@code true} if all data is valid; {@code false} if not
     */
    public boolean isStepValid() {
        return isValid;
    }

    /**
     * Provides a means for the {@link Wizard} to install the
     * {@link WizardController} that is being used.
     *
     * @param controller the controller
     */
    void setController(WizardController controller) {
        this.controller = controller;
    }

    /**
     * Retrieves the installed {@link WizardController}.
     *
     * @return the controller
     */
    protected WizardController getController() {
        return controller;
    }

    /**
     * Sets the forward navigation mode for the {@link Wizard}.
     *
     * @param navigationMode the forward navigation mnode
     */
    protected void setForwardNavigationMode(int navigationMode) {
        controller.setForwardNavigationMode(navigationMode);
    }

    /**
     * Convenience method for setting data to the {@link Wizard Wizard's} data
     * map.
     *
     * @param key   the key for the data
     * @param value the value to store
     */
    protected final void putDataEntry(String key, Object value) {
        if (controller != null) {
            logger.info(String.format("Putting data into the map: Key = %s, "
                    + "Value = %s", key, value));
            controller.putMapData(key, value);
        }
    }

    /**
     * Convenience method for retrieving a data value from the
     * {@link Wizard Wizard's} data map.
     *
     * @param key the key for the value to retrieve
     *
     * @return the value stored with that key
     */
    protected final Object getDataEntry(String key) {
        return controller != null ? controller.getMapValue(key) : null;
    }

    /**
     * Retrieves the {@link Wizard Wizard's} data map in its entirety.
     *
     * @return the data map
     */
    protected final Map<String, Object> getDataMap() {
        return controller != null ? controller.getDataMap() : null;
    }

    /**
     * Validates the data in this {@code WizardStep} and notifies any
     * {@link PropertyChangeListener PropertyChangeLisetners} that listen on the
     * &ldquo;problem&rdquo; property.
     * <p>
     * If this method validates the user's input and finds no problems, it
     * should return {@code null}. If problems are found, then a localized
     * problem message should be returned.
     *
     * @return a localized message if a problem is found, or {@code null} when
     *         all input is valid
     */
    protected abstract String validateStep();

    /**
     * This method is called from the {@link WizardController} to force the
     * update of the navigation buttons. This method calls the
     * {@link #validateStep() } method to have the data entered on this
     * {@code WizardStep} validated, and then it updates the navigation state
     * for this {@code WizardStep}.
     */
    protected final void validateAndNotify() {
        logger.info("Starting validateAndNotify");
        String validationMessage = validateStep();
        logger.info(String.format("validationMessage = %s",
                (validationMessage == null ? "[null]" : validationMessage)));
        boolean wasValid = isValid;
        isValid = validationMessage == null;

        logger.info(String.format(
                "wasValid=%s, isValid=%s, validationMessage=%s",
                wasValid, isValid, validationMessage));
        if (wasValid != isValid || !isValid) {
            logger.info(String.format("Firing property change event for "
                    + "\"problem\" property: wasValid = %s, isValid = %s, "
                    + "validationMessage = \"%s\"", wasValid, isValid,
                    validationMessage));
            firePropertyChange("problem", wasValid ? null : "",
                    validationMessage);
        } else {
            firePropertyChange("problem", validationMessage, null);
        }
    }

    /**
     * Retrieves the {@link ApplicationContext} in which this {@code WizardStep}
     * is being utilized.
     *
     * @return the context
     */
    protected final ApplicationContext getContext() {
        return context;
    }

    /**
     * Retrieves the {@link Application} in which this {@code WizardStep} is
     * being utilized.
     *
     * @return the {@code Application} instance
     */
    protected final Application getApplication() {
        return getContext().getApplication();
    }

    /**
     * Retrieves the {@link ResourceMap} for this {@code WizardStep}'s
     * configuration resources.
     *
     * @return the {@code ResourceMap}
     */
    protected final ResourceMap getResourceMap() {
        return getContext().getResourceMap(getClass());
    }

    private final ApplicationContext context;
    private final Logger logger;

    private WizardController controller;
    private boolean stepCompleted = false;
    private boolean isValid = false;

}
