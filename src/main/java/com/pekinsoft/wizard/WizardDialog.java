/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   WizardDialog2.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 8, 2024
 *  Modified   :   Nov 8, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 8, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.wizard;

import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.utils.DebugLogger;
import com.pekinsoft.utils.MessageBox;
import java.awt.CardLayout;
import java.awt.event.*;
import java.util.*;
import javax.swing.*;

/**
 * The {@code WizardDialog} maintains the responsibility of displaying the
 * {@link Wizard} to the user.
 *
 * @author Sean Carrick
 */
public class WizardDialog extends javax.swing.JDialog {

    /**
     * Creates new form WizardDialog.
     *
     * @param context the {@link ApplicationContext} in which this dialog is
     *                being used
     * @param steps   the {@link WizardStep} instances that make up the
     *                {@link Wizard} for which this {@code WizardDialog} was
     *                created
     */
    public WizardDialog(ApplicationContext context, WizardStep... steps) {
        super((WindowManager.getDefault() == null
               ? null
               : WindowManager.getDefault().getMainFrame()), true);
        this.context = context;
        this.resourceMap = context.getResourceMap(getClass());
        this.logger = context.getLogger(getClass());
        this.steps = Arrays.asList(steps);

        stepTitles = new ArrayList<>();
        for (WizardStep step : steps) {
            stepTitles.add(step.getStepTitle());
        }

        this.actionMap = context.getActionMap(getClass(), this);

        initComponents();
    }

    /**
     * Retrieves the &ldquo;Next&rdquo; button for automatic default button
     * updates.
     *
     * @return the Next button
     */
    JButton getNextButton() {
        return nextButton;
    }

    /**
     * Determines whether the &ldquo;Next&rdquo; button, or its action, is
     * enabled.
     *
     * @return {@code true} if enabled; {@code false} if not
     */
    boolean isNextButtonEnabled() {
        return nextButton.isEnabled() || nextButton.getAction().isEnabled();
    }

    /**
     * Retrieves the &ldquo;Finish&rdquo; button for automatic default button
     * updates.
     *
     * @return the Finish button
     */
    JButton getFinishButton() {
        return finishButton;
    }

    /**
     * Determines whether the &ldquo;Finish&rdquo; button, or its action, is
     * enabled.
     *
     * @return {@code true} if enabled; {@code false} if not
     */
    boolean isFinishButtonEnabled() {
        return finishButton.isEnabled() || finishButton.getAction().isEnabled();
    }

    /**
     * Determines whether forward navigation is allowed.
     *
     * @return {@code true} if allowed; {@code false} if not
     */
    public boolean isCanGoForward() {
        return canGoNext;
    }

    /**
     * Determines whether the {@link Wizard} has been cancelled by the user.
     *
     * @return {@code true} if cancelled; {@code false} if finished
     */
    public boolean isCancelled() {
        return controller.isCancelled();
    }

    /**
     * Sets whether forward navigation is allowed.
     *
     * @param canGoForward {@code true} if allowed; {@code false} if not
     */
    public void setCanGoForward(boolean canGoForward) {
        logger.info(String.format("Setting the canGoForward property to: %s",
                canGoForward));
        boolean old = isCanGoForward();
        this.canGoNext = canGoForward;
        firePropertyChange("canGoForward", old, isCanGoForward());
    }

    /**
     * Determines whether backward navigation is allowed.
     *
     * @return {@code true} if allowed; {@code false} if not
     */
    public boolean isCanGoBack() {
        return canGoBack;
    }

    /**
     * Sets whether backward navigation is allowed.
     *
     * @param canGoBack {@code true} if allowed; {@code false} if not
     */
    public void setCanGoBack(boolean canGoBack) {
        logger.info(String.format("Setting the canGoBack property to: %s",
                canGoBack));
        boolean old = isCanGoBack();
        this.canGoBack = canGoBack;
        firePropertyChange("canGoBack", old, isCanGoBack());
    }

    /**
     * Determines whether the {@link Wizard} may be finished.
     *
     * @return {@code true} if allowed; {@code false} if not
     */
    public boolean isCanFinish() {
        return canFinish;
    }

    /**
     * Sets whether the {@link Wizard} may be finished.
     *
     * @param canFinish {@code true} to allow; {@code false} to not
     */
    public void setCanFinish(boolean canFinish) {
        logger.info(String.format("Setting the canFinish property to %s",
                canFinish));
        boolean old = isCanFinish();
        this.canFinish = canFinish;
        firePropertyChange("canFinish", old, isCanFinish());
    }

    /**
     * Performs the backward navigation.
     *
     * @param evt the event that caused this action
     */
    @AppAction(enabledProperty = "canGoBack")
    public void goBack(ActionEvent evt) {
        controller.goBack();
    }

    /**
     * Performs the forward navigation.
     *
     * @param evt the event that caused this action
     */
    @AppAction(enabledProperty = "canGoForward")
    public void goForward(ActionEvent evt) {
        controller.goForward();
    }

    @AppAction(enabledProperty = "canFinish")
    public void finish(ActionEvent evt) {
        controller.finish();
    }

    /**
     * Performs the cancellation of this {@code Wizard}.
     *
     * @param evt the event that caused this action
     */
    @AppAction
    public void doCancel(ActionEvent evt) {
        int choice = MessageBox.askQuestion(null,
                resourceMap.getString("cancel.confirm.message"),
                resourceMap.getString("cancel.confirm.title"), false);
        if (choice == MessageBox.YES_OPTION) {
            controller.cancel();
        }
    }

    /**
     * Determines whether there is help available for this {@link Wizard}.
     *
     * @return {@code true} if so; {@code false} if not
     */
    public boolean isHelpAvailable() {
        return helpAvailable;
    }

    /**
     * Sets whether there is help available for this {@link Wizard}.
     *
     * @param helpAvailable {@code true} if so; {@code false} if not
     */
    public void setHelpAvailable(boolean helpAvailable) {
        logger.info(String.format("Setting the helpAvailable property to: %s",
                helpAvailable));
        boolean old = isHelpAvailable();
        this.helpAvailable = helpAvailable;
        firePropertyChange("helpAvailable", old, isHelpAvailable());
    }

    /**
     * Shows the help for this {@link Wizard} if it is available
     *
     * @param evt the event that caused this action
     */
    @AppAction(enabledProperty = "helpAvailable")
    public void showHelp(ActionEvent evt) {
        // TODO: Implement showHelp
    }

    void setCurrentStep(WizardStep step, int index) {
        titleLabel.setText(step.getStepTitle());

        // Show the current step in the CardLayout
        CardLayout layout = (CardLayout) stepsPanel.getLayout();
        layout.show(stepsPanel, step.getStepTitle());
        stepsPanel.revalidate();
        stepsPanel.repaint();

        // Update SidePanel to indicate the current step
        ((SidePanel) sideBar).setCurrentStep(index);
    }

    /**
     * Sets the problem message for the current {@link WizardStep step}. The
     * message value should be a localized string.
     *
     * @param problem the problem message
     */
    public void setProblem(String problem) {
        problemLabel.setText(problem);
    }

    /**
     * Sets the {@link WizardController} for this dialog.
     *
     * @param controller the {@code WizardController}
     */
    void setController(WizardController controller) {
        this.controller = controller;
    }

    private void setupEscapeKey() {
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(KeyStroke.getKeyStroke("pressed ESCAPE"), "cancel");
        getRootPane().getActionMap().put("cancel", context.getActionMap(this)
                .get("doCancel"));
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        sideBar = new SidePanel(stepTitles);
        navigationPanel = new javax.swing.JPanel();
        cancelButton = new javax.swing.JButton();
        finishButton = new javax.swing.JButton();
        nextButton = new javax.swing.JButton();
        backButton = new javax.swing.JButton();
        helpButton = new javax.swing.JButton();
        centerPanel = new javax.swing.JPanel();
        titleLabel = new javax.swing.JLabel();
        problemLabel = new javax.swing.JLabel();
        stepsPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        sideBar.setName("sideBar"); // NOI18N

        javax.swing.GroupLayout sideBarLayout = new javax.swing.GroupLayout(
                sideBar);
        sideBar.setLayout(sideBarLayout);
        sideBarLayout.setHorizontalGroup(
                sideBarLayout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 100, Short.MAX_VALUE)
        );
        sideBarLayout.setVerticalGroup(
                sideBarLayout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 476, Short.MAX_VALUE)
        );

        getContentPane().add(sideBar, java.awt.BorderLayout.LINE_START);

        navigationPanel.setName("navigationPanel"); // NOI18N

        cancelButton.setText("cancel");
        cancelButton.setName("cancelButton"); // NOI18N
        cancelButton.setAction(actionMap.get("doCancel"));

        finishButton.setText("finish");
        finishButton.setName("finishButton"); // NOI18N
        finishButton.setAction(actionMap.get("finish"));

        nextButton.setText("next");
        nextButton.setName("nextButton"); // NOI18N
        nextButton.setAction(actionMap.get("goForward"));

        backButton.setText("back");
        backButton.setName("backButton"); // NOI18N
        backButton.setAction(actionMap.get("goBack"));

        helpButton.setText("help");
        helpButton.setName("helpButton"); // NOI18N
        helpButton.setAction(actionMap.get("showHelp"));

        javax.swing.GroupLayout navigationPanelLayout = new javax.swing.GroupLayout(
                navigationPanel);
        navigationPanel.setLayout(navigationPanelLayout);
        navigationPanelLayout.setHorizontalGroup(
                navigationPanelLayout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING,
                                navigationPanelLayout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(helpButton)
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                                360, Short.MAX_VALUE)
                                        .addComponent(backButton)
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(nextButton)
                                        .addPreferredGap(
                                                javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(finishButton)
                                        .addGap(18, 18, 18)
                                        .addComponent(cancelButton)
                                        .addContainerGap())
        );
        navigationPanelLayout.setVerticalGroup(
                navigationPanelLayout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(navigationPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(navigationPanelLayout
                                        .createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(cancelButton)
                                        .addComponent(finishButton)
                                        .addComponent(nextButton)
                                        .addComponent(backButton)
                                        .addComponent(helpButton))
                                .addContainerGap(
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        Short.MAX_VALUE))
        );

        getContentPane().add(navigationPanel, java.awt.BorderLayout.PAGE_END);

        centerPanel.setName("centerPanel"); // NOI18N
        centerPanel.setLayout(new java.awt.BorderLayout());

        titleLabel.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        titleLabel.setText("Step Title");
        titleLabel.setName("titleLabel"); // NOI18N
        centerPanel.add(titleLabel, java.awt.BorderLayout.PAGE_START);

        problemLabel.setFont(new java.awt.Font("Dialog", 1, 11)); // NOI18N
        problemLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        problemLabel.setText("jLabel1");
        problemLabel.setName("problemLabel"); // NOI18N
        centerPanel.add(problemLabel, java.awt.BorderLayout.PAGE_END);

        stepsPanel.setName("stepsPanel"); // NOI18N
        stepsPanel.setLayout(new java.awt.CardLayout());
        centerPanel.add(stepsPanel, java.awt.BorderLayout.CENTER);

        getContentPane().add(centerPanel, java.awt.BorderLayout.CENTER);

        CardLayout layout = (CardLayout) stepsPanel.getLayout();
        for (WizardStep step : steps) {
            stepsPanel.add(step, step.getStepTitle());
        }

//        // Find the appropropriate size for the WizardDialog.
//        int width = 0;
//        int height = 0;
//
//        for (WizardStep step : steps) {
//            width = Math.max(width, step.getPreferredSize().width);
//            height = Math.max(height, getPreferredSize().height);
//        }
//
//        DebugLogger.debug(logger, "Largest WizardStep width/height = %s/%s",
//                width, height);
//        width += sideBar.getSize().width;
//        height += navigationPanel.getSize().height;
//
//        DebugLogger.debug(logger, "Total calculated width/height = %s/%s",
//                width, height);
//        addComponentListener(new SizeListener());
//        setMaximumSize(new Dimension(1024, 768));
//        setMinimumSize(new Dimension(640, 480));
//        setPreferredSize(new Dimension(width, height));
//        setSize(new Dimension(width, height));
        setupEscapeKey();

        resourceMap.injectComponents(this);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backButton;
    private javax.swing.JButton cancelButton;
    private javax.swing.JPanel centerPanel;
    private javax.swing.JButton finishButton;
    private javax.swing.JButton helpButton;
    private javax.swing.JPanel navigationPanel;
    private javax.swing.JButton nextButton;
    private javax.swing.JLabel problemLabel;
    private javax.swing.JPanel sideBar;
    private javax.swing.JPanel stepsPanel;
    private javax.swing.JLabel titleLabel;
    // End of variables declaration//GEN-END:variables

    private final ApplicationContext context;
    private final ResourceMap resourceMap;
    private final Logger logger;
    private final List<String> stepTitles;
    private final List<WizardStep> steps;
    private final ActionMap actionMap;

    private WizardController controller = null;

    private boolean canFinish = false;
    private boolean canGoBack = false;
    private boolean canGoNext = false;
    private boolean helpAvailable = false;

    private class SizeListener extends ComponentAdapter {

        @Override
        public void componentResized(ComponentEvent e) {
            DebugLogger.debug(logger, "Current Size: width=%s, height=%s",
                    getSize().width, getSize().height);
        }

    }

}
