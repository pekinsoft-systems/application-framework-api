/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   PluginManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 14, 2024
 *  Modified   :   Nov 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.plugins;

import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.AppAction;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.lookup.Lookup;
import com.pekinsoft.plugins.ui.PluginManagerDialog;
import com.pekinsoft.security.LogonManager;
import com.pekinsoft.security.SystemUser;
import java.awt.event.ActionEvent;
import java.util.*;
import java.util.prefs.Preferences;
import java.util.stream.Collectors;
import javax.swing.ActionMap;

/**
 * The {@code PluginManager} class is responsible for managing the lifecycle of
 * plugins within the application.
 * It provides methods to load, install, activate, deactivate, and uninstall
 * plugins, as well as retrieve
 * information about the installed plugins.
 */
public class PluginManager {

    private static PluginManager instance;
    private final Map<String, Plugin> plugins;
    private final ApplicationContext context;

    /**
     * Private constructor to initialize the {@code PluginManager} with the
     * provided application context.
     *
     * @param context The application context used for plugin management.
     */
    private PluginManager(ApplicationContext context) {
        this.context = context;
        this.plugins = new HashMap<>();
    }

    /**
     * Returns the singleton instance of {@code PluginManager}.
     *
     * @param context The application context used for plugin management.
     *
     * @return The singleton instance of {@code PluginManager}.
     */
    public static synchronized PluginManager getInstance(
            ApplicationContext context) {
        if (instance == null) {
            instance = new PluginManager(context);
        }
        return instance;
    }

    /**
     * Loads all available plugins using the {@code Lookup} mechanism and
     * installs or activates them
     * based on user preferences and authorization.
     */
    public void loadPlugins() {
        // Load all available plugins from META-INF/services using Lookup
        for (Plugin plugin : Lookup.getDefault().lookupAll(Plugin.class)) {
            plugins.put(plugin.getPluginName(), plugin);

            // Check preferences for install and activate status
            Preferences prefs = context.getPreferences(ApplicationContext.USER);
            boolean shouldInstall = prefs.getBoolean(
                    plugin.getPluginName() + ".shouldInstall", true);
            boolean shouldActivate = prefs.getBoolean(
                    plugin.getPluginName() + ".shouldActivate", true);

            if (shouldInstall && isUserAllowedToInstall(plugin)) {
                installPlugin(plugin.getPluginName());
            }
            if (shouldActivate && plugin.isDeactivated() && isUserAllowedToActivate(
                    plugin)) {
                activatePlugin(plugin.getPluginName());
            }
        }
    }

    /**
     * Checks if the current user is allowed to install the specified plugin.
     *
     * @param plugin The plugin to check for installation authorization.
     *
     * @return {@code true} if the user is allowed to install the plugin,
     *         {@code false} otherwise.
     */
    private boolean isUserAllowedToInstall(Plugin plugin) {
        SystemUser user = LogonManager.getInstance().getLoggedOnUser();
        return plugin.getAllowedGroups().contains(user.getPrimaryGroup())
                || user.getMembershipGroups().stream().anyMatch(plugin
                        .getAllowedGroups()::contains);
    }

    /**
     * Checks if the current user is allowed to activate the specified plugin.
     *
     * @param plugin The plugin to check for activation authorization.
     *
     * @return {@code true} if the user is allowed to activate the plugin,
     *         {@code false} otherwise.
     */
    private boolean isUserAllowedToActivate(Plugin plugin) {
        SystemUser user = LogonManager.getInstance().getLoggedOnUser();
        return plugin.getAllowedGroups().contains(user.getPrimaryGroup())
                || user.getMembershipGroups().stream().anyMatch(plugin
                        .getAllowedGroups()::contains);
    }

    /**
     * Installs the specified plugin by its name.
     *
     * @param pluginName The name of the plugin to install.
     */
    public void installPlugin(String pluginName) {
        Plugin plugin = plugins.get(pluginName);
        if (plugin != null && plugin.shouldInstall()) {
            plugin.install();
            plugin.setUninstalled(false);
        }
    }

    /**
     * Activates the specified plugin by its name.
     *
     * @param pluginName The name of the plugin to activate.
     */
    public void activatePlugin(String pluginName) {
        Plugin plugin = plugins.get(pluginName);
        if (plugin != null && plugin.shouldActivate()) {
            plugin.activate();
            plugin.setDeactivated(false);
        }
    }

    /**
     * Deactivates the specified plugin by its name.
     *
     * @param pluginName The name of the plugin to deactivate.
     */
    public void deactivatePlugin(String pluginName) {
        Plugin plugin = plugins.get(pluginName);
        if (plugin != null && !plugin.isDeactivated()) {
            plugin.deactivate();
            plugin.setDeactivated(true);
        }
    }

    /**
     * Uninstalls the specified plugin by its name.
     *
     * @param pluginName The name of the plugin to uninstall.
     */
    public void uninstallPlugin(String pluginName) {
        Plugin plugin = plugins.get(pluginName);
        if (plugin != null && !plugin.isUninstalled()) {
            plugin.uninstall();
            plugin.setUninstalled(true);
        }
    }

    /**
     * Returns a collection of all installed plugins.
     *
     * @return An unmodifiable collection of installed plugins.
     */
    public Collection<Plugin> getInstalledPlugins() {
        return Collections.unmodifiableCollection(plugins.values());
    }

    /**
     * Retrieves a plugin by its name.
     *
     * @param name The name of the plugin to retrieve.
     *
     * @return The plugin with the specified name, or {@code null} if not found.
     */
    public Plugin getPluginByName(String name) {
        return plugins.values().stream()
                .filter(plugin -> plugin.getPluginName().equalsIgnoreCase(name))
                .findFirst()
                .orElse(null);
    }

    /**
     * Retrieves a collection of plugins by their version.
     *
     * @param version The version of the plugins to retrieve.
     *
     * @return A collection of plugins with the specified version.
     */
    public Collection<Plugin> getPluginsByVersion(String version) {
        return plugins.values().stream()
                .filter(plugin -> plugin.getPluginVersion().equalsIgnoreCase(
                version))
                .collect(Collectors.toList());
    }

    /**
     * Retrieves a collection of plugins by their vendor.
     *
     * @param vendor The vendor of the plugins to retrieve.
     *
     * @return A collection of plugins with the specified vendor.
     */
    public Collection<Plugin> getPluginsByVendor(String vendor) {
        return plugins.values().stream()
                .filter(plugin -> plugin.getVendor().equalsIgnoreCase(vendor))
                .collect(Collectors.toList());
    }

    @AppAction(menuBaseName = "tools",
               menuActionIndex = 126,
               menuSepAfter = true,
               menuSepBefore = true)
    public void showPluginManager(ActionEvent evt) {
        WindowManager.getDefault().show(PluginManagerDialog.getInstance());
    }

    public List<ActionMap> getActionMaps() {
        List<ActionMap> actionMaps = new ArrayList<>();
        actionMaps.add(context.getActionMap(getClass(), this));
        for (Plugin plugin : plugins.values()) {
            if (!plugin.isUninstalled() && !plugin.isDeactivated()) {
                actionMaps.addAll(plugin.mergeGlobalActions());
            }
        }
        return actionMaps;
    }

}
