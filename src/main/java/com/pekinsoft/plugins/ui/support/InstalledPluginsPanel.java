/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   InstalledPluginsPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 20, 2024
 *  Modified   :   Jul 20, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 20, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.plugins.ui.support;

import com.pekinsoft.framework.*;
import com.pekinsoft.plugins.Plugin;
import com.pekinsoft.plugins.PluginManager;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Objects;
import javax.swing.ActionMap;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author Sean Carrick
 */
public class InstalledPluginsPanel extends JPanel implements
        PropertyChangeListener {

    /**
     * Creates new form InstalledPluginsPanel
     */
    public InstalledPluginsPanel(ApplicationContext context) {
        Objects
                .requireNonNull(context,
                        "The ApplicationContext cannot be null.");
        this.context = context;
        model = new InstalledTableModel(context);
        initComponents();
    }

    public boolean isActivatable() {
        int idx = pluginsTable.getSelectedRow();
        return selected && model.getSelectedPlugin(idx).isDeactivated();
    }

    public void setActivatable(boolean activatable) {
        boolean old = isActivatable();
        this.activatable = activatable;

        firePropertyChange("activatable", old, isActivatable());
    }

    public boolean isDeactivatable() {
        int idx = pluginsTable.getSelectedRow();
        return selected && !model.getSelectedPlugin(idx).isDeactivated();
    }

    public void setDeactivatable(boolean deactivatable) {
        boolean old = isDeactivatable();
        this.deactivatable = deactivatable;
        firePropertyChange("deactivatable", old, isDeactivatable());
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        boolean old = isSelected();
        this.selected = selected;
        firePropertyChange("selected", old, isSelected());
        firePropertyChange("deactivatable", old, isDeactivatable());
        firePropertyChange("activatable", old, isActivatable());
    }

    @AppAction(enabledProperty = "activatable")
    public void activateSelected(ActionEvent evt) {
        int[] selectedRows = pluginsTable.getSelectedRows();
        for (int row : selectedRows) {
            boolean selected = (boolean) model.getValueAt(row, 0);
            if (selected) {
                Plugin plugin = model.getSelectedPlugin(row);
                PluginManager.getInstance(context)
                        .activatePlugin(plugin.getPluginName());
                model.setValueAt(false, row, 0);
            }
        }
        setSelected(false);
        model.fireTableDataChanged();
    }

    @AppAction(enabledProperty = "deactivatable")
    public void deactivateSelected(ActionEvent evt) {
        int[] selectedRows = pluginsTable.getSelectedRows();
        for (int row : selectedRows) {
            boolean selected = (boolean) model.getValueAt(row, 0);
            if (selected) {
                Plugin plugin = model.getSelectedPlugin(row);
                PluginManager.getInstance(getContext())
                        .deactivatePlugin(plugin.getPluginName());
                model.setValueAt(false, row, 0);
            }
        }
        setSelected(false);
        model.fireTableDataChanged();
    }

    @AppAction(enabledProperty = "selected")
    public void uninstallSelected(ActionEvent evt) {
        int[] selectedRows = pluginsTable.getSelectedRows();
        for (int row : selectedRows) {
            boolean selected = (boolean) model.getValueAt(row, 0);
            if (selected) {
                Plugin plugin = model.getSelectedPlugin(row);
                PluginManager.getInstance(getContext())
                        .uninstallPlugin(plugin.getPluginName());
            }
        }
        setSelected(false);
        model.fireTableDataChanged();
        firePropertyChange("restartRequired", false, true);
    }

    @AppAction
    public void cellEditedAction(ActionEvent evt) {
        TableCellListener source = (TableCellListener) evt.getSource();
        Object value = source.getNewValue();

        if (source.getColumn() == 0) {
            setSelected((boolean) value);
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() != null) {
            switch (evt.getPropertyName()) {
                case "selectionChanged" -> {
                    boolean old = isSelected();
                    boolean selected = false;
                    for (int x = 0; x < pluginsTable.getRowCount(); x++) {
                        selected = (boolean) pluginsTable.getValueAt(x, 0);
                        if (!selected) {
                            continue;
                        }
                    }
                    if (old != selected) {
                        setSelected(selected);
                    }
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        commandPanel = new javax.swing.JPanel();
        activateButton = new javax.swing.JButton();
        deactivateButton = new javax.swing.JButton();
        uninstallButton = new javax.swing.JButton();
        installedSplit = new javax.swing.JSplitPane();
        pluginsScrollPane = new javax.swing.JScrollPane();
        pluginsTable = new javax.swing.JTable();
        detailsScrollPane = new javax.swing.JScrollPane();
        detailsPane = new javax.swing.JEditorPane();

        setName("Form"); // NOI18N
        setLayout(new java.awt.BorderLayout());

        commandPanel.setName("commandPanel"); // NOI18N

        activateButton.setText("jButton1");
        activateButton.setName("activateButton"); // NOI18N
        activateButton.setAction(myActionMap().get("activateSelected"));

        deactivateButton.setText("jButton2");
        deactivateButton.setName("deactivateButton"); // NOI18N
        deactivateButton.setAction(myActionMap().get("deactivateSelected"));

        uninstallButton.setText("jButton3");
        uninstallButton.setName("uninstallButton"); // NOI18N
        uninstallButton.setAction(myActionMap().get("uninstallSelected"));

        javax.swing.GroupLayout commandPanelLayout = new javax.swing.GroupLayout(commandPanel);
        commandPanel.setLayout(commandPanelLayout);
        commandPanelLayout.setHorizontalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(activateButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(deactivateButton)
                .addGap(18, 18, 18)
                .addComponent(uninstallButton)
                .addContainerGap(496, Short.MAX_VALUE))
        );
        commandPanelLayout.setVerticalGroup(
            commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(commandPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(commandPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(activateButton)
                    .addComponent(deactivateButton)
                    .addComponent(uninstallButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        add(commandPanel, java.awt.BorderLayout.PAGE_END);

        installedSplit.setName("installedSplit"); // NOI18N

        pluginsScrollPane.setName("pluginsScrollPane"); // NOI18N

        pluginsTable.setModel(model);
        getApplication().addPropertyChangeListener(model);
        Font f = pluginsTable.getFont();
        FontMetrics fm = pluginsTable.getFontMetrics(f);
        pluginsTable.setColumnModel(new InstalledColumnModel(context, fm));
        pluginsTable.getColumnModel().getColumn(0).addPropertyChangeListener(this);
        pluginsTable.setName("pluginsTable"); // NOI18N
        pluginsTable.getSelectionModel().addListSelectionListener(new RowSelectionChanged());
        TableCellListener listener = new TableCellListener(pluginsTable, myActionMap().get("cellEditedAction"));
        pluginsScrollPane.setViewportView(pluginsTable);

        installedSplit.setLeftComponent(pluginsScrollPane);

        detailsScrollPane.setName("detailsScrollPane"); // NOI18N

        detailsPane.setEditable(false);
        detailsPane.setContentType("text/html"); // NOI18N
        detailsPane.setName("detailsPane"); // NOI18N
        detailsPane.setOpaque(false);
        detailsPane.setEditorKit(new HTMLEditorKit());
        detailsScrollPane.setViewportView(detailsPane);

        installedSplit.setRightComponent(detailsScrollPane);

        add(installedSplit, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private ApplicationContext getContext() {
        return context;
    }

    private Application getApplication() {
        return getContext().getApplication();
    }

    private ResourceMap getResourceMap() {
        return getContext().getResourceMap(getClass());
    }

    private ActionMap myActionMap() {
        return context.getActionMap(getClass(), this);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton activateButton;
    private javax.swing.JPanel commandPanel;
    private javax.swing.JButton deactivateButton;
    private javax.swing.JEditorPane detailsPane;
    private javax.swing.JScrollPane detailsScrollPane;
    private javax.swing.JSplitPane installedSplit;
    private javax.swing.JScrollPane pluginsScrollPane;
    private javax.swing.JTable pluginsTable;
    private javax.swing.JButton uninstallButton;
    // End of variables declaration//GEN-END:variables

    private final ApplicationContext context;
    private final PluginManagerTableModel model;

    private boolean activatable = false;
    private boolean deactivatable = false;
    private boolean selected = false;

    private class RowSelectionChanged implements ListSelectionListener {

        @Override
        public void valueChanged(ListSelectionEvent e) {
            if (!e.getValueIsAdjusting()) {
                if (pluginsTable.getSelectedRow() != -1) {
                    Plugin plugin = model.getSelectedPlugin(pluginsTable
                            .getSelectedRow());
                    String header = "<html><body><h1>" + plugin.getPluginName() + "</h1>";
                    String second = "<h3><strong>Version</strong> " + plugin
                            .getPluginVersion() + "</h3>";
//                    detailsPane.setOpaque(true);
                    detailsPane.setText(header + second + plugin
                            .getPluginDescription() + "</body></html>");
                    detailsPane.getCaret().setDot(0);
                } else {
//                    detailsPane.setOpaque(false);
                    detailsPane.setText("<html><body></body></html>");
                    detailsPane.getCaret().setDot(0);
                }
            }
        }

    }

}
