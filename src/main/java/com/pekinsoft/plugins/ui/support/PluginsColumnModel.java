/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   PluginsColumnModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 19, 2024
 *  Modified   :   Jul 19, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.plugins.ui.support;

import com.pekinsoft.framework.*;
import java.awt.FontMetrics;
import java.util.Objects;
import javax.swing.table.*;

/**
 * An abstraction of the {@link DefaultTableColumnModel} for use within the
 * application's <em>Plugin Manager</em>. Since each tab of the Plugin Manager
 * represent the plugins in a different way, this abstraction was created to
 * ease the development of the individual table columns for the various table
 * models.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class PluginsColumnModel extends DefaultTableColumnModel {

    private static final long serialVersionUID = 1L;

    public PluginsColumnModel(ApplicationContext context, FontMetrics fm) {
        Objects
                .requireNonNull(context,
                        "The ApplicationContext cannot be null.");
        Objects.requireNonNull(fm, "The FontMetrics cannot be null.");
        this.context = context;
        this.fm = fm;
    }

    /**
     * Generates a {@link TableColumn} from the specified parameters.
     * <p>
     * Subclasses can simply call this method from within the constructor to
     * create the various columns that are needed for the plugins table being
     * created, and {@link #addColumn(TableColumn) add} it to the table.
     *
     * @param columnIndex the index of the column being created (base 0)
     * @param width       the initial width of the column
     * @param resizable   whether the column should be able to be resized
     * @param text        the text to display as the column header
     *
     * @return the {@code TableColumn} created from the specified parameters
     */
    protected TableColumn createColumn(int columnIndex, int width,
            boolean resizable, String text) {
        int textWidth = fm.stringWidth(text + "   ");
        if (width < textWidth) {
            width = textWidth;
        }

        TableColumn col = new TableColumn(columnIndex);
        col.setCellRenderer(createCellRenderer(columnIndex));
        col.setHeaderRenderer(createHeaderRenderer());
        col.setHeaderValue(text);
        col.setPreferredWidth(width);
        if (!resizable) {
            col.setMaxWidth(width);
            col.setMinWidth(width);
        }
        col.setResizable(resizable);
        return col;
    }

    /**
     * Allows the subclass to generate a specific implementation of the
     * {@link TableCellRenderer} they need for their columns.
     *
     * @param column the column number for which the cell renderer is being
     *               created
     *
     * @return the {@code TableCellRenderer} to use for rendering the data
     */
    protected abstract TableCellRenderer createCellRenderer(int column);

    /**
     * Allows subclasses to generate a specific {@code TableCellRenderer} for
     * the column header. The default implementation returns {@code null}.
     *
     * @return the {@code TableCellRenderer} to use for the column header, or
     *         {@code null}
     */
    protected TableCellRenderer createHeaderRenderer() {
        return null;
    }

    protected ApplicationContext getContext() {
        return context;
    }

    protected Application getApplication() {
        return getContext().getApplication();
    }

    protected ResourceMap getResourceMap() {
        return getContext().getResourceMap(getClass(), PluginsColumnModel.class);
    }

    protected final FontMetrics fm;
    private final ApplicationContext context;

}
