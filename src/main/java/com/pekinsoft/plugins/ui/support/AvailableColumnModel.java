/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   InstalledColumnModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 20, 2024
 *  Modified   :   Jul 20, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 20, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.plugins.ui.support;

import com.pekinsoft.framework.ApplicationContext;
import java.awt.FontMetrics;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class AvailableColumnModel extends PluginsColumnModel {

    private static final long serialVersionUID = 2849351760036112888L;
    
    public AvailableColumnModel (ApplicationContext context, FontMetrics fm) {
        super(context, fm);
        
        int digit = fm.stringWidth("0");
        int alpha = fm.stringWidth("W");
        
        addColumn(createColumn(0, alpha * 2, false, getResourceMap().getString("col0.text")));
        addColumn(createColumn(1, alpha * 45, true, getResourceMap().getString(
                "col1.text")));
        addColumn(createColumn(2, alpha * 2, false, getResourceMap().getString(
                "col2.text")));
    }

    @Override
    protected TableCellRenderer createCellRenderer(int column) {
        return new AvailableCellRenderer();
    }
    
}
