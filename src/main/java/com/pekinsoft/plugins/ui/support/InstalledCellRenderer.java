/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   InstalledCellRenderer.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 20, 2024
 *  Modified   :   Jul 20, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 20, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.plugins.ui.support;

import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import java.awt.Component;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class InstalledCellRenderer extends DefaultTableCellRenderer {

    public InstalledCellRenderer() {
        rm = Application.getInstance().getContext().getResourceMap(getClass());
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        return switch (column) {
            case 0 ->
                createCheckBox((Boolean) value, isSelected);
            case 1 ->
                createNameLabel((String) value, isSelected);
            case 3 ->
                createActivatedLabel((Boolean) value, isSelected);
            default ->
                createVersionLabel((String) value, isSelected);
        };
    }

    private JLabel createActivatedLabel(boolean value, boolean isSelected) {
        JLabel label = new JLabel();
        label.setOpaque(true);
        label.setHorizontalAlignment(CENTER);
        if (value) {
            label.setIcon(rm.getIcon("activated.icon"));
        } else {
            label.setIcon(rm.getIcon("deactivated.icon"));
        }
        if (isSelected) {
            label.setForeground(UIManager.getColor("Table.selectionForeground"));
            label.setBackground(UIManager.getColor("Table.selectionBackground"));
        } else {
            label.setForeground(UIManager.getColor("Table.foreground"));
            label.setBackground(UIManager.getColor("Table.background"));
        }
        return label;
    }

    private JLabel createVersionLabel(String value, boolean isSelected) {
        JLabel label = new JLabel(value);
        label.setOpaque(true);
        label.setHorizontalAlignment(CENTER);
        if (isSelected) {
            label.setForeground(UIManager.getColor("Table.selectionForeground"));
            label.setBackground(UIManager.getColor("Table.selectionBackground"));
        } else {
            label.setForeground(UIManager.getColor("Table.foreground"));
            label.setBackground(UIManager.getColor("Table.background"));
        }
        return label;
    }

    private JLabel createNameLabel(String value, boolean isSelected) {
        JLabel label = new JLabel("<html>" + value + "</html>");
        label.setOpaque(true);
        if (isSelected) {
            label.setForeground(UIManager.getColor("Table.selectionForeground"));
            label.setBackground(UIManager.getColor("Table.selectionBackground"));
        } else {
            label.setForeground(UIManager.getColor("Table.foreground"));
            label.setBackground(UIManager.getColor("Table.background"));
        }
        return label;
    }

    private JCheckBox createCheckBox(boolean selected, boolean rowSelected) {
        JCheckBox checkBox = new JCheckBox();
        checkBox.setOpaque(true);
        checkBox.setHorizontalAlignment(CENTER);
        checkBox.setSelected(selected);
        checkBox.addChangeListener(e -> {
            firePropertyChange("selectionChanged", !checkBox.isSelected(),
                    checkBox.isSelected());
        });
        if (rowSelected) {
            checkBox.setForeground(UIManager.getColor(
                    "Table.selectionForeground"));
            checkBox.setBackground(UIManager.getColor(
                    "Table.selectionBackground"));
        } else {
            checkBox.setForeground(UIManager.getColor("Table.foreground"));
            checkBox.setBackground(UIManager.getColor("Table.background"));
        }

        return checkBox;
    }

    private final ResourceMap rm;

}
