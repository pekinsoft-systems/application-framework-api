/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ntos-base-module
 *  Class      :   PluginManagerDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 20, 2024
 *  Modified   :   Jul 20, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 20, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.plugins.ui;

import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.plugins.ui.support.AvailablePluginsPanel;
import com.pekinsoft.plugins.ui.support.InstalledPluginsPanel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class PluginManagerDialog extends JDialog implements
        PropertyChangeListener {

    private static final long serialVersionUID = 9023876444276817253L;
    private static final Logger logger = Application.getInstance().getContext()
            .getLogger(PluginManagerDialog.class);
    private static PluginManagerDialog instance = null;

    public static PluginManagerDialog getInstance() {
        if (instance == null) {
            instance = new PluginManagerDialog(Application.getInstance()
                    .getContext());
        }
        return instance;
    }

    private PluginManagerDialog(ApplicationContext context) {
        this.context = context;

        initComponents();
    }

    public boolean isHelpAvailable() {
        return false;
    }

    public void setHelpAvailable(boolean helpAvailable) {
        firePropertyChange("helpAvailable", true, false);
    }

    @AppAction
    public void close(ActionEvent evt) {
        if (restartRequired) {
            String message = getResourceMap().getString("message.restart");
            String title = getResourceMap().getString("message.restart.title");
            int choice = JOptionPane.showConfirmDialog(this, message, title,
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (choice == JOptionPane.YES_OPTION) {
                WindowManager.getDefault().close(this);
                context.getApplication().exit(SysExits.EX_RESTART);
            }
        }
        WindowManager.getDefault().close(this);
    }

    @AppAction(enabledProperty = "helpAvailable")
    public void showHelp(ActionEvent evt) {
        // TODO: Figure out the integrated help system.
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() != null
                && "restartRequired".equals(evt.getPropertyName())) {
            restartRequired = (boolean) evt.getNewValue();
        }
    }

    private void initComponents() {
        commandPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        commandPanel.setName("commandPanel");

        closeButton = new JButton(myActionMap().get("close"));
        closeButton.setName("closeButton");

        helpButton = new JButton(myActionMap().get("showHelp"));
        helpButton.setName("helpButton");

        managerTabs = new JTabbedPane();
        managerTabs.setName("managerTabs");

        logger.log(Level.INFO, "Instantiating our AvailablePluginsPanel");
        availableTab = new AvailablePluginsPanel(context);
        availableTab.setName("availableTab");
        availableTab.addPropertyChangeListener(this);

        logger.log(Level.INFO, "Instantiating our InstalledPluginsPanel");
        installedTab = new InstalledPluginsPanel(context);
        installedTab.setName("installedTab");
        installedTab.addPropertyChangeListener(this);

        setLayout(new BorderLayout());
        setName("PluginManager");
        setPreferredSize(new Dimension(800, 600));

        // Make the closeButton's action work with the Enter key.
        getRootPane().setDefaultButton(closeButton);

        // Make the closeButton's action work with the Escape key.
        KeyStroke close = (KeyStroke) myActionMap().get("close")
                .getValue(Action.ACCELERATOR_KEY);
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                .put(close, "close");
        getRootPane().getActionMap().put("close", myActionMap().get("close"));

        add(commandPanel, BorderLayout.SOUTH);
        commandPanel.add(closeButton);
        commandPanel.add(helpButton);
        add(managerTabs, BorderLayout.CENTER);
        managerTabs.addTab(getResourceMap().getString("availableTab.tabTitle"),
                availableTab);
        managerTabs.addTab(getResourceMap().getString("installedTab.tabTitle"),
                installedTab);

        getResourceMap().injectComponents(this);
    }

    private ResourceMap getResourceMap() {
        return context.getResourceMap(getClass());
    }

    private ActionMap myActionMap() {
        return context.getActionMap(this);
    }

    private final ApplicationContext context;

    private JTabbedPane managerTabs;
    private AvailablePluginsPanel availableTab;
    private InstalledPluginsPanel installedTab;
    private JPanel commandPanel;
    private JButton closeButton;
    private JButton helpButton;

    private boolean restartRequired = false;

}
