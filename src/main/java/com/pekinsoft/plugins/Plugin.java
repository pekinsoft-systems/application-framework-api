/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Plugin.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.plugins;

import com.pekinsoft.api.DockingManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.security.*;
import java.util.*;
import javax.swing.ActionMap;

/**
 * An interface that may be implemented by application modules to allow them to
 * configure themselves as necessary for use, and to cleanup after themselves at
 * application termination.
 * <p>
 * The {@link #install() } method is used to perform any startup
 * initializations that need to be done <em>before</em> the GUI is visible to
 * the user.
 * <p>
 * The {@link #mergeGlobalActions() } method is used to return a list of
 * ActionMaps for {@link AppAction} methods that should be included
 * in the application's actions system (menus and toolbars).
 * <p>
 * The {@link #ready() } method is used to allow the {@code Plugin} to perform
 * any last minute configurations that needed the GUI available in order to do.
 * <p>
 * The {@link #activate() } method is called if the {@code Plugin} is
 * deactivated and then activated again while the application is running.
 * <p>
 * The {@link #deactivate() } method is called if the {@code Plugin} is
 * deactivated while the GUI is running.
 * <p>
 * The {@link #uninstall() } method is called during application shutdown to
 * allow the {@code Plugin} to clean up after itself.
 * <dl>
 * <dt><strong><em>Developer's Note</em></strong>:</dt>
 * <dd>This base class has a {@code protected} {@link List} that stores
 * {@code UserGroup} user groups that are allowed to use the features provided
 * by this {@code Plugin}. This list needs to be appended in the constructor of
 * the subclass to allow for installation/activation if the currently logged on
 * user is a member of one of the allowed groups.</dd></dl>
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class Plugin extends AbstractBean {

    /**
     * Constructs a new {@code Plugin}. Called from the subclass to establish
     * the {@link ApplicationContext} in which this {@code Plugin} is being
     * used.
     *
     * @param context the {@code ApplicationContext}
     */
    protected Plugin(ApplicationContext context) {
        this.context = context;
        resourceMap = context.getResourceMap(getClass(), Plugin.class);
    }

    /**
     * Called during application startup to load the {@code Plugin} into the
     * application's structure. This method allows the {@code Plugin} to perform
     * any initializations that it may need. This method is called during
     * application initialization, prior to any GUI being constructed.
     */
    public abstract void install();

    /**
     * Called once the application's main GUI is showing to the user and is
     * &ldquo;ready&rdquo; to be used. This method gives the {@code Plugin} an
     * opportunity to perform any initializations that it may need to do once
     * the GUI is up and running.
     * <p>
     * This method is called from the application's ready method, prior to the
     * dismissal of the application's splash screen.
     * <p>
     * The default implementation does nothing.
     */
    public void ready() {

    }

    /**
     * Called whenever the {@code Plugin} is activated within the application.
     * This method should be used to add any of its actions back into the
     * application's {@link ActionManager}. This will place its actions back
     * into the application's menus and toolbars.
     * <p>
     * The default implementation sets the {@code deactivated} property to
     * {@code false}.
     */
    public void activate() {
        deactivated = false;
    }

    /**
     * Called whenever the {@code Plugin} is deactivated within the application.
     * This method should be used to remove any of its actions from the
     * application's {@link ActionManager}. This will remove its actions from
     * the application's menus and toolbars.
     * <p>
     * The default implementation sets the {@code deactivated} property to
     * {@code true}.
     */
    public void deactivate() {
        deactivated = true;
    }

    /**
     * Determines whether this {@code Plugin} has been deactivated in the
     * application.
     * <p>
     * This method should track a flag that is turned on (set to {@code true})
     * in the {@link #deactivate() } method, and turned off (set to
     * {@code false}) in the {@link #activate() } method.
     *
     * @return {@code true} if the {@code Plugin} is currently not active
     */
    public final boolean isDeactivated() {
        return deactivated;
    }

    /**
     * Called during application shutdown to allow the {@code Plugin} a chance
     * to perform any necessary cleanup.
     */
    public abstract void uninstall();

    /**
     * Determines whether the {@code Plugin} is currently installed in the
     * application. An implementation of {@code Plugin} needs to track its
     * installation status.
     * <p>
     * This method is used by the actions system to determine whether to call
     * {@link #mergeGlobalActions} to place the global actions from this
     * {@code Plugin} into the application's menus, toolbars and/or
     * BackgroundTask panes.
     * <p>
     * Whenever a {@code Plugin} is installed, such as at application startup,
     * the global actions are queried and pulled into the actions system if they
     * are required. Whenever the {@code Plugin} is uninstalled, the actions
     * system is regenerated to remove the {@code Plugin}'s global actions.
     *
     * @return {@code true} if the plugin is not currently installed
     */
    public final boolean isUninstalled() {
        return uninstalled;
    }

    /**
     * Called whenever a {@code Plugin} is {@link #install() installed} into an
     * application to merge its global actions into the application's actions
     * system, such as {@link javax.swing.JMenu menus} and/or
     * {@link javax.swing.JToolBar toolbars}.
     * <p>
     * A {@code Plugin} needs to create a {@link java.util.List list} of action
     * maps of the classes in the plugin that have methods decorated with the
     * {@link AppAction} annotation. The annotation parameters are used to
     * define the part(s) of the actions system from which the action should be
     * available. The actions object instances will be used to retrieve the
     * objects' {@link ActionMapX action maps} from which to generate the global
     * actions system.
     * <p>
     * The default implementation returns an empty list.
     *
     * @return the list of object instances that have globally available
     *         {@code AppAction} decorated methods within the {@code Plugin}
     *         which should be globally available
     */
    public List<ActionMap> mergeGlobalActions() {
        return Collections.emptyList();
    }

    /**
     * Retrieves a {@link java.util.Map} of all {@code Plugin}s on which this
     * {@code Plugin} depends. This allows for a &ldquo;Plugin Suite&rdquo;,
     * where one plugin causes others to be found and installed/activated.
     * <p>
     * The default implementation return an empty map.
     *
     * @return a map of dependencies on which this plugin relies
     */
    public Map<String, String> getDependencies() {
        return Collections.emptyMap();
    }

    /**
     * Retrieves the name of this {@code Plugin} for display in the
     * {@link Application Application's} about dialog.
     * <p>
     * The default implementation returns the value stored in the
     * {@link ResourceMap} key &ldquo;plugin.name&rdquo;.
     *
     * @return the {@code Plugin}'s name
     */
    public String getPluginName() {
        return getResourceMap().getString("plugin.name");
    }

    /**
     * Retrieves the name of the developer or company that created this
     * {@code Plugin}. Used for querying the {@link PluginManager} to locate
     * installed or activated {@code Plugin}s.
     * <p>
     * The default implementation returns the value stored in the
     * {@link ResourceMap} key &ldquo;plugin.vendor&rdquo;.
     *
     * @return the {@code Plugin} developer or vendor
     */
    public String getVendor() {
        return getResourceMap().getString("plugin.vendor");
    }

    /**
     * Retrieves the version of the {@code Plugin}, typically in Dewey Decimal
     * format, but this is not a requirement. This information is used for
     * display in the {@link Application Application's} about dialog.
     * <p>
     * The default implementation returns the value stored in the
     * {@link ResourceMap} key &ldquo;plugin.version&rdquo;.
     *
     * @return the {@code Plugin}'s version
     */
    public String getPluginVersion() {
        return getResourceMap().getString("plugin.version");
    }

    /**
     * Retrieves the description of the {@code Plugin} for use in the plugin
     * manager dialog. This value should describe the primary purpose(s) and
     * features of the {@code Plugin}, so that a user can decide whether s/he
     * wants to install the {@code Plugin} into their application. This value
     * should also be a resource that can be translated for
     * internationalization.
     * <p>
     * The default implementation returns the value stored in the
     * {@link ResourceMap} key &ldquo;plugin.description&rdquo;.
     *
     * @return the {@code Plugin}'s description
     */
    public String getPluginDescription() {
        return getResourceMap().getString("plugin.description");
    }

    /**
     * Provides a means by which the {@code Plugin} can have any default windows
     * that it may contain loaded automatically.
     * <p>
     * This method is useful for
     * {@link com.pekinsoft.framework.Application Applications} that implement
     * the {@link DockingManager} interface and that need to have certain
     * windows, such as navigator windows, loaded when the main window loads.
     * The {@code DockingManager} will be able to loop through all installed and
     * activated {@code Plugin}s and call their {@code loadDefaultWindows}
     * method to have those default windows opened. This can be accomplished in
     * the {@link DockingManager#preload() DockingManager.preload} method.
     * <p>
     * In most use-cases, the default windows to be loaded on startup will be
     * docked along one of the sides of the main window. These windows are used
     * to navigate the data that is fronted by the {@code Application} and allow
     * the user to access the data in various ways.
     * <p>
     * Since not all {@code Plugin}s will have windows that need to be loaded
     * automatically at application startup, this method defaults to an empty
     * implementation. It may simply be overridden by those {@code Plugin}s that
     * require this feature.
     */
    public void loadDefaultWindows() {

    }

    /**
     * May be used to determine if the {@code Plugin} had been previously
     * uninstalled from the application, in which case it <em>should not</em> be
     * installed at application startup.
     * <p>
     * The default implementation checks the {@link java.util.prefs.Preferences}
     * on the user node for whether this {@code Plugin} was installed when the
     * application last exited. This method returns that value, with a default
     * value of {@code true}, if the preference had not been set.
     *
     * @return {@code true} if not previously uninstalled and should be
     *         installed at application startup
     */
    public boolean shouldInstall() {
        SystemUser user = LogonManager.getInstance().getLoggedOnUser();
        return (allowedGroups.contains(user.getPrimaryGroup())
                || user.getMembershipGroups().stream().anyMatch(
                        allowedGroups::contains))
                && getContext().getPreferences(ApplicationContext.USER)
                        .getBoolean("installed", true);
    }

    /**
     * May be used to determine if the {@code Plugin} had been previously
     * deactivated in the application, in which case it <em>should not</em> be
     * activated at application startup, though it may be installed.
     * <p>
     * The default implementation checks the {@link java.util.prefs.Preferences}
     * on the user node for whether this {@code Plugin} was activated when the
     * application last exited. This method returns that value, with a default
     * value of {@code true}, if the preference had not been set.
     *
     * @return {@code true} if not previously deactivated and should be
     *         activated at application startup
     */
    public boolean shouldActivate() {
        SystemUser user = LogonManager.getInstance().getLoggedOnUser();
        return (allowedGroups.contains(user.getPrimaryGroup())
                || user.getMembershipGroups().stream().anyMatch(
                        allowedGroups::contains))
                && getContext().getPreferences(ApplicationContext.USER)
                        .getBoolean("activated", true);
    }

    /**
     * Retrieves a list of {@link UserGroup groups} that have access rights to
     * the feature(s) provided by this {@code Plugin}. This list should be a
     * complete list of user groups that can access this {@code Plugin}'s
     * feature set, which assists the application in maintaining the proper
     * security of its data.
     * <p>
     * The default implementation simply returns the {@code allowedGroups} list.
     *
     * @return the list of {@code UserGroup}s that have access to the features
     */
    public List<UserGroup> getAllowedGroups() {
        return allowedGroups;
    }

    /**
     * Allows subclasses to set the {@code uninstalled} property. This
     * should be
     * set to {@code true} when the {@link #uninstall() uninstall}
     * method is
     * called.
     * <p>
     * This property allows the plugin manager for the application to
     * keep track
     * of which plugins are installed and participating in the global
     * actions
     * system, so that only those installed/activated plugins have their
     * menu
     * items and toolbar buttons available to the user.
     * <p>
     * This is a bound property.
     *
     * @param uninstalled {@code true} if uninstalled; {@code false} if
     *                    installed
     */
    protected void setUninstalled(boolean uninstalled) {
        boolean old = isUninstalled();
        this.uninstalled = uninstalled;
        firePropertyChange("uninstalled", old, isUninstalled());
    }

    /**
     * Allows subclasses to set the {@code deactivated} property. This should be
     * set to {@code true} when the {@link #deactivate() deactivate} method is
     * called.
     * <p>
     * This property allows the plugin manager for the application to keep track
     * of which plugins are installed and participating in the global actions
     * system, so that only those installed/activated plugins have their menu
     * items and toolbar buttons available to the user.
     * <p>
     * This is a bound property.
     *
     * @param deactivated {@code true} if deactivated; {@code false} if
     *                    activated
     */
    protected void setDeactivated(boolean deactivated) {
        boolean old = isDeactivated();
        this.deactivated = deactivated;
        firePropertyChange("deactivated", old, isDeactivated());
    }

    /**
     * Retrieves the {@link ApplicationContext} with which this {@code Plugin}
     * was instantiated.
     *
     * @return the {@code ApplicationContext}
     */
    protected ApplicationContext getContext() {
        return context;
    }

    /**
     * Retrieves the {@link ResourceMap} for this {@code Plugin}, including the
     * resource map for the parent class, if any.
     *
     * @return the resource map
     */
    protected ResourceMap getResourceMap() {
        return resourceMap;
    }

    private final ApplicationContext context;
    private final ResourceMap resourceMap;

    protected final List<UserGroup> allowedGroups = new ArrayList<>();
    private boolean uninstalled = true;
    private boolean deactivated = true;

}
