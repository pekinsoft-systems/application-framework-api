/*
 * $Id: ActionContainerFactory.java 3980 2011-03-28 20:24:46Z kschaefe $
 *
 * Copyright 2004 Sun Microsystems, Inc., 4150 Network Circle,
 * Santa Clara, California 95054, U.S.A. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
package com.pekinsoft.framework;

import com.pekinsoft.logging.Logger;
import com.pekinsoft.utils.DebugLogger;
import com.pekinsoft.utils.PreferenceKeys;
import java.awt.Component;
import java.awt.Insets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.swing.*;

/**
 * Creates user interface elements based on action ids and lists of action ids.
 * All action ids must represent actions managed by the ActionManager.
 * <p>
 * <h2>Action Lists</h2>
 * Use the createXXX(List) methods to construct containers of actions like menu
 * bars, menus, popups and toolbars from actions represented as action ids in a
 * <i>java.util.List</i>. Each element in the action-list can be one of 3 types:
 * <ul>
 * <li>action id: corresponds to an action managed by the ActionManager
 * <li>null: indicates a separator should be inserted.
 * <li>java.util.List: represents a submenu. See the note below which describes
 * the configuration of menus.
 * </ul>
 * The order of elements in an action-list determines the arrangement of the ui
 * components which are constructed from the action-list.
 * <p>
 * For a menu or submenu, the first element in the action-list represents a menu
 * and subsequent elements represent menu items or separators (if null).
 * <p>
 * This class can be used as a general component factory which will construct
 * components from Actions if the <code>create&lt;comp&gt;(Action,...)</code>
 * methods are used.
 *
 * @see ActionManager
 */
public class ActionContainerFactory implements PreferenceKeys {

    /**
     * Standard margin for toolbar buttons to improve their look
     */
    private final static Insets TOOLBAR_BUTTON_MARGIN = new Insets(1, 1, 1, 1);

    private static final Logger logger = Application.getInstance().getContext()
            .getLogger(ActionContainerFactory.class);

    /**
     * Generates a JMenuBar for an application based on the provided ActionMaps.
     *
     * @param ctx        the ApplicationContext in which we are running
     * @param actionMaps a list of ActionMaps containing actions
     *
     * @return a populated JMenuBar for the application
     */
    public static JMenuBar createApplicationMenuBar(ApplicationContext ctx,
            List<ActionMap> actionMaps) {
        logger.info("Starting createApplicationMenuBar");

        Map<String, List<ActionX>> menuMap = extractMenuActionMap(actionMaps);

        ActionContainerFactory factory = new ActionContainerFactory(ctx,
                ctx.getApplicationClass());

        JMenuBar menuBar = factory.createMenuBar(menuMap);
        logger.info("Finished createApplicationMenuBar with {0}",
                menuBar);
        return menuBar;
    }

    /**
     * Generates the JToolBars for an application based on the provided
     * ActionMaps.
     *
     * @param ctx        The ApplicationContext in which we are running
     * @param actionMaps a list of ActionMaps containing actions
     *
     * @return a list of JToolBar instances to be added to the main window
     */
    public static List<JToolBar> createApplicationToolBars(
            ApplicationContext ctx, List<ActionMap> actionMaps) {
        logger.info("Starting createApplicationToolBars");

        ActionContainerFactory factory = new ActionContainerFactory(ctx,
                ctx.getApplicationClass());
        Map<String, List<ActionX>> toolbarMap = extractToolbarActionMap(
                actionMaps);

        // Sort the toolbars by their baseName.tooblar.index resource
        List<Map.Entry<String, List<ActionX>>> sortedToolbarEntries = toolbarMap
                .entrySet().stream()
                .sorted(Comparator.comparingInt(entry -> getToolbarIndex(ctx,
                entry.getKey())))
                .collect(Collectors.toList());

        List<JToolBar> toolbars = sortedToolbarEntries.stream()
                .map(entry -> factory.createToolBar(entry.getKey(), entry
                .getValue()))
                .collect(Collectors.toList());

        logger.info("Finished createApplicationToolbars with {0}",
                toolbars);
        return toolbars;
    }

    private static int getToolbarIndex(ApplicationContext ctx,
            String toolbarBaseName) {
        String indexKey = toolbarBaseName + ".toolbar.index";
        Integer index = ctx.getResourceMap().getInteger(indexKey);
        return index != null ? index : 0;
    }

    private final ApplicationContext context;
    private final Class<?> actionsClass;
    private final ResourceMap resourceMap;

    private ActionMap actionMap;

    // Map between group id + component and the ButtonGroup
    private Map<Integer, ButtonGroup> groupMap;

    /**
     * Constructs an container factory which uses managed actions.
     *
     * @param context      the ApplicationContext in which being constructed
     * @param actionsClass the class in which the action methods are dedebugd
     */
    public ActionContainerFactory(ApplicationContext context,
            Class<?> actionsClass) {
        this.context = context;
        this.actionsClass = actionsClass;
        this.resourceMap = context.getResourceMap(actionsClass);
        this.groupMap = new HashMap<>();
    }

    /**
     * Gets the ActionMap instance. If the ActionMap has not been
     * explicitly set then the default ActionMap instance will be used.
     *
     * @return the ActionMap used by the ActionContainerFactory.
     */
    public ActionMap getActionMap() {
        if (actionMap == null) {
            actionMap = context.getActionMap(actionsClass);
        }
        return actionMap;
    }

    /**
     * Constructs a toolbar from a list of action identifiers.
     * Each element in the list can be an action id or null (for a separator).
     *
     * @param toolbarBaseName the base name for the toolbar, such as "edit" or
     *                        "file"
     * @param actions         a list of action ids for toolbar items
     *
     * @return a JToolBar populated with the specified actions
     */
    public JToolBar createToolBar(String toolbarBaseName, List<ActionX> actions) {
        logger.info("Starting createToolBar for {0}",
                toolbarBaseName);

        JToolBar toolbar = new JToolBar();
        toolbar.setName(toolbarBaseName + "ToolBar");
        toolbar.setFloatable(false);

        actions.sort(Comparator.comparingInt(ActionX::getToolBarActionIndex));

        for (ActionX actionItem : actions) {
            if (actionItem.isToolBarSeparatorBefore()) {
                toolbar.addSeparator();
            }

            AbstractButton button = createButton(actionItem, toolbar);
            if (button != null) {
                toolbar.add(button);
            }

            if (actionItem.isToolBarSeparatorAfter()) {
                toolbar.addSeparator();
            }
        }

        cleanUpSeparators(toolbar);

        logger.info("Finished createToolBar - returning: {0}",
                toolbar);
        return toolbar;
    }

    private boolean shouldHideActionText() {
        boolean hideActionText = context.getPreferences(ApplicationContext.USER)
                .getBoolean(PREF_HIDE_ACTION_TEXT, true);
        logger.info("Showing action text: {0}", hideActionText);
        return hideActionText;
    }

    /**
     * Constructs a JPopupMenu from a list representing a menu hierarchy.
     * Each element in the list can be an action id, null (for separator), or a
     * nested list (for submenus).
     *
     * @param popupBaseName the base name for the JPopupMenu for resource
     *                      injection
     * @param actions       the list of action ids or lists representing popup
     *                      menu structure
     *
     * @return a JPopupMenu populated with specific actions
     */
    public JPopupMenu createPopup(String popupBaseName, List<ActionX> actions) {
        logger.info("Starting createPopup");

        JPopupMenu popup = new JPopupMenu();
        popup.setName(popupBaseName + "Menu");

        for (ActionX actionItem : actions) {
            if (actionItem.isMenuSeparatorBefore()) {
                popup.addSeparator();
            }

            JMenuItem menuItem = createMenuElement(actionItem);

            if (menuItem != null) {
                popup.add(menuItem);
            }

            if (actionItem.isMenuSeparatorAfter()) {
                popup.addSeparator();
            }
        }

        cleanUpSeparators(popup);

        logger.info("Finished createPopup - returning: {0}",
                popup);
        return popup;
    }

    /**
     * Constructs a menu bar from a list representing menu hierarchy.
     * Each element in the list can be an action id, null (for separator), or a
     * nested list (for submenus).
     *
     * @param menuMap the list of action ids or lists representing menu
     *                structure
     *
     * @return a JMenuBar with populated menus and items
     */
    public JMenuBar createMenuBar(Map<String, List<ActionX>> menuMap) {
        logger.info("Starting createMenuBar");

        JMenuBar menuBar = new JMenuBar();
        menuBar.setName("menuBar");

        List<Map.Entry<String, List<ActionX>>> orderedEntries
                = menuMap.entrySet().stream()
                        .sorted(Comparator.comparingInt(entry -> getMenuIndex(
                        entry.getKey().split("/")[0])))
                        .collect(Collectors.toList());

        for (Map.Entry<String, List<ActionX>> entry : orderedEntries) {
            String rootName = entry.getKey().split("/")[0];

            JMenu menu = buildMenuHierarchy(menuMap, rootName);
            if (menu != null) {
                menuBar.add(menu);
            }
        }

        logger.info("Finished createMenuBar - returning: {0}",
                menuBar);
        return menuBar;
    }

    private JMenu buildMenuHierarchy(Map<String, List<ActionX>> menuMap,
            String rootName) {
        logger.info("Starting buildMenuHierarchy for root {0}",
                rootName);
        JMenu rootMenu = new JMenu();
        rootMenu.setName(context.getResourceMap().getString(
                rootName + ".menu.name"));

        // Collect all entries that start with the rootName, representing this
        //+ menu level
        List<Map.Entry<String, List<ActionX>>> sortedEntries
                = menuMap.entrySet().stream()
                        .filter(entry -> entry.getKey().startsWith(rootName))
                        .sorted(Comparator.comparingInt(
                                entry -> getMenuIndexForLevel(
                                        entry.getKey(), rootName)))
                        .collect(Collectors.toList());

        for (Map.Entry<String, List<ActionX>> entry : sortedEntries) {
            String fullPath = entry.getKey();
            List<ActionX> actions = entry.getValue();

            // Split the full path to understand its hierarchy level
            String[] levels = fullPath.split("/");
            if (levels.length > 1) {
                // We have a deeper hierarchy; create or retrieve submenus
                JMenu currentMenu = rootMenu;
                StringBuilder pathBuilder = new StringBuilder(rootName);

                // Traverse each level, creating or finding existing submenus
                for (int i = 1; i < levels.length; i++) {
                    pathBuilder.append("/").append(levels[i]);
                    String subMenuName = levels[i];
                    JMenu subMenu = findOrCreateSubMenu(currentMenu,
                            subMenuName, pathBuilder.toString());
                    currentMenu = subMenu;
                }

                // Add sorted actions to the deepest submenu
                actions.sort(Comparator
                        .comparingInt(ActionX::getMenuActionIndex));
                addActionsToMenu(currentMenu, actions);
                cleanUpSeparators(currentMenu);
            } else {
                // No further nesting; add actions directly to the root menu
                actions.sort(Comparator
                        .comparingInt(ActionX::getMenuActionIndex));
                addActionsToMenu(rootMenu, actions);
            }
        }

        cleanUpSeparators(rootMenu);
        logger.debug(String
                .format("Finished buildMenuHierarchy - returning {0}",
                        rootMenu));
        return rootMenu;
    }

    private JMenu findOrCreateSubMenu(JMenu parentMenu, String subMenuName,
            String fullPath) {
        logger.debug("Starting findOrCreateSubMenu for {0}",
                subMenuName);
        // Check if the sumenu already exists.
        for (int i = 0; i < parentMenu.getMenuComponentCount(); i++) {
            Component comp = parentMenu.getComponent(i);
            String subName = context.getResourceMap().getString(subMenuName
                    + ".menu.name");
            if (comp instanceof JMenu m && m.getName().equals(subName)) {
                return m;
            }
        }

        // Submenu doesn't exist; create it.
        JMenu subMenu = new JMenu();
        subMenu.setName(context.getResourceMap().getString(subMenuName
                + ".menu.name"));

        // Set the menu's position based on its index
        int index = getMenuIndex(subMenuName);
        DebugLogger.debug(logger, "Current Menu: {0} :: Menu Index: {1}",
                subMenuName, index);
        parentMenu.add(subMenu, Math.min(Math.max(index, 0), parentMenu
                .getMenuComponentCount()));

        logger.debug("Finished findOrCreateSubMenu for {0}",
                subMenuName);
        return subMenu;
    }

    private void addActionsToMenu(JMenu menu, List<ActionX> actions) {
        logger.info("Starting addActionsToMenu for {0}",
                menu.getName());

        for (ActionX action : actions) {
            if (action.isMenuSeparatorBefore()) {
                menu.addSeparator();
            }

            JMenuItem menuItem = createMenuElement(action);
            if (menuItem != null) {
                menu.add(menuItem);
            }

            if (action.isMenuSeparatorAfter()) {
                menu.addSeparator();
            }
        }

        logger.info("Finished addActionsToMenu for {0}",
                menu.getName());
    }

    private int getMenuIndexForLevel(String fullPath, String currentLevel) {
        String[] levels = fullPath.split("/");
        for (int i = 0; i < levels.length; i++) {
            if (levels[i].equals(currentLevel)) {
                return getMenuIndex(currentLevel);
            }
        }
        return Integer.MAX_VALUE;
    }

    private int getMenuIndex(String menuBaseName) {
        logger.info("Getting the menu index for {0}",
                menuBaseName);
        String indexKey = menuBaseName + ".menu.index";
        int index = resourceMap.getInteger(indexKey);

        logger.info("Finished getting the menu index with {0}",
                index);
        return index;
    }

    /**
     * Creates a menu or submenu from a list representing actions and
     * separators.
     *
     * @param menuBaseName the base name of the menu being created
     * @param actions      the list representing the menu and its items
     *
     * @return a JMenu with nested items
     */
    public JMenu createMenu(String menuBaseName, List<ActionX> actions) {
        logger.info("Starting createMenu");

        if (actions.isEmpty()) {
            return null;
        }

        // Split the base names to determine menu levels (i.e., view/reports)
        String[] levels = menuBaseName.split("/");
        JMenu rootMenu = new JMenu();
        rootMenu.setName(levels[0] + "Menu");

        JMenu currentMenu = rootMenu;

        // Handle nested levels, creating intermediate menus as necessary
        for (int i = 1; i < levels.length; i++) {
            JMenu subMenu = new JMenu();
            subMenu.setName(levels[i] + "Menu");
            currentMenu.add(subMenu);
            currentMenu = subMenu;
        }

        // Sort actions within this level by getMenuActionIndex
        actions.sort(Comparator.comparingInt(ActionX::getMenuActionIndex));

        for (ActionX actionItem : actions) {
            if (actionItem.isMenuSeparatorBefore()) {
                currentMenu.addSeparator();
            }

            JMenuItem menuItem = createMenuElement(actionItem);
            if (menuItem != null) {
                currentMenu.add(menuItem);
            }

            if (actionItem.isMenuSeparatorAfter()) {
                currentMenu.addSeparator();
            }
        }

        cleanUpSeparators(currentMenu);

        logger.info("Finished createMenu - returning: {0}",
                rootMenu);
        return rootMenu;
    }

    /**
     * Helper to create individual menu items or submenus based on element type.
     * Handles individual actions, separators, and nested lists (submenus).
     *
     * @param action the {@code ActionX} representing a menu item, separator, or
     *               submenu
     *
     * @return a JMenuItem or JMenu, based on the element type
     */
    private JMenuItem createMenuElement(ActionX action) {
        logger.info("Starting createMenuElement");

        JMenuItem menuItem;

        if (action.isStateAction()) {
            if (getGroupId(action) == null) {
                menuItem = new JCheckBoxMenuItem(action);
            } else {
                menuItem = createRadioButtonMenuItem(action, getGroupId(action),
                        null);
            }
        } else {
            menuItem = new JMenuItem(action);
        }

        configureMenuItem(menuItem, action);

        logger.info("Finished createMenuElement with {0}",
                menuItem);
        return menuItem;
    }

    /**
     * Convenience method to get the action from an ActionManager.
     */
    private Action getAction(Object id) {
        logger.info("Returning getAction({0})");
        return getActionMap().get(id);
    }

    /**
     * Creates a menu item based on the attributes of the action element.
     * Will return a JMenuItem, JRadioButtonMenuItem or a JCheckBoxMenuItem
     * depending on the context of the Action.
     *
     * @return a JMenuItem or subclass depending on type.
     */
    private JMenuItem createMenuItem(Object id, JComponent container) {
        logger.info("Starting createMenuItem");
        Action action = getAction(id);
        if (action == null) {
            return null;
        }

        JMenuItem menuItem;
        String groupId = getGroupId(action);

        if (isToggleAction(action)) {
            menuItem = groupId != null
                       ? createRadioButtonMenuItem(action, groupId, container)
                       : new JCheckBoxMenuItem(action);
        } else {
            menuItem = new JMenuItem(action);
        }

        configureMenuItem(menuItem, action);
        logger.info("Finished createMenuItem - returning: {0}",
                menuItem);
        return menuItem;
    }

    /**
     * Unified creation of a button, with toggle support.
     *
     * @param action    the ActionX for which a button should be created
     * @param container the parent component for ButtonGroup management
     *
     * @return configured AbstractButton based on the action type
     */
    public AbstractButton createButton(ActionX action, JComponent container) {
        logger.info("Starting createButton");

        if (action == null) {
            return null;
        }

        AbstractButton button;
        String groupId = getGroupId(action);

        if (isToggleAction(action)) {
            button = groupId != null
                     ? createToggleButton(action, groupId, container)
                     : new JToggleButton(action);
        } else {
            button = new JButton(action);
        }

        configureButton(button, action, container);

        logger.info("Finished createButton - returning: {0}",
                button);
        return button;
    }

    /**
     * This method will be called after buttons created from an action. Override
     * for custom configuration.
     *
     * @param button    the button to be configured
     * @param action    the action used to construct the menu item.
     * @param container the container for ButtonGroup management
     */
    protected void configureButton(AbstractButton button, Action action,
            JComponent container) {
        logger.info("Starting configureButton with {0}", button);
        button.setName((String) action.getValue(Action.ACTION_COMMAND_KEY)
                + "Button");
        button.setFocusable(false);
        if (action.getValue(Action.SHORT_DESCRIPTION) == null) {
            button.setToolTipText((String) action.getValue(Action.NAME));
        }
        if (action.getValue(Action.LONG_DESCRIPTION) != null) {
            button.addMouseListener(new ActionHelpProvider(context));
        }
        if (button.getIcon() != null) {
            // Hide text if icon is set, unless the user wants it shown
            button.putClientProperty("hideActionText", shouldHideActionText());
        }
        // Additional configuration for toolbars
        if (container instanceof JToolBar) {
            button.setMargin(TOOLBAR_BUTTON_MARGIN);
            button.setBorderPainted(false);
        }
        logger.info("Finished createButton with {0}", button);
    }

    /**
     * Configures a JMenuItem with tooltip and other properties from action.
     *
     * @param menuItem the menu item to configure
     * @param action   the associated action
     */
    protected void configureMenuItem(JMenuItem menuItem,
            Action action) {
        logger.info("Starting configureMenuItem with {0}",
                menuItem);
        menuItem.setName(action.getValue(Action.ACTION_COMMAND_KEY)
                + "MenuItem");
        if (action.getValue(Action.SHORT_DESCRIPTION) == null) {
            menuItem.setToolTipText(((String) action.getValue(Action.NAME)));
        }
        if (action.getValue(Action.LONG_DESCRIPTION) != null) {
            menuItem.addMouseListener(new ActionHelpProvider(context));
        }
        logger.info("Finished createMenuBar with {0}",
                menuItem);
        if (action instanceof ActionX ax) {
            ax.getResourceMap().injectComponent(menuItem);
        }
    }

    /**
     * Helper method to create a toggle button with ButtonGroup support.
     */
    private JToggleButton createToggleButton(Action action, String groupId,
            JComponent container) {
        logger.info("Starting createToggleButton");
        JToggleButton toggleButton = new JToggleButton(action);
        ButtonGroup group = getButtonGroup(groupId, container);
        group.add(toggleButton);
        logger.info(String
                .format("Finished createToggleButton - returning: {0}",
                        toggleButton));
        return toggleButton;
    }

    /**
     * Helper method to add a radio button menu item.
     */
    private JRadioButtonMenuItem createRadioButtonMenuItem(Action action,
            String groupId, JComponent container) {
        logger.info("Starting createRadioButtonMenuItem");
        JRadioButtonMenuItem radioItem = new JRadioButtonMenuItem(action);
        ButtonGroup group = getButtonGroup(groupId, container);
        group.add(radioItem);
        logger.info("Finished createRadioButtonMenuItem - "
                + "returning: {0}", radioItem);
        return radioItem;
    }

    /**
     * Gets or creates a ButtonGroup based on a groupId and container.
     */
    private ButtonGroup getButtonGroup(String groupId, JComponent container) {
        logger.info("Starting getButtonGroup");
        int hashCode = groupId.hashCode() ^ (container != null
                                             ? container.hashCode()
                                             : 0);
        ButtonGroup buttonGroup = groupMap.computeIfAbsent(hashCode, k
                -> new ButtonGroup());
        logger.info("Finished getButtonGroup - returning: {0}",
                buttonGroup);
        return buttonGroup;
    }

    /**
     * Recursively creates or retrieves a JMenu based on menu levels, supporting
     * nested structures. Uses ResourceMap to set the correct component name.
     *
     * @param levels     the array of menu levels (i.e., ["view", "logs"])
     * @param levelIndex the current level index in the recursion
     * @param action     the action for the final menu item
     *
     * @return the generated or retrieved JMenu
     */
    private JMenu getOrCreateMenu(String[] levels, int levelIndex,
            ActionX action) {
        logger.info("Starting getOrCreateMenu with {0}",
                Arrays.toString(levels));
        String baseName = levels[levelIndex];
        String resourceName = resourceMap.getString(baseName + ".menu.name");

        JMenu menu = new JMenu();
        menu.setName(resourceName); // Ensures consistent property injection

        // Set properties for the final level in the hierarchy
        if (levelIndex == levels.length - 1) {
            JMenuItem menuItem = new JMenuItem(action);
            menuItem.setName(action.getName() + "MenuItem");
            menu.add(menuItem);
        } else {
            // Recurse to create or retrieve submenus at the next level
            JMenu subMenu = getOrCreateMenu(levels, levelIndex + 1, action);
            menu.add(subMenu);
        }

        logger.info("Finished getOrCreateMenu with {0}", menu);
        return menu;
    }

    /**
     * Determines if an action is a toggle action.
     */
    private boolean isToggleAction(Action action) {
        logger.info(String
                .format("Determining if action is a toggle action: {0}",
                        action));
        return action instanceof ActionX && ((ActionX) action).isStateAction();
    }

    /**
     * Retrieves the group ID for an action, if available.
     */
    private String getGroupId(Action action) {
        logger.info("Getting groupId for action {0}", action);
        return (action instanceof ActionX)
               ? (String) ((ActionX) action).getGroup()
               : null;
    }

    /**
     * Extracts a structured list of actions for menu generation from
     * ActionMaps. Groups actions by their intended menu base name and organizes
     * by menuActionIndex.
     *
     * @param a list of ActionMaps containing actions
     *
     * @return a structured list of menu actions and submenus
     */
    private static Map<String, List<ActionX>> extractMenuActionMap(
            List<ActionMap> actionMaps) {
        logger.info("Starting extractMenuActionList");
        Map<String, List<ActionX>> menuStructure = new HashMap<>();
        Set<String> uniqueActions = new HashSet<>(); // Track unique action names

        actionMaps.stream()
                .forEach(map -> {
                    // Process each key in the ActionMap
                    Stream<Object> keyStream = map.keys() != null
                                               ? Arrays.stream(map.keys())
                                               : Arrays.stream(map.allKeys());

                    keyStream
                            .filter(Objects::nonNull) // Ignore nulls
                            .map(map::get) // Retrieve action associated with keys
                            .filter(ActionX.class::isInstance) // ONLY ActionX
                            .map(ActionX.class::cast)
                            .filter(action -> uniqueActions.add(
                            (String) action.getValue(Action.NAME)))
                            .forEach(action -> {
                                String menuBaseName = action.getMenuBaseName();

                                // Group actions by menu base name
                                menuStructure.computeIfAbsent(menuBaseName,
                                        k -> new ArrayList<>()).add(action);
                            });
                });

        logger.info("Finished extractMenuActionMap with {0}",
                menuStructure);
        return menuStructure;
    }

    /**
     * Extracts a structured list of actions for toolbar generation from
     * ActionMaps. Organizes actions by toolbarActionIndex to maintain intended
     * order on the toolbar.
     *
     * @param actionMaps a list of ActionMaps containing actions
     *
     * @return a structured list of toolbar actions
     */
    private static Map<String, List<ActionX>> extractToolbarActionMap(
            List<ActionMap> actionMaps) {
        logger.info("Starting extractToolbarActionMap");
        Map<String, List<ActionX>> toolbarActionMap = new HashMap<>();

        actionMaps.stream()
                .flatMap(map -> {
                    Stream<Object> keyStream = map.keys() != null
                                               ? Arrays.stream(map.keys())
                                               : Arrays.stream(map.allKeys());
                    return keyStream.map(key -> map.get(key));
                })
                .filter(ActionX.class::isInstance)
                .map(ActionX.class::cast)
                .filter(action -> action.getToolbarName() != null)
                .forEach(action -> {
                    String toolbarBaseName = action.getToolbarName();
                    toolbarActionMap
                            .computeIfAbsent(toolbarBaseName,
                                    k -> new ArrayList<>())
                            .add(action);
                });

        toolbarActionMap.values().forEach(actions
                -> actions.sort(Comparator.comparingInt(
                        ActionX::getToolBarActionIndex)));
        logger.info("Finished extractToolbarActionList with {0}",
                toolbarActionMap.keySet());
        return toolbarActionMap;
    }

    /**
     * Helper method to remove leading and trailing separators from a list. This
     * ensures that {@code JMenu}s do not start nor end with a separator.
     * This method also makes sure that two separators never reside directly
     * together within the {@code JMenu}s.
     *
     * @param menu the JMenu to cleanup
     */
    private void cleanUpSeparators(JMenu menu) {
        logger.info("Starting cleanUpSeparators for JMenu");

        if (menu.getMenuComponentCount() == 0) {
            DebugLogger.debug(logger, String
                    .format("Menu \"{0}\" has no items.",
                            menu.getName()));
            return;
        }

        if (menu.getMenuComponent(0) instanceof JSeparator) {
            menu.remove(0);
        }
        int last = menu.getMenuComponentCount() - 1;
        if (menu.getMenuComponent(last) instanceof JSeparator) {
            menu.remove(last);
        }

        for (int i = 0; i < menu.getMenuComponentCount(); i++) {
            if (i > 0) {
                if (menu.getMenuComponent(i) instanceof JSeparator
                        && menu.getMenuComponent(i - 1) instanceof JSeparator) {
                    menu.remove(i);
                    i--; // Adjust index after removal
                }
            }
        }

        logger.info("Finished cleanUpSeparators for JMenu");
    }

    /**
     * Helper method to remove leading and trailing separators from a list. This
     * ensures that {@code JPopupMenu}s do not start nor end with a separator.
     * This method also makes sure that two separators never reside directly
     * together within the {@code JPopupMenu}s.
     *
     * @param popup the JPopupMenu to clean up
     */
    private void cleanUpSeparators(JPopupMenu popup) {
        logger.info("Starting cleanUpSeparators for JPopupMenu");

        if (popup.getComponentCount() > 0) {
            if (popup.getComponent(0) instanceof JPopupMenu.Separator) {
                popup.remove(0);
            }
            int last = popup.getComponentCount() - 1;
            if (popup.getComponent(last) instanceof JPopupMenu.Separator) {
                popup.remove(last);
            }

            for (int i = 0; i < popup.getComponentCount(); i++) {
                if (i > 0) {
                    if (popup.getComponent(i) instanceof JPopupMenu.Separator
                            && popup.getComponent(i - 1) instanceof JPopupMenu.Separator) {
                        popup.remove(i);
                        i--;  // Adjust index after removal
                    }
                }
            }
        }

        logger.info("Finished cleanUpSeparators for JPopupMenu");
    }

    /**
     * Helper method to remove leading and trailing separators from a list. This
     * ensures that {@code JToolBar}s do not start nor end with a separator.
     * This method also makes sure that two separators never reside directly
     * together within the {@code JToolBar}s.
     *
     * @param toolbar the JToolBar to clean up
     */
    private void cleanUpSeparators(JToolBar toolbar) {
        logger.info("Starting cleanUpSeparators for JToolBar");

        if (toolbar.getComponentCount() > 0) {
            if (toolbar.getComponent(0) instanceof JSeparator) {
                toolbar.remove(0);
            }
            int last = toolbar.getComponentCount() - 1;
            if (toolbar.getComponent(last) instanceof JSeparator) {
                toolbar.remove(last);
            }

            for (int i = 0; i < toolbar.getComponentCount(); i++) {
                if (i > 0) {
                    if (toolbar.getComponent(i) instanceof JSeparator
                            && toolbar.getComponent(i - 1) instanceof JSeparator) {
                        toolbar.remove(i);
                        i--; // Adjust index after removal
                    }
                }
            }
        }

        logger.info("Finished cleanUpSeparators for JToolBar");
    }

}
