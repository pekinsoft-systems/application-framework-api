/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   NoTargetHandlerException.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.framework;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class NoTargetHandlerException extends RuntimeException {

    /**
     * Constructs an instance of <code>NoTargetHandlerException</code> with the
     * specified detail message.
     *
     * @param actionsClass the class containing a method decorated with
     *                     {@link AppAction} and has its
     *                     {@link AppAction#isTargetable()} parameter set to
     *                     {@code true}, but does not have a static method named
     *                     {@code getTargetable}
     */
    public NoTargetHandlerException(Class<?> actionsClass) {
        super(String.format(
                "The class %1$s contains a TargetableAction AppAction "
                + "annotated method, but does not have a static getTargetable "
                + "method available.", actionsClass));
    }
}
