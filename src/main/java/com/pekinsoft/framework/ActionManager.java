/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   ActionManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 13, 2024
 *  Modified   :   Jul 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.framework;

import com.pekinsoft.logging.Logger;
import java.awt.KeyboardFocusManager;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.lang.ref.WeakReference;
import java.util.*;
import javax.swing.*;

/**
 * The application's {@code ActionManager} provides <em>read-only</em>, cached
 * access to {@link ActionMapX action maps} that contain one entry for each
 * method decorated with the {@code AppAction} annotation in a class.
 *
 * @see ActionMapX
 * @see ActionX
 * @see AppAction
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class ActionManager extends AbstractBean {

    private static ActionManager instance = null;

    public static ActionManager getInstance() {
        if (instance == null) {
            throw new IllegalStateException("The ActionManager has not been "
                    + "configured by the application yet.");
        }
        return instance;
    }

    public static void configureActionManager(Application application) {
        instance = new ActionManager(application.getContext());
        instance.application = application;
        instance.appRM = application.getContext().getResourceMap();
        instance.globalActionMap = instance.createActionMapChain(
                instance.context.getApplicationClass(),
                Application.class, instance.application, instance.appRM);
    }

    protected ActionManager(ApplicationContext context) {
        if (context == null) {
            throw new IllegalArgumentException("null context");
        }
        this.context = context;
        this.application = context.getApplication();
        logger = context.getLogger(getClass());
        actionMaps = new WeakHashMap<>();
    }

    /**
     * The {@code ActionMapX} chain for the entire application.
     * <p>
     * Returns an{@code ActionMapX} with the {@link AppAction AppActions}
     * defined in the application's class. The remainder of the chain contains
     * one {@code ActionMapX} for each superclass, if any. Logically, this
     * {@code ActionMapX} returned by this method contains all of the
     * application-global actions.
     * <p>
     * The value returned by this method is cached.
     *
     * @return the {@code ActionMapX} containing the application-global actions
     *
     * @see #getActionMap(Class, Object)
     * @see ActionMapX
     * @see AppAction
     */
    public ActionMapX getActionMap() {
        if (globalActionMap == null) {
            globalActionMap = createActionMapChain(context.getApplicationClass(),
                    Application.class, context.getApplication(), appRM);
        }

        return globalActionMap;
    }

    /**
     * Returns the {@link ActionMapX} chain for the specified actions class and
     * target object.
     * <p>
     * The specified class can contain methods decorated with {@link AppAction}
     * annotations. Each one will be turned into an {@link ActionX} object and
     * all of them will be added to a single {@code ActionMapX}. All of the
     * {@code ActionX}es invoke their {@code actionPerformed} method on the
     * specified {@code actionsObject}. The parent of the return
     * {@code ActionMapX} is the global {@code ActionMapX} that contains the
     * {@code @AppAction}s defined in the application's class, and its
     * superclasses, if any.
     * <p>
     * To bind an {@code @AppAction} to a Swing component, one specifies the
     * {@code @AppAction}'s name in an expression like this:{@snippet lang="java":
     * MyActions myActions = new MyActions();
     * myComponent.setAction(application.getActionMap(myActions).get("myAction"));
     * // WHERE "application" is an instance of the application object.
     * }
     * <p>
     * The value returned by this method is cached. The lifetime of the cached
     * entry will be the same as the lifetime of the {@code actionsObject} and
     * the {@code ActionMapX}, and any {@code ActionX}es that refer to it. In
     * other words, if all references to the {@code actionsObject} are dropped,
     * including its {@code ActionX}es and their {@code ActionMapX}es, then the
     * cached {@code ActionMapX} entry will be cleared.
     *
     * @param actionsClass  the class containing {@code @AppAction} decorated
     *                      methods
     * @param actionsObject an instance of the {@code actionsClass}
     *
     * @return the {@code ActionMapX} chain for {@code actionsClass} and
     *         {@code actionsObject}
     *
     * @see #getActionMap()
     * @see ActionMapX
     * @see AppAction
     */
    public ActionMapX getActionMap(Class actionsClass, Object actionsObject) {
        Objects.requireNonNull(actionsClass, "The actionsClass cannot be null.");
        Objects.requireNonNull(actionsObject,
                "The actionsObject cannot be null.");
        if (!actionsClass.isAssignableFrom(actionsObject.getClass())) {
            throw new IllegalArgumentException(
                    "actionsObject is not an instance "
                    + "of actionsClass.");
        }
        synchronized (actionMaps) {
            WeakReference<ActionMapX> ref = actionMaps.get(actionsObject);
            ActionMapX classActionMap = (ref != null) ? ref.get() : null;
            if ((classActionMap == null)
                    || (classActionMap.getActionsClass() != actionsClass)) {
                Class actionsObjectClass = actionsObject.getClass();
                ResourceMap rm = Application.getInstance().getContext()
                        .getResourceMap(actionsObjectClass,
                                actionsClass);
                classActionMap = createActionMapChain(actionsObjectClass,
                        actionsClass, actionsObject, rm);
                ActionMap lastActionMap = classActionMap;
                while (lastActionMap.getParent() != null) {
                    lastActionMap = lastActionMap.getParent();
                }

                lastActionMap.setParent(getActionMap());
                actionMaps.put(actionsObject, new WeakReference(classActionMap));
            }

            return classActionMap;
        }
    }

    private void initProxyActionSupport() {
        KeyboardFocusManager kfm = KeyboardFocusManager.
                getCurrentKeyboardFocusManager();
        kfm.addPropertyChangeListener(new KeyboardFocusPCL());
    }

    private ActionMapX createActionMapChain(Class start, Class stop,
            Object actionsObject, ResourceMap resourceMap) {
        // All of the classes from start to stop, inclusive.
        List<Class> classes = new ArrayList<>();
        Class current = start;
        while (!current.equals(stop.getSuperclass())) {
            classes.add(current);
            current = current.getSuperclass();
        }

        Collections.reverse(classes);

        // Create the ActionMapX chain, one per class.
        ActionMapX parent = null;
        for (Class cls : classes) {
            ActionMapX appAM = new ActionMapX(context, cls, actionsObject,
                    context.getResourceMap(cls));
            appAM.setParent(parent);
            parent = appAM;
        }

        return parent;
    }

    /*
     * For each proxyAction in each ActionMapX, if the newFocusOwner's ActionMap
     * includes an Action with the same name, then bind the proxyAction to it.
     * Otherwise, set the proxyAction's proxyBinding to null.
     */
    private void updateAllProxyActions(JComponent oldFocusOwner,
            JComponent newFocusOwner) {
        if (newFocusOwner != null) {
            ActionMap ownerActionMap = newFocusOwner.getActionMap();
            if (ownerActionMap != null) {
                updateProxyActions(getActionMap(), ownerActionMap, newFocusOwner);
                for (WeakReference<ActionMapX> appAMRef : actionMaps.values()) {
                    ActionMapX appAM = appAMRef.get();
                    if (appAM == null) {
                        continue;
                    }
                    updateProxyActions(appAM, ownerActionMap, newFocusOwner);
                }
            }
        }
    }

    private void updateProxyActions(ActionMapX appAM, ActionMap ownerAM,
            JComponent focusOwner) {
        for (ActionX proxyAction : appAM.getProxyActions()) {
            String proxyActionName = proxyAction.getName();
            Action proxy = ownerAM.get(proxyActionName);
            if (proxy != null) {
                proxyAction.setProxy(proxy);
                proxyAction.setProxySource(focusOwner);
            } else {
                proxyAction.setProxy(null);
                proxyAction.setProxySource(null);
            }
        }
    }

    private final Logger logger;

    private final WeakHashMap<Object, WeakReference<ActionMapX>> actionMaps;

    private Application application = null;
    private ApplicationContext context = null;
    private ResourceMap appRM = null;
    private ActionMapX globalActionMap = null;

    private final class KeyboardFocusPCL implements PropertyChangeListener {

        private final TextActions textActions;

        KeyboardFocusPCL() {
            textActions = new TextActions(Application.getInstance().getContext());
        }

        @Override
        public void propertyChange(PropertyChangeEvent pce) {
            if (pce.getPropertyName().equals("permanentFocusOwner")) {
                FocusManager focusManager = ServiceLoader.load(
                        FocusManager.class)
                        .findFirst().get();
                JComponent oldOwner = context.getFocusOwner();
                Object newValue = pce.getNewValue();
                JComponent newOwner = (newValue instanceof JComponent jc)
                                      ? jc
                                      : null;
                textActions.updateFocusOwner(oldOwner, newOwner);

                context.setFocusOwner(newOwner);

                updateAllProxyActions(oldOwner, newOwner);
            }
        }

    }

}
