/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   TaskListener.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.framework;

import java.util.List;

/**
 * Listener used for observing `BackgroundTask` execution. A `TaskListener` is
 * particularly useful for monitoring the the intermediate results
 * {@link BackgroundTask#publish published} by a BackgroundTask in situations where it's not
 * practical to override the BackgroundTask's {@link BackgroundTask#process process} method. Note
 * that if what you really want to do is monitor a BackgroundTask's state and progress, a
 * PropertyChangeListener is probably more appropriate.
 * 
 * The BackgroundTask class runs all TaskListener methods on the event dispatching thread
 * and the source of all TaskEvents is the BackgroundTask object.
 *
 * @see BackgroundTask#addTaskListener
 * @see BackgroundTask#removeTaskListener
 * @see BackgroundTask#addPropertyChangeListener
 *
 * @author Hans Muller (Hans.Muller@Sun.COM)
 */
public interface TaskListener<T, V> {

    /**
     * Called just before the BackgroundTask's {@link BackgroundTask#doInBackground
     * doInBackground} method is called, i.e. just before the task begins
     * running. The {@code event's} source is the BackgroundTask and its value is {@code null}.
     *
     * @param event a TaskEvent whose source is the `BackgroundTask` object, value
     * is {@code null}
     * @see BackgroundTask#doInBackground
     * @see TaskEvent#getSource
     */
    void doInBackground(TaskEvent<Void> event);

    /**
     * Called each time the BackgroundTask's {@link BackgroundTask#process process} method is
     * called. The value of the event is the list of values passed to the
     * process method.
     *
     * @param event a TaskEvent whose source is the `BackgroundTask` object and
     * whose value is a list of the values passed to the {@code BackgroundTask.process()}
     * method
     * @see BackgroundTask#doInBackground
     * @see BackgroundTask#process
     * @see TaskEvent#getSource
     * @see TaskEvent#getValue
     */
    void process(TaskEvent<List<V>> event);

    /**
     * Called after the BackgroundTask's {@link BackgroundTask#succeeded succeeded} completion
     * method is called. The event's value is the value returned by the BackgroundTask's
     * `get` method, i.e. the value that is computed by
     * {@link BackgroundTask#doInBackground}.
     *
     * @param event a TaskEvent whose source is the `BackgroundTask` object, and
     * whose value is the value returned by {@code BackgroundTask.get()}.
     * @see BackgroundTask#succeeded
     * @see TaskEvent#getSource
     * @see TaskEvent#getValue
     */
    void succeeded(TaskEvent<T> event);

    /**
     * Called after the BackgroundTask's {@link BackgroundTask#failed failed} completion method is
     * called. The event's value is the Throwable passed to
     * {@code BackgroundTask.failed()}.
     *
     * @param event a TaskEvent whose source is the `BackgroundTask` object, and
     * whose value is the Throwable passed to {@code BackgroundTask.failed()}.
     * @see BackgroundTask#failed
     * @see TaskEvent#getSource
     * @see TaskEvent#getValue
     */
    void failed(TaskEvent<Throwable> event);

    /**
     * Called after the BackgroundTask's {@link BackgroundTask#cancelled cancelled} method is
     * called. The {@code event's} source is the BackgroundTask and its value is {@code null}.
     *
     * @param event a TaskEvent whose source is the `BackgroundTask` object, value
     * is {@code null}
     * @see BackgroundTask#cancelled
     * @see BackgroundTask#get
     * @see TaskEvent#getSource
     */
    void cancelled(TaskEvent<Void> event);

    /**
     * Called after the BackgroundTask's {@link BackgroundTask#interrupted interrupted} method is
     * called. The {@code event's} source is the BackgroundTask and its value is the
     * InterruptedException passed to {@code BackgroundTask.interrupted()}.
     *
     * @param event a TaskEvent whose source is the `BackgroundTask` object, and
     * whose value is the InterruptedException passed to
     * {@code BackgroundTask.interrupted()}.
     * @see BackgroundTask#interrupted
     * @see TaskEvent#getSource
     * @see TaskEvent#getValue
     */
    void interrupted(TaskEvent<InterruptedException> event);

    /**
     * Called after the BackgroundTask's {@link BackgroundTask#finished finished} method is called.
     * The {@code event's} source is the BackgroundTask and its value is {@code null}.
     *
     * @param event a TaskEvent whose source is the `BackgroundTask` object, value
     * is {@code null}.
     * @see BackgroundTask#interrupted
     * @see TaskEvent#getSource
     */
    void finished(TaskEvent<Void> event);

    /**
     * Convenience class that stubs all of the TaskListener interface methods.
     * Using TaskListener.Adapter can simplify building TaskListeners.
     */
    class Adapter<T, V> implements TaskListener<T, V> {

        @Override
        public void doInBackground(TaskEvent<Void> event) {
        }

        @Override
        public void process(TaskEvent<List<V>> event) {
        }

        @Override
        public void succeeded(TaskEvent<T> event) {
        }

        @Override
        public void failed(TaskEvent<Throwable> event) {
        }

        @Override
        public void cancelled(TaskEvent<Void> event) {
        }

        @Override
        public void interrupted(TaskEvent<InterruptedException> event) {
        }

        @Override
        public void finished(TaskEvent<Void> event) {
        }
    }
}
