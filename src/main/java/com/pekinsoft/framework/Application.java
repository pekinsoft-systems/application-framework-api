
/*
 * Copyright (C) 2006 Sun Microsystems, Inc. All rights reserved. Use is
 * subject to license terms.
 */
package com.pekinsoft.framework;

import com.jtattoo.plaf.acryl.AcrylLookAndFeel;
import com.jtattoo.plaf.aero.AeroLookAndFeel;
import com.jtattoo.plaf.aluminium.AluminiumLookAndFeel;
import com.jtattoo.plaf.bernstein.BernsteinLookAndFeel;
import com.jtattoo.plaf.fast.FastLookAndFeel;
import com.jtattoo.plaf.graphite.GraphiteLookAndFeel;
import com.jtattoo.plaf.hifi.HiFiLookAndFeel;
import com.jtattoo.plaf.luna.LunaLookAndFeel;
import com.jtattoo.plaf.mcwin.McWinLookAndFeel;
import com.jtattoo.plaf.mint.MintLookAndFeel;
import com.jtattoo.plaf.noire.NoireLookAndFeel;
import com.jtattoo.plaf.smart.SmartLookAndFeel;
import com.pekinsoft.api.*;
import com.pekinsoft.desktop.SplashScreen;
import com.pekinsoft.desktop.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.lookup.Lookup;
import com.pekinsoft.security.LogonManager;
import com.pekinsoft.utils.*;
import java.awt.*;
import java.awt.event.*;
import java.beans.Beans;
import java.io.IOException;
import java.lang.reflect.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.prefs.Preferences;
import javax.swing.*;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

/**
 * The base class for Swing applications.
 * <p>
 * <p>
 * This class defines a simple lifecyle for Swing applications:
 * {@code initialize}, {@code startup}, {@code ready}, and {@code shutdown}. The
 * {@code Application}'s {@code startup} method is responsible for creating the
 * initial GUI and making it visible, and the {@code shutdown} method for hiding
 * the GUI and performing any other cleanup actions before the application
 * exits. The {@code initialize} method can be used configure system properties
 * that must be set before the GUI is constructed and the {@code ready} method
 * is for applications that want to do a little bit of extra work once the GUI
 * is {@code ready} to use. Concrete subclasses must override the
 * {@code startup} method.
 * <p>
 * Applications are started with the static {@code launch} method. Applications
 * use the {@code ApplicationContext} {@link Application#getContext singleton}
 * to find resources, actions, local storage, and so on.
 * <p>
 * All {@code Application} subclasses must override {@code startup} and they
 * should call {@link #exit} (which calls {@code shutdown}) to exit. Here's an
 * example of a complete "Hello World" Application:{@snippet lang="java":
 * public class MyApplication extends Application {
 *     JFrame mainFrame = null;
 *
 *     @Override
 *     protected void startup() {
 *         mainFrame = new JFrame("Hello World");
 *         mainFrame.add(new JLabel("Hello World"));
 *         mainFrame.addWindowListener(new MainFrameListener());
 *         mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
 *         mainFrame.pack();
 *         mainFrame.setVisible(true);
 *     }
 *
 *     @Override
 *     protected void shutdown() {
 *         mainFrame.setVisible(false);
 *     }
 *
 *     private class MainFrameListener extends WindowAdapter {
 *         public void windowClosing(WindowEvent e) {
 *             exit();
 *         }
 *     }
 *
 *     public static void main(String[]  args) {
 *         Application.launch(MyApplication.class, args);
 * }
 * }
 * }
 * <p>
 * The {@code mainFrame}'s {@code defaultCloseOperation} is set to
 * {@code DO_NOTHING_ON_CLOSE} because we're handling attempts to close the
 * window by calling {@code ApplicationContext} {@link #exit}.
 * <p>
 * <p>
 * <blockquote><strong>NOTE</strong>:<br><br>All of the Application's methods
 * are called (<em><strong>must</strong> be called</em>) on the EDT.
 * </blockquote>
 * <p>
 * All but the most trivial applications should define a ResourceBundle in the
 * resources subpackage with the same name as the application class (like
 * `resources/MyApplication.properties`). This ResourceBundle contains resources
 * shared by the entire application and should begin with the following the
 * standard Application resources:
 * <pre>
 * Application.name = A short name, typically just a few words
 * Application.id = Suitable for Application specific identifiers, like file
 * names
 * Application.title = A title suitable for dialogs and frames
 * Application.version = A version string that can be incorporated into messages
 * Application.vendor = A proper name, like Sun Microsystems, Inc.
 * Application.vendorId = suitable for Application-vendor specific identifiers,
 * like file names.
 * Application.homepage = A URL like http://www.javadesktop.org
 * Application.description = One brief sentence
 * Application.lookAndFeel = either system, default, or a LookAndFeel class name
 * </pre>
 * <p>
 * The {@code Application.lookAndFeel} resource is used to initialize the
 * {@code UIManager lookAndFeel} as follows:
 * <p>
 * - {@code system} - the system (native) look and feel: \image html
 * lookAndFeelEx-System.png "Running on Linux with a GTK desktop" width=35%
 * height=35% - {@code default} - use the JVM default, typically the cross
 * platform look and feel: \image html lookAndFeelEx-Default.png "The
 * Application API uses Nimbus for cross-platform" width=35% height=35% - a
 * LookAndFeel class name - use the specified class: \image html
 * lookAndFeelEx-SpecifiedClassName.png "CDE/Motif class name specified"
 * width=35% height=35%
 *
 *
 * @see ApplicationContext
 * @see UIManager#setLookAndFeel
 * @author Hans Muller (Hans.Muller@Sun.COM) &mdash; Original Author
 * @author Sean Carrick (sean at pekinsoft dot com) &mdash; Updating and
 * Reconfiguring Author
 * @version 2.4
 */
@ProxyActions(values = {"cut", "copy", "paste", "delete"})

public abstract class Application extends AbstractBean
        implements PreferenceKeys, PropertyKeys {

    private static Logger LOGGER = null;

    private static boolean loading = true;

    protected List<ActionMap> actionMaps;
    protected Properties lafProps;

    private static Application application = null;
    private final ApplicationContext context;
    private final List<ExitListener> exitListeners;
    private final List<PrintCookie> printCookies;
    private final List<SaveCookie> saveCookies;
    private List<Logger> loggers;
    private JFrame mainFrame;

    // This flag will be set once the ready() method is reached. This will be
    //+ able to be used by modules that are restoring windows which were visible
    //+ when the application last shut down.
    private boolean ready = false;

    /**
     * Not to be called directly, see {@link #launch launch}.
     * <p>
     * Subclasses can provide a no-args constructor to initialize private final
     * state however GUI initialization, and anything else that might refer to
     * public API, should be done in the {@link #startup startup} method.
     */
    protected Application() {
        context = new ApplicationContext();
        exitListeners = new CopyOnWriteArrayList<>();
        printCookies = new CopyOnWriteArrayList<>();
        saveCookies = new CopyOnWriteArrayList<>();
        lafProps = new Properties();
    }

    /**
     * If the {@code Application} is using a database server, and is starting
     * that server (as well as stopping it) within the confines of the
     * {@code Application}, then the {@code serverStarted} flag can be set upon
     * successfully starting the server. This flag can be checked in the {@link
     * #shutdown() shutdown} method prior to attempting to stop the server, in
     * order to be sure that it was initially started. By having this flag, the
     * {@code shutdown} method should not throw any errors if the server was
     * never started in the first place.
     *
     * @param serverStarted whether ({@code true}) or not ({@code false}) the
     *                      server has been successfully started, to notify
     *                      {@code shutdown} that it is safe to attempt to stop
     *                      the server
     */
    protected void setServerStarted(boolean serverStarted) {
        context.setServerStarted(serverStarted);
    }

    /**
     * Creates an instance of the specified {@code Application} subclass, sets
     * the {@code ApplicationContext} {@code Application} property, and then
     * calls the new {@code Application}'s {@code startup} method.The
     * {@code launch}method is typically called from the Application's
     * {@code main}
     * <pre>
     *     public static void main(String[] args) {
     *         Application.launch(MyApplication.class, args);
     *     }
     * </pre>
     * <p>
     * The {@code launch} method accesses the
     * {@link Preferences#systemNodeForPackage(Class) Preferences system node}
     * for the {@code applicationClass} to initialize certain preferences based
     * upon these recognized command line arguments:
     * <dl>
     * <dt><strong>{@code --all} | {@code -a}</strong></dt>
     * <dd>Logging Level: {@link Level#ALL }</dd>
     * <dt><strong>{@code --trace} | {@code -t}</strong></dt>
     * <dd>Logging Level: {@link Level#TRACE }</dd>
     * <dt><strong>{@code --debug} | {@code -d}</strong></dt>
     * <dd>Logging Level: {@link Level#DEBUG }</dd>
     * <dt><strong>{@code --config} | {@code -c}</strong></dt>
     * <dd>Logging Level: {@link Level#CONFIG }</dd>
     * <dt><strong>{@code --ide} | {@code --devel}</strong></dt>
     * <dd>Advises that project is currently running in development mode, which
     * means that when {@link ApplicationContext#getPreferences(int) } is
     * called, it will always return the user node, regardless of whether
     * {@code ApplicationContext.SYSTEM} or {@code ApplicationContext.USER} is
     * specified.<br><br>This is to prevent
     * {@link java.util.prefs.BackingStoreException BackingStoreExceptions} from
     * being thrown due to developer not having write access to the Java
     * Preferences API system-wide location.<br><br>In a production environment,
     * the calls for {@code ApplicationContext.SYSTEM} preferences will return
     * the system node of the preferences tree.</dd>
     * </dl>
     * <p>
     * The {@code applicationClass} constructor and {@code startup}
     * methods run on the event dispatching thread.
     *
     * @param <T>              the {@code Application} subclass type to be
     *                         launched
     * @param applicationClass the {@code Application} class to launch
     * @param args             {@code main} method arguments
     *
     * @see #shutdown
     * @see ApplicationContext#getApplication
     */
    public static synchronized <T extends Application> void launch(
            final Class<T> applicationClass, final String[] args) {
        ArgumentParser parser = new ArgumentParser(args);
        boolean inIde = parser.isSwitchPresent("--ide")
                || parser.isSwitchPresent("--devel");
        Preferences prefs;
        if (inIde) {
            prefs = Preferences.userNodeForPackage(applicationClass);
        } else {
            prefs = Preferences.systemNodeForPackage(applicationClass);
        }

        if (parser.isSwitchPresent("--all") || parser.isSwitchPresent("-a")) {
            prefs.put(PREF_LOG_LEVEL, "ALL");
        } else if (parser.isSwitchPresent("--trace")
                || parser.isSwitchPresent("-t")) {
            prefs.put(PREF_LOG_LEVEL, "TRACE");
        } else if (parser.isSwitchPresent("--debug")
                || parser.isSwitchPresent("-d")) {
            prefs.put(PREF_LOG_LEVEL, "DEBUG");
        } else if (parser.isSwitchPresent("--config")
                || parser.isSwitchPresent("-c")) {
            prefs.put(PREF_LOG_LEVEL, "CONFIG");
        }

        prefs.putBoolean(PREF_DEVELOPMENT_ENVIRONMENT, inIde);
        if (inIde) {
            prefs.put(PREF_LOG_LEVEL, "ALL");
        }

        Runnable doCreateAndShowGUI = () -> {
            try {
                application = create(applicationClass);
                loading = false;
                if (splashScreen != null) {
                    splashScreen.updateStep(2, "startup.message.2");
                    splashScreen.sleep(1000);
                }
                application.initSecurityModel();
                if (splashScreen != null) {
                    splashScreen.updateStep(3, "startup.message.3");
                    splashScreen.sleep(500);
                }
                application.initialize(args);
                application.setLookAndFeel();
                if (splashScreen != null) {
                    splashScreen.updateStep(4, "startup.message.4");
                    splashScreen.sleep(750);
                }
                application.startup();
                application.waitForReady();
            } catch (Exception e) {
                String msg = String.format("Application %s failed to launch",
                        applicationClass);
                System.err.println("[" + Level.ERROR.getName() + "] " + msg + e);
                throw new Error(msg, e);
            }
        };
        SwingUtilities.invokeLater(doCreateAndShowGUI);
    }

    private static SplashScreen splashScreen = null;

    /**
     * Provides a manner of launching the {@code Application} with a splash
     * screen.
     * <p>
     * When using this version of {@code launch}, you need to define the startup
     * messages in the application resource bundle using the keys
     * &ldquo;{@code startup.message.0}&rdquo;,
     * &ldquo;{@code startup.message.1}&rdquo;, etc.
     * <p>
     * The {@code launch} method uses {@code startup.message.0} through
     * {@code startup.message.4}, which are defined in the superclass' resource
     * bundle. Therefore, subclasses need only define the messages for any later
     * steps. {@code launch} displays a message just before calling
     * {@code initialize}, and again just before calling {@code startup}.
     * <p>
     * The {@code startup.message.3}, which is displayed just before calling
     * {@code initialize}, has the value &ldquo;Initializing
     * ${Application.title}...&rdquo;. If the subclass is installing modules in
     * the {@code initialize} method and wants the message to state that fact,
     * then simply override &ldquo;{@code startup.message.3}&rdquo; in its
     * resource bundle. This same thing may be done for any messages that the
     * subclass wants to change.
     *
     * @param <T>              the {@code Application} subclass type
     * @param applicationClass the {@code Application} subclass
     * @param startupSteps     the total number of startup steps
     * @param args             the command line arguments
     */
    public static synchronized <T extends Application> void launch(
            final Class<T> applicationClass, int startupSteps,
            final String[] args) {
        SplashScreen splashScreen = SplashScreen.getInstance();
        splashScreen.setTotalStartupSteps(startupSteps);
        launch(applicationClass, args);
    }

    public static boolean isLoading() {
        return loading;
    }

    /*
     * Initializes the ApplicationContext applicationClass and application
     * properties.
     *
     * Note that, as of Java SE 5, referring to a class literal doesn't force
     * the class to be loaded. More info:
     * http://java.sun.com/javase/technologies/compatibility.jsp#literal It's
     * important to perform these initializations early, so that Application
     * static blocks/initializers happen afterwards.
     */
    static <T extends Application> T create(Class<T> applicationClass) throws Exception {

        if (!Beans.isDesignTime()) {
            /*
             * A common mistake for privileged applications that make network
             * requests (and aren't applets or web started) is to not configure
             * the http.proxyHost/Port system properties. We paper over that
             * issue here.
             */
            try {
                System.setProperty("java.net.useSystemProxies", "true");
            } catch (SecurityException ignoreException) {
                // Unsigned apps can't set this property.
            }
        }

        /*
         * Construct the Application object. The following complications,
         * relative to just calling applicationClass.newInstance(), allow a
         * privileged app to have a private static inner Application subclass.
         */
        Constructor<T> ctor = applicationClass.getDeclaredConstructor();
        if (!ctor.canAccess(null)) {
            try {
                ctor.setAccessible(true);
            } catch (SecurityException ignore) {
                // ctor.newInstance() will throw an IllegalAccessException
            }
        }
        T app = ctor.newInstance();

        /*
         * Initialize the ApplicationContext application properties
         */
        ApplicationContext ctx = app.getContext();
        ctx.setApplicationClass(applicationClass);
        ctx.setApplication(app);
        /*
         * Load the application resource map, notably the Application.*
         * properties.
         */
        ResourceMap appRM = ctx.getResourceMap();
        if (splashScreen != null) {
            splashScreen.setImage(appRM
                    .getImageIcon("Application.splash").getImage());
            splashScreen.setApplicationTitle(appRM
                    .getString("Application.title"));
            splashScreen.setCopyrightNotice(appRM
                    .getString("Application.copyright"));
            splashScreen.setVendor(appRM.getString("Application.vendor"));
            splashScreen.show();
            splashScreen.updateStep(0, appRM.getString("startup.message.0"));
            splashScreen.sleep(1500);
        }
        ctx.getLocalStorage().getDirectory();
        ctx.getSessionStorage();
        ctx.getTaskMonitor();
        ctx.getActionMap();
        LOGGER = ctx.getLogger();

        appRM.putResource("platform", platform());

        if (!Beans.isDesignTime()) {
            /*
             * Initialize the UIManager lookAndFeel property with the
             * Application.lookAndFeel resource. If the the resource isn't
             * defined we default to "system".
             */
            if (splashScreen != null) {
                splashScreen.updateStep(1, appRM.getString("startup.message.1"));
            }
            app.installLaFs();
            app.setLookAndFeel();
        }

        return app;
    }

    /*
     * Defines the default values for the platform resource, either "osx" or
     * "default".
     */
    private static String platform() {
        String platform = "windows";
        try {
            String osName = System.getProperty("os.name");
            if ((osName != null) && osName.toLowerCase().startsWith("mac os x")) {
                platform = "osx";
            } else if ((osName != null) && (osName.toLowerCase().contains(
                    "linux")
                    || osName.toLowerCase().contains("solaris"))) {
                platform = "linux";
            }
        } catch (SecurityException ignore) {
        }
        return platform;
    }

    /*
     * Call the ready method when the eventQ is quiet.
     */
    void waitForReady() {
        new DoWaitForEmptyEventQ().execute();
    }

    void initSecurityModel() {
        LogonManager.getInstance().initialize();
    }

    /**
     * Responsible for initializations that must occur before the GUI is
     * constructed by {@code startup}.
     * <p>
     * This method is called by the static {@code launch} method, before
     * {@code startup} is called. Subclasses that want to do any initialization
     * work before {@code startup} must override it. The {@code initialize}
     * method runs on the event dispatching thread.
     * <p>
     * By default {@code initialize()} does nothing.
     *
     * @param args the main method's arguments.
     *
     * @see #launch
     * @see #startup
     * @see #shutdown
     */
    protected void initialize(String[] args) {
    }

    /**
     * Responsible for starting the application; for creating and showing the
     * initial GUI.
     * <p>
     * This method is called by the static {@code launch} method, subclasses
     * must override it. It runs on the event dispatching thread.
     *
     * @see #launch
     * @see #initialize
     * @see #shutdown
     */
    protected abstract void startup();

    /**
     * Called after the {@code startup()} method has returned and there are no
     * more events on the
     * {@link Toolkit#getSystemEventQueue system event queue}. When this method
     * is called, the application's GUI is ready to use.
     * <p>
     * It's usually important for an application to start up as quickly as
     * possible. Applications can override this method to do some additional
     * start up work, after the GUI is up and ready to use.
     * <p>
     * <em>The default implementation does nothing.</em>
     *
     * @see #launch
     * @see #startup
     * @see #shutdown
     */
    protected void ready() {
    }

    /**
     * Retrieves whether this {@code Application} is in its {@code ready} state.
     *
     * @return {@code true} if the {@code Application} is {@code ready};
     *         {@code false} otherwise
     */
    public boolean isReady() {
        return ready;
    }

    /**
     * Provides a means of restarting the main window in order to update the
     * look and feel to the user's selection.
     */
    public void restart() {
        getMainFrame().dispose();
        setLookAndFeel();

        for (Window w : getVisibleSecondaryWindows()) {
            SwingUtilities.updateComponentTreeUI(w);
        }

        exit(SysExits.EX_RESTART);
    }

    private boolean shuttingDown = false;

    /**
     * Called when the application {@link #exit exits}. Subclasses may override
     * this method to do any cleanup tasks that are necessary before exiting.
     * Obviously, you'll want to try and do as little as possible at this point.
     * This method runs on the event dispatching thread.
     * <dl>
     * <dt><strong><em>Developer's Note</em></strong>:</dt>
     * <dd>The default implementation of this method takes care of the
     * following:
     * <ul>
     * <li>Shutting down all {@link com.pekinsoft.framework.TaskService
     * TaskService}s
     * <li>Storing session state for the main window
     * <li>Storing session state for any visible secondary windows
     * <li>Calling the {@code shutdown} methods of all modules that implement
     * the {@link com.pekinsoft.plugins.Plugin Plugin interface}
     * </ul>
     * Therefore, overriding classes will want to make a call to
     * {@code super.shutdown()} at some point during their overridden
     * implementation.
     * </dd></dl>
     *
     * @see #startup
     * @see #ready
     * @see #exit
     * @see #addExitListener
     */
    protected void shutdown() {

    }

    /*
     * An event that sets a flag when it's dispatched and another flag, see
     * isEventQEmpty(), that indicates if the event queue was empty at dispatch
     * time.
     */
    private static class NotifyingEvent extends PaintEvent implements
            ActiveEvent {

        private static final long serialVersionUID = -188406235468422381L;

        private boolean dispatched = false;
        private boolean qEmpty = false;

        NotifyingEvent(Component c) {
            super(c, PaintEvent.UPDATE, null);
        }

        synchronized boolean isDispatched() {
            return dispatched;
        }

        synchronized boolean isEventQEmpty() {
            return qEmpty;
        }

        @Override
        public void dispatch() {
            EventQueue q = Toolkit.getDefaultToolkit().getSystemEventQueue();
            synchronized (this) {
                qEmpty = (q.peekEvent() == null);
                dispatched = true;
                notifyAll();
            }
        }
    }

    /*
     * Keep queuing up NotifyingEvents until the event queue is empty when the
     * NotifyingEvent is dispatched().
     */
    private void waitForEmptyEventQ() {
        boolean qEmpty = false;
        JPanel placeHolder = new JPanel();
        EventQueue q = Toolkit.getDefaultToolkit().getSystemEventQueue();
        while (!qEmpty) {
            NotifyingEvent e = new NotifyingEvent(placeHolder);
            q.postEvent(e);
            synchronized (e) {
                while (!e.isDispatched()) {
                    try {
                        e.wait();
                    } catch (InterruptedException ie) {
                    }
                }
                qEmpty = e.isEventQEmpty();
            }
        }
    }

    /*
     * When the event queue is empty, give the app a chance to do something, now
     * that the GUI is {@code ready}.
     */
    private class DoWaitForEmptyEventQ extends BackgroundTask<Void, Void> {

        DoWaitForEmptyEventQ() {
            super(Application.this);
        }

        @Override
        protected Void doInBackground() {
            waitForEmptyEventQ();
            return null;
        }

        @Override
        protected void finished() {
            Application.this.ready = true;
            ready();
            Application.this.firePropertyChange("ready", false, true);
        }
    }

    /**
     * Gracefully shutdown the application, calls exit(null, SysExits.EX_OK)}
     * This version of exit() is convenient if the decision to exit the
     * application wasn't triggered by an event and is a normal exit
     * none-the-less.
     *
     * @see #exit(EventObject)
     */
    public final void exit() {
        exit(null, SysExits.EX_OK);
    }

    /**
     * Gracefully shutdown the application, calls {@code exit(null, status)}.
     * This version of exit() is convenient if the decision to exit was not
     * triggered by an event, but needs a {@link com.pekinsoft.framework.SysExits
     * SysExits} exit status other than the default.
     *
     * @param status the {@code SysExits} exit status constant to apply
     */
    public final void exit(SysExits status) {
        exit(null, status);
    }

    /**
     * Gracefully shutdown the application, calls
     * {@code exit(event, SysExits.EX_OK)}. This version of exit() is convenient
     * if the decision to exit was triggered by an event, but the exit status is
     * normal.
     *
     * @param event the {@code EventObject} that triggered the exit
     */
    public void exit(EventObject event) {
        exit(event, SysExits.EX_OK);
    }

    /**
     * Gracefully shutdown the application.
     * <p>
     * If none of the {@code ExitListener.canExit()} methods return false, calls
     * the {@code ExitListener.willExit()} methods, then {@code shutdown()}, and
     * then exits the Application with {@link #end end}. Exceptions thrown while
     * running willExit() or shutdown() are logged but otherwise ignored.
     * <p>
     * If the caller is responding to an GUI event, it's helpful to pass the
     * event along so that ExitListeners' canExit methods that want to popup a
     * dialog know on which screen to show the dialog. For example:
     * <pre>
     * class ConfirmExit implements Application.ExitListener {
     *     public boolean canExit(EventObject e) {
     *         Object source = (e != null) ? e.getSource() : null;
     *         Component owner = (source instanceof Component) ? (Component)source : null;
     *         int option = JOptionPane.showConfirmDialog(owner, "Really Exit?");
     *         return option == JOptionPane.YES_OPTION;
     *     }
     *     public void willExit(EventObejct e) {}
     * }
     * myApplication.addExitListener(new ConfirmExit());
     * </pre> The {@code EventObject} argument may be null, e.g. if the exit
     * call was triggered by non-GUI code, and {@code canExit}, {@code willExit}
     * methods must guard against the possibility that the {@code EventObject}
     * argument's {@code source} is not a {@code Component}.
     *
     * @param event  the EventObject that triggered this call or {@code null}
     * @param status the exit status for the {@code Application} as one of the
     *               {@link SysExits} enumerated constants
     *
     * @see #addExitListener
     * @see #removeExitListener
     * @see #shutdown
     * @see #end
     */
    public void exit(EventObject event, SysExits status) {
        LOGGER.log(Level.TRACE, "ExitListener listeners.size = "
                + exitListeners.size());

        for (ExitListener listener : Lookup.getDefault().lookupAll(
                ExitListener.class)) {
            if (!exitListeners.contains(listener)) {
                exitListeners.add(listener);
            }
        }

        for (ExitListener listener : exitListeners) {
            if (!listener.canExit(event)) {
                return;
            }
        }
        try {
            for (ExitListener listener : exitListeners) {
                try {
                    listener.willExit(event);
                } catch (Exception e) {
                    LOGGER.log(Level.WARNING, "ExitListener.willExit() failed",
                            e);
                }
            }

            for (Logger logger : getLoggers()) {
                logger.save();
            }
            shutdown();
        } catch (Exception e) {
            LOGGER.log(Level.WARNING,
                    "unexpected error in Application.shutdown()", e);
        } finally {
            end(status);
        }
    }

    /**
     * Called by {@link #exit exit} to terminate the application. Calls
     * {@code Runtime.getRuntime().exit(0)}, which halts the JVM.
     *
     * @param status the exit status of one of the {@code SysExits} constants
     *
     * @see #exit
     */
    protected void end(SysExits status) {
        Runtime.getRuntime().exit(status.getValue());
    }

    /**
     * @deprecated The removal of {@code addExitListener} will be happening in
     * the not-too-distant future, since the {@link Lookup} has been finalized.
     * <em>It is imperative that classes which implement {@code ExitListener}
     * start creating {@code META-INF/service/com.pekinsoft.api.ExitListener}
     * entries so they will still be called during
     * {@link #exit() exit}.</em>
     * <p>
     * Add an {@link com.pekinsoft.api.ExitListener
     * ExitListener} to the list.
     * <p>
     * If the specified {@code listener} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param listener the {@code ExitListener}
     *
     * @see #removeExitListener
     * @see #getExitListeners
     */
    @Deprecated
    public void addExitListener(ExitListener listener) {
        if (listener == null) {
            return;
        }
        exitListeners.add(listener);
    }

    /**
     * @deprecated The removal of {@code addExitListener} and
     * {@code removeExitListener} will be happening in the not-too-distant
     * future, since the {@link Lookup} has been finalized. <em>To find all
     * {@code ExitListener}s, use
     * {@link Lookup#lookupAll(Class) Lookup.getDefault().lookupAll(ExitListener.class}.
     * </em><p>
     * Remove an {@link com.pekinsoft.api.ExitListener
     * ExitListener} from the list.
     * <p>
     * If the specified {@code listener} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param listener the {@code ExitListener}
     *
     * @see #addExitListener
     * @see #getExitListeners
     */
    @Deprecated
    public void removeExitListener(ExitListener listener) {
        if (listener == null) {
            return;
        }
        exitListeners.remove(listener);
    }

    public synchronized void addLogger(Logger logger) {
        if (loggers == null) {
            loggers = new ArrayList<>();
        }
        if (logger == null || loggers.contains(logger)) {
            return;
        }
        loggers.add(logger);
    }

    public synchronized void removeLogger(Logger logger) {
        if (loggers == null) {
            return;
        }
        if (logger == null || !loggers.contains(logger)) {
            return;
        }
        loggers.add(logger);
    }

    private synchronized List<Logger> getLoggers() {
        return loggers == null
               ? Collections.emptyList()
               : loggers;
    }

    /**
     * @deprecated The removal of {@code addExitListener} will be happening in
     * the not-too-distant future, since the {@link Lookup} has been finalized.
     * <em>It is imperative that classes which implement {@code ExitListener}
     * start creating {@code META-INF/service/com.pekinsoft.api.ExitListener}
     * entries so they will still be called during
     * {@link #exit() exit}.</em>
     * <p>
     * All of the {@link com.pekinsoft.api.ExitListener
     * ExitListener} added so far.
     *
     * @return all of the {@code ExitListener}s added so far.
     */
    public ExitListener[] getExitListeners() {
        int size = exitListeners.size();
        return exitListeners.toArray(new ExitListener[size]);
    }

    /**
     * Add a {@link com.pekinsoft.api.PrintCookie PrintCookie} instance to the
     * list of {@code PrintCookie}s.
     * <p>
     * If the specified {@code cookie} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param cookie the {@code PrintCookie} to be added
     *
     * @see #removePrintCookie(com.pekinsoft.api.PrintCookie)
     * @see #getPrintCookies()
     */
    public void addPrintCookie(PrintCookie cookie) {
        if (cookie == null) {
            return;
        }
        printCookies.add(cookie);
        context.firePropertyChange(
                "printAvailable",
                !printCookies.isEmpty(),
                printCookies.isEmpty()
        );

        LOGGER.log(Level.TRACE,
                "(addPrintCookie) printCookies.size() == " + printCookies.size());
    }

    /**
     * Remove a {@link com.pekinsoft.api.PrintCookie PrintCookie} instance from
     * the list of {@code PrintCookie}s.
     * <p>
     * If the specified {@code cookie} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param cookie the {@code PrintCookie} to be removed
     *
     * @see #addPrintCookie(com.pekinsoft.api.PrintCookie)
     * @see #getPrintCookies()
     */
    public void removePrintCookie(PrintCookie cookie) {
        if (cookie == null) {
            return;
        }
        printCookies.remove(cookie);
        context.firePropertyChange(
                "printAvailable",
                !printCookies.isEmpty(),
                printCookies.isEmpty()
        );

        LOGGER.log(Level.TRACE,
                "(removePrintCookie) printCookies.size() == " + printCookies
                        .size());
    }

    /**
     * Retrieves all of the {@link com.pekinsoft.api.PrintCookie PrintCookie}s
     * added to the list so far.
     *
     * @return an array of {@code PrintCookie} instances
     *
     * @see #addPrintCookie(com.pekinsoft.api.PrintCookie)
     * @see #removePrintCookie(com.pekinsoft.api.PrintCookie)
     */
    public PrintCookie[] getPrintCookies() {
        return printCookies.toArray(PrintCookie[]::new);
    }

    /**
     * Add a {@link com.pekinsoft.api.SaveCookie SaveCookie} to the list of
     * {@code SaveCookie}s.
     * <p>
     * If the specified {@code cookie} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param cookie the {@code SaveCookie} to be added to the list
     *
     * @see #removeSaveCookie(com.pekinsoft.api.SaveCookie)
     * @see #getSaveCookies()
     */
    public void addSaveCookie(SaveCookie cookie) {
        if (cookie == null) {
            return;
        }
        saveCookies.add(cookie);
    }

    /**
     * Remove a {@link com.pekinsoft.api.SaveCookie SaveCookie} from the list of
     * {@code SaveCookie}s added so far.
     * <p>
     * If the specified {@code cookie} is {@code null}, no exception is thrown
     * and no action is taken.
     *
     * @param cookie the {@code SaveCookie} to be removed from the list
     *
     * @see #addSaveCookie(com.pekinsoft.api.SaveCookie)
     * @see #getSaveCookies()
     */
    public void removeSaveCookie(SaveCookie cookie) {
        if (cookie == null) {
            return;
        }
        saveCookies.remove(cookie);
    }

    /**
     * Returns all of the {@link com.pekinsoft.api.SaveCookie SaveCookie}s added
     * to the list so far.
     *
     * @return an array of all {@code SaveCookie}s
     *
     * @see #addSaveCookie(com.pekinsoft.api.SaveCookie)
     * @see #removeSaveCookie(com.pekinsoft.api.SaveCookie)
     */
    public SaveCookie[] getSaveCookies() {
        return saveCookies.toArray(SaveCookie[]::new);
    }

    /**
     * The default {@code Action} for quitting an application, {@code quit} just
     * exits the application by calling {@code exit(e)}.
     *
     * @param e the triggering event
     *
     * @see #exit(EventObject)
     */
    @AppAction(menuBaseName = "file",
               menuActionIndex = 127,
               menuSepBefore = true,
               toolbarBaseName = "file",
               toolbarActionIndex = -128,
               toolbarSepAfter = true)
    public void quit(ActionEvent e) {
        exit(e, SysExits.EX_OK);
    }

    /**
     * The ApplicationContext singleton for this Application.
     *
     * @return the Application's ApplicationContext singleton
     */
    public final ApplicationContext getContext() {
        return context;
    }

    /**
     * The {@code Application} singleton.
     * <p>
     * Typically this method is only called after an Application has been
     * launched however in some situations, like tests, it's useful to be able
     * to get an {@code Application} object without actually launching. In that
     * case, an instance of the specified class is constructed and configured as
     * it would be by the {@link #launch launch} method. However it's
     * {@code initialize} and {@code startup} methods are not run.
     *
     * @param <T>              {@code Application} subclass type
     * @param applicationClass this Application's subclass
     *
     * @return the launched Application singleton.
     *
     * @see Application#launch
     */
    public static synchronized <T extends Application> T getInstance(
            Class<T> applicationClass) {
        if (application == null) {
            /*
             * Special case: the application hasn't been launched. We're
             * constructing the applicationClass here to get the same effect as
             * the NoApplication class serves for getInstance(). We're not
             * launching the app, no initialize/startup/wait steps.
             */
            try {
                application = create(applicationClass);
            } catch (Exception e) {
                String msg = String.format("Couldn't construct %s",
                        applicationClass);
                throw new Error(msg, e);
            }
        }
        return applicationClass.cast(application);
    }

    /**
     * The {@code Application} singleton, or a placeholder if {@code launch}
     * hasn't been called yet.
     * <p>
     * Typically this method is only called after an Application has been
     * launched however in some situations, like tests, it's useful to be able
     * to get an {@code Application} object without actually launching. The
     * <em>placeholder</em> Application object provides access to an
     * {@code ApplicationContext} singleton and has the same semantics as
     * launching an Application defined like this:
     * <pre>
     * public class PlaceholderApplication extends Application {
     *     public void startup() { }
     * }
     * Application.launch(PlaceholderApplication.class);
     * </pre>
     *
     * @return the Application singleton or a placeholder
     *
     * @see Application#launch
     * @see Application#getInstance(Class)
     */
    public static synchronized Application getInstance() {
        if (application == null) {
            application = new NoApplication();
        }
        return application;
    }

    /*
     * An implementation of the Application abstract class for testing purposes.
     */
    private static class NoApplication extends Application {

        protected NoApplication() {
            ApplicationContext ctx = getContext();
            ctx.setApplicationClass(getClass());
            ctx.setApplication(this);
            ResourceMap appResourceMap = ctx.getResourceMap();
            appResourceMap.putResource("platform", platform());
        }

        @Override
        protected void startup() {
        }

        @Override
        public void show(JInternalFrame frame) {
        }
    }

    /*
     * Look and Feel support
     */
    /**
     * Installs the JTattoo third-party look and feel classes into the {@link
     * javax.swing.UIManager UIManager}, so that they may be listed within the
     * {@code Application}, if needed, using:
     * <pre>
     * LookAndFeelInfo[] lafs = UIManager.getInstalledLookAndFeels();
     * </pre>
     */
    protected void installLaFs() {
        UIManager.installLookAndFeel("Acryl",
                "com.jtattoo.plaf.acryl.AcrylLookAndFeel");
        UIManager.installLookAndFeel("Aero",
                "com.jtattoo.plaf.aero.AeroLookAndFeel");
        UIManager.installLookAndFeel("Aluminium",
                "com.jtattoo.plaf.aluminium.AluminiumLookAndFeel");
        UIManager.installLookAndFeel("Bernstein (pretty loud)",
                "com.jtattoo.plaf.bernstein.BernsteinLookAndFeel");
        UIManager.installLookAndFeel("Fast (lightweight)",
                "com.jtattoo.plaf.fast.FastLookAndFeel");
        UIManager.installLookAndFeel("Graphite (clean, light gray)",
                "com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
        UIManager.installLookAndFeel("HiFi (darker theme)",
                "com.jtattoo.plaf.hifi.HiFiLookAndFeel");
        UIManager.installLookAndFeel("Luna (WinXP-ish)",
                "com.jtattoo.plaf.luna.LunaLookAndFeel");
        UIManager.installLookAndFeel("McWin (Mac OS-X-ish)",
                "com.jtattoo.plaf.mcwin.McWinLookAndFeel");
        UIManager.installLookAndFeel("Mint (light green)",
                "com.jtattoo.plaf.mint.MintLookAndFeel");
        UIManager.installLookAndFeel("Noire (darkest theme)",
                "com.jtattoo.plaf.noire.NoireLookAndFeel");
        UIManager.installLookAndFeel("Smart (lightweight)",
                "com.jtattoo.plaf.smart.SmartLookAndFeel");
        UIManager.installLookAndFeel("FlatLaf Light",
                "com.formdev.flatlaf.FlatLightLaf");
        UIManager.installLookAndFeel("FlatLaf Dark",
                "com.formdev.flatlaf.FlatDarkLaf");
        UIManager.installLookAndFeel("Darcula",
                "com.formdev.flatlaf.FlatDarculaLaf");
    }

    /**
     * Sets the {@code Application}'s look and feel based upon the
     * {@code Application.lookAndFeel} resource setting. If this setting is not
     * present, then the "system" look and feel is used. Besides checking the
     * {@code Application}
     * {@link com.pekinsoft.framework.ResourceMap ResourceMap}, it also checks
     * the User preferences (on key "Application.lookAndFeel") to see if it has
     * been previously set. If that preference key has been set, then its values
     * is used instead of the {@code ResourceMap} setting.
     * <p>
     * If neither the {@code ResourceMap} nor the user preference has been set,
     * the system-specific look and feel is used.
     * <p>
     * If the {@code ResourceMap} setting is set to "default", the "updated",
     * cross-platform look and feel ("Nimbus") is used, instead of the out-dated
     * "Metal" look and feel.
     * <p>
     * When using the "system" look and feel on Linux and/or Linux-derivative
     * operating systems, the "GTK+" look and feel is used, as it typically
     * takes on the appearance of the desktop on which it is running, whether
     * the desktop is KDE, GNOME, XFCE, LXDE, or other graphical Linux desktop.
     * <p>
     */
    protected void setLookAndFeel() {
        String platform = getContext().getResourceMap().getString("platform");
        String defaultLaf = getContext()
                .getResourceMap()
                .getString("Application.lookAndFeel");
        String laf2use = getContext()
                .getPreferences(ApplicationContext.USER)
                .get("Application.lookAndFeel", defaultLaf);

        try {
            if (laf2use.equalsIgnoreCase("system")) {
                if (platform.toLowerCase().equals("linux")) {
                    laf2use = "com.sun.java.swing.plaf.gtk.GTKLookAndFeel";
                } else {
                    laf2use = UIManager.getSystemLookAndFeelClassName();
                }
            } else if (laf2use.equalsIgnoreCase("default")) {
                laf2use = "javax.swing.plaf.nimbus.NimbusLookAndFeel";
            }
            UIManager.setLookAndFeel(laf2use);
        } catch (ClassNotFoundException
                | IllegalAccessException
                | InstantiationException
                | UnsupportedLookAndFeelException e) {
            LOGGER.log(Level.WARNING, laf2use);
        }
    }

    /**
     * This method is provided as a convenience for {@code Application}
     * subclasses that are using the JTattoo look and feel library, so that the
     * default text "JTattoo" can be replaced on the menus of the
     * {@code Application} with the {@code Application.title} or
     * {@code Application.name} resource, as the {@code Application} developer
     * chooses.
     * <p>
     * In order to use this method, the protected field `lafProps` simply needs
     * to be instantiated as a {@link java.util.Properties Properties} object
     * and have the property "logoString" set to
     * {@code getResourceMap().getString("Application.title")} or
     * {@code getResourceMap().getString("Application.name")}.
     *
     * @param laf the Look and Feel whose theme is to be updated
     */
    protected void updateLaFTheme(LookAndFeel laf) {
        if (!laf.getClass().getCanonicalName().contains("jtattoo")) {
            return;
        }

        switch (laf.getName()) {
            case "Acryl" ->
                AcrylLookAndFeel.setCurrentTheme(lafProps);
            case "Aero" ->
                AeroLookAndFeel.setCurrentTheme(lafProps);
            case "Aluminium" ->
                AluminiumLookAndFeel.setCurrentTheme(lafProps);
            case "Bernstein" ->
                BernsteinLookAndFeel.setCurrentTheme(lafProps);
            case "Fast" ->
                FastLookAndFeel.setCurrentTheme(lafProps);
            case "Graphite" ->
                GraphiteLookAndFeel.setCurrentTheme(lafProps);
            case "HiFi" ->
                HiFiLookAndFeel.setCurrentTheme(lafProps);
            case "Luna" ->
                LunaLookAndFeel.setCurrentTheme(lafProps);
            case "McWin" ->
                McWinLookAndFeel.setCurrentTheme(lafProps);
            case "Mint" ->
                MintLookAndFeel.setCurrentTheme(lafProps);
            case "Noire" ->
                NoireLookAndFeel.setCurrentTheme(lafProps);
            case "Smart" ->
                SmartLookAndFeel.setCurrentTheme(lafProps);
        }

        SwingUtilities.updateComponentTreeUI(getMainFrame());
    }

    /**
     * Return the JFrame used to show this application.
     * <p>
     * The frame's name is set to "mainFrame", its title is initialized with the
     * value of the {@code Application.title} resource and a
     * {@code WindowListener} is added that calls {@code exit} when the user
     * attempts to close the frame.
     * <p>
     * <p>
     * This method may be called at any time; the JFrame is created lazily and
     * cached. For example:
     * <pre>
     * protected void startup() {
     *     getMainFrame().setJMenuBar(createMenuBar());
     *     show(createMainPanel());
     * }
     * </pre>
     *
     * @return this application's main frame
     *
     * @see #setMainFrame
     * @see #show
     * @see JFrame#setName
     * @see JFrame#setTitle
     * @see JFrame#addWindowListener
     */
    public final JFrame getMainFrame() {
        return getMainView().getFrame();
    }

    /**
     * Sets the JFrame use to show this application.
     * <p>
     * This method should be called from the startup method by a subclass that
     * wants to construct and initialize the main frame itself. Most
     * applications can rely on the fact that {@code getMainFrame} lazily
     * constructs the main frame and initializes the {@code mainFrame} property.
     * <p>
     * If the main frame property was already initialized, either implicitly
     * through a call to {@code getMainFrame} or by explicitly calling this
     * method, an IllegalStateException is thrown. If {@code mainFrame} is null,
     * an IllegalArgumentException is thrown.
     * <p>
     * This property is bound.
     *
     * @param mainFrame the new values of the mainFrame property
     *
     * @see #getMainFrame
     */
    protected final void setMainFrame(JFrame mainFrame) {
        if (this.mainFrame != null) {
            throw new IllegalArgumentException("mainFrame already set");
        }

        this.mainFrame = mainFrame;
        this.mainFrame.setName("mainFrame");
        this.mainFrame.addWindowListener(new MainFrameListener());
        this.mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

//        getMainView().setFrame(mainFrame);
    }

    /**
     * Sets the {@link com.pekinsoft.desktop.View View}, or a subclass
     * thereof, as the main view for the {@code Application}.
     *
     * @param mainView the {@code View} or subclass thereof to use as the
     *                 {@code Application}'s main view window
     */
    protected final void setMainView(View mainView) {
        if (!(mainView instanceof FrameView)) {
            throw new IllegalArgumentException(
                    "mainView is not an instance of FrameView");
        }
        if (this.mainView != null) {
            throw new IllegalStateException("mainView already set");
        }

        this.mainView = (FrameView) mainView;
        this.mainView.getFrame().setName("mainFrame");
        this.mainView.getFrame().addWindowListener(new MainFrameListener());
        this.mainView.getFrame().setDefaultCloseOperation(
                JFrame.DO_NOTHING_ON_CLOSE);

        setMainFrame(this.mainView.getFrame());
    }

    private String sessionFilename(Component window) {
        if (window == null) {
            return null;
        } else {
            String name = window.getName();
            return (name == null) ? null : name + ".session.xml";
        }
    }

    /**
     * Initialize the hierarchy with the specified root by injecting resources.
     * <p>
     * By default the {@code show} methods
     * {@link ResourceMap#injectComponents inject resources} before initializing
     * the JFrame or JDialog's size, location, and restoring the window's
     * session state. If the app is showing a window whose resources have
     * already been injected, or that shouldn't be initialized via resource
     * injection, this method can be overridden to defeat the default behavior.
     *
     * @param root the root of the component hierarchy
     *
     * @see ResourceMap#injectComponents
     * @see #show(JComponent)
     * @see #show(JFrame)
     * @see #show(JDialog)
     */
    protected void configureWindow(Component root) {
        getContext().getResourceMap().injectComponents(root);
    }

    /**
     * Initializes all of the properties of the specified {@link
     * javax.swing.RootPaneContainer RootPaneContainer} {@code c}. This method
     * attempts to add various listeners to the container to handle proper
     * shutdown and session state saving behind the scenes, as well as to
     * attempt to restore the previous session state, if any.
     *
     * @param c the {@code RootPaneContainer} to initialize
     */
    protected void initRootPaneContainer(RootPaneContainer c) {
        JComponent rootPane = c.getRootPane();
        // These initializations are only done once
        Object k = "SingleFrameApplication.initRootPaneContainer";
        if (rootPane.getClientProperty(k) != null) {
            return;
        }
        rootPane.putClientProperty(k, Boolean.TRUE);
        // Inject resources
        Container root = rootPane.getParent();
        if (root instanceof Component component) {
            configureWindow(component);
        }
        // If this is the mainFrame, then close == exit
        JFrame mf = getMainFrame();
        if (c == mf) {
            mf.addWindowListener(new Application.MainFrameListener());
            mf.addHierarchyListener(new Application.SecondaryWindowListener());
            mf.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        } else if (root instanceof Component window) { // close == save session state
            window.addHierarchyListener(
                    new Application.SecondaryWindowListener());
        }
        // If this is a JFrame monitor "normal" (not maximized) bounds
        if (root instanceof JFrame) {
            root.addComponentListener(new Application.FrameBoundsListener());
        } else if (root instanceof JInternalFrame jInternalFrame) {
            jInternalFrame.setDefaultCloseOperation(
                    JInternalFrame.DO_NOTHING_ON_CLOSE);
            jInternalFrame.addInternalFrameListener(
                    new Application.InternalFrameListener());
        }
        // If the window's bounds don't appear to have been set, do it
        switch (root) {
            case Window window -> {
                if (!root.isValid() || (root.getWidth() == 0) || (root
                        .getHeight() == 0)) {
                    window.pack();

                    if (window.getWidth() <= 80 || window.getHeight() <= 80) {
                        Dimension dim = (Dimension) getContext()
                                .getResourceMap()
                                .getObject("Application.defaultWindowSize",
                                        Dimension.class);
                        if (dim == null) {
                            ResourceMap rm = getContext().getResourceMap(root
                                    .getClass());
                            dim = (Dimension) rm.getObject("window.defaultSize",
                                    Dimension.class);
                            if (dim == null) {
                                dim = new Dimension(1024, 768);
                            }
                        }

                        window.setSize(dim);
                    }
                }
                if (!window.isLocationByPlatform() && (root.getX() == 0) && (root
                        .getY() == 0)) {
                    Component owner = window.getOwner();
                    if (owner == null) {
                        owner = (window != mf) ? mf : null;
                    }
                    window.setLocationRelativeTo(owner);  // center the window
                }
            }
            case JInternalFrame window -> {
                if (!window.isValid() || (window.getWidth() == 0) || (window
                        .getHeight() == 0)) {
                    window.pack();
                }
            }
            default -> {
            }
        }
        // Restore session state
        if (root instanceof Component component) {
            String filename = sessionFilename(component);
            if (filename != null) {
                try {
                    getContext().getSessionStorage().restore(root, filename);
                } catch (IOException e) {
                    String msg = String.format("couldn't restore sesssion [%s]",
                            filename);
                    LOGGER.log(Level.WARNING, msg, e);
                }
            }
        }
    }

    /**
     * Show the specified component in the {@link #getMainFrame main frame}.
     * Typical applications will call this method after constructing their main
     * GUI panel in the {@code startup} method.
     * <p>
     * Before the main frame is made visible, the properties of all of the
     * components in the hierarchy are initialized with {@link
     * ResourceMap#injectComponents ResourceMap.injectComponents} and then
     * restored from saved session state (if any) with {@link
     * SessionStorage#restore SessionStorage.restore}. When the application
     * shuts down, session state is saved.
     * <p>
     * Note that the name of the lazily created main frame (see
     * {@link #getMainFrame getMainFrame}) is set by default. Session state is
     * only saved for top level windows with a valid name and then only for
     * component descendants that are named.
     *
     * @param component the main frame's contentPane child
     *
     * @throws IllegalArgumentException if {@code Component} is {@code null}
     */
    protected void show(JComponent component) {
        if (component == null) {
            throw new IllegalArgumentException("null component");
        }
        JFrame f = getMainFrame();
        f.getContentPane().add(component, BorderLayout.CENTER);
        f.setLocationRelativeTo(getMainFrame());
        initRootPaneContainer(f);
        f.setVisible(true);
    }

    /**
     * Initialize and show the JDialog.
     * <p>
     * This method is intended for showing "secondary" windows, like message
     * dialogs, about boxes, and so on. Unlike the {@code mainFrame}, dismissing
     * a secondary window will not exit the application.
     * <p>
     * Session state is only automatically saved if the specified JDialog has a
     * name, and then only for component descendants that are named.
     * <p>
     * Throws an IllegalArgumentException if {@code dialog} is {@code null}
     *
     * @param dialog the main frame's contentPane child
     *
     * @see #show(JComponent)
     * @see #show(JFrame)
     * @see #configureWindow
     */
    public void show(JDialog dialog) {
        if (dialog == null) {
            throw new IllegalArgumentException("null dialog");
        }
        dialog.setLocationRelativeTo(getMainFrame());
        initRootPaneContainer(dialog);
        dialog.setVisible(true);
    }

    /**
     * Allows a subclass to provide for a Multiple Document Interface (MDI) and
     * show {@link javax.swing.JInternalFrame JInternalFrames} in the main
     * window's {@link javax.swing.JDesktopPane JDesktopPane}. For applications
     * that are not making use of the MDI paradigm, nothing needs to be done, as
     * the default implementation does nothing, just like {@link
     * #ready() ready}.
     * <p>
     * For {@code Application} subclasses that do override this method, it is
     * advised to check the supplied `JInternalFrame` to be sure it is not null,
     * and simply return if it is.
     *
     * @param frame the `JInternalFrame` to be shown
     */
    public void show(JInternalFrame frame) {

    }

    /**
     * Initialize and show the secondary JFrame.
     * <p>
     * This method is intended for showing "secondary" windows, like message
     * dialogs, about boxes, and so on. Unlike the {@code mainFrame}, dismissing
     * a secondary window will not exit the application.
     * <p>
     * Session state is only automatically saved if the specified JFrame has a
     * name, and then only for component descendants that are named.
     * <p>
     * Throws an IllegalArgumentException if {@code frame} is {@code null}
     *
     * @param frame the {@code JFrame} to show}
     *
     * @see #show(JComponent)
     * @see #show(JDialog)
     * @see #configureWindow
     */
    public void show(JFrame frame) {
        if (frame == null) {
            throw new IllegalArgumentException("null frame");
        }
        if (!frame.equals(getMainFrame())) {
            frame.setLocationRelativeTo(getMainFrame());
        } else {
            frame.setLocationRelativeTo(null);
        }

        if (frame instanceof ExitListener exitListener) {
            exitListeners.add(exitListener);
        }

        frame.setLocationRelativeTo(getMainFrame());
        initRootPaneContainer(frame);
        frame.setVisible(true);
    }

    private void saveSession(Component window) {
        String filename = sessionFilename(window);
        if (filename != null) {
            try {
                getContext().getSessionStorage().save(window, filename);
            } catch (IOException | NullPointerException e) {
                if (e instanceof NullPointerException) {
                    String msg = String.format("Trapped %s in Application."
                            + "saveSession() on \"%s\".",
                            e.getClass().getSimpleName(), window.getName());
                    System.err.println(msg);
                    return;
                }
                LOGGER.log(Level.WARNING, "could not save sesssion: %1$s", e);
            }
        }
    }

    private void restoreSession(Component window) {
        String filename = sessionFilename(window);
        if (filename != null) {
            try {
                getContext().getSessionStorage().restore(window, filename);
            } catch (IOException e) {
                LOGGER.log(Level.WARNING, "could not restore session: %1$s", e);
            }
        }
    }

    private boolean isVisibleWindow(Window w) {
        return w.isVisible()
                && ((w instanceof JFrame) || (w instanceof JDialog) || (w instanceof JWindow));
    }

    /**
     * Return all of the visible JWindows, JDialogs, and JFrames per
     * Window.getWindows() on Java SE 6, or Frame.getFrames() for earlier Java
     * versions.
     */
    private List<Window> getVisibleSecondaryWindows() {
        List<Window> rv = new ArrayList<>();
        Method getWindowsM = null;
        try {
            getWindowsM = Window.class.getMethod("getWindows");
        } catch (NoSuchMethodException
                | SecurityException ignore) {
        }
        if (getWindowsM != null) {
            Window[] windows = null;
            try {
                windows = (Window[]) getWindowsM.invoke(null);
            } catch (IllegalAccessException
                    | IllegalArgumentException
                    | InvocationTargetException e) {
                throw new Error("HCTB - can't get top level windows list", e);
            }
            if (windows != null) {
                for (Window window : windows) {
                    if (isVisibleWindow(window)) {
                        rv.add(window);
                    }
                }
            }
        } else {
            Frame[] frames = Frame.getFrames();
            if (frames != null) {
                for (Frame frame : frames) {
                    if (isVisibleWindow(frame)) {
                        rv.add(frame);
                    }
                }
            }
        }
        return rv;
    }

    private class MainFrameListener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            saveSession(mainFrame);
            exit(e);
        }
    }

    public class InternalFrameListener extends InternalFrameAdapter {

        @Override
        public void internalFrameClosed(InternalFrameEvent e) {
            getContext().firePropertyChange("visibilityChanged", null,
                    e.getInternalFrame());
        }

        @Override
        public void internalFrameClosing(InternalFrameEvent e) {
            maybeSaveFrameSize(e);
            saveSession(e.getInternalFrame());
        }

        @Override
        public void internalFrameDeactivated(InternalFrameEvent e) {
            if (e.getInternalFrame() instanceof PrintCookie printCookie) {
                removePrintCookie(printCookie);
            }
        }

        @Override
        public void internalFrameActivated(InternalFrameEvent e) {
            if (e.getInternalFrame() instanceof PrintCookie printCookie) {
                addPrintCookie(printCookie);
            }
            try {
                Preferences prefs = getContext().getPreferences(
                        ApplicationContext.USER);
                JInternalFrame f = e.getInternalFrame();
                int defaultWidth = f.getMinimumSize().width == 0
                                   ? f.getDesktopPane().getWidth() / 2
                                   : f.getMinimumSize().width;
                int defaultHeight = f.getMinimumSize().height == 0
                                    ? f.getDesktopPane().getHeight()
                                    : f.getMinimumSize().height;
                f.setSize(prefs.getInt(f.getTitle() + ".width", defaultWidth),
                        prefs.getInt(f.getTitle() + ".height", defaultHeight));
                f.setLocation(prefs.getInt(f.getTitle() + ".left", 0),
                        prefs.getInt(f.getTitle() + ".top", 0));
            } catch (Exception ex) {
                LOGGER.log(Level.WARNING, "Unable to restore "
                        + "session state for {0}", e.getInternalFrame()
                                .getTitle());
            }
        }

        @Override
        public void internalFrameOpened(InternalFrameEvent e) {
            if (e.getInternalFrame() instanceof PrintCookie printCookie) {
                addPrintCookie(printCookie);
            }

            try {
                restoreSession(e.getInternalFrame());

                Preferences prefs = getContext().getPreferences(
                        ApplicationContext.USER);
                JInternalFrame f = e.getInternalFrame();
                int defaultWidth = f.getMinimumSize().width == 0
                                   ? f.getDesktopPane().getWidth() / 2
                                   : f.getMinimumSize().width;
                int defaultHeight = f.getMinimumSize().height == 0
                                    ? f.getDesktopPane().getHeight()
                                    : f.getMinimumSize().height;
                f.setSize(prefs.getInt(f.getTitle() + ".width", defaultWidth),
                        prefs.getInt(f.getTitle() + ".height", defaultHeight));
                f.setLocation(prefs.getInt(f.getTitle() + ".left", 0),
                        prefs.getInt(f.getTitle() + ".top", 0));
            } catch (Exception ex) {
                LOGGER.log(Level.WARNING, "Unable to restore "
                        + "session state for {0}", e.getInternalFrame()
                                .getTitle());
            }
        }

        private void maybeSaveFrameSize(InternalFrameEvent e) {
            JInternalFrame f = e.getInternalFrame();
            if (!f.isMaximum()) {
                String clientPropertyKey = "JInternalFrame.normalBounds";
                f.getRootPane().putClientProperty(clientPropertyKey, f
                        .getBounds());
            }
            try {
                Preferences prefs = getContext().getPreferences(
                        ApplicationContext.USER);
                prefs.putInt(f.getTitle() + ".width", f.getWidth());
                prefs.putInt(f.getTitle() + ".height", f.getHeight());
                prefs.putInt(f.getTitle() + ".top", f.getY());
                prefs.putInt(f.getTitle() + ".left", f.getX());
            } catch (Exception ex) {
                LOGGER.log(Level.WARNING, "Unable to save "
                        + "session state for {0}", e.getInternalFrame()
                                .getTitle());
            }
        }

    }

    /*
     * Although it would have been simpler to listen for changes in the
     * secondary window's visibility per either a PropertyChangeEvent on the
     * "visible" property or a change in visibility per ComponentListener,
     * neither listener is notified if the secondary window is disposed.
     * HierarchyEvent.SHOWING_CHANGED does report the change in all cases, so we
     * use that.
     */
    public class SecondaryWindowListener implements HierarchyListener {

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                if (e.getSource() instanceof Component secondaryWindow) {
                    if (!secondaryWindow.isShowing()) {
                        try {
                            saveSession(secondaryWindow);
                        } catch (NullPointerException npe) {
                            String msg = String.format("Trapped %s in "
                                    + "SecondaryWindowListener.hierarchyChanged() "
                                    + "on component \"%s\".", npe.getClass()
                                            .getSimpleName(),
                                    e.getComponent().getName());
                            System.err.println(msg);
                        }
                    }

                    getContext().getPreferences(ApplicationContext.USER)
                            .putBoolean(secondaryWindow.getName() + ".visible",
                                    secondaryWindow.isShowing());
                }
            }
        }
    }

    /*
     * In order to properly restore a maximized JFrame, we need to record it's
     * normal (not maximized) bounds. They're recorded under a rootPane client
     * property here, so that they've can be session-saved by
     * WindowProperty#getSessionState().
     */
    public static class FrameBoundsListener implements ComponentListener {

        private void maybeSaveFrameSize(ComponentEvent e) {
            if (e.getComponent() instanceof JFrame f) {
                if ((f.getExtendedState() & Frame.MAXIMIZED_BOTH) == 0) {
                    String clientPropertyKey = "WindowState.normalBounds";
                    f.getRootPane().putClientProperty(clientPropertyKey, f
                            .getBounds());
                }
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            maybeSaveFrameSize(e);
        }

        /*
         * BUG: on Windows XP, with JDK6, this method is called once when the
         * frame is a maximized, with x,y=-4 and getExtendedState() == 0.
         */
        @Override
        public void componentMoved(ComponentEvent e) {
            /*
             * maybeSaveFrameSize(e);
             */ }

        @Override
        public void componentHidden(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }
    }

    /*
     * Prototype support for the View type
     */
    private FrameView mainView = null;

    public FrameView getMainView() {
        if (mainView == null) {
            mainView = new FrameView(getContext());
        }
        return mainView;
    }

    public void show(View view) {
        if (view == null) {
            throw new IllegalArgumentException("null view");
        }
        JFrame window = null;
        if ((mainView == null) && (view instanceof FrameView)) {
            mainView = (FrameView) view;
            window = ((FrameView) view).getFrame();
        }
        if (window != null) {
            window.setLocationRelativeTo(null);
        }
        if (window != null && window instanceof ExitListener) {
            exitListeners.add((ExitListener) window);
        } else {
            if (view instanceof ExitListener exitListener) {
                exitListeners.add(exitListener);
            }
        }
        RootPaneContainer c = (RootPaneContainer) view.getRootPane().getParent();
        initRootPaneContainer(c);
        ((Component) c).setVisible(true);
    }

    public void hide(View view) {
        view.getRootPane().getParent().setVisible(false);
    }

}
