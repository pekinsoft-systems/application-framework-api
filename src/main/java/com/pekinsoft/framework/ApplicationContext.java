/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   ApplicationContext.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.framework;

import com.pekinsoft.api.*;
import com.pekinsoft.logging.*;
import com.pekinsoft.lookup.Lookup;
import com.pekinsoft.utils.PreferenceKeys;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.prefs.Preferences;
import javax.swing.JComponent;

/**
 * A singleton that manages shared objects, like actions, resources, and tasks,
 * for {@code Applications}.
 * <p>
 * {@link Application Applications} use the {@code ApplicationContext} singleton
 * to find global values and services. The majority of the Swing Application
 * Application API can be accessed through {@code
 * ApplicationContext}. The static {@code getInstance} method returns the
 * singleton Typically it's only called after the application has been
 * {@link Application#launch launched}, however it is always safe to call
 * {@code getInstance}.
 *
 * @see Application
 * @author Hans Muller (Hans.Muller@Sun.COM)
 */
public class ApplicationContext extends AbstractBean {

    /**
     * The {@code Application}-wide preferences.
     */
    public static final int SYSTEM = 0;
    /**
     * The user-specific preferences
     */
    public static final int USER = 1;

    private final List<TaskService> taskServices;
    private final List<TaskService> taskServicesReadOnly;
    private ResourceManager resourceManager = null;
    private ActionManager actionManager = null;
    private LocalStorage localStorage = null;
    private SessionStorage sessionStorage = null;
    private Application application = null;
    private Class applicationClass = null;
    private JComponent focusOwner = null;
    private Clipboard clipboard = null;
    private Throwable uncaughtException = null;
    private TaskMonitor taskMonitor = null;

    private Boolean constructing;
    private boolean serverStarted;

    protected ApplicationContext() {
        constructing = true;
        serverStarted = false;
        taskServices = new CopyOnWriteArrayList<>();
        taskServices.add(new TaskService("default"));
        taskServicesReadOnly = Collections.unmodifiableList(taskServices);
        constructing = false;
    }

    /**
     * Checks to see if the application is being conducted.
     *
     * @return {@code true} if application is being constructed; {@code false}
     *         otherwise
     */
    public boolean isConstructing() {
        return constructing;
    }

    /**
     * Checks if the database server has been started.
     *
     * @return {@code true} if the server has been started; {@code false} if not
     */
    public boolean isServerStarted() {
        return serverStarted;
    }

    void setServerStarted(boolean serverStarted) {
        this.serverStarted = serverStarted;
    }

    /**
     * Get the {@code Application} or user preferences instance.
     *
     * @param type either {@code ApplicationContext.SYSTEM} or {@code
     *          ApplicationContext.USER} constant for the desired preferences
     *
     * @return the specified preferences type
     *
     * @throws IllegalArgumentException if the specified {@code type} is not a
     *                                  valid preferences type
     */
    public final Preferences getPreferences(int type) {
        // If we are in the development environment, always return the user node.
        if (Preferences.userNodeForPackage(applicationClass)
                .getBoolean(PreferenceKeys.PREF_DEVELOPMENT_ENVIRONMENT, false)) {
            return Preferences.userNodeForPackage(applicationClass);
        }

        // Otherwise, in production environments, return the requested node.
        switch (type) {
            case SYSTEM -> {
                return Preferences.systemNodeForPackage(applicationClass);
            }
            case USER -> {
                return Preferences.userNodeForPackage(applicationClass);
            }
            default -> {
                String msg = String.format("Type %s is not a valid preferences "
                        + "type.", type);
                throw new IllegalArgumentException(msg);
            }
        }
    }

    /**
     * Returns the application's class or {@code null} if the application hasn't
     * been
     * launched and this property hasn't been set. Once the application has been
     * launched, the value returned by this method is the same as
     * {@code getApplication().getClass()}.
     *
     * @return the application's class or {@code null}
     *
     * @see #setApplicationClass
     * @see #getApplication
     */
    public final synchronized Class getApplicationClass() {
        if (applicationClass == null) {
            applicationClass = Application.getInstance().getClass();
        }
        return applicationClass;
    }

    /**
     * Called by {@link Application#launch Application.launch()} to record the
     * application's class.
     * <p>
     * This method is only intended for testing, or design time configuration.
     * Normal applications shouldn't need to call it directly.
     *
     * @see #getApplicationClass
     */
    public final synchronized void setApplicationClass(Class applicationClass) {
        if (this.application != null) {
            throw new IllegalStateException("application has been launched");
        }
        this.applicationClass = applicationClass;
    }

    /**
     * The {@code Application} singleton, or {@code null} if {@code launch}
     * hasn't been
     * called yet.
     *
     * @return the launched Application singleton.
     *
     * @see Application#launch
     */
    public final synchronized Application getApplication() {
        return application;
    }

    /*
     * Called by Application.launch().
     */
    synchronized void setApplication(Application application) {
        if (this.application != null) {
            throw new IllegalStateException(
                    "application has already been launched");
        }
        this.application = application;
    }

    /**
     * Retrieves the application's own {@link Logger}. The returned logger
     * will be configured to the currently set {@link Level} for the
     * application.
     * <p>
     * Loggers may not write messages to file immediately, as they are not
     * configured for log files until the application has hit its
     * {@link Application#ready() ready} state. At that time, the loggers know
     * that it is safe to access application resources in order to configure
     * their log file. All messages logged prior to this are buffered until they
     * can be written to file.
     *
     * @return the fully configured logger
     */
    public synchronized Logger getLogger() {
        return getLogger(getApplicationClass());
    }

    /**
     * Retrieves a {@link Logger} for the specified class. The returned logger
     * will be configured to the currently set {@link Level} for the
     * application.
     * <p>
     * Loggers may not write messages to file immediately, as they are not
     * configured for log files until the application has hit its
     * {@link Application#ready() ready} state. At that time, the loggers know
     * that it is safe to access application resources in order to configure
     * their log file. All messages logged prior to this are buffered until they
     * can be written to file.
     *
     * @param clazz the class for the logger
     *
     * @return the fully configured logger
     */
    public synchronized Logger getLogger(Class<?> clazz) {
        return getLogger(clazz.getName());
    }

    /**
     * Retrieves a {@link Logger} with the specified name. The returned logger
     * will be configured to the currently set {@link Level} for the
     * application.
     * <p>
     * Loggers may not write messages to file immediately, as they are not
     * configured for log files until the application has hit its
     * {@link Application#ready() ready} state. At that time, the loggers know
     * that it is safe to access application resources in order to configure
     * their log file. All messages logged prior to this are buffered until they
     * can be written to file.
     *
     * @param name the name for the logger
     *
     * @return the fully configured logger
     */
    public synchronized Logger getLogger(String name) {
        Logger logger = LogManager.getLogger(this, name);
        logger.setLevel(Level.parse(getPreferences(SYSTEM).get(
                PreferenceKeys.PREF_LOG_LEVEL, "INFO")));
        return logger;
    }

    /**
     * The application's {@code ResourceManager} provides read-only cached
     * access to resources in ResourceBundles via the
     * {@link ResourceMap ResourceMap} class.
     *
     * @return this application's ResourceManager.
     *
     * @see #getResourceMap(Class, Class)
     */
    public final ResourceManager getResourceManager() {
        if (resourceManager == null) {
            resourceManager = new ResourceManager(this);
        }
        return resourceManager;
    }

    /**
     * Change this application's {@code ResourceManager}. An
     * {@code ApplicationContext} subclass that wanted to fundamentally change
     * the way {@code ResourceMaps} were created and cached could replace this
     * property in its constructor.
     * <p>
     * Throws an IllegalArgumentException if resourceManager is {@code null}.
     *
     * @param resourceManager the new value of the resourceManager property.
     *
     * @see #getResourceMap(Class, Class)
     * @see #getResourceManager
     */
    protected void setResourceManager(ResourceManager resourceManager) {
        if (resourceManager == null) {
            throw new IllegalArgumentException("null resourceManager");
        }
        Object oldValue = this.resourceManager;
        this.resourceManager = resourceManager;
        firePropertyChange("resourceManager", oldValue, this.resourceManager);
    }

    /**
     * Returns a {@link ResourceMap#getParent chain} of two or more
     * ResourceMaps. The first encapsulates the ResourceBundles defined for the
     * specified class, and its parent encapsulates the ResourceBundles defined
     * for the entire application.
     * <p>
     * This is just a convenience method that calls {@code null} {@code null}
     * {@link ResourceManager#getResourceMap(Class, Class)
     * ResourceManager.getResourceMap()}. It's defined as:
     * <pre>
     * return getResourceManager().getResourceMap(cls, cls);
     * </pre>
     *
     * @param cls the class that defines the location of ResourceBundles
     *
     * @return a {@code ResourceMap} that contains resources loaded from
     *         {@code ResourceBundle}s found in the resources subpackage of the
     *         specified class's package.
     *
     * @see ResourceManager#getResourceMap(Class)
     */
    public final ResourceMap getResourceMap(Class cls) {
        return getResourceManager().getResourceMap(cls, cls);
    }

    /**
     * Returns a {@link ResourceMap#getParent chain} of two or more
     * ResourceMaps. The first encapsulates the ResourceBundles defined for the
     * all of the classes between {@code startClass} and {@code stopClass}
     * inclusive. It's parent encapsulates the ResourceBundles defined for the
     * entire application.
     * <p>
     * This is just a convenience method that calls {@code null} {@code null}
     * {@link ResourceManager#getResourceMap(Class, Class)
     * ResourceManager.getResourceMap()}. It's defined as:
     * <pre>
     * return getResourceManager().getResourceMap(startClass, stopClass);
     * </pre>
     *
     * @param startClass the first class whose ResourceBundles will be included
     * @param stopClass  the last class whose ResourceBundles will be included
     *
     * @return a {@code ResourceMap} that contains resources loaded from
     *         {@code ResourceBundle}s found in the resources subpackage of the
     *         specified class's package.
     *
     * @see ResourceManager#getResourceMap(Class, Class)
     */
    public final ResourceMap getResourceMap(Class startClass, Class stopClass) {
        return getResourceManager().getResourceMap(startClass, stopClass);
    }

    /**
     * Returns the {@link ResourceMap#getParent chain} of ResourceMaps that's
     * shared by the entire application, beginning with the one defined for the
     * Application class, i.e. the value of the {@code applicationClass}
     * property.
     * <p>
     * This is just a convenience method that calls {@code null} {@code null}
     * {@link ResourceManager#getResourceMap()
     * ResourceManager.getResourceMap()}. It's defined as:
     * <pre>
     * return getResourceManager().getResourceMap();
     * </pre>
     *
     * @return the Application's ResourceMap
     *
     * @see ResourceManager#getResourceMap()
     * @see #getApplicationClass
     */
    public final ResourceMap getResourceMap() {
        return getResourceManager().getResourceMap();
    }

    /**
     *
     * @return this application's ActionManager.
     *
     * @see #getActionMap(Object)
     */
    public final ActionManager getActionManager() {
        if (actionManager == null) {
            actionManager = new ActionManager(this);
        }
        return actionManager;
    }

    /**
     * Change this application's {@code ActionManager}. An
     * {@code ApplicationContext} subclass that wanted to fundamentally change
     * the way {@code ActionManagers} were created and cached could replace this
     * property in its constructor.
     * <p>
     * Throws an IllegalArgumentException if actionManager is {@code null}.
     *
     * @param actionManager the new value of the actionManager property.
     *
     * @see #getActionManager
     * @see #getActionMap(Object)
     */
    protected void setActionManager(ActionManager actionManager) {
        if (actionManager == null) {
            throw new IllegalArgumentException("null actionManager");
        }
        Object oldValue = this.actionManager;
        this.actionManager = actionManager;
        firePropertyChange("actionManager", oldValue, this.actionManager);
    }

    /**
     * Returns the shared {@code ActionMap} chain for the entire
     * {@code Application}.
     * <p>
     * This is just a convenience method that calls {@code null} {@code null}
     * {@link ActionManager#getActionMap()
     * ActionManager.getActionMap()}. It's defined as:
     * <pre>
     * return getActionManager().getActionMap()
     * </pre>
     *
     * @return the {@code ActionMap} chain for the entire {@code Application}.
     *
     * @see ActionManager#getActionMap()
     */
    public final ActionMapX getActionMap() {
        return getActionManager().getActionMap();
    }

    /**
     * Returns the {@code ActionMapX} chain for the specified actions
     * class and target object.
     * <p>
     * This is just a convenience method that calls {@code null} {@code null}
     * {@link ActionManager#getActionMap()
     * ActionManager.getActionMap(Class, Object)}. It's defined as:
     * <pre>
     * return getActionManager().getActionMap(actionsClass, actionsObject)
     * </pre>
     *
     * @return the {@code ActionMap} chain for the entire {@code Application}.
     *
     * @see ActionManager#getActionMap(Class, Object)
     */
    public final ActionMapX getActionMap(Class actionsClass,
            Object actionsObject) {
        return getActionManager().getActionMap(actionsClass, actionsObject);
    }

    /**
     * Defined as {@code getActionMap(actionsObject.getClass(), actionsObject)}.
     *
     * @return the {@code ActionMap} for the specified object
     *
     * @see #getActionMap(Class, Object)
     */
    public final ActionMapX getActionMap(Object actionsObject) {
        if (actionsObject == null) {
            throw new IllegalArgumentException("null actionsObject");
        }
        return getActionManager().getActionMap(actionsObject.getClass(),
                actionsObject);
    }

    /**
     * The shared {@link LocalStorage LocalStorage} object.
     *
     * @return the shared {@link LocalStorage LocalStorage} object.
     */
    public final LocalStorage getLocalStorage() {
        if (localStorage == null) {
            localStorage = new LocalStorage(this);
        }
        return localStorage;
    }

    /**
     * The shared {@link LocalStorage LocalStorage} object.
     *
     * @param localStorage the shared {@link LocalStorage LocalStorage} object.
     */
    protected void setLocalStorage(LocalStorage localStorage) {
        if (localStorage == null) {
            throw new IllegalArgumentException("null localStorage");
        }
        Object oldValue = this.localStorage;
        this.localStorage = localStorage;
        firePropertyChange("localStorage", oldValue, this.localStorage);
    }

    /**
     * The shared {@link SessionStorage SessionStorage} object.
     *
     * @return the shared {@link SessionStorage SessionStorage} object.
     */
    public final SessionStorage getSessionStorage() {
        if (sessionStorage == null) {
            sessionStorage = new SessionStorage(this);
        }
        return sessionStorage;
    }

    /**
     * The shared {@link SessionStorage SessionStorage} object.
     *
     * @param sessionStorage the shared {@link SessionStorage SessionStorage}
     *                       object.
     */
    protected void setSessionStorage(SessionStorage sessionStorage) {
        if (sessionStorage == null) {
            throw new IllegalArgumentException("null sessionStorage");
        }
        Object oldValue = this.sessionStorage;
        this.sessionStorage = sessionStorage;
        firePropertyChange("sessionStorage", oldValue, this.sessionStorage);
    }

    /**
     * A shared {@code Clipboard}.
     */
    public Clipboard getClipboard() {
        if (clipboard == null) {
            try {
                clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            } catch (SecurityException e) {
                clipboard = new Clipboard("sandbox");
            }
        }
        return clipboard;
    }

    /**
     * The application's focus owner.
     */
    public JComponent getFocusOwner() {
        return focusOwner;
    }

    void setFocusOwner(JComponent focusOwner) {
        Object oldValue = this.focusOwner;
        this.focusOwner = focusOwner;
        firePropertyChange("focusOwner", oldValue, this.focusOwner);
    }

    private List<TaskService> copyTaskServices() {
        return new ArrayList<>(taskServices);
    }

    public void addTaskService(TaskService taskService) {
        if (taskService == null) {
            throw new IllegalArgumentException("null taskService");
        }
        List<TaskService> oldValue = null, newValue = null;
        boolean changed = false;
        synchronized (taskServices) {
            if (!taskServices.contains(taskService)) {
                oldValue = copyTaskServices();
                taskServices.add(taskService);
                newValue = copyTaskServices();
                changed = true;
            }
        }
        if (changed) {
            firePropertyChange("taskServices", oldValue, newValue);
        }
    }

    public void removeTaskService(TaskService taskService) {
        if (taskService == null) {
            throw new IllegalArgumentException("null taskService");
        }
        List<TaskService> oldValue = null, newValue = null;
        boolean changed = false;
        synchronized (taskServices) {
            if (taskServices.contains(taskService)) {
                oldValue = copyTaskServices();
                taskServices.remove(taskService);
                newValue = copyTaskServices();
                changed = true;
            }
        }
        if (changed) {
            firePropertyChange("taskServices", oldValue, newValue);
        }
    }

    public TaskService getTaskService(String name) {
        if (name == null) {
            throw new IllegalArgumentException("null name");
        }
        for (TaskService taskService : taskServices) {
            if (name.equals(taskService.getName())) {
                return taskService;
            }
        }
        return null;
    }

    /**
     * Returns the default TaskService, i.e. the one named "default":
     * {@code return getTaskService("default")}. The
     * {@link ActionX#actionPerformed ActionX actionPerformed}
     * method executes background {@code BackgroundTask}s on the default
     * TaskService.
     * Application's can launch Tasks in the same way, e.g.
     * <pre>
     * ApplicationContext.getInstance().getTaskService().execute(myTask);
     * </pre>
     *
     * @return the default TaskService.
     *
     * @see #getTaskService(String)
     *
     */
    public final TaskService getTaskService() {
        return getTaskService("default");
    }

    /**
     * Returns a read-only view of the complete list of TaskServices.
     *
     * @return a list of all of the TaskServices.
     *
     * @see #addTaskService
     * @see #removeTaskService
     */
    public List<TaskService> getTaskServices() {
        return taskServicesReadOnly;
    }

    /**
     * Returns a shared TaskMonitor object. Most applications only need one
     * TaskMonitor for the sake of status bars and other status indicators.
     *
     * @return the shared TaskMonitor object.
     */
    public final TaskMonitor getTaskMonitor() {
        if (taskMonitor == null) {
            taskMonitor = new TaskMonitor(this);
        }
        return taskMonitor;
    }

    /**
     * Retrieves the {@link WindowManager} instance for the application.
     *
     * @return the application's {@code WindowManager}
     */
    public WindowManager getWindowManager() {
        if (getApplication().getMainView() instanceof WindowManager wm) {
            return wm;
        } else {
            return Lookup.getDefault().lookup(WindowManager.class
            );
        }
    }

    /**
     * Retrieves the {@link DockingManager} instance for the application. The
     * {@code DockingManager} is an extension of the {@link WindowManager}
     * interface, so calling this method alone will retrieve the object that
     * handles showing {@link com.pekinsoft.desktop.Dockable Dockables}, as well
     * as
     * the object that handles showing {@link javax.swing.JFrame JFrames} and
     * {@link javax.swing.JDialog JDialogs}.
     *
     * @return the application's {@code DockingManager}
     */
    public DockingManager getDockingManager() {
        if (getApplication().getMainView() instanceof DockingManager dm) {
            return dm;
        } else {
            return Lookup.getDefault().lookup(DockingManager.class
            );
        }
    }

    /**
     * Retrieves the {@link StatusDisplayer} instance for the application. The
     * {@code StatusDisplayer} may be used for showing specialized messages to
     * the user. These messages can change the {@link java.awt.Font Font} and
     * foreground {@link java.awt.Color Color} of the displayed message to have
     * it call more attention to itself.
     *
     * @return the application's {@code StatusDisplayer}
     */
    public StatusDisplayer getStatusDisplayer() {
        if (getApplication().getMainView().getStatusBar() instanceof StatusDisplayer sd) {
            return sd;
        } else {
            return Lookup.getDefault().lookup(StatusDisplayer.class);
        }
    }

    /**
     * Retrieves the {@link ProgressHandler} instance for the application. The
     * {@code ProgressHandler} provides a means of updating a progress bar that
     * is shown to the user showing the on-going progress of a long-running or
     * background task.
     *
     * @return the application's {@code ProgressHandler}
     */
    public ProgressHandler getProgressHandler() {
        if (getApplication().getMainView().getStatusBar() instanceof ProgressHandler ph) {
            return ph;
        } else {
            return Lookup.getDefault().lookup(ProgressHandler.class);
        }
    }
}
