/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   StorageLocations.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.framework;

/**
 * An enumeration of storage locations accessible from within an 
 * {@link Application}, which follows the rules established by the operating
 * system on which the {@code Application} is running.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 1.0
 */
public enum StorageLocations {
    /**
     * The primary directory for the {@link Application}.
     */
    APP_DIR,
    /**
     * The {@link Application Application's} configuration directory. This is
     * where all configuration and session state files should be stored.
     */
    CONFIG_DIR,
    /**
     * The {@link Application Application's} data directory. For any data files
     * created from within the application, as well as any embedded database the
     * application may create/use, this is the directory in which these files
     * should be created.
     */
    DATA_DIR,
    /**
     * If the {@link Application} stores error output files, they should be put
     * into this directory.
     */
    ERROR_DIR,
    /**
     * This storage location is a catch-all location for creating/reading files,
     * which starts from the root of the file system and has the complete path
     * to the named file.
     */
    FILE_SYSTEM,
    /**
     * The {@link Application} should store all of its generated log files in
     * this directory.
     */
    LOG_DIR,
    /**
     * The user's home directory. This is the value obtained from the Java
     * runtime's system properties.
     */
    USER_HOME
}
