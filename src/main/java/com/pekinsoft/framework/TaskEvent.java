/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   TaskEvent.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.framework;

import java.util.EventObject;

/**
 * An encapsulation of the value produced one of the `BackgroundTask` execution
 * methods: `doInBackground()`, `process`, `done`. The source
 * of a `TaskEvent` is the `BackgroundTask` that produced the value.
 *
 * @see TaskListener
 * @see BackgroundTask
 */
public class TaskEvent<T> extends EventObject {

    private final T value;

    /**
     * Returns the value this event represents.
     *
     * @return the {@code value} constructor argument.
     */
    public final T getValue() {
        return value;
    }

    /**
     * Construct a `TaskEvent`.
     *
     * @param source the `BackgroundTask` that produced the value.
     * @param value the value, {@code null} if type `T` is `Void`.
     */
    public TaskEvent(BackgroundTask source, T value) {
        super(source);
        this.value = value;
    }
}
