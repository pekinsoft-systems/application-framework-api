/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   AbstractBean.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 13, 2024
 *  Modified   :   Jul 13, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.framework;

import java.beans.*;
import javax.swing.event.SwingPropertyChangeSupport;

/**
 * An encapsulation of the {@link PropertyChangeSupport} methods based on
 * java.beans.PropertyChangeSupport. {@link PropertyChangeListener}s are fired 
 * on the
 * event dispatching thread.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 2.3
 * @since 1.0
 */
public class AbstractBean {

    private final PropertyChangeSupport pcs;

    public AbstractBean() {
        pcs = new SwingPropertyChangeSupport(this, true);
    }

    /**
     * Add a PropertyChangeListener to the listener list. The listener is
     * registered for all properties and its {@code propertyChange} method will
     * run on the event dispatching thread.
     * 
     * If {@code listener} is null, no exception is thrown and no action is
     * taken.
     *
     * @param listener the PropertyChangeListener to be added.
     * @see #removePropertyChangeListener
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener
     */
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    /**
     * Remove a PropertyChangeListener from the listener list.
     * 
     * If {@code listener} is null, no exception is thrown and no action is
     * taken.
     *
     * @param listener the PropertyChangeListener to be removed.
     * @see #addPropertyChangeListener
     * @see java.beans.PropertyChangeSupport#removePropertyChangeListener
     */
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    /**
     * Add a PropertyChangeListener for a specific property. The listener will
     * be invoked only when a call on firePropertyChange names that specific
     * property. The same listener object may be added more than once. For each
     * property, the listener will be invoked the number of times it was added
     * for that property. If {@code propertyName} or {@code listener}
     * is null, no exception is thrown and no action is taken.
     *
     * @param propertyName The name of the property to listen on.
     * @param listener the PropertyChangeListener to be added
     * @see java.beans.PropertyChangeSupport#addPropertyChangeListener(String,
     * PropertyChangeListener)
     */
    public void addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(propertyName, listener);
    }

    /**
     * Remove a PropertyChangeListener for a specific property. If
     * {@code listener} was added more than once to the same event source
     * for the specified property, it will be notified one less time after being
     * removed. If {@code propertyName} is null, no exception is thrown and
     * no action is taken. If {@code listener} is null, or was never added
     * for the specified property, no exception is thrown and no action is
     * taken.
     *
     * @param propertyName The name of the property that was listened on.
     * @param listener The PropertyChangeListener to be removed
     * @see
     * java.beans.PropertyChangeSupport#removePropertyChangeListener(String,
     * PropertyChangeListener)
     */
    public synchronized void removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(propertyName, listener);
    }

    /**
     * An array of all of the {@code PropertyChangeListener}s added so far.
     *
     * @return all of the {@code PropertyChangeListener}s added so far.
     * @see java.beans.PropertyChangeSupport#getPropertyChangeListeners
     */
    public PropertyChangeListener[] getPropertyChangeListeners() {
        return pcs.getPropertyChangeListeners();
    }

    /**
     * Called whenever the value of a bound property is set.
     * 
     * If oldValue is not equal to newValue, invoke the {@code propertyChange} method 
     * on all of the {@code PropertyChangeListener}s added so far, on the event
     * dispatching thread.
     *
     * @param propertyName the name of the property that was changed
     * @param oldValue the old value of the property
     * @param newValue the new value of the property
     * @see #addPropertyChangeListener
     * @see #removePropertyChangeListener
     * @see java.beans.PropertyChangeSupport#firePropertyChange(String, Object,
     * Object)
     */
    public void firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (oldValue != null && newValue != null && oldValue.equals(newValue)) {
            return;
        }
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }
    
    /**
     * Called whenever the value of the bound {@code boolean} property changes.
     * 
     * If {@code oldValue} is not equal to {@code newValue}, invoke the
     * {@code propertyChange} method on all of the {@code PropertyChangeListener}s
     * added so far, on the EDT.
     * 
     * @param propertyName the name of the property that was changed
     * @param oldValue the old value of the property
     * @param newValue the new value of the property
     * @see #addPropertyChangeListener
     * @see #removePropertyChangeListener
     * @see java.beans.PropertyChangeSupport#firePropertyChange(java.lang.String, 
     * boolean, boolean) 
     */
    protected void firePropertyChange(String propertyName, boolean oldValue, boolean newValue) {
        if (oldValue == newValue) {
            return;
        }
        
        pcs.firePropertyChange(propertyName, oldValue, newValue);
    }

    /**
     * Fire an existing PropertyChangeEvent
     * 
     * If the event's oldValue property is not equal to newValue, invoke the
     * {@code propertyChange} method on all of the {@code PropertyChangeListener}s added so 
     * far, on the event dispatching thread.
     *
     * @param e the {@code PropertyChangeEvent} that was fired
     * 
     * @see #addPropertyChangeListener
     * @see #removePropertyChangeListener
     * @see java.beans.PropertyChangeSupport#firePropertyChange(PropertyChangeEvent e)
     */
    protected void firePropertyChange(PropertyChangeEvent e) {
        pcs.firePropertyChange(e);
    }

}
