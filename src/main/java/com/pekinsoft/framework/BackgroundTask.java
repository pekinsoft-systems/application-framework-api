/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   BackgroundTask.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.framework;

import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.concurrent.*;
import javax.swing.SwingWorker;
import javax.swing.SwingWorker.StateValue;

/**
 * A type of {@link SwingWorker} that represents an application background task.
 * Tasks add descriptive properties that can be shown to the user, a new set of
 * methods for customizing task completion, support for blocking input to the
 * GUI while the BackgroundTask is executing, and a `TaskListener` that enables
 * one to monitor the three key SwingWorker methods: `doInBackground`, `process`
 * and `done`.When a BackgroundTask completes, the `final done` method invokes
 * one of `succeeded`, `cancelled`, `interrupted`, or `failed`. The `final done`
 * method invokes `finished` when the completion method returns or throws an
 * exception.
 * <p>
 * Tasks should provide localized values for the `title`, `description`, and
 * `message` properties in a ResourceBundle for the BackgroundTask subclass. A
 * {@link ResourceMap} is loaded automatically using the BackgroundTask subclass
 * as the {@code startClass} and BackgroundTask.class the {@code stopClass}.
 * This ResourceMap is also used to look up format strings used in calls to
 * {@link #message message}, which is used to set the `message` property.
 * <p>
 * For example: given a BackgroundTask called `MyTask` defined like this:{@snippet lang="java":
 * class MyTask extends BackgroundTask<MyResultType, Void> { protected
 * MyResultType doInBackground() { message("startMessage",
 * getPlannedSubtaskCount()); // do the work ... if an error is encountered:
 * message("errorMessage"); message("finishedMessage", getActualSubtaskCount(),
 * getFailureCount()); // .. return the result }
 * }
 * }
 * Typically the resources for this class would be defined in the MyTask
 * ResourceBundle, `resources/MyTask.properties`:
 * <pre>
 * title = My BackgroundTask
 * description = A task of mine for my own purposes.
 * startMessage = Starting: working on %s subtasks...
 * errorMessage = An unexpected error occurred, skipping subtask
 * finishedMessage = Finished: completed %1$s subtasks, %2$s failures
 * </pre>
 * <p>
 * BackgroundTask subclasses can override resource values in their own
 * ResourceBundles:
 * <pre>
 * class MyTaskSubclass extends MyTask {
 * }
 * </pre>
 * <pre>
 * # resources/MyTaskSubclass.properties title = My BackgroundTask
 * Subclass description = An appropriate description
 * # ... all other resources are inherited
 * </pre>
 * <p>
 * Tasks can specify that input to the GUI is to be blocked while they're being
 * executed. The `inputBlocker` property specifies what part of the GUI is to be
 * blocked and how that's accomplished. The `inputBlocker` is set automatically
 * when an {@code &#064;AppAction} method that returns a BackgroundTask
 * specifies a
 * {@link BlockingScope} value for the {@code block} annotation parameter. To
 * customize the way blocking is implemented you can define your own
 * `BackgroundTask.InputBlocker`. For example, assume that `busyGlassPane` is a
 * component that consumes (and ignores) keyboard and mouse input:{@snippet lang="java":
 * class MyInputBlocker extends InputBlocker {
 *     BusyIndicatorInputBlocker(BackgroundTask task) {
 *         super(task, BackgroundTask.BlockingScope.WINDOW, myGlassPane);
 *     }
 *     protected void block() {
 *         myFrame.setGlassPane(myGlassPane);
 *         busyGlassPane.setVisible(true);
 *     }
 *     protected void unblock() {
 *         busyGlassPane.setVisible(false);
 *     }
 * }
 * // ...
 * myTask.setInputBlocker(new MyInputBlocker(myTask));
 * }
 * All of the settable properties in this class are bound, i.e. a
 * PropertyChangeEvent is fired when the value of the property changes. As with
 * the `SwingWorker` superclass, all {@code PropertyChangeListener}s run on the
 * event dispatching thread. This is also true of `TaskListener`s.
 * <p>
 * Unless specified otherwise specified, this class is thread-safe. All of the
 * BackgroundTask properties can be get/set on any thread.
 *
 * @param <T> the result type returned by the `doInBackground` method
 * @param <V> the intermediate result type returned by methods such as `process`
 *            and `publish`
 *
 * @author Hans Muller (Hans.Muller@Sun.COM)
 * @see ApplicationContext
 * @see ResourceMap
 * @see TaskListener
 * @see TaskEvent
 */
public abstract class BackgroundTask<T, V> extends SwingWorker<T, V> {

    private final Logger logger;
    private final Application application;
    private String resourcePrefix;
    private ResourceMap resourceMap;
    private List<TaskListener<T, V>> taskListeners;
    private InputBlocker inputBlocker;
    private String name = null;
    private String title = null;
    private String description = null;
    private long messageTime = -1L;
    private String message = null;
    private long startTime = -1L;
    private long doneTime = -1L;
    private boolean userCanCancel = true;
    private boolean progressPropertyIsValid = false;
    private TaskService taskService = null;

    /**
     * Specifies to what extent the GUI should be blocked a BackgroundTask is
     * executed by a TaskService. Input blocking is carried out by the
     * BackgroundTask's {@link #getInputBlocker inputBlocker}.
     *
     * @see BackgroundTask.InputBlocker
     * @see AppAction#block
     */
    public enum BlockingScope {
        /**
         * Don't block the GUI while this BackgroundTask is executing.
         */
        NONE,
        /**
         * Block an {@link ActionX AppAction} while the task is executing,
         * typically by temporarily disabling it.
         */
        ACTION,
        /**
         * Block a component while the task is executing, typically by
         * temporarily disabling it.
         */
        COMPONENT,
        /**
         * Block a top level window while the task is executing, typically by
         * showing a window-modal dialog.
         */
        WINDOW,
        /**
         * Block all of the application's top level windows, typically by
         * showing a application-modal dialog.
         */
        APPLICATION
    };

    private void initTask(ResourceMap resourceMap, String title) {
        this.resourceMap = resourceMap;
        if ((title == null) || (title.length() == 0)) {
            resourcePrefix = "";
        } else if (title.endsWith(".")) {
            resourcePrefix = title;
        } else {
            resourcePrefix = title + ".";
        }
        if (resourceMap != null) {
            title = resourceMap.getString(resourceName("title"));
            description = resourceMap.getString(resourceName("description"));
            message = resourceMap.getString(resourceName("message"));
            if (message != null) {
                messageTime = System.currentTimeMillis();
            }
        }
        addPropertyChangeListener(new StatePCL());
        taskListeners = new CopyOnWriteArrayList<>();
    }

    private ResourceMap defaultResourceMap(Application application) {
        return application.getContext().getResourceMap(getClass(),
                BackgroundTask.class);
    }

    /**
     *
     * Construct a `BackgroundTask` with the specified resource name prefix,
     * whose ResourceMap is the value of `
     * ApplicationContext.getInstance().getResourceMap(this.getClass(),
     * BackgroundTask.class) `. The `resourcePrefix` is used to construct the
     * resource names for the intial values of the `title`, `description`, and
     * `message` BackgroundTask properties and for message
     * {@link java.util.Formatter format} strings.
     *
     * @param application the {@link com.pekinsoft.framework.Application
     * Application} in which this `BackgroundTask` is being utilized
     * @param title       prefix for resource names, can be {@code null}
     *
     * @see #getResourceMap
     * @see #setTitle
     * @see #setDescription
     * @see #setMessage
     * @see #resourceName
     * @see ApplicationContext#getResourceMap
     */
    public BackgroundTask(Application application, String title) {
        this.application = application;
        initTask(defaultResourceMap(application), title);
        logger = application.getContext()
                .getLogger(BackgroundTask.class.getName() + "-" + getTitle());
    }

    /**
     * Construct a `BackgroundTask` with an empty (`""`) resource name prefix,
     * whose ResourceMap is the value of
     * `ApplicationContext.getInstance().getResourceMap(this.getClass(),
     * BackgroundTask.class)`.
     */
    public BackgroundTask(Application application) {
        this(application, "[UNNAMED]");
    }

    public final Application getApplication() {
        return application;
    }

    public final ApplicationContext getContext() {
        return getApplication().getContext();
    }

    /**
     * Returns the TaskService that this BackgroundTask has been submitted to,
     * or {@code null}. This property is set when a task is executed by a
     * TaskService, cleared when the task is done and all of its completion
     * methods have run.
     * <p>
     * This is a read-only bound property.
     *
     * @return the value of the taskService property.
     *
     * @see TaskService#execute
     * @see #done
     */
    public synchronized TaskService getTaskService() {
        return taskService;
    }

    /**
     * Set when a task is executed by a TaskService, cleared when the task is
     * done and all of its completion methods have run.
     */
    synchronized void setTaskService(TaskService taskService) {
        TaskService oldTaskService, newTaskService;
        synchronized (this) {
            oldTaskService = this.taskService;
            this.taskService = taskService;
            newTaskService = this.taskService;
        }
        firePropertyChange("taskService", oldTaskService, newTaskService);
    }

    /**
     * Returns a BackgroundTask resource name with the specified suffix.
     * BackgroundTask resource names are the simple name of the constructor's
     * `resourceClass` parameter, followed by ".", followed by `suffix`. If the
     * resourceClass parameter was null, then this method returns an empty
     * string.
     * <p>
     * This method would only be of interest to subclasses that wanted to look
     * up additional BackgroundTask resources (beyond `title`, `message`, etc..)
     * using the same naming convention.
     *
     * @param suffix the resource name's suffix
     *
     * @see #getResourceMap
     * @see #message
     */
    protected final String resourceName(String suffix) {
        return resourcePrefix + suffix;
    }

    /**
     * Returns the {@code ResourceMap} used by the constructor to initialize the
     * `title`, `message`, etc properties, and by the {@link #message message}
     * method to look up format strings.
     *
     * @return this BackgroundTask's {@code ResourceMap}
     *
     * @see #resourceName
     */
    public final ResourceMap getResourceMap() {
        return resourceMap;
    }

    /**
     * Return the value of the `title` property. The default value of this
     * property is the value of the {@link #getResourceMap resourceMap's}
     * `title` resource.
     * <p>
     * Returns a brief one-line description of the this BackgroundTask that
     * would be useful for describing this task to the user. The default value
     * of this property is {@code null}.
     *
     * @return the value of the `title` property.
     *
     * @see #setTitle
     * @see #setDescription
     * @see #setMessage
     */
    public synchronized String getTitle() {
        return title;
    }

    /**
     * Set the `title` property. The default value of this property is the value
     * of the {@link #getResourceMap resourceMap's} `title` resource.
     * <p>
     * The title is a brief one-line description of the this BackgroundTask that
     * would be useful for describing it to the user. The `title` should be
     * specific to this BackgroundTask, for example "Loading image sunset.png"
     * is better than "Image Loader". Similarly the title isn't intended for
     * ephemeral messages, like "Loaded 27.3% of sunset.png". The
     * {@link #setMessage message} property is for reporting the
     * BackgroundTask's current status.
     *
     * @param title a brief one-line description of the this BackgroundTask.
     *
     * @see #getTitle
     * @see #setDescription
     * @see #setMessage
     */
    protected void setTitle(String title) {
        String oldTitle, newTitle;
        synchronized (this) {
            oldTitle = this.title;
            this.title = title;
            newTitle = this.title;
        }
        firePropertyChange("title", oldTitle, newTitle);
    }

    /**
     * Return the value of the `description` property. The default value of this
     * property is the value of the {@link #getResourceMap resourceMap's}
     * `description` resource.
     * <p>
     * A longer version of the BackgroundTask's title; a few sentences that
     * describe what the BackgroundTask is for in terms that an application user
     * would understand.
     *
     * @return the value of the `description` property.
     *
     * @see #setDescription
     * @see #setTitle
     * @see #setMessage
     */
    public synchronized String getDescription() {
        return description;
    }

    /**
     * Set the `description` property. The default value of this property is the
     * value of the {@link #getResourceMap resourceMap's} `description`
     * resource.
     * <p>
     * The description is a longer version of the BackgroundTask's title. It
     * should be a few sentences that describe what the BackgroundTask is for,
     * in terms that an application user would understand.
     *
     * @param description a few sentences that describe what this BackgroundTask
     *                    is for.
     *
     * @see #getDescription
     * @see #setTitle
     * @see #setMessage
     */
    protected void setDescription(String description) {
        String oldDescription, newDescription;
        synchronized (this) {
            oldDescription = this.description;
            this.description = description;
            newDescription = this.description;
        }
        firePropertyChange("description", oldDescription, newDescription);
    }

    /**
     * Returns the length of time this BackgroundTask has run. If the task
     * hasn't started yet (i.e. if its state is still `StateValue.PENDING`),
     * then this method returns 0. Otherwise it returns the duration in the
     * specified time units. For example, to learn how many seconds a
     * BackgroundTask has run so far:
     * <pre>
     * long nSeconds = myTask.getExecutionDuration(TimeUnit.SECONDS);
     * </pre>
     *
     * @param unit the time unit of the return value
     *
     * @return the length of time this BackgroundTask has run.
     *
     * @see #execute
     */
    public long getExecutionDuration(TimeUnit unit) {
        long startTime, doneTime, dt;
        synchronized (this) {
            startTime = this.startTime;
            doneTime = this.doneTime;
        }
        if (startTime == -1L) {
            dt = 0L;
        } else if (doneTime == -1L) {
            dt = System.currentTimeMillis() - startTime;
        } else {
            dt = doneTime - startTime;
        }
        return unit.convert(Math.max(0L, dt), TimeUnit.MILLISECONDS);
    }

    /**
     * Return the value of the `message` property. The default value of this
     * property is the value of the {@link #getResourceMap resourceMap's}
     * `message` resource.
     * <p>
     * Returns a short, one-line, message that explains what the task is up to
     * in terms appropriate for an application user.
     *
     * @return a short one-line status message.
     *
     * @see #setMessage
     * @see #getMessageDuration
     */
    public String getMessage() {
        return message;
    }

    /**
     * Set the `message` property. The default value of this property is the
     * value of the {@link #getResourceMap resourceMap's} `message` resource.
     * <p>
     * Returns a short, one-line, message that explains what the task is up to
     * in terms appropriate for an application user. This message should reflect
     * that BackgroundTask's dynamic state and can be reset as frequently one
     * could reasonably expect a user to understand. It should not repeat the
     * information in the BackgroundTask's title and should not convey any
     * information that the user shouldn't ignore.
     * <p>
     * For example, a BackgroundTask whose `doInBackground` method loaded a
     * photo from a web service might set this property to a new value each time
     * a new internal milestone was reached, e.g.:
     * <pre>
     * loadTask.setTitle("Loading photo from http://photos.com/sunset");
     * // ...
     * loadTask.setMessage("opening connection to photos.com");
     * // ...
     * loadTask.setMessage("reading thumbnail image file sunset.png");
     * // ... etc
     * </pre>
     * <p>
     * Each time this property is set, the {@link #getMessageDuration
     * messageDuration} property is reset. Since status messages are intended to
     * be ephemeral, application GUI elements like status bars may want to clear
     * messages after 20 or 30 seconds have elapsed.
     * <p>
     * Localized messages that require parameters can be constructed with the
     * {@link #message message} method.
     *
     * @param message a short one-line status message.
     *
     * @see #getMessage
     * @see #getMessageDuration
     * @see #message
     */
    protected void setMessage(String message) {
        String oldMessage, newMessage;
        synchronized (this) {
            oldMessage = this.message;
            this.message = message;
            newMessage = this.message;
            messageTime = System.currentTimeMillis();
        }
        firePropertyChange("message", oldMessage, newMessage);
    }

    /**
     * Set the message property to a string generated with `String.format` and
     * the specified arguments. The `formatResourceKey` names a resource whose
     * value is a format string. See the BackgroundTask class JavaDoc for an
     * example.
     * <p>
     * Note that if the no arguments are specified, this method is comparable
     * to:
     * <pre>
     * setMessage(getResourceMap().getString(resourceName(formatResourceKey)));
     * </pre>
     * <p>
     * If a {@code ResourceMap} was not specified for this BackgroundTask, then
     * set the `message` property to `formatResourceKey`.
     *
     * @param formatResourceKey the suffix of the format string's resource name.
     * @param args              the arguments referred to by the placeholders in
     *                          the format string
     *
     * @see #setMessage
     * @see ResourceMap#getString(String, Object...)
     * @see java.text.MessageFormat
     */
    protected final void message(String formatResourceKey, Object... args) {
        ResourceMap resourceMap = getResourceMap();
        if (resourceMap != null) {
            setMessage(resourceMap.getString(resourceName(formatResourceKey),
                    args));
        } else {
            setMessage(formatResourceKey);
        }
    }

    /**
     * Returns the length of time that has elapsed since the `message` property
     * was last set.
     *
     * @param unit units for the return value
     *
     * @return elapsed time since the `message` property was last set.
     *
     * @see #setMessage
     */
    public long getMessageDuration(TimeUnit unit) {
        long messageTime;
        synchronized (this) {
            messageTime = this.messageTime;
        }
        long dt = (messageTime == -1L) ? 0L : Math.max(0L,
                System.currentTimeMillis() - messageTime);
        return unit.convert(dt, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns the value of the `userCanCancel` property. The default value of
     * this property is true.
     * <p>
     * Generic GUI components, like a BackgroundTask progress dialog, can use
     * this property to decide if they should provide a way for the user to
     * cancel this task.
     *
     * @return true if the user can cancel this BackgroundTask.
     *
     * @see #setUserCanCancel
     */
    public synchronized boolean getUserCanCancel() {
        return userCanCancel;
    }

    /**
     * Sets the `userCanCancel` property. The default value of this property is
     * true.
     * <p>
     * Generic GUI components, like a BackgroundTask progress dialog, can use
     * this property to decide if they should provide a way for the user to
     * cancel this task. For example, the value of this property might be bound
     * to the enabled property of a cancel button.
     * <p>
     * This property has no effect on the {@link #cancel} cancel method. It's
     * just advice for GUI components that display this BackgroundTask.
     *
     * @param userCanCancel true if the user should be allowed to cancel this
     *                      BackgroundTask.
     *
     * @see #getUserCanCancel
     */
    protected void setUserCanCancel(boolean userCanCancel) {
        boolean oldValue, newValue;
        synchronized (this) {
            oldValue = this.userCanCancel;
            this.userCanCancel = userCanCancel;
            newValue = this.userCanCancel;
        }
        firePropertyChange("userCanCancel", oldValue, newValue);
    }

    /**
     * Returns true if the {@link #setProgress progress} property has been set.
     * Some Tasks don't update the progress property because it's difficult or
     * impossible to determine how what percentage of the task has been
     * completed. GUI elements that display BackgroundTask progress, like an
     * application status bar, can use this property to set the @{link
     * JProgressBar#indeterminate indeterminate} `JProgressBar` property.
     * <p>
     * A task that does keep the progress property up to date should initialize
     * it to 0, to ensure that `isProgressPropertyValid` is always true.
     *
     * @return true if the {@link #setProgress progress} property has been set.
     *
     * @see #setProgress
     */
    public synchronized boolean isProgressPropertyValid() {
        return progressPropertyIsValid;
    }

    /**
     * A convenience method that sets the `progress` property to the following
     * ratio normalized to 0 .. 100.
     * <pre>
     * value - min / max - min
     * </pre>
     *
     * @param value a value in the range min ... max, inclusive
     * @param min   the minimum value of the range
     * @param max   the maximum value of the range
     *
     * @see #setProgress(int)
     */
    protected final void setProgress(int value, int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("invalid range: min >= max");
        }
        if ((value < min) || (value > max)) {
            throw new IllegalArgumentException("invalid value");
        }
        float percentage = (float) (value - min) / (float) (max - min);
        setProgress(Math.round(percentage * 100.0f));
    }

    /**
     * A convenience method that sets the `progress` property to `percentage *
     * 100`.
     *
     * @param percentage a value in the range 0.0 ... 1.0 inclusive
     *
     * @see #setProgress(int)
     */
    protected final void setProgress(float percentage) {
        if ((percentage < 0.0) || (percentage > 1.0)) {
            throw new IllegalArgumentException("invalid percentage");
        }
        setProgress(Math.round(percentage * 100.0f));
    }

    /**
     * A convenience method that sets the `progress` property to the following
     * ratio normalized to 0 .. 100.
     * <pre>
     * value - min / max - min
     * </pre>
     *
     * @param value a value in the range min ... max, inclusive
     * @param min   the minimum value of the range
     * @param max   the maximum value of the range
     *
     * @see #setProgress(int)
     */
    protected final void setProgress(float value, float min, float max) {
        if (min >= max) {
            throw new IllegalArgumentException("invalid range: min >= max");
        }
        if ((value < min) || (value > max)) {
            throw new IllegalArgumentException("invalid value");
        }
        float percentage = (value - min) / (max - min);
        setProgress(Math.round(percentage * 100.0f));
    }

    /**
     * Equivalent to `getState() == StateValue.PENDING`.
     * <p>
     * When a pending BackgroundTask's state changes to `StateValue.STARTED` a
     * PropertyChangeEvent for the "started" property is fired. Similarly when a
     * started BackgroundTask's state changes to `StateValue.DONE`, a "done"
     * PropertyChangeEvent is fired.
     */
    public final boolean isPending() {
        return getState() == StateValue.PENDING;
    }

    /**
     * Equivalent to `getState() == StateValue.STARTED`.
     * <p>
     * When a pending BackgroundTask's state changes to `StateValue.STARTED` a
     * PropertyChangeEvent for the "started" property is fired. Similarly when a
     * started BackgroundTask's state changes to `StateValue.DONE`, a "done"
     * PropertyChangeEvent is fired.
     */
    public final boolean isStarted() {
        return getState() == StateValue.STARTED;
    }

    /**
     * {@inheritDoc}
     * <p>
     * This method fires the TaskListeners' {@link TaskListener#process process}
     * method. If you override `process` and do not call
     * {@code super.process(values)}, then the TaskListeners will not run.
     *
     * @param values {@inheritDoc}
     */
    @Override
    protected void process(List<V> values) {
        fireProcessListeners(values);
    }

    @Override
    protected final void done() {
        try {
            if (isCancelled()) {
                cancelled();
            } else {
                try {
                    succeeded(get());
                } catch (InterruptedException e) {
                    interrupted(e);
                } catch (ExecutionException e) {
                    failed(e.getCause());
                }
            }
        } finally {
            try {
                finished();
            } finally {
                setTaskService(null);
            }
        }
    }

    /**
     * Called when this BackgroundTask has been cancelled by
     * {@link #cancel(boolean)}.
     * <p>
     * This method runs on the EDT. It does nothing by default.
     *
     * @see #done
     */
    protected void cancelled() {
    }

    /**
     * Called when this BackgroundTask has successfully completed, i.e. when its
     * `get` method returns a value. Tasks that compute a value should override
     * this method.
     * <p>
     * <p>
     * This method runs on the EDT. It does nothing by default.
     *
     * @param result the value returned by the `get` method
     *
     * @see #done
     * @see #get
     * @see #failed
     */
    protected void succeeded(T result) {
    }

    /**
     * Called if the BackgroundTask's Thread is interrupted but not explicitly
     * cancelled.
     * <p>
     * This method runs on the EDT. It does nothing by default.
     *
     * @param e the `InterruptedException` thrown by `get`
     *
     * @see #cancel
     * @see #done
     * @see #get
     */
    protected void interrupted(InterruptedException e) {
    }

    /**
     * Called when an execution of this BackgroundTask fails and an
     * `ExecutionExecption` is thrown by `get`.
     * <p>
     * This method runs on the EDT. It Logs an error message by default.
     *
     * @param cause the {@link Throwable#getCause cause} of the
     *              {@code ExecutionException}
     *
     * @see #done
     * @see #get
     * @see #failed
     */
    protected void failed(Throwable cause) {
        String msg = String.format("%s failed: %s", this, cause);
        logger.log(Level.ERROR, msg, cause);
    }

    /**
     * Called unconditionally (in a `finally` clause) after one of the
     * completion methods, `succeeded`, `failed`, `cancelled`, or `interrupted`,
     * runs. Subclasses can override this method to cleanup before the `done`
     * method returns.
     * <p>
     * This method runs on the EDT. It does nothing by default.
     *
     * @see #done
     * @see #get
     * @see #failed
     */
    protected void finished() {
    }

    /**
     * Adds a `TaskListener` to this BackgroundTask. The listener will be
     * notified when the BackgroundTask's state changes to `STARTED`, each time
     * the `process` method is called, and when the BackgroundTask's state
     * changes to `done`. All of the listener methods will run on the event
     * dispatching thread.
     *
     * @param listener the `TaskListener` to be added
     *
     * @see #removeTaskListener
     */
    public void addTaskListener(TaskListener<T, V> listener) {
        if (listener == null) {
            throw new IllegalArgumentException("null listener");
        }
        taskListeners.add(listener);
    }

    /**
     * Removes a `TaskListener` from this BackgroundTask. If the specified
     * listener doesn't exist, this method does nothing.
     *
     * @param listener the `TaskListener` to be added
     *
     * @see #addTaskListener
     */
    public void removeTaskListener(TaskListener<T, V> listener) {
        if (listener == null) {
            throw new IllegalArgumentException("null listener");
        }
        taskListeners.remove(listener);
    }

    /**
     * Returns a copy of this BackgroundTask's `TaskListener`s.
     *
     * @return a copy of this BackgroundTask's `TaskListener`s.
     *
     * @see #addTaskListener
     * @see #removeTaskListener
     */
    public TaskListener<T, V>[] getTaskListeners() {
        return taskListeners.toArray(new TaskListener[taskListeners.size()]);
    }

    /*
     * This method is guaranteed to run on the EDT, it's called from
     * SwingWorker.process().
     */
    private void fireProcessListeners(List<V> values) {
        TaskEvent<List<V>> event = new TaskEvent(this, values);
        for (TaskListener<T, V> listener : taskListeners) {
            listener.process(event);
        }
    }

    /*
     * This method runs on the EDT because it's called from StatePCL (see
     * below).
     */
    private void fireDoInBackgroundListeners() {
        TaskEvent<Void> event = new TaskEvent(this, null);
        for (TaskListener<T, V> listener : taskListeners) {
            listener.doInBackground(event);
        }
    }

    /*
     * This method runs on the EDT because it's called from StatePCL (see
     * below).
     */
    private void fireSucceededListeners(T result) {
        TaskEvent<T> event = new TaskEvent(this, result);
        for (TaskListener<T, V> listener : taskListeners) {
            listener.succeeded(event);
        }
    }

    /*
     * This method runs on the EDT because it's called from StatePCL (see
     * below).
     */
    private void fireCancelledListeners() {
        TaskEvent<Void> event = new TaskEvent(this, null);
        for (TaskListener<T, V> listener : taskListeners) {
            listener.cancelled(event);
        }
    }

    /*
     * This method runs on the EDT because it's called from StatePCL (see
     * below).
     */
    private void fireInterruptedListeners(InterruptedException e) {
        TaskEvent<InterruptedException> event = new TaskEvent(this, e);
        for (TaskListener<T, V> listener : taskListeners) {
            listener.interrupted(event);
        }
    }

    /*
     * This method runs on the EDT because it's called from StatePCL (see
     * below).
     */
    private void fireFailedListeners(Throwable e) {
        TaskEvent<Throwable> event = new TaskEvent(this, e);
        for (TaskListener<T, V> listener : taskListeners) {
            listener.failed(event);
        }
    }

    /*
     * This method runs on the EDT because it's called from StatePCL (see
     * below).
     */
    private void fireFinishedListeners() {
        TaskEvent<Void> event = new TaskEvent(this, null);
        for (TaskListener<T, V> listener : taskListeners) {
            listener.finished(event);
        }
    }

    /*
     * This method runs on the EDT because it's called from StatePCL (see
     * below).
     */
    private void fireCompletionListeners() {
        try {
            if (isCancelled()) {
                fireCancelledListeners();
            } else {
                try {
                    fireSucceededListeners(get());
                } catch (InterruptedException e) {
                    fireInterruptedListeners(e);
                } catch (ExecutionException e) {
                    fireFailedListeners(e.getCause());
                }
            }
        } finally {
            fireFinishedListeners();
        }
    }

    private class StatePCL implements PropertyChangeListener {

        @Override
        public void propertyChange(PropertyChangeEvent e) {
            String propertyName = e.getPropertyName();
            if ("state".equals(propertyName)) {
                StateValue state = (StateValue) (e.getNewValue());
                BackgroundTask task = (BackgroundTask) (e.getSource());
                switch (state) {
                    case STARTED:
                        taskStarted(task);
                        break;
                    case DONE:
                        taskDone(task);
                        break;
                }
            } else if ("progress".equals(propertyName)) {
                synchronized (BackgroundTask.this) {
                    progressPropertyIsValid = true;
                }
            }
        }

        private void taskStarted(BackgroundTask task) {
            synchronized (BackgroundTask.this) {
                startTime = System.currentTimeMillis();
            }
            firePropertyChange("started", false, true);
            fireDoInBackgroundListeners();
        }

        private void taskDone(BackgroundTask task) {
            synchronized (BackgroundTask.this) {
                doneTime = System.currentTimeMillis();
            }
            try {
                task.removePropertyChangeListener(this);
                firePropertyChange("done", false, true);
                fireCompletionListeners();
            } finally {
                firePropertyChange("completed", false, true);
            }
        }
    }

    /**
     * Return this task's InputBlocker.
     * <p>
     * This is a bound property.
     *
     * @see #setInputBlocker
     */
    public final InputBlocker getInputBlocker() {
        return inputBlocker;
    }

    /**
     * Set this task's InputBlocker. The InputBlocker defines to what extent the
     * GUI should be blocked while the BackgroundTask is executed by a
     * TaskService. It is not used by the BackgroundTask directly, it's used by
     * the TaskService that executes the BackgroundTask.
     * <p>
     * This property may only be set before the BackgroundTask is
     * {@link TaskService#execute submitted} to a TaskService for execution. If
     * it's called afterwards, an IllegalStateException is thrown.
     * <p>
     * This is a bound property.
     *
     * @see #getInputBlocker
     */
    public final void setInputBlocker(InputBlocker inputBlocker) {
        if (getTaskService() != null) {
            throw new IllegalStateException("task already being executed");
        }
        InputBlocker oldInputBlocker, newInputBlocker;
        synchronized (this) {
            oldInputBlocker = this.inputBlocker;
            this.inputBlocker = inputBlocker;
            newInputBlocker = this.inputBlocker;
        }
        firePropertyChange("inputBlocker", oldInputBlocker, newInputBlocker);
    }

    /**
     * Specifies to what extent input to the Application's GUI should be blocked
     * while this BackgroundTask is being executed and provides a pair of
     * methods, {@code block} and `unblock` that do the work of blocking the
     * GUI. For the sake of input blocking, a BackgroundTask begins executing
     * when it's {@link TaskService#execute submitted} to a {@code TaskService},
     * and it finishes executing after the BackgroundTask's completion methods
     * have been called.
     * <p>
     * The InputBlocker's {@link BackgroundTask.BlockingScope BlockingScope} and
     * the blocking {@code target} object define what part of the GUI's input
     * will be blocked:
     * <dl>
     * <dt><strong><code>BackgroundTask.BlockingScope.NONE</code></strong></dt><dd>Don't
     * block input. The blocking target is ignored in this case.</dd>
     * <dt><strong><code>BackgroundTask.BlockingScope.ACTION</code></strong></dt><dd>Disable
     * the target {@link javax.swing.Action AppAction} while the BackgroundTask
     * is
     * executing.</dd>
     * <dt><strong><code>BackgroundTask.BlockingScope.COMPONENT</code></strong></dt><dd>Disable
     * the target {@link java.awt.Component} Component while the BackgroundTask
     * is executing.
     * </dd>
     * <dt><strong><code>BackgroundTask.BlockingScope.WINDOW</code></strong></dt><dd>Block
     * the Window ancestor of the target Component while the BackgroundTask is
     * executing.</dd>
     * <dt><strong><code>BackgroundTask.BlockingScope.Application</code></strong></dt><dd>Block
     * the entire Application while the BackgroundTask is executing. The
     * blocking target is ignored in this case.</dd>
     * </dl>
     * <p>
     * Input blocking begins when the {@code block} method is called and ends
     * when `unblock` is called. Each method is only called once, typically by
     * the {@code TaskService}.
     *
     * @see BackgroundTask#getInputBlocker
     * @see BackgroundTask#setInputBlocker
     * @see TaskService
     * @see AppAction
     */
    public static abstract class InputBlocker extends AbstractBean {

        private final BackgroundTask task;
        private final BlockingScope scope;
        private final Object target;
        private final ActionX action;

        /**
         * Construct an InputBlocker with four immutable properties. If the
         * BackgroundTask is {@code null} or if the BackgroundTask has already
         * been executed by a TaskService, then an exception is thrown. If scope
         * is `BlockingScope.ACTION` then target must be a
         * {@link javax.swing.Action AppAction}. If scope is
         * `BlockingScope.WINDOW`
         * or `BlockingScope.COMPONENT` then target must be a Component.
         *
         * @param task   block input while this BackgroundTask is executing
         * @param scope  how much of the GUI will be blocked
         * @param target the GUI element that will be blocked
         * @param action the {@code &#064;AppAction} that triggered running the
         *               task, or {@code null}
         *
         * @see TaskService#execute
         */
        public InputBlocker(BackgroundTask task, BlockingScope scope,
                Object target, ActionX action) {
            if (task == null) {
                throw new IllegalArgumentException("null task");
            }
            if (task.getTaskService() != null) {
                throw new IllegalStateException("task already being executed");
            }
            switch (scope) {
                case ACTION:
                    if (!(target instanceof javax.swing.Action)) {
                        throw new IllegalArgumentException(
                                "target not an AppAction");
                    }
                    break;
                case COMPONENT:
                case WINDOW:
                    if (!(target instanceof Component)) {
                        throw new IllegalArgumentException(
                                "target not a Component");
                    }
                    break;
            }
            this.task = task;
            this.scope = scope;
            this.target = target;
            this.action = action;
        }

        /**
         * Construct an InputBlocker. If {@code target} is an {@code ActionX},
         * it becomes the InputBlocker's {@code AppAction}. If the
         * BackgroundTask
         * is {@code null} or if the BackgroundTask has already been executed by
         * a TaskService, then an exception is thrown.
         *
         * @param task   block input while this BackgroundTask is executing
         * @param scope  how much of the GUI will be blocked
         * @param target the GUI element that will be blocked
         *
         * @see TaskService#execute
         */
        public InputBlocker(BackgroundTask task, BlockingScope scope,
                Object target) {
            this(task, scope, target,
                    (target instanceof ActionX) ? (ActionX) target : null);

        }

        /**
         * The {@code block} method will block input while this BackgroundTask
         * is being executed by a TaskService.
         *
         * @return the value of the read-only BackgroundTask property
         *
         * @see #block
         * @see #unblock
         */
        public final BackgroundTask getTask() {
            return task;
        }

        /**
         * Defines the extent to which the GUI is blocked while the task is
         * being executed.
         *
         * @return the value of the read-only blockingScope property
         *
         * @see #block
         * @see #unblock
         */
        public final BlockingScope getScope() {
            return scope;
        }

        /**
         * Specifies the GUI element that will be blocked while the task is
         * being executed.
         * <p>
         * This property may be {@code null}.
         *
         * @return the value of the read-only target property
         *
         * @see #getScope
         * @see #block
         * @see #unblock
         */
        public final Object getTarget() {
            return target;
        }

        /**
         * The ActionX ({@code &#064;AppAction}) that caused the task to be
         * executed. The DefaultInputBlocker uses the action's {@code name} and
         * {@code ResourceMap} to configure its blocking dialog if `scope` is
         * `BlockingScope.WINDOW`.
         * <p>
         * This property may be {@code null}.
         *
         * @return the value of the read-only action property
         *
         * @see #getScope
         * @see #block
         * @see #unblock
         * @see ActionX#getName
         * @see ActionX#getResourceMap
         */
        public final ActionX getAction() {
            return action;
        }

        /**
         * Block input to the GUI per the `scope` and {@code target} properties.
         * This method will only be called once.
         *
         * @see #unblock
         * @see TaskService#execute
         */
        protected abstract void block();

        /**
         * Unblock input to the GUI by undoing whatever the {@code block} method
         * did. This method will only be called once.
         *
         * @see #block
         * @see TaskService#execute
         */
        protected abstract void unblock();
    }
}
