/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   LocalStorage.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.framework;

import com.pekinsoft.api.Notifier;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import java.beans.*;
import java.io.*;
import java.nio.file.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Access to <em>per {@code Application}</em>, <em>per user</em> local file
 * storage, using the {@link java.nio.file Java NIO API}. While the original
 * {@code LocalStorage} class did a decent job, we felt that this class would do
 * a better job, in a better manner.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 1.0
 */
public class LocalStorage extends AbstractBean {

    private enum OSId {
        WINDOWS, OSX, UNIX
    }

    private final ApplicationContext context;
    private final Path unspecifiedFile = new File("unspecified").toPath();
    private Logger logger;
    private long storageLimit = -1L;
    private Path directory = unspecifiedFile;

    protected LocalStorage(ApplicationContext context) {
        if (context == null) {
            throw new IllegalArgumentException("null context");
        }
        this.context = context;
        logger = context.getLogger(getClass());
    }

    /**
     * Retrieves the {@link com.pekinsoft.framework.ApplicationContext
     * ApplicationContext singleton}.
     *
     * @return the {@code ApplicationContext} singleton
     */
    protected final ApplicationContext getContext() {
        return context;
    }

    private void checkFileName(String fileName) {
        if (fileName == null) {
            throw new IllegalArgumentException("null fileName");
        }
    }

    /**
     * Allows for changing the {@link com.pekinsoft.framework.Application
     * Application's} home directory to a different one. If the directory is
     * changed, a property change is fired on the property name "directory".
     *
     * @param directory a {@link java.nio.file.Path Path} to the directory to
     *                  use for the {@code Application}'s home directory
     *
     * @throws IllegalArgumentException if {@code directory} is {@code null}
     */
    public void setDirectory(Path directory) {
        if (directory == null) {
            throw new IllegalArgumentException("null directory");
        }
        Path oldValue = this.directory;
        this.directory = directory;
        firePropertyChange("directory", oldValue, this.directory);
    }

    /**
     * Retrieves the currently set storage limit for the {@link
     * com.pekinsoft.framework.Application Application}. If the retrieved limit
     * is {@code -1L}, then there is no limit on storage that may be used.
     *
     * @return the current storage limit
     *
     * @see #setStorageLimit(long)
     */
    public long getStorageLimit() {
        return storageLimit;
    }

    /**
     * Sets the storage limit for the {@link com.pekinsoft.framework.Application
     * Application}. If no limit should be enforced, the pass {@code -1L} as the
     * {@code storageLimit} value (this is the default). If the storage limit is
     * changed, a property change is fired on the property name "storageLimit".
     *
     * @param storageLimit the number of bytes to which storage for the {@code
     * Application} should be limited
     *
     * @throws IllegalArgumentException if the specified {@code storageLimit} is
     *                                  less than {@code -1L}
     */
    public void setStorageLimit(long storageLimit) {
        if (storageLimit < -1L) {
            throw new IllegalArgumentException("invalid storageLimit");
        }
        long oldValue = this.storageLimit;
        this.storageLimit = storageLimit;
        firePropertyChange("storageLimit", oldValue, this.storageLimit);
    }

    /**
     * Retrieves a {@link java.io.FileReader FileReader} object for reading data
     * from a file on disk.
     *
     * @param location one of the {@link com.pekinsoft.framework.StorageLocations
     * StorageLocations} enum constants for where the file is located
     * @param fileName the name of the file
     *
     * @return a {@code FileReader} object on the specified {@code fileName}
     *
     * @throws IOException in the event any input/output errors occur, including
     *                     denial of access due to file permissions
     */
    public synchronized BufferedReader openFileReader(StorageLocations location,
            String fileName) throws IOException {
        checkFileName(fileName);

        Path filePath = getDirectoryPath(location).resolve(fileName);

        // Verify that the file is readable.
        if (!Files.isReadable(filePath)) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("illegal.file.access.message", fileName,
                            filePath.getParent(),
                            System.getProperty("user.name"));
            throw new IOException(msg);
        }
        try {
            return Files.newBufferedReader(filePath);
        } catch (IOException e) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("file.open.error.message", fileName,
                            location.name());
            throw new IOException(msg, e);
        }
    }

    /**
     * Retrieves a {@link java.io.FileInputStream FileInputStream} object for
     * reading data from a file on disk.
     *
     * @param location one of the {@link com.pekinsoft.framework.StorageLocations
     * StorageLocations} enum constants for where the file is located
     * @param fileName the name of the file
     *
     * @return a {@code FileInputStream} object on the specified
     *         {@code fileName}
     *
     * @throws IOException in the event any input/output errors occur, including
     *                     denial of access due to file permissions
     */
    public synchronized InputStream openInputFile(StorageLocations location,
            String fileName) throws IOException {
        checkFileName(fileName);

        Path filePath = getDirectoryPath(location).resolve(fileName);

        // Verify that the file is readable.
        if (!Files.isReadable(filePath)) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("illegal.file.access.message", fileName,
                            filePath.getParent(),
                            System.getProperty("user.name"));
            throw new IOException(msg);
        }

        // Attempt to open the file input stream with clear error handling.
        try {
            return Files.newInputStream(filePath);
        } catch (IOException e) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("file.open.error.message", fileName,
                            location.name());
            throw new IOException(msg, e);
        }
    }

    /**
     * Retrieves a {@link java.io.FileWriter FileWriter} object for writing data
     * to a file on disk.
     *
     * @param location one of the {@link com.pekinsoft.framework.StorageLocations
     * StorageLocations} enum constants for where the file is located
     * @param fileName the name of the file
     * @param append   let's the file be opened allowing an existing file to be
     *                 appended if {@code true}, or be overwritten if
     *                 {@code false}
     *
     * @return a {@code FileWriter} object on the specified {@code fileName}
     *
     * @throws IOException in the event any input/output errors occur, including
     *                     denial of access due to file permissions
     */
    public synchronized BufferedWriter openFileWriter(StorageLocations location,
            String fileName, boolean append) throws IOException {
        checkFileName(fileName);

        Path parentDir = getDirectoryPath(location);
        Path filePath = parentDir.resolve(fileName);

        // Check that the file is within the target directory.
        if (!filePath.getParent().equals(parentDir)) {
            throw new UncheckedIOException(new IOException(
                    getContext().getResourceMap(getClass())
                            .getString("illegal.file.access.message",
                                    fileName)));
        }

        // Ensure the file path is writable.
        if (!canWrite(parentDir)) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("read.only.file.message", fileName,
                            filePath.getParent(),
                            System.getProperty("user.name"));
            throw new IOException(msg);
        }

        // Open the output stream with clear error handling
        try {
            if (append) {
                return Files.newBufferedWriter(filePath,
                        StandardOpenOption.CREATE, StandardOpenOption.WRITE,
                        StandardOpenOption.APPEND);
            } else {
                return Files.newBufferedWriter(filePath,
                        StandardOpenOption.CREATE, StandardOpenOption.WRITE,
                        StandardOpenOption.TRUNCATE_EXISTING);
            }
        } catch (NullPointerException e) {
            String msg = String.format("Trapped NPE in openOutputFile() for "
                    + "StorageLocations \"%s\" and fileName \"%s\" (filePath="
                    + "%s).",
                    location, fileName, filePath);
            logger.warning(msg);
            return null;
        } catch (IOException e) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("cannot.open.output.message", fileName, location,
                            e.getClass().getSimpleName(), e.getMessage());
            throw new IOException(msg, e);
        }
    }

    /**
     * Retrieves a {@link java.io.FileOutputStream FileOutputStream} object for
     * writing data to a file on disk.
     *
     * @param location one of the {@link com.pekinsoft.framework.StorageLocations
     * StorageLocations} enum constants for where the file is located
     * @param fileName the name of the file
     * @param append   let's the file be opened allowing an existing file to be
     *                 appended if {@code true}, or be overwritten if
     *                 {@code false}
     *
     * @return a {@code FileWriter} object on the specified {@code fileName}
     *
     * @throws IOException in the event any input/output errors occur, including
     *                     denial of access due to file permissions
     */
    public synchronized OutputStream openOutputFile(StorageLocations location,
            String fileName, boolean append) throws IOException {
        checkFileName(fileName);
        Path parent = getDirectoryPath(location);
        Path file = Paths.get(getDirectoryPath(location).toString(), fileName);

        if (!file.getParent().equals(parent)) {
            throw new UncheckedIOException(
                    new IOException(getContext().getResourceMap(getClass())
                            .getString("illegal.file.access.message",
                                    fileName)));
        }

        if (!canWrite(parent)) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("read.only.file.message", fileName,
                            file.getParent(),
                            System.getProperty("user.name"));
            throw new IOException(msg);
        }
        try {
            if (append) {
                return Files.newOutputStream(file, StandardOpenOption.CREATE,
                        StandardOpenOption.WRITE, StandardOpenOption.APPEND);
            } else {
                return Files.newOutputStream(file, StandardOpenOption.CREATE,
                        StandardOpenOption.WRITE,
                        StandardOpenOption.TRUNCATE_EXISTING);
            }
        } catch (IOException | NullPointerException e) {
            if (e instanceof NullPointerException) {
                String msg = String.format(
                        "Trapped NPE in LocalStorage.openOutputFile() "
                        + "for StorageLocations \"%s\" and fileName \"%s\" (file = %s).",
                        location, fileName, file);
                System.err.println(msg);
                return null;
            }
            String msg = getContext().getResourceMap(getClass())
                    .getString("cannot.open.output.message", fileName, location,
                            e.getClass().getSimpleName(), e.getMessage());
            throw new IOException(msg, e);
        }
    }

    /**
     * Deletes the specified file from the specified {@link
     * com.pekinsoft.framework.StorageLocations StorageLocations} on the disk.
     * This method first checks to see if the specified {@code fileName} is a
     * directory. If it is, then it is checked to see if it is empty. If not, an
     * {@link java.io.IOException IOException} is thrown, as only empty
     * directories may be deleted by this method.
     * <p>
     * Once it is verified to be either an empty directory or a regular file,
     * the user's permission level on the file is then checked. If the user has
     * proper access rights to the file, the file will be deleted. Otherwise, an
     * {@code IOException} will be thrown.
     *
     * @param location one of the {@link com.pekinsoft.framework.StorageLocations
     * StorageLocations} enum constants for where the file is located
     * @param fileName the name of the file
     *
     * @return {@code true} upon successful deletion; {@code false} upon failure
     *
     * @throws IOException in the event any input/output errors occur, including
     *                     denial of access due to file permissions or the
     *                     specified {@code fileName} is a non-empty directory
     */
    public synchronized boolean deleteFile(StorageLocations location,
            String fileName) throws IOException {
        checkFileName(fileName);

        Path parent = getDirectoryPath(location);
        Path file = parent.resolve(fileName);

        if (!file.getParent().equals(parent)) {
            throw new UncheckedIOException(
                    new IOException(
                            "Illegal file access. Cannot access file: \""
                            + fileName));
        }

        if (Files.isDirectory(file)
                && Files.newDirectoryStream(file).iterator().hasNext()) {
            if (Files.newDirectoryStream(file).iterator().hasNext()) {
                String msg = getContext().getResourceMap(getClass())
                        .getString("illegal.file.access.message",
                                fileName);
                throw new IOException(msg);
            }
        }
        boolean readOnly = !canWrite(file);

        if (readOnly) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("delete.nonempty.directory.message",
                            fileName, file.getParent(),
                            System.getProperty("user.name"));
            throw new IOException(msg);
        }
        return Files.deleteIfExists(file);
    }

    /**
     * Retrieves a {@link java.io.File File} object from the specified {@link
     * com.pekinsoft.framework.StorageLocations storage location}. If the
     * specified {@code location} is {@code StorageLocations.FILE_SYSTEM}, the
     * {@code fileName} specified needs to be the complete path to the file,
     * starting at the root of the file system.
     * <p>
     * <em>Note</em>: This method simply returns the results of calling {@link
     * #getFilePath(com.pekinsoft.framework.StorageLocations, java.lang.String)
     * getFilePath()} and converting that {@link java.nio.file.Path Path} to a
     * {@code File} object:
     * <pre>
     * public File getFile(StorageLocations location, String fileName) throws IOException {
     * return getFilePath(location, fileName).toFile();
     * }
     * </pre>
     *
     * @param location the {@code StorageLocations} enum constant for the
     *                 location of the file to get
     * @param fileName the name (or complete path for {@code
     * StorageLocations.FILE_SYSTEM} locations) of the file to get
     *
     * @return the specified file, guaranteed to physically exist on disk
     *
     * @throws IOException in the event that an input/output error occurs,
     *                     including if the current user does not have read or
     *                     write permissions to the storage location
     */
    public File getFile(StorageLocations location, String fileName)
            throws IOException {
        return getFilePath(location, fileName).toFile();
    }

    /**
     * Retrieves a specific {@link java.nio.file.Path Path} on the physical file
     * system, from the specified location. If {@code location} is {@link
     * com.pekinsoft.framework.StorageLocations StorageLocations.FILE_SYSTEM},
     * {@code fileName} needs to be a complete and absolute path, starting at
     * the user's home directory.
     * <p>
     * <em>Note</em>: Since we are using the Java NIO API, it is guaranteed that
     * the requested {@code fileName} is readable/writable and physically exists
     * on the file system.
     *
     * @param location the {@code StorageLocations} enum constant for the
     *                 location of the file to get
     * @param fileName the name (or complete path for {@code
     * StorageLocations.FILE_SYSTEM} locations) of the file to get
     *
     * @return the specified file as a {@code Path} and guaranteed to physically
     *         exist on disk, as well as be readable/writable
     *
     * @throws IOException in the event that an input/output error occurs,
     *                     including if the current user does not have read or
     *                     write permissions to the storage location
     */
    public Path getFilePath(StorageLocations location, String fileName)
            throws IOException {
        checkFileName(fileName);

        Path dir = getDirectoryPath(location);

        if (!canWrite(dir)) {
            throw new IOException(getContext().getResourceMap(getClass())
                    .getString("read.only.directory.message", dir));
        }

        if (!Files.exists(dir)) {
            dir = Files.createDirectories(dir);
        }

        Path file = dir.resolve(fileName);
        if (!Files.exists(file)) {
            Files.createFile(file);
        }

        return file;
    }

    /**
     * Retrieves a specific {@link java.nio.file.Path Path} on the physical file
     * system, from the specified location. If {@code location} is {@link
     * com.pekinsoft.framework.StorageLocations StorageLocations.FILE_SYSTEM},
     * {@code fileName} needs to be a complete and absolute path, starting at
     * the user's home directory.
     * <p>
     * <em>Note</em>: Since we are using the Java NIO API, it is guaranteed that
     * the requested {@code fileName} is readable/writable and physically exists
     * on the file system.
     *
     * @param location      the {@code StorageLocations} enum constant for the
     *                      location of the file to get
     * @param directoryName the name (or complete path for {@code
     * StorageLocations.FILE_SYSTEM} locations) of the directory to get
     *
     * @return the specified directory as a {@code Path} and guaranteed to
     *         physically exist on disk, as well as be readable/writable
     *
     * @throws IOException in the event that an input/output error occurs,
     *                     including if the current user does not have read or
     *                     write permissions to the storage location
     */
    public Path getDirectoryPath(StorageLocations location, String directoryName)
            throws IOException {
        checkFileName(directoryName);

        Path dir = getDirectoryPath(location);

        if (!canWrite(dir)) {
            throw new IOException(getContext().getResourceMap(getClass())
                    .getString("read.only.directory.message", dir));
        }

        if (!Files.exists(dir)) {
            dir = Files.createDirectories(dir);
        }

        Path directory = dir.resolve(directoryName);
        if (!Files.exists(directory)) {
            Files.createDirectories(directory);
        }

        return directory;
    }

    /**
     * Gets the OS-specific location for the home folder of a given application.
     * This method makes sure to follow the "rules" of the operating system as
     * to where the application stores its files.
     * <p>
     * Applications can make whatever folder structure needed on the user's PC
     * hard drive. However, operating systems tend to desire that applications
     * do not make folders willy-nilly on the hard drive. Therefore, each
     * operating system vendor has determined locations for third-party
     * application developers to store their application's files. The
     * OS-specific locations are detailed in the table below.
     * <table>
     * <caption>OS-Specific Application Top-Level Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>{@code ${user.home}\\AppData\\${application.name}\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>{@code ${user.home}/Library/Application Data/${application.name}/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>{@code ${user.home}/.${application.name}/}</td></tr>
     * </table>
     * <p>
     * In order to follow these guidelines, this method has been created. It
     * checks the system upon which the Java Virtual Machine (JVM) is running to
     * get its name, then builds the application folder path for that OS in a
     * {@code java.lang.String} and returns it to the calling class.
     * <p>
     * <strong><em>Note</em></strong>: When the directory is retrieved using
     * this method, it is guaranteed that the directory exits.
     *
     * @return a path representing the application's OS-specific folder location
     *         as a {@code java.nio.file.Path} object
     *
     * @see #getConfigDirectory()
     * @see #getDataDirectory()
     * @see #getErrorDirectory()
     * @see #getLogDirectory()
     */
    public Path getDirectory() {
        if (directory == null || directory == unspecifiedFile) {
            directory = Paths.get(getUserHome());
            String userHome = getUserHome();

            if (userHome != null) {
                String applicationId = getApplicationId();
                OSId osId = getOSId();

                if (osId == null) {
                    logger.config("osId is null, using default directory of "
                            + "${user.home}/.${applicationId}");
                    // ${user.home}/.${applicationId}
                    directory = directory.resolve("." + applicationId);
                } else {
                    directory = getOsSpecificDirectory(osId, applicationId);
                }
            }
        }

        ensureDirectoryExists(directory, "Unable to create the application "
                + "directory for the application.");

        return directory;
    }

    /**
     * A convenience method for getting a standard configuration file directory
     * in which applications settings and configurations may be stored.
     * <p>
     * For applications that wish to store their config files in a separate
     * directory from any other files the application may create, this method
     * will get a directory named "etc", with the application's home directory
     * prepended to it. The path returned is described in the table below, by
     * OS.
     * <table>
     * <caption>OS-Specific Application Configuration Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Configuration Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>{@code ${user.home}\\AppData\\${application.name}\\config\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>{@code ${user.home}/Library/Preferences/${application.name}/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>{@code ${user.home}/.${application.name}/etc/}</td></tr>
     * </table>
     * <p>
     * <strong><em>Note</em></strong>: When the directory is retrieved using
     * this method, it is guaranteed that the directory exits.
     *
     * @return a path representing the application's OS-specific folder location
     *         as a {@code java.nio.file.Path} object
     *
     * @see #getDirectory()
     * @see #getDataDirectory()
     * @see #getErrorDirectory()
     * @see #getLogDirectory()
     */
    public Path getConfigDirectory() {
        Path dir = null;
        OSId osId = getOSId();
        dir = switch (osId) {
            case OSX ->
                // ${user.home}/Library/Preferences/${applicationId}
                Paths.get(getUserHome()).resolve("Library")
                .resolve("Preferences").resolve(getApplicationId());
            case WINDOWS ->
                // APPDATA\${vendorId}\${applicationId}\config
                getDirectory().resolve("config");
            default ->
                // ${user.home}/.config/${applicationId}/etc
                getDirectory().resolve("etc");
        };

        try {
            if (canWrite(dir)) {
                Files.createDirectories(dir);
            }
        } catch (IOException e) {
            String title = getContext().getResourceMap(getClass())
                    .getString("read.only.directory.title");
            String msg = getContext().getResourceMap(getClass())
                    .getString("read.only.directory.message", dir);
            logger.warning(msg);
            Notifier.getDefault().notify(createErrorInfo(title, e.getMessage(),
                    msg, e));
        }

        ensureDirectoryExists(dir, "Unable to create the config directory.");

        return dir;
    }

    /**
     * A convenience method for getting a standard data directory in which to
     * store files from an application.
     * <p>
     * For applications that wish to store their data in a separate directory
     * from any other files the application may create, this method will get a
     * directory named "data", with the application's home directory prepended
     * to it. The path returned is described in the table below, by OS.
     * <table>
     * <caption>OS-Specific Application Data Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Data Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>{@code ${user.home}\\AppData\\${application.name}\\data\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>{@code ${user.home}/Library/Application Data/${application.name}/data/}
     * </td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>{@code ${user.home}/.${application.name}/data/}</td></tr>
     * </table>
     * <p>
     * <strong><em>Note</em></strong>: When the directory is retrieved using
     * this method, it is guaranteed that the directory exits.
     *
     * @return a path representing the application's OS-specific folder location
     *         as a {@code java.nio.file.Path} object
     *
     * @see #getDirectory()
     * @see #getConfigDirectory()
     * @see #getErrorDirectory()
     * @see #getLogDirectory()
     */
    public Path getDataDirectory() {
        Path dir = getDirectory().resolve("data");

        boolean canWrite = false;
        try {
            canWrite = canWrite(dir);
        } catch (IOException e) {
            String title = getContext().getResourceMap(getClass())
                    .getString("read.only.directory.title");
            String msg = getContext().getResourceMap(getClass())
                    .getString("read.only.directory.message", dir);
            logger.warning(msg);
            Notifier.getDefault().notify(createErrorInfo(title, e.getMessage(),
                    msg, e));
        }

        if (!canWrite) {
            dir = Paths.get(getUserHome()).resolve("data");
        }

        ensureDirectoryExists(dir, getContext().getResourceMap(getClass())
                .getString("read.only.directory.message", dir));

        return dir;
    }

    /**
     * A convenience method for getting a standard log file directory in which
     * applications may store log files.
     * <p>
     * For applications that wish to store their log files in a separate
     * directory from any other files the application may create, this method
     * will get a directory named "var/log" for Linux applications, with the
     * application's home directory prepended to it. However, other operating
     * systems have very specific locations where they want log files created.
     * The path returned for each OS is described in the table below, by OS.
     * <table>
     * <caption>OS-Specific Application Log Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Log Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>{@code %SystemRoot%\\System32\\Config\\${application.name}\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>{@code ${user.home}/Library/Logs/${application.name}/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>{@code /var/log/${application.name}} or
     * {@code ${user.home}/.${application.name}/var/log/}, if the system logs
     * directory is not writable. Typically, an application that is installed
     * system wide will have write access to the
     * {@code /var/log/${application.name}} folder. If a user installs the
     * application just for his/herself, then the logs will be stored in the
     * {@code ${user.home}/.${application.name}/var/log/} folder.</td></tr>
     * </table>
     * <p>
     * <strong><em>Note</em></strong>: When the directory is retrieved using
     * this method, it is guaranteed that the directory exits.
     *
     * @return a path representing the application's OS-specific folder location
     *         as a {@code java.nio.file.Path} object
     *
     * @see #getDirectory()
     * @see #getConfigDirectory()
     * @see #getDataDirectory()
     * @see #getErrorDirectory()
     */
    public Path getLogDirectory() {
        OSId osId = getOSId();
        Path dir = null;

        switch (osId) {
            case WINDOWS -> {
                // %SystemRoot%\System32\config\${applicationId}
                dir = Paths.get(System.getenv("%SystemRoot%"))
                        .resolve("System32").resolve("config")
                        .resolve(getApplicationId());
            }
            case OSX -> {
                // ${userHome}/Library/Logs/${applicationId}/
                dir = Paths.get(getUserHome()).resolve("Library")
                        .resolve("Logs").resolve(getApplicationId());
            }
            default -> {
                // /var/log/${applicationId}
                dir = Paths.get("/").resolve("var").resolve("log")
                        .resolve(getApplicationId());
            }
        }

        boolean canWrite = false;
        try {
            canWrite = canWrite(dir);
        } catch (IOException e) {
            String title = getContext().getResourceMap(getClass())
                    .getString("read.only.directory.title");
            String msg = getContext().getResourceMap(getClass())
                    .getString("read.only.directory.message", dir);
            logger.warning(msg);
            Notifier.getDefault().notify(createErrorInfo(title, e.getMessage(),
                    msg, e));
        }

        if (!canWrite) {
            dir = getVariantDirectory().resolve("log");
        }

        ensureDirectoryExists(dir, getContext().getResourceMap(getClass())
                .getString("read.only.directory.message", dir));

        return dir;
    }

    /**
     * A convenience method for getting a standard error log file directory in
     * which applications may store error log files.
     * <p>
     * For applications that wish to store their error log files in a separate
     * directory from any other files the application may create, this method
     * will get a directory named "var/err" for Linux applications, with the
     * application's home directory prepended to it. However, other operating
     * systems have very specific locations where they want log files created.
     * The path returned for each OS is described in the table below, by OS.
     * <table>
     * <caption>OS-Specific Application Error Log Folder Locations</caption>
     * <tr>
     * <th>OS</th><th>Application Error Log Folder Location</th></tr>
     * <tr><td>Microsoft Windows<sup>&trade;</sup></td>
     * <td>{@code %SystemRoot%\\System32\\Config\\${application.name}\\err\\}</td></tr>
     * <tr><td>Apple Mac OS-X<sup>&trade;</sup></td>
     * <td>{@code ${user.home}/Library/Logs/${application.name}/err/}</td></tr>
     * <tr><td>Linux and Solaris</td>
     * <td>{@code /var/log/${application.name}/err/} or
     * {@code ${user.home}/.${application.name}/var/log/err/}, if the system
     * logs directory is not writable. Typically, an application that is
     * installed system wide will have write access to the
     * {@code /var/log/${application.name}} folder. If a user installs the
     * application just for his/herself, then the logs will be stored in the
     * {@code ${user.home}/.${application.name}/var/log/err/} folder.</td></tr>
     * </table>
     * <p>
     * <strong><em>Note</em></strong>: When the directory is retrieved using
     * this method, it is guaranteed that the directory exits.
     *
     * @return a pat representing the application's OS-specific folder location
     *         as a {@code java.nio.file.Path} object
     *
     * @see #getDirectory()
     * @see #getConfigDirectory()
     * @see #getDataDirectory()
     * @see #getLogDirectory()
     */
    public Path getErrorDirectory() {
        Path dir = getLogDirectory().resolve("..").resolve("err");

        boolean canWrite = false;

        try {
            canWrite = canWrite(dir);
        } catch (IOException e) {
            String msg = getContext().getResourceMap(getClass())
                    .getString("check.write.error.message", dir);
            String title = getContext().getResourceMap(getClass())
                    .getString("check.write.error.title");
            logger.warning(msg, e);
            Notifier.getDefault().notify(createErrorInfo(title, e.getMessage(),
                    msg, e));
        }

        if (!canWrite) {
            dir = getVariantDirectory().resolve("error");
        }

        if (!Files.exists(dir)) {
            ensureDirectoryExists(dir, getContext().getResourceMap(getClass())
                    .getString("check.write.error.message", dir));
        }

        return dir;
    }

    /**
     * Retrieves the appropriate variant ("{@code var}") directory for the
     * application. Typically, the {@link #getLogDirectory() } and
     * {@link #getErrorDirectory() } directories reside beneath this one. Other
     * data can be stored there as well, but that is a rare use-case.
     *
     * @return the application's variant directory
     */
    public Path getVariantDirectory() {
        Path dir = getDirectory().resolve("var");

        ensureDirectoryExists(dir, getContext().getResourceMap(getClass())
                .getString("check.write.error.message", dir));

        return dir;
    }

    /**
     * Loads data from a session storage XML file. The data loaded is returned
     * as a generic {@link java.lang.Object Object} and it is up to the calling
     * class to cast it to the appropriate type.
     *
     * @param location the {@code StorageLocations} enum constant for the
     *                 location of the file to get
     * @param fileName the name (or complete path for {@code
     * StorageLocations.FILE_SYSTEM} locations) of the file to get
     *
     * @return a generic {@code Object} containing the loaded data
     *
     * @throws IOException in the event that an input/output error occurs,
     *                     including if the current user does not have read or
     *                     write permissions to the storage location
     */
    public Object load(StorageLocations location, String fileName)
            throws IOException {
        checkFileName(fileName);

        InputStream ist = openInputFile(location, fileName);
        AbortExceptionListener el = new AbortExceptionListener();
        try (XMLDecoder d = new XMLDecoder(ist)) {
            d.setExceptionListener(el);
            Object bean = d.readObject();
            if (el.exception != null) {
                throw new IOException("Failed to load \"" + fileName + "\"",
                        el.exception);
            }
            return bean;
        }
    }

    /**
     * Saves the data of the specified {@code bean} to a session storage XML
     * file.
     *
     * @param location the {@code StorageLocations} enum constant for the
     *                 location of the file to get
     * @param bean     the {@code Object} whose data is to be saved
     * @param fileName the name (or complete path for {@code
     * StorageLocations.FILE_SYSTEM} locations) of the file to get
     *
     * @throws IOException in the event that an input/output error occurs,
     *                     including if the current user does not have read or
     *                     write permissions to the storage location
     */
    public void save(StorageLocations location, Object bean,
            final String fileName) throws IOException {
        checkFileName(fileName);
        if (bean == null) {
            throw new IllegalArgumentException("null bean");
        }

        AbortExceptionListener el = new AbortExceptionListener();
        XMLEncoder e = null;
        ByteArrayOutputStream bst = new ByteArrayOutputStream();
        try {
            e = new XMLEncoder(bst);
            e.setExceptionListener(el);
            e.writeObject(bean);
        } finally {
            if (e != null) {
                e.close();
            }
        }
        if (el.exception != null) {
            throw new IOException("Failed to save \"" + fileName + "\"",
                    el.exception);
        }
        try (OutputStream ost = openOutputFile(location, fileName, false)) {
            ost.write(bst.toByteArray());
        } catch (NullPointerException npe) {
            String msg = String.format("Trapped NPE in LocalStorage.save() on "
                    + "StorageLocation \"%s\" with bean \"%s\" and fileName \""
                    + "%s\".", location, bean, fileName);
            System.err.println(msg);
        }
    }

    private boolean canWrite(Path path) throws IOException {
        Path target = Files.isDirectory(path) ? path : path.getParent();

        if (target == null) {
            throw new IOException("Path does not have a writable directory.");
        }

        return Files.isWritable(path);
    }

    private String getUserHome() {
        String userHome = null;
        try {
            userHome = System.getProperty("user.home");
        } catch (SecurityException ignore) {
        }

        return userHome == null ? getVendorId() : userHome;
    }

    private Path getDirectoryPath(StorageLocations location) {
        Path path = null;

        switch (location) {
            case CONFIG_DIR ->
                path = getConfigDirectory();
            case DATA_DIR ->
                path = getDataDirectory();
            case ERROR_DIR ->
                path = getErrorDirectory();
            case LOG_DIR ->
                path = getLogDirectory();
            case APP_DIR ->
                path = getDirectory();
            default -> {
                path = Paths.get(getUserHome());
            }
        }
        // StorageLocations.FILE_SYSTEM
        // TODO: Figure this out what, if anything, to do as default action.

        if (path == null) {
            String msg = String.format(
                    "Trapped NPE in LocalStorage.getDirectoryPath() "
                    + "for StorageLocations \"%s\".", location);
        }

        return path;
    }

    private String getId(String key, String def) {
        ResourceMap appResourceMap = getContext().getResourceMap();
        String id = appResourceMap.getString(key);
        if (id == null) {
            if (logger != null) {
                logger.log(Level.WARNING,
                        "unspecified resource " + key + " using " + def);
            }
            id = def;
        } else if (id.trim().length() == 0) {
            if (logger != null) {
                logger.log(Level.WARNING,
                        "empty resource " + key + " using " + def);
            }
            id = def;
        }

        return id;
    }

    private String getApplicationId() {
        return getId("Application.id", getContext().getApplicationClass()
                .getSimpleName());
    }

    private String getVendorId() {
        return getId("Application.vendorId", "UnknownApplicationVendor");
    }

    private OSId getOSId() {
        OSId id = OSId.UNIX;
        String osName = System.getProperty("os.name");
        if (osName != null) {
            if (osName.toLowerCase().startsWith("mac os x")) {
                id = OSId.OSX;
            } else if (osName.contains("Windows")) {
                id = OSId.WINDOWS;
            }
        }

        return id;
    }

    private Path ensureDirectoryExists(Path dir, String errorMessage) {
        if (!Files.exists(dir)) {
            try {
                if (canWrite(dir.getParent())) {
                    Files.createDirectories(dir);
                } else {
                    String msg = getContext().getResourceMap(getClass())
                            .getString("read.only.directory.message",
                                    dir.getParent());
                    logger.error(msg);
                    Notifier.getDefault().notify(createErrorInfo(
                            getContext().getResourceMap(getClass())
                                    .getString("read.only.directory.title"),
                            errorMessage, msg, null));
                }
            } catch (IOException e) {
                String msg = getContext().getResourceMap(getClass()).getString(
                        "read.only.directory.message");
                logger.error(msg, e);
                Notifier.getDefault().notify(createErrorInfo(
                        getContext().getResourceMap(getClass())
                                .getString("read.only.directory.title"),
                        errorMessage, msg, null));
            }
        }

        return dir;
    }

    private Path getOsSpecificDirectory(OSId osId, String applicationId) {
        switch (osId) {
            case WINDOWS -> {
                Path appDataDir = getWindowsAppDataDirectory();
                String vendorId = getVendorId();
                if (appDataDir != null && appDataDir.toFile().isDirectory()) {
                    // $APPDATA%\${vendorId}\${applicationId}
                    return appDataDir.resolve(vendorId).resolve(applicationId);
                } else {
                    // ${user.home}\Application Data\${vendorId}\${applicationId}
                    return Paths.get(getUserHome()).resolve("Application Data")
                            .resolve(vendorId).resolve(applicationId);
                }
            }
            case OSX -> {
                // ${user.home}/Library/Application Support/${applicationId}
                return Paths.get(getUserHome()).resolve("Library")
                        .resolve("Application Support").resolve(applicationId);
            }
            default -> {
                // ${user.home}/.config/${applicationId}
                return Paths.get(getUserHome()).resolve(".config")
                        .resolve(applicationId);
            }
        }
    }

    private Path getWindowsAppDataDirectory() {
        String appDataEV = System.getenv("APPDATA");
        if (appDataEV != null && !appDataEV.isEmpty()) {
            return Paths.get(appDataEV);
        }
        return null;
    }

    private ErrorInfo createErrorInfo(String title, String basicMessage,
            String detailMessage, Throwable cause) {
        if (cause == null) {
            cause = new IOException(detailMessage);
        }

        return new ErrorInfo(title, basicMessage, detailMessage,
                getContext().getResourceMap().getString("category.storage"),
                cause, Level.ERROR, createStateMap());
    }

    private Map<String, String> createStateMap() {
        Map<String, String> stateMap = new HashMap<>();

        stateMap.put("Application Title", context.getResourceMap()
                .getString("Application.title"));
        stateMap.put("Application Version", context.getResourceMap()
                .getString("Application.version"));
        stateMap.put("Application Home", getDirectory().toString());
        stateMap.put("Current User", System.getProperty("user.name"));

        return stateMap;
    }

    private static class AbortExceptionListener implements ExceptionListener {

        public Exception exception = null;

        @Override
        public void exceptionThrown(Exception e) {
            if (exception == null) {
                exception = e;
            }
        }

    }
}
