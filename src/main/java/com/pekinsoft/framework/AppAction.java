/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   AppAction.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 13, 2024
 *  Modified   :   Jul 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.framework;

import com.pekinsoft.api.Targetable;
import java.lang.annotation.*;

/**
 * Marks a method that will be used to define a Swing {@code Action} object's
 * {@code actionPerformed} method. It also identifies the resources that will be
 * used to initialize the Action's properties. Additional
 * {@code &#064;AppAction} parameters can be used to specify the name of the
 * bound properties (from the same class) that indicate if the Action is to be
 * enabled/selected, and if the GUI should be blocked while the Action's
 * background {@link BackgroundTask} is running.
 * <p>
 * With the 2.3 update, {@code @AppAction}s now contain parameters that specify
 * where within an {@link Application Application's} action system the action
 * should be installed. Each of these new parameters are documented for easier
 * understanding.
 * <p>
 * The {@link ActionMapX} class creates an {@code ActionMap} that contains one
 * {@link ActionX} for each &#064;AppAction found in a target or "actions"
 * class. Typically applications will use {@link
 * ApplicationContext#getActionMap(Class, Object) getActionMap} to lazily
 * construct and cache ApplicationContextActionMaps, rather than constructing
 * them directly. By default the ActionMapX's {@link ActionMapX#get key} for an
 * &#064;AppAction is the name of the method. The {@code name} parameter can be
 * used to specify a different key.
 * <p>
 * The {@code ActionX's} properties are initialized with resources loaded from a
 * ResourceBundle with the same name as the actions class. The list of
 * properties initialized this way is documented by the
 * {@link ActionX ActionX's} constructor.
 * <p>
 * The method marked with &#064;AppAction, can have no parameters, or a single
 * ActionEvent parameter. The method's return type can be {@code void} or
 * {@link BackgroundTask}. If the return type is BackgroundTask, the
 * BackgroundTask will be executed by the ActionX's {@code actionPerformed}
 * method.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.3
 * @since 1.0
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AppAction {

    /**
     * The {@code name} property redefines the {@code AppAction}'s
     * {@link javax.swing.Action#ACTION_COMMAND_KEY} value, which defaults to
     * the name of the decorated method.
     *
     * @return the {@code ACTION_COMMAND_KEY} value
     */
    String name() default "";

    /**
     * The {@code enabledProperty} provides the name of the property to listen
     * on for changes in the {@code AppAction}'s enabled/disabled state.
     *
     * @return the property name for enabled/disabled state
     */
    String enabledProperty() default "";

    /**
     * The {@code selectedProperty} provdies the name of the property to listen
     * on for changes in the {@code AppAction}'s selected state. If this
     * property is not {@code null}, then the resulting component installed in a
     * menu is a {@link javax.swing.JCheckBoxMenuItem JCheckBoxMenuItem}, unless
     * the {@link #groupId() groupId} is not {@code null} or empty, which would
     * then make the component created be a
     * {@link javax.swing.JRadioButtonMenuItem JRadioButtonMenuItem}.
     *
     * @return the property name for the selected state
     */
    String selectedProperty() default "";

    /**
     * Defines a unique identification value for the
     * {@link javax.swing.ButtonGroup ButtonGroup} of which this state action is
     * a member. {@code ButtonGroup}s allow for only a single member to be
     * selected at once.
     *
     * @return the unique identifier for the {@code ButtonGroup} of which this
     *         {@code AppAction} is a member
     */
    String groupId() default "";

    /**
     * Determines whether this is a {@link Targetable targetable action}. If
     * this parameter is set to {@code true}, then there must be a class that
     * implements {@link Targetable} that can handle the
     * {@link Targetable#hasCommand(java.lang.String)  Targetable.hasCommand}
     * and
     * {@link Targetable#doCommand(java.lang.String, java.util.EventObject) doCommand}
     * methods that will handle calling this action's method.
     * <p>
     * If this parameter is set to {@code true}, the class in which the method
     * decorated with the {@code AppAction} annotation <em>must have</em> a
     * static method named &ldquo;{@code getTargetable}&rdquo; that may be
     * called to retrieve the instance of the class that implements the
     * {@code Targetable} interface. If it does not, an exception will be thrown
     * during actions system initialization.
     *
     * @return {@code true} if this is to be a {@code Targetable} action
     */
    boolean isTargetable() default false;

    /**
     * Defines the amount of the GUI to be blocked while the {@code AppAction}
     * method is executing. When this property is set to something other than
     * the default, the method decorated by {@code @AppAction} should return a
     * {@link BackgroundTask}.
     *
     * @return the amount of the GUI to block during execution
     */
    BackgroundTask.BlockingScope block() default BackgroundTask.BlockingScope.NONE;

    /**
     * Defines the base name of the menu into which this {@code AppAction}
     * should be installed. The menu base name is used to locate the name of the
     * menu and its index position in the toolbar. By using this base name, the
     * actions generation system can simply look for an
     * {@link Application}-defined key in its {@link ResourceMap} that explains
     * the menu's name and index position in the menu bar. This keeps the
     * definitions of this information at the application level, so that it may
     * easily be changed in a future revision.
     * <p>
     * For example, if this value returns &ldquo;file&rdquo;, then the actions
     * generations system could append &ldquo;.menu.index&rdquo; and
     * &ldquo;.menu.name&rdquo; to that base name to learn the name and position
     * of the menu. With the name known, the menu can be injected to have its
     * text set properly, when the application defines, in this example, the
     * resource key &ldquo;fileMenu.text&rdquo;.
     * <p>
     * If the action is to be installed in a sub-menu, the value returned by
     * this parameter would be, for example, &ldquo;view/toolbars&rdquo;. The
     * application would then need to define the index positions and names of
     * both the parent &ldquo;view&rdquo; menu, as well as the
     * &ldquo;toolbars&rdquo; sub-menu.
     * <p>
     * All menu names and positions, as well as the text to be displayed, should
     * be defined by the application's {@code ResourceMap}.
     *
     * @return the base name of the menu in which the {@code AppAction} should
     *         be installed
     */
    String menuBaseName() default "";

    /**
     * Defines the position within the menu in which the {@code AppAction} is
     * installed. This value should specify where, top-to-bottom, the created
     * {@link javax.swing.JMenuItem menu item} should be placed.
     * <p>
     * <em>This is only a <strong>hint</strong> to the actions system.</em>
     * There is no guarantee that the {@code AppAction} will be installed at the
     * exact index position requested.
     *
     * @return the {@code AppAction}'s position within it menu
     */
    byte menuActionIndex() default -1;

    /**
     * Determines whether the actions system should place a separator before the
     * {@code AppAction} in the menu.
     *
     * @return {@code true} for a separator to be added before the action is
     *         installed; {@code false} (<em>default</em>) for no separator
     *         before the action
     */
    boolean menuSepBefore() default false;

    /**
     * Determines whether the actions system should place a separator after the
     * {@code AppAction} in the menu.
     *
     * @return {@code true} for a separator to be added after the action is
     *         installed; {@code false} (<em>default</em>) for no separator
     *         after the action
     */
    boolean menuSepAfter() default false;

    /**
     * If this parameter returns a value other than the default empty string,
     * the created action will appear in the toolbar named by the value
     * returned.
     * <p>
     * The toolbar names and positions should be defined by the application.
     *
     * @return the name of the toolbar into which the {@code AppAction} should
     *         be installed
     */
    String toolbarBaseName() default "";

    /**
     * If {@link #toolbarBaseName() } returns an <em>non-empty</em> string, this
     * defines the index position at which the {@code AppAction} should be
     * installed within the {@link javax.swing.JToolBar toolbar}.
     * <p>
     * <em>This is only a <strong>hint</strong> to the actions system.</em>
     * There is no guarantee that the {@code AppAction} will be installed at the
     * exact index position requested.
     *
     * @return the index position within the toolbar where the {@code AppAction}
     *         should be installed
     */
    byte toolbarActionIndex() default -1;

    /**
     * If {@link #toolbarBaseName() } returns an <em>non-empty</em> string, this
     * determines whether the actions system should insert a separator prior to
     * the {@code AppAction} within the {@link javax.swing.JToolBar toolbar}.
     *
     * @return {@code true} for a separator before the {@code AppAction};
     *         {@code false} (<em>default</em>) for no separator before it
     */
    boolean toolbarSepBefore() default false;

    /**
     * If {@link #toolbarBaseName() } returns an <em>non-empty</em> string, this
     * determines whether the actions system should insert a separator after the
     * {@code AppAction} within the {@link javax.swing.JToolBar toolbar}.
     *
     * @return {@code true} for a separator after the {@code AppAction};
     *         {@code false} (<em>default</em>) for no separator after it
     */
    boolean toolbarSepAfter() default false;

    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.PARAMETER)
    @interface Parameter {

        String value() default "";
    }

}
