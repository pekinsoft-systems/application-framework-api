/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ServiceProviders
 *  Class      :   Exceptions.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 27, 2023
 *  Modified   :   Jan 27, 2023
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 27, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.lookup.Lookup;
import java.util.logging.Level;

/**
 * The {@code Exceptions} ...
 *
 * @author Sean Carrick [sean at pekinsoft dot com]
 * <dl><dt>Copyright Notice</dt>
 * <dd>Copyright© 2006-2023 Sean Carrick. All rights under copyright reserved
 * <br>Released under the terms of the [NO LICENSE PROVIDED].</dd></dl>
 *
 * @version 1.0
 * @since 1.0
 */
public interface Exceptions {

    /**
     * Retrieves the default implementation of the {@code Exceptions} interface.
     * This method simply uses the
     * {@link com.pekinsoft.lookup.Lookup Lookup API} to find the first provider
     * of {@code Exceptions}.
     *
     * @return the default implementation of the {@code Exceptions} interface
     */
    public static Exceptions getDefault() {
        return Lookup.getDefault().lookup(Exceptions.class);
    }

    /**
     * Handles an exception in a simple manner.
     *
     * @param thrown the exception that was caught
     */
    void print(Throwable thrown);

    /**
     * Handles an exception in a manner that allows some additional context to
     * be provided as well. This method should handle the specified exception
     * as a {@link Level#SEVERE Level.SEVERE} error.
     *
     * @param thrown   the exception that was caught
     * @param xtraInfo addition context regarding the exception
     */
    void print(Throwable thrown, String xtraInfo);

    /**
     * Handles an exception in a manner that allows some additional context to
     * be provided, and where the {@link Level} can be set to lower than the
     * default used in {@link #print(Throwable, String) }.
     *
     * @param thrown    the exception that was caught
     * @param extraInfo additional context regarding the exception
     * @param level     the level to be associated with this exception
     */
    void print(Throwable thrown, String extraInfo, Level level);

    /**
     * Handles a situation where an {@link ErrorInfo} needs to be processed, to
     * possibly include submitting a bug report.
     *
     * @param info the {@code Level}} object that contains the information
     */
    void handle(ErrorInfo info);

}
