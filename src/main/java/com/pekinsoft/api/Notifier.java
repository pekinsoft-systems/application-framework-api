/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Notifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 28, 2024
 *  Modified   :   Oct 28, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 28, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.desktop.error.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.lookup.Lookup;

/**
 * The {@code Notifier} interface provides a means for the application to fire
 * off a notification to the user. A &ldquo;notification&rdquo; is an instance
 * of the {@link ErrorInfo} class, which carries all required data with it.
 * <p>
 * The way the default notification system is set up, if the {@code Level}}
 * has an {@link Level} of either {@link Level#CRITICAL} or
 * {@link Level#FATAL}, then an attempt is made to attach the default
 * implementation of {@link ErrorReporter} to the {@link ErrorPane} when it is
 * displayed to the user.
 * <p>
 * If the application has not defined an {@code ErrorReporter} implementation,
 * then no &ldquo;Submit Bug Report&rdquo; button will be shown on the
 * {@code ErrorPane}.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public interface Notifier {

    public static Notifier getDefault() {
        return Lookup.getDefault().lookup(Notifier.class);
    }

    /**
     * Sets a notification to be displayed to the user. A notification is an
     * instance of the {@link Level} class, which carries with it all of the
     * necessary data to provide to the user.
     * <p>
     * It is important to note that if the {@link Level} of the specified
     * {@code Level} is either {@link Level#CRITICAL} or
     * {@link Level#FATAL}, then the default {@link ErrorReporter} will be
     * given over to the {@link ErrorPane} when it is shown to the user. If no
     * {@code ErrorReporter} is registered in the application, then no
     * &ldquo;Submit Bug Report&rdquo; button will appear on the dialog.
     *
     * @param info the notification
     */
    void notify(ErrorInfo info);

}
