/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   Application
 *  Class      :   StatusDisplayer.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 12, 2022
 *  Modified   :   Dec 12, 2022
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 12, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.logging.Level;
import com.pekinsoft.lookup.Lookup;

/**
 * {@code StatusDisplayer} allows for an application to provide an object that
 * displays status messages to the user. Typically, an application will only
 * provide a single {@code StatusDisplayer}, which will be, by default, the
 * *default {@code StatusDisplayer}*.
 * <p>
 * To retrieve the application's {@code StatusDisplayer} implementation, one
 * only needs to do the following in order to display a message in it:
 * <pre>
 * StatusDisplayer.getDefault().setMessage("message to set", Level.NORMAL);
 * </pre>
 * By doing the above, the default {@code StatusDisplayer} will display
 * the text "message to set" in its message component. By setting the type of
 * the message displayed to {@code Level.NORMAL}, the message will be
 * displayed in the desktop's default color. To alter the color, simply use one
 * of the other `Level` constants:
 * <ul>
 * <li>{@code Level.INFO} &mdash; <samp style="color: blue"><strong>
 * Displays in bold, blue text</strong></samp>.
 * <li>{@code Level.WARNING} &mdash; <samp style="color: orange"><strong>
 * Displays in bold, orange text</strong></samp>.
 * <li>{@code Level.ERROR} &mdash; <samp style="color: red"><strong>
 * Displays in bold, red text</strong></samp>.
 * <li>{@code Level.NORMAL} &mdash; <samp>Displays in the standard,
 * default colored text</samp>.
 * </ul>
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 1.0
 */
public interface StatusDisplayer {

    static StatusDisplayer getDefault() {
        return Lookup.getDefault().lookup(StatusDisplayer.class);
    }

    /**
     * Sets the message to be displayed in the {@code StatusDisplayer}
     * implementation, as well as the type of message that is to be displayed,
     * which needs to be one of the `Level` constants:
     * <p>
     * - {@code Level.INFO} &mdash; <samp style="color: blue">**
     * Displays in bold, blue text**</samp>.
     * - {@code Level.WARNING} &mdash;
     * <samp style="color: orange">**
     * Displays in bold, orange text**</samp>.
     * - {@code Level.ERROR} &mdash; <samp style="color: red">**
     * Displays in bold, red text**</samp>.
     * - {@code Level.NORMAL} &mdash; <samp>Displays in the standard,
     * default colored text</samp>.
     *
     * @param message the message text to be displayed
     * @param type    the type of message to be displayed; one of the
     *                `Level`
     *                constants for `NORMAL`, `INFO`, `WARNING`,
     *                or `ERROR`
     */
    public void setMessage(String message, Level type);

    /**
     * Convenience method for setting a {@code Level.NORMAL} message to
     * the {@code StatusDisplayer} implementation. This default method simply
     * calls the {@link #setMessage(java.lang.String, com.pekinsoft.spi.StatusDisplayer.Level)
     * setMessage(String, Level)} method passing on the `message`
     * and setting the type to {@code Level.NORMAL}.
     *
     * @param message the message text to be displayed
     */
    public default void setMessage(String message) {
        setMessage(message, Level.NORMAL);
    }

}
