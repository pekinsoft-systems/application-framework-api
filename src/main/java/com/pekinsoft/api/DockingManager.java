/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   DockingManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.desktop.Dockable;
import com.pekinsoft.lookup.Lookup;
import com.pekinsoft.plugins.Plugin;

/**
 * {@code DockingManager} is a {@link WindowManager} extension that allows for
 * registering/deregistering, docking/undocking, pinning/unpinning, and
 * floating/unfloating docks from within the main window.
 * <p>
 * This manager requires that all {@link Dockable Dockables} be registered prior
 * to being used, and being deregistered when they are no longer needed.
 * Typically, a {@code Dockable} implementation will register itself with the
 * {@code DockingManager} in its constructor, so that it is ready for use. If
 * the {@link com.pekinsoft.framework.Application Application} is a modular one,
 * the {@link Plugin} may register its {@code Dockable}s when it is activated,
 * and deregister its {@code Dockable}s when it is deactivated.
 * <p>
 * Once registered, a {@code Dockable} may be docked/undocked, pinned/unpinned,
 * and floated/unfloated by calling the appropriate method and passing the
 * {@code Dockable}'s {@code dockId} value. When the {@code Dockable} is no
 * longer required by the {@code Application}, it may be deregistered by passing
 * it's {@code dockId} to the {@link #deregister(String) deregister} method.
 * <p>
 * Once registered, the {@code Dockable} may have all available actions taken on
 * it, and if it is undocked, it will still be in existence. It is not until the
 * {@code Dockable} is deregistered that its {@link Dockable#close() close}
 * method is called.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 1.5
 */
public interface DockingManager extends WindowManager {

    /**
     * Retrieves the default implementation of the {@code DockingManager}
     * interface. This method simply uses the
     * {@link com.pekinsoft.lookup.Lookup Lookup API} to find an provider of
     * this interface.
     *
     * @return the first found implementation of {@code DockingManager}
     */
    public static DockingManager getDefault() {
        return (DockingManager) Lookup.getDefault().lookup(DockingManager.class);
    }

    /**
     * Registers the specified {@link Dockable dock} with the docking framework.
     * <p>
     * All {@code Dockable}s <em>must be registered <strong>prior to
     * use</strong></em>. Any attempt to use a {@code Dockable} that has not
     * been previously registered, or that has already been
     * {@link #deregister(String) deregistered}, will result in an
     * {@link java.lang.IllegalStateException IllegalStateException} being
     * thrown.
     * <p>
     * Once registered, the {@code Dockable} exists in the docking framework
     * until such time that it is deregistered. Even if the {@code Dockable} is
     * not currently visible, its reference within the framework still exists.
     * <p>
     * If the specified {@code dock} is {@code null}, a
     * {@link NullPointerException} is thrown.
     * <p>
     * If the specified {@code dock} has already been registered, a
     * {@link DockRegistrationException}.
     *
     * @param dock the {@code Dockable} to be registered
     *
     * @throws NullPointerException      if the specified {@code dock} is
     *                                   {@code null}
     * @throws DockRegistrationException if the specified {@code dock} has
     *                                   already been registered
     *
     * @see #deregister(java.lang.String)
     */
    void register(Dockable dock);

    /**
     * Deregisters the specified {@link Dockable dock} from the docking
     * framework.
     * <p>
     * Once the {@code Dockable} is deregistered, its {@link Dockable#close()}
     * method is called so that it may perform any necessary cleanup it may need
     * to do. When this method returns, the {@code Dockable} no longer exists in
     * the docking framework and will need to be
     * {@link #register(Dockable) registered} again before being used.
     * <p>
     * If the specified {@code dockId} is blank, empty, or {@code null}, no
     * exception is thrown and no action is taken.
     * <p>
     * If the specified {@code dockId} does not exist in the docking framework's
     * registry, a {@link DockRegistrationException} is thrown.
     *
     * @param dockId the identifier of the {@code Dockable} to deregister
     *
     * @throws DockRegistrationException if the {@code Dockable} was never
     *                                   registered
     *
     * @see #register(com.pekinsoft.api.Dockable)
     */
    void deregister(String dockId);

    /**
     * Determines whether a {@link Dockable} with the specified {@code dockId}
     * has been registered. This method should be called <em>prior</em> to
     * {@link #register(Dockable) registering} or
     * {@link #deregister(String) deregistering} a {@code Dockable} to prevent a
     * {@link DockRegistrationException} from being thrown.
     * <p>
     * If the specified {@code dockId} is blank, empty, or {@code null}, a
     * {@link NullPointerException} is thrown.
     *
     * @param dockId the unique identifier for a {@code Dockable} to check if it
     *               has been registered
     *
     * @return {@code true} if a {@code Dockable} with the specified
     *         {@code dockId} has been registered with the framework
     *
     * @throws NullPointerException if the specified {@code dockId} is blank,
     *                              empty, or {@code null}
     * @see #register(com.pekinsoft.api.Dockable)
     * @see #deregister(java.lang.String)
     */
    boolean isRegistered(String dockId);

    /**
     * Docks the {@code Dockable} according to its desired
     * {@link Dockable#getDockingLocation() docking location}.
     * <p>
     * If the specified {@code dockid} is blank, empty, or {@code null}, no
     * exception is thrown and no action is taken.
     * <p>
     * If the specified {@code dockId} has not been previously used to
     * {@link #register(Dockable) register} a {@code Dockable}, a
     * {@link DockRegistrationException} is thrown.
     *
     * @param dockId the {@code Dockable}'s unique identifier string
     *
     * @throws DockRegistrationException if the {@code Dockable} has not been
     *                                   previously registered
     */
    void dock(String dockId);

    /**
     * Undocks the {@link Dockable} identified by the specified unique
     * identifier string. When undocked, the {@link Dockable#close() } method is
     * called to destroy the {@code Dockable} instance.
     * <p>
     * If the specified {@code dockId} is blank, empty, or {@code null}, no
     * exception is thrown and no action is taken.
     * <p>
     * If the specified {@code dockId} has not been previously used to
     * {@link #register(Dockable) register} a {@code Dockable}, a
     * {@link DockRegistrationException} is thrown.
     *
     * @param dockId the {@code Dockable}'s unique identifier string
     *
     * @throws DockRegistrationException if the {@code Dockable} has not been
     *                                   previously registered
     */
    void undock(String dockId);

}
