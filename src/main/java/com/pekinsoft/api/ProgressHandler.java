/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   ProgressHandler.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.lookup.Lookup;

/**
 * {@code ProgressHandler} simply updates the progress of a
 * {@link javax.swing.JProgressBar JProgressBar} within an
 * {@link com.pekinsoft.framework.Application Application}. Typically, this
 * progress bar will be located on the main window's status bar and provide
 * visual feedback of some background task to the user.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 2.0
 */
public interface ProgressHandler {

    /**
     * Retrieves the first located instance of {@code ProgressHandler} via the
     * {@link com.pekinsoft.lookup.Lookup Lookup} mechanism.
     *
     * @return the default {@code ProgressHandler} implementation
     */
    public static ProgressHandler getDefault() {
        return Lookup.getDefault().lookup(ProgressHandler.class);
    }

    /**
     * Sets the new value of the {@link javax.swing.JProgressBar progress bar}
     * in the {@link com.pekinsoft.framework.Application Application}.
     *
     * @param progress the new value of the progress
     */
    void setProgress(int progress);

}
