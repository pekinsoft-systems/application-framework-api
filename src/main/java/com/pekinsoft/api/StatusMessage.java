/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   StatusMessage.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.logging.Level;

/**
 * {@code StatusMessage} is a class that may be created and fired as a
 * {@link java.beans.PropertyChangeEvent PropertyChangeEvent} on the
 * &ldquo;statusMessage&rdquo; property against the
 * {@link com.pekinsoft.framework.Application Application} or the
 * {@link com.pekinsoft.framework.ApplicationContext context} to display an
 * ephemeral message to the user.
 * <p>
 * An instance of the class <em>must be</em> constructed with a message value,
 * and that value <em>may not be</em> {@code null}. If the specified
 * {@link com.pekinsoft.logging.Level level} is {@code null}, it will
 * be
 * defaulted to the
 * {@link com.pekinsoft.logging.Level#INFO Level.INFO} level.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 1.0
 */
public class StatusMessage {

    private final String message;
    private final Level level;

    /**
     * Constructs a {@code StatusMessage} to be displayed to the user via the
     * {@link com.pekinsoft.framework.Application Application's} UI. Typically,
     * this message would be displayed on a status bar and would be ephemeral in
     * nature. In other words, the message supplied would only be visible for a
     * short time, and then disappear.
     * <p>
     * A {@code StatusMessage} should be localized for the
     * {@link java.util.Locale Locale} in which the application is being
     * executed.
     * <p>
     * If the {@code message} is {@code null}, an
     * {@link java.lang.IllegalArgumentException IllegalArgumentException} is
     * thrown.
     *
     * @param message the message to be displayed to the user
     * @param level   the {@link com.pekinsoft.logging.Level level}
     *                of the
     *                message
     *
     * @throws IllegalArgumentException if the {@code message} is {@code null}
     *
     * @see #StatusMessage(java.lang.String)
     */
    public StatusMessage(String message, Level level) {
        if (message == null) {
            throw new IllegalArgumentException("The message cannot be null.");
        }
        if (level == null) {
            level = Level.INFO;
        }

        this.message = message;
        this.level = level;
    }

    /**
     * Constructs a basic {@code StatusMessage} that will display at the
     * {@link com.pekinsoft.logging.Level#INFO Level.INFO} level.
     * <p>
     * A {@code StatusMessage} should be localized for the
     * {@link java.util.Locale Locale} in which the application is being
     * executed.
     * <p>
     * If the {@code message} is {@code null}, an
     * {@link java.lang.IllegalArgumentException IllegalArgumentException} is
     * thrown.
     *
     * @param message the message to be displayed to the user
     *
     * @throws IllegalArgumentException if the {@code message} is {@code null}
     *
     * @see #StatusMessage(String, Level)
     */
    public StatusMessage(String message) {
        this(message, Level.INFO);
    }

    /**
     * Retrieves the {@code StatusMessage} to be displayed to the user.
     *
     * @return the value of the {@code message} property with which this
     *         {@code StatusMessage} was constructed
     */
    public String getMessage() {
        return message;
    }

    /**
     * Retrieves the {@link com.pekinsoft.logging.Level level} at
     * which this
     * message should be displayed.
     *
     * @return the value of the {@code level} property with which this
     *         {@code StatusMessage} was constructed
     */
    public Level getLevel() {
        return level;
    }

}
