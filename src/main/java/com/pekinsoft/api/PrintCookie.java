/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework Application API
 *  Class      :   PrintCookie.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 8, 2022
 *  Modified   :   Dec 8, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 8, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.api;

/**
 * Exposes a single method to allow for a component to provide the ability to 
 * create print outs of its data.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 2.4
 * @since 1.0
 */
public interface PrintCookie {
    
    /**
     * Provides a means for an object to provide printing capability for itself.
     * This method will be called by any print action that may be available in
     * an {@code com.pekinsoft.framework.Application Application}.
     */
    public void print();

}
