/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   ExitListener.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 12, 2024
 *  Modified   :   Nov 12, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 12, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.framework.Application;
import java.util.EventObject;

/**
 * Give the Application a chance to veto an attempt to exit/quit. An
 * {@code ExitListener}'s {@code canExit} method should return false if
 * there are pending decisions that the user must make before the app exits.
 * A typical {@code ExitListener} would prompt the user with a modal dialog.
 * <p>
 * The {@code EventObject} argument will be the the values passed to
 * {@link Application#exit(EventObject) exit()}. It may be {@code null}.
 * <p>
 * The {@code willExit} method is called after the exit has been confirmed.
 * An ExitListener that's going to perform some cleanup work should do so in
 * {@code willExit}.
 * <p>
 * {@code ExitListener}s run on the event dispatching thread.
 * <p>
 * The {@link Application} class maintains the
 * {@link Application#addExitListener(ExitListener) addExitListener} and
 * {@link Application#removeExitListener(ExitListener) removeExitListener}
 * methods for historical purposes. While the {@code ExitListener} list is still
 * checked during exit, the {@code Application} mostly relies on the
 * {@link com.pekinsoft.lookup.Lookup Lookup} for locating
 * {@code ExitListener}s, so it is good practice to prepare your
 * {@code ExitListener} implementations for when those add/remove methods are
 * removed.
 *
 * @see Application#exit(EventObject)
 * @see Application#addExitListener
 * @see Application#removeExitListener
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public interface ExitListener {

    boolean canExit(EventObject event);

    void willExit(EventObject event);

}
