/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   NTOS_Project
 *  Class      :   SaveCookie.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 7, 2022
 *  Modified   :   Oct 7, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 7, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.api;

/**
 * {@code SaveCookie} is an interface that allows for objects to identify
 * themselves as being savable. Applications can then find all of the objects
 * that identify as such and call their {@link #save() save} methods to make
 * sure that any changes to the underlying data are saved properly.
 * <p>
 * {@code SaveCookie}s must be properly
 * {@link SaveCookieRegistrar#register(SaveCookie) registered} in order to be
 * called from a global context.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 2.4
 * @since 1.0
 */
public interface SaveCookie {
    
    /**
     * Provides the method for saving changes to the class that has implemented
     * this interface.
     * 
     * @return {@code true} upon successful save; {@code false} on failure
     */
    boolean save();
    
}
