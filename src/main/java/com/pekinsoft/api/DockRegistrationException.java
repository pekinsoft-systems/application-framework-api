/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   DockRegistrationException.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.api;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class DockRegistrationException extends RuntimeException {

    /**
     * Creates a new instance of <code>DockRegistrationException</code> without detail message.
     */
    public DockRegistrationException() {
    }


    /**
     * Constructs an instance of <code>DockRegistrationException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public DockRegistrationException(String msg) {
        super(msg);
    }
}
