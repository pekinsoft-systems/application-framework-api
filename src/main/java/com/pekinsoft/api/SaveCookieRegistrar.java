/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   SaveCookieRegistrar.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.api;

import com.pekinsoft.lookup.Lookup;

/**
 * Any class that wants to be a registrar for {@link SaveCookie save cookies}
 * must implement this interface. {@code SaveCookieRegistrar} exposes those
 * methods required to register and manage {@code SaveCookie}s:
 * {@link #register(SaveCookie) register},
 * {@link #deregister(SaveCookie) deregister}, and
 * {@link #getRegisteredSaveCookies() getRegisteredSaveCookies}.
 *
 * @see SaveCookie
 * @see #register(SaveCookie)
 * @see #deregister(SaveCookie)
 * @see #getRegisteredSaveCookies()
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public interface SaveCookieRegistrar {

    /**
     * Retrieves the first implementation of {@code SaveCookieRegistrar} that is
     * found in the {@link com.pekinsoft.lookup.Lookup Lookup}. If there is no
     * implementation of {@code SaveCookieRegistrar}, then {@code null} is
     * returned. If more than one class implements {@code SaveCookieRegistrar},
     * only the first one located is returned.
     *
     * @return the first implementation of {@code SaveCookieRegistrar} found, or
     *         {@code null}
     */
    static SaveCookieRegistrar getDefault() {
        return Lookup.getDefault().lookup(SaveCookieRegistrar.class);
    }

    /**
     * Registers the {@code SaveCookie}. This method is typically called from an
     * implementation of {@code SaveCookie} whenever its data has been changed
     * and is needing to be saved.
     * <p>
     * If the specified {@code cookie} is {@code null} or has already been
     * registered, no exception is thrown and no action is taken.
     *
     * @param cookie the {@code SaveCookie} to register
     *
     * @see #deregister(SaveCookie)
     * @see #getRegisteredSaveCookies()
     */
    void register(SaveCookie cookie);

    /**
     * Deregisters the {@link SaveCookie}. The method is typically called from
     * the {@link SaveCookie#save() SaveCookie.save} method once it has been
     * called.
     * <p>
     * If the specified {@code cookie} is {@code null} or has not been
     * previously registered, no exception is thrown and no action is taken.
     *
     * @param cookie the {@code SaveCookie} to be deregistered
     *
     * @see #register(SaveCookie)
     * @see #getRegisteredSaveCookies()
     */
    void deregister(SaveCookie cookie);

    /**
     * Retrieves a <em>read-only</em> list of all {@code SaveCookie}s
     * {@link #register(SaveCookie) registered} so far. If none have been
     * registered, or all registered {@code SaveCookie}s have already been
     * {@link #deregister(SaveCookie) deregistered}, then an empty list is
     * returned. This method is guaranteed to never return {@code null}.
     *
     * @return a <em>read-only</em> list of all registered {@code SaveCookie}s,
     *         or an empty list
     *
     * @see #register(SaveCookie)
     * @see #deregister(SaveCookie)
     */
    SaveCookie[] getRegisteredSaveCookies();

}
