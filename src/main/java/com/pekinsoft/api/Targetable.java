/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Targetable.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.api;

import com.pekinsoft.framework.TargetManager;
import java.util.EventObject;

/**
 * An interface which exposes the allowable actions to a TargetManager. The
 * getCommands method will expose the allowable actions to another class and the
 * doCommand method is called to invoke an action on the class.
 * <p>
 * Usually, the command key will be the key value of the Action. For components
 * This could be the ActionMap keys. For actions managed with the ActionManager,
 * this will be the value of an actions Action.ACTION_COMMAND_KEY
 *
 * @see TargetManager
 * @author Sean Carrick
 * 
 * @version 2.4
 * @version 2.0
 */
public interface Targetable {

    static final String COMMON_ACTION_CANCEL = "cancel";
    static final String COMMON_ACTION_EDIT = "edit";
    static final String COMMON_ACTION_INSPECT = "inspect";
    static final String COMMON_ACTION_NEW = "addNew";
    static final String COMMON_ACTION_OPEN = "open";
    static final String COMMON_ACTION_PRINT = "print";
    static final String COMMON_ACTION_REFRESH = "refresh";
    static final String COMMON_ACTION_SAVE = "save";
    static final String COMMON_ACTION_SAVE_AS = "saveAs";

    /**
     * Perform the command using the object value.
     *
     * @param command is a Action.ACTION_COMMAND_KEY
     * @param value   an arbitrary value. Usually this will be EventObject which
     *                triggered the command.
     */
    boolean doCommand(String command, EventObject value);

    /**
     * Return a flag that indicates if a command is supported.
     *
     * @param command is a Action.ACTION_COMMAND_KEY
     *
     * @return true if command is supported; false otherwise
     */
    boolean hasCommand(String command);

    /**
     * Returns an array of supported commands. If this Targetable doesn't
     * support any commands (which is unlikely) then an empty array is returned.
     *
     * @return array of supported commands
     */
    String[] getCommands();
}
