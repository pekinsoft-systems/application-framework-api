/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-utilities
 *  Class      :   PropertyKeys.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 11, 2024
 *  Modified   :   Jun 11, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 11, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import com.pekinsoft.api.Targetable;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public interface PropertyKeys {

    /**
     * The {@link java.beans.PropertyChangeEvent PropertyChangeEvent} key for
     * showing an {@link com.pekinsoft.desktop.error.ErrorInfo ErrorInfo}
     * notification
     * to the user via the <em>Notifications System</em> of the Platform.
     * <p>
     * This property is expecting either the aforementioned {@code Level}},
     * or alternatively, an
     * {@link com.pekinsoft.desktop.error.ErrorReporter ErrorReporter} instance
     * so that
     * the user may choose to submit a bug report to the developer.
     */
    public static final String KEY_ADD_NOTIFICATION = "notification";
    /**
     * The {@link java.beans.PropertyChangeEvent PropertyChangeEvent} key for
     * removing an {@link com.pekinsoft.desktop.error.ErrorInfo ErrorInfo}
     * notification
     * from the <em>Notifications System</em>, typically because the user either
     * viewed the notification (automatic removal) or manually deleted the
     * notification.
     * <p>
     * This property is expecting the aforementioned {@code Level}} instance
     * that is to be removed from the system.
     */
    public static final String KEY_REMOVE_NOTIFICATION = "removeNotification";
    /**
     * The key for determining whether the {@code Application} is currently
     * being run inside of a development environment.
     * <p>
     * This is a {@code boolean} property. Its return value indicates whether
     * the application is running in a development ({@code true}) or production
     * ({@code false}) environment. It is set by the command line parameter
     * &ldquo;{@code --ide}&rdquo;. Of course, a subclass could change this
     * argument name as it sees fit.
     */
    public static final String KEY_DEVEL_ENVIRONMENT = "development.environment";
    /**
     * The {@link java.beans.PropertyChangeEvent PropertyChangeEvent} key for
     * updating the progress display of a running background
     * {@link com.pekinsoft.framework.BackgroundTask BackgroundTask}.
     * <p>
     * This property is expecting an {@code int} value that indicates the
     * current progress value.
     */
    public static final String KEY_TASK_PROGRESS = "progress";
    /**
     * The {@link java.beans.PropertyChangeEvent PropertyChangeEvent} key for
     * for messages that are sent out by currently executing background
     * {@link com.pekinsoft.framework.BackgroundTask Tasks}. These ephemeral
     * messages
     * are typically related to the process(es) the background BackgroundTask is
     * currently computing.
     * <p>
     * This property is expecting a {@code String} value that contains the
     * contents of the message to be displayed to the user. This value should be
     * localized based upon the system's current Locale.
     */
    public static final String KEY_TASK_MESSAGE = "message";
    /**
     * The {@link java.beans.PropertyChangeEvent PropertyChangeEvent} key for
     * for status messages that are typically not fired by background Tasks.
     * These ephemeral messages provide various information that needs to be
     * shown to the user, and should have their values localized to the system's
     * current Locale.
     * <p>
     * This property is expecting an instance of the
     * {@link com.pekinsoft.api.StatusMessage StatusMessage} object, that
     * carries with it an
     * {@link com.pekinsoft.logging.Level Level} value
     * that determines the type of message being displayed.
     */
    public static final String KEY_STATUS_MESSAGE = "statusMessage";
    /**
     * The {@link java.beans.PropertyChangeEvent PropertyChangeEvent} key for
     * when a background
     * {@link com.pekinsoft.framework.BackgroundTask BackgroundTask}
     * has begun executing. This state change is fired by the
     * {@code BackgroundTask} superclass, and subclasses need not worry about
     * it.
     * <p>
     * This property is expecting a {@code boolean} value.
     */
    public static final String KEY_TASK_STARTED = "started";
    /**
     * The {@link java.beans.PropertyChangeEvent PropertyChangeEvent} key for
     * when a background
     * {@link com.pekinsoft.framework.BackgroundTask BackgroundTask}
     * has completed executing. This state change is fired by the
     * {@code BackgroundTask} superclass when the final completion method has
     * fired, and subclasses need not worry about it.
     * <p>
     * This property is expecting a {@code boolean} value.
     */
    public static final String KEY_TASK_DONE = "done";
    /**
     * Key for whether the application has a {@link Targetable} action available
     * to it. This property is used by the actions system for the enablement of
     * targetable actions in the menus, BackgroundTask panes, and toolbars.
     */
    public static final String KEY_APPLICATION_TARGET_AVAILABLE = "appTargetAvailable";

}
