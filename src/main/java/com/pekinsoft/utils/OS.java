/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ApplicationAPI
 *  Class      :   OS.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 8, 2024
 *  Modified   :   Jun 8, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 8, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

/**
 * A utility class for determining, with some specificity, on which operating
 * system an application is running. This information is imporatant for such
 * things as placing application files in the appropriate place and, possibly,
 * for OS-specific resources.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class OS {

    public static String getOsName() {
        return os;
    }

    public static boolean isMacOSX() {
        return osIsMacOsX;
    }

    public static boolean isWindows() {
        return osIsWindows;
    }

    public static boolean isWindowsXP() {
        return osIsWindowsXP;
    }

    public static boolean isWindows2003() {
        return osIsWindows2003;
    }

    public static boolean isWindowsVista() {
        return osIsWindowsVista;
    }

    public static boolean isWindows7() {
        return osIsWindows7;
    }

    public static boolean isWindows8() {
        return osIsWindows8;
    }

    public static boolean isWindows10() {
        return osIsWindows10;
    }

    public static boolean isWindows11() {
        return osIsWindows11;
    }

    public static boolean isLinux() {
        return osIsLinux;
    }

    public static boolean isSolaris() {
        return osIsSolaris;
    }

    private OS() {
        // No instantiation allowed.
    }

    private static final boolean osIsMacOsX;
    private static final boolean osIsWindows;
    private static final boolean osIsWindowsXP;
    private static final boolean osIsWindows2003;
    private static final boolean osIsWindowsVista;
    private static final boolean osIsWindows7;
    private static final boolean osIsWindows8;
    private static final boolean osIsWindows10;
    private static final boolean osIsWindows11;
    private static final boolean osIsLinux;
    private static final boolean osIsSolaris;

    private static final String os;

    static {
        String osName = System.getProperty("os.name");
        if (osName != null) {
            os = osName.toLowerCase();
        } else {
            os = "unknown";
        }

        osIsMacOsX = "mac os x".equals(os);
        osIsWindows = os != null && os.contains("windows");
        osIsWindowsXP = "windows xp".equals(os);
        osIsWindows2003 = "windows 2003".equals(os);
        osIsWindowsVista = "windows vista".equals(os);
        osIsWindows7 = "windows 7".equals(os);
        osIsWindows8 = "windows 8".equals(os);
        osIsWindows10 = "windows 10".equals(os);
        osIsWindows11 = "windows 11".equals(os);
        osIsLinux = os != null && os.contains("linux");
        osIsSolaris = os != null && os.contains("solaris");
    }

}
