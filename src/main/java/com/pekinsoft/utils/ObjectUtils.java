/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   AppFrameworkAPI
 *  Class      :   ObjectUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   May 28, 2024
 *  Modified   :   May 28, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  May 28, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class ObjectUtils {

    /**
     * Retrieves a debugging string for the specified {@link Object}. The string
     * returned is useful for debugging purposes as it contains the complete
     * state of the object.
     * <p>
     * For example, for a class defined as follows:{@snippet lang="java":
     * package com.myco.employees;
     *
     * class Employee {
     *     private final String name;
     *     private final Date hireDate;
     *     private double salary;
     *
     *     public Employee(String name, int month, int day, int year, double salary) {
     *         this.name = name;
     *         GregorianCalendar cal = new GregorianCalendar(year, month - 1, day);
     *         this.hireDate = cal.getTime();
     *         this.salary = salary;
     *     }
     *
     *     // The getters for the fields.
     *
     *     public void giveRaise(int percent) {
     *         if (percent < 0 || percent > 1) { throw new
     * IllegalArgumentException("Percentage of raise is out " + "range: 0 - 1,
     * inclusive."); }
     *
     * double raiseRate = percent / 100; double raiseAmount = getSalary() *
     * raiseRate; this.salary = this.salary + raiseAmount; }
     * }
     * }
     * <p>
     * Later on in the application, an instance of the <em>Employee</em> class
     * is created, and passed to this method to get a debugging string:{@snippet lang="java":
     * Employee employee = new Employee("John Dough", 2022, 1, 17, 50000.00);
     * System.out.println(ObjectUtils.toString(employee);
     * employee.giveRaise(5);
     * System.out.println(ObjectUtils.toString(employee);
     * }
     * <p>
     * In this example, the output of the program would be:
     * <pre>
     * com.myco.employees.Employee[name=John Dough, hireDate=Mon Jan 17 00:00:00 CST 2022, salary=50000.0]
     * com.myco.employees.Employee[name=John Dough, hireDate=Mon Jan 17 00:00:00 CST 2022, salary=52500.0]
     * </pre>
     * <p>
     * As can be seen from the example above, this method provides the complete
     * state of the {@code Object} at the time it is passed to this method. By
     * using this method to generate the debugging string, it frees up the
     * {@link Object#toString() Object.toString} method to be used to create
     * user-friendly string representations of the {@code Object} for use as the
     * display value in {@link javax.swing.JTree JTree nodes} and
     * {@link javax.swing.JTable JTable cells}, for example.
     * <p>
     * If the specified {@code obj} is {@code null}, then a {@code null} value
     * is returned.
     *
     * @param obj the {@code Object} for which a debugging string should be
     *            retrieved
     *
     * @return a string that is explains the current state of the {@code Object}
     */
    public static String debuggingString(Object obj) {
        if (obj == null) {
            return null;
        }
        Class cl = obj.getClass();
        StringBuilder sb = new StringBuilder(obj.getClass().getName());

        do {
            sb.append("[");
            Field[] fields = cl.getDeclaredFields();
            AccessibleObject.setAccessible(fields, true);

            // Get the names and values of all the fields.
            for (int i = 0; i < fields.length; i++) {
                Field f = fields[i];
                sb.append(f.getName()).append("=");
                try {
                    Object val = f.get(obj);
                    sb.append(val.toString());
                } catch (IllegalAccessException
                        | IllegalArgumentException e) {
                    e.printStackTrace(System.err);
                }

                if (i < fields.length - 1) {
                    sb.append(", ");
                }
            }

            sb.append("]");
            cl = cl.getSuperclass();
        } while (cl != Object.class);

        return sb.toString();
    }

    /**
     * Tests whether the two specified {@link Object Objects} are equal,
     * including their superclasses in the testing, up to but not including the
     * {@code Object} class.
     * <p>
     * This method is guaranteed to enforce and abide by all of the rules for
     * equality as set forth in the JavaDocs for {@link Object#equals(Object)}.
     *
     * @param a an {@code Object}
     * @param b another {@code Object} for comparison
     *
     * @return {@code true} if the objects are completely equal; {@code false}
     *         otherwise
     */
    public static boolean equals(Object a, Object b) {
        if (a == b) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        Class aC = a.getClass();
        if (aC != b.getClass()) {
            return false;
        }
        do {
            Field[] fields = a.getClass().getDeclaredFields();
            AccessibleObject.setAccessible(fields, true);
            for (int i = 0; i < fields.length; i++) {
                try {
                    Field f = fields[i];
                    if (!f.get(a).equals(f.get(b))) {
                        return false;
                    }
                } catch (IllegalAccessException
                        | IllegalArgumentException e) {
                    e.printStackTrace(System.err);
                }
            }
            aC = aC.getSuperclass();
        } while (aC != Object.class);

        return true;
    }

    private ObjectUtils() {
        // No instantiation allowed.
    }

}
