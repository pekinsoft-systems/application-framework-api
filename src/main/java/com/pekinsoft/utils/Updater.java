/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   pekinsoft-launcher-module
 *  Class      :   Updater.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 9, 2024
 *  Modified   :   Jul 9, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 9, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.utils;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Updater {
    
    public Updater () {
        
    }
    
    public boolean hasInternetConnection() {
        // TODO: Fix hasInternetConnection to actually check for connection.
        return true;
    }
    
    public boolean areUpdatesAvailable() {
        // TODO: Fix areUpdatesAvailable to actually check for updates.
        return false;
    }
    
    public void update() {
        // TODO: Implement the update method.
    }

}
