/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   com-pekinsoft-api
 *  Class      :   DateTimeUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 13, 2024
 *  Modified   :   Jan 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author Sean Carrick (sean@pekinsoft.com)
 *
 * @version 1.0
 * @since 1.0
 */
public class DateTimeUtils {

    /**
     * Retrieves the difference between two {@link java.util.Date date} objects.
     *
     * @param earlierDate the earlier of the two dates
     * @param laterDate   the later of the two dates
     * @param timeUnit    the desired
     *                    {@link java.util.concurrent.TimeUnit unit of time} in
     *                    which to return the difference
     *
     * @return the difference between the two date object in the specified unit
     *         of time
     */
    public static long getDateDiff(Date earlierDate, Date laterDate,
            TimeUnit timeUnit) {
        long diffInMillis = laterDate.getTime() - earlierDate.getTime();

        return timeUnit.convert(diffInMillis, TimeUnit.MILLISECONDS);
    }

    public static boolean isLeapYear() {
        GregorianCalendar cal = (GregorianCalendar) Calendar.getInstance();
        return cal.isLeapYear(cal.get(Calendar.YEAR));
    }

    private DateTimeUtils() {

    }

}
