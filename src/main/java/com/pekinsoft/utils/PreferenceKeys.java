/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-management-api
 *  Class      :   PreferenceKeys.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 19, 2024
 *  Modified   :   Jun 19, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 19, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import java.text.MessageFormat;
import java.util.prefs.Preferences;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public interface PreferenceKeys {

    /**
     * {@code Preferences} key for the user-defined date format.
     */
    static final String PREF_DATE_FORMAT = "date.format";
    /**
     * {@code Preferences} key for the user-defined time format.
     */
    static final String PREF_TIME_FORMAT = "time.format";
    /**
     * {@code Preferences} key for the user-defined date/time format.
     */
    static final String PREF_DATE_TIME_FORMAT = "date.time.format";
    /**
     * {@code Preferences} key for the selected look and feel to be used by the
     * application.
     */
    static final String PREF_LOOK_AND_FEEL = "Application.lookAndFeel";
    /**
     * {@code Preferences} key for determining whether the project is running
     * within the development environment. Useful for handling different tasks
     * in a special manner when not in a production environment.
     */
    static final String PREF_DEVELOPMENT_ENVIRONMENT = "devel.environment";
    /**
     * {@code Preferences} key for the logging level for messages to be printed
     * and/or stored in the application's log file(s).
     */
    static final String PREF_LOG_LEVEL = "logging.level";
    /**
     * {@code Preferences} key for the user option of hiding or showing the text
     * of the actions in the application's toolbar(s).
     */
    static final String PREF_HIDE_ACTION_TEXT = "hide.action.text";
    /**
     * {@code Preferences} key for the user option of showing/hiding the
     * application's status bar.
     */
    static final String PREF_HIDE_STATUSBAR = "hide.status.bar";
    /**
     * {@code Preferences} key for having the application automatically log the
     * user out after a set period of inactivity.
     */
    static final String PREF_AUTO_LOGOUT = "auto.logout.for.inactivity";
    /**
     * {@code Preferences} key for the company policy for how long a user may
     * sit idle within the application before it automatically logs out and
     * blocks the application's window.
     */
    static final String PREF_LOGIN_TIMEOUT = "login.timeout.length";
    /**
     * {@code Preferences} key for storing whether the application requires
     * users to logon.
     */
    static final String PREF_REQUIRE_LOGON = "login.required";
    /**
     * {@code Preferences} key for the maximum number of logon attempts that
     * may be tried before the application exits for logon failure.
     */
    static final String PREF_MAX_LOGON_ATTEMPTS = "login.max.attempts";
    /**
     * {@code Preferences} key that determines whether a user is allowed to
     * store their logon password for future sessions.
     */
    static final String PREF_PASSWORD_ALLOW_STORAGE = "password.allow.storage";
    /**
     * Application's {@link Preferences} key for the last user to log into the
     * application. This value can be used to populate the username field on the
     * {@code LogonDialog} window.
     */
    static final String PREF_STORE_USER = "last.user";
    /**
     * {@code Preferences} key for where to store the user's password for future
     * sessions. This is a {@link MessageFormat} template, where the username is
     * the only parameter. The stored password should be hashed, and not in
     * clear text.
     */
    static final String PREF_PASSWORD_STORAGE = "{0}.last.hash";
    /**
     * {@code Preferences} key for the minimum length of a user's password.
     */
    static final String PREF_PASSWORD_MIN_LENGTH = "password.minimum.length";
    /**
     * {@code Preferences} key for the maximum length of a user's password. If
     * no maximum is set, this value should be set to -1 in the preferences.
     */
    static final String PREF_PASSWORD_MAX_LENGTH = "password.maximum.length";
    /**
     * {@code Preferences} key for the minimum number of uppercase letters that
     * should be included in a user's password.
     */
    static final String PREF_PASSWORD_MIN_UPPER = "password.minimum.uppercase";
    /**
     * {@code Preferences} key for the minimum number of symbols that should
     * be included in a user's password.
     */
    static final String PREF_PASSWORD_MIN_SYMBOLS = "password.minimum.symbols";
    /**
     * {@code Preferences} key for the minimum number of numbers that should
     * be included in a user's password.
     */
    static final String PREF_PASSWORD_MIN_NUMBERS = "password.minimum.numbers";
    /**
     * {@code Preferences} key for whether the user is required to change the
     * password at first logon. This is useful so systems/application
     * administrators may use a temporary password when creating the account.
     */
    static final String PREF_PASSWORD_AGING_CHANGE_FIRST = "password.change.first.use";
    /**
     * {@code Preferences} key for the length of time when the user will be
     * forced to change their password. This amount of time, plus the date on
     * which the user last changed their password, creates the expiration date.
     */
    static final String PREF_PASSWORD_AGING_MUST_CHANGE = "password.change.required";
    /**
     * {@code Preferences} key for the number of days prior to password
     * expiration that the user should be warned that their password is about to
     * expire.
     */
    static final String PREF_PASSWORD_AGING_WARN_PERIOD = "password.change.warning";
    /**
     * {@code Preferences} key for the number of days beyond the expiration date
     * of the password that the account remains active allowing the user to
     * change their password. Once this period expires, the account is
     * automatically deactivated and it will require systems/application
     * administrator intervention to reactivate the account.
     */
    static final String PREF_PASSWORD_AGING_GRACE_PERIOD = "password.change.grace";

}
