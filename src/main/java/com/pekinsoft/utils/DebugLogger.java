/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   DebugLogger.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 31, 2024
 *  Modified   :   Oct 31, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 31, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;

/**
 * A utility class for logging debugging messages, without the need of
 * cluttering up the class code with {@link Logger#isLoggable(Level) } checks.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class DebugLogger {

    /**
     * Logs messages at the {@link Level#TRACE} logging level, if the specified
     * {@code logger} will log it. The message may be a
     * {@link String#format(String, Object...) format string}.
     *
     * @param logger  the {@link Logger} to use
     * @param message the message to log (may be a {@code String.format} format
     * @param args    the arguments to the message for {@code String.format}
     */
    public static void trace(Logger logger, String message, Object... args) {
        if (logger.isLoggable(Level.TRACE)) {
            if (args != null) {
                logger.trace(String.format(message, args));
            } else {
                logger.trace(message);
            }
        }
    }

    /**
     * Logs messages at the {@link Level#DEBUG} logging level, if the specified
     * {@code logger} will log it. The message may be a
     * {@link String#format(String, Object...) format string}.
     *
     * @param logger  the {@link Logger} to use
     * @param message the message to log (may be a {@code String.format} format
     * @param args    the arguments to the message for {@code String.format}
     */
    public static void debug(Logger logger, String message, Object... args) {
        if (logger.isLoggable(Level.DEBUG)) {
            if (args != null) {
                logger.debug(String.format(message, args));
            } else {
                logger.debug(message);
            }
        }
    }

    /**
     * Logs messages at the {@link Level#CONFIG} logging level, if the specified
     * {@code logger} will log it. The message may be a
     * {@link String#format(String, Object...) format string}.
     *
     * @param logger  the {@link Logger} to use
     * @param message the message to log (may be a {@code String.format} format
     * @param args    the arguments to the message for {@code String.format}
     */
    public static void config(Logger logger, String message, Object... args) {
        if (logger.isLoggable(Level.CONFIG)) {
            if (args != null) {
                logger.config(String.format(message, args));
            } else {
                logger.config(message);
            }
        }
    }

    private DebugLogger() {

    }

}
