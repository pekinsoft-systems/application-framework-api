/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   AppFrameworkAPI
 *  Class      :   DebugString.java
 *  Author     :   Sean Carrick
 *  Created    :   May 1, 2024
 *  Modified   :   May 1, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  May 1, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;

/**
 * The {@code DebugString} class provides a single static method called
 * {@code toString}, which takes an object to convert to a debugging string that
 * can be logged.
 * <p>
 * The purpose for this class is due to some Java Swing components using the
 * class' {@code toString} method to display information that should be
 * formatted for the user of the application. Since those classes will have
 * their {@code toString} method otherwise occupied, this class'
 * {@code toString} method will create a string suitable for debugging purposes
 * to be printed to the console or log files.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class DebugString {

    public static final String toString(Object obj) {
        Class cl = obj.getClass();
        String r = cl.getName();

        do {
            r += " [";
            Field[] fields = cl.getDeclaredFields();
            AccessibleObject.setAccessible(fields, true);

            for (Field f : fields) {
                r += f.getName() + "=";
                try {
                    Object val = f.get(obj);
                    r += val.toString();
                } catch (IllegalAccessException
                        | IllegalArgumentException e) {
                    e.printStackTrace(System.err);
                }

                r += ", ";
            }

            if (r.endsWith(", ")) {
                r = r.substring(0, r.length() - 2);
            }
            r += "]";

            cl = cl.getSuperclass();
        } while (cl != Object.class);

        return r;
    }

    private DebugString() {
        // No instantiation allowed.
    }

}
