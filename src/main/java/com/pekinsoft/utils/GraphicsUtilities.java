/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   Application_Framework_API
 *  Class      :   GraphicsUtilities.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 14, 2024
 *  Modified   :   Apr 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import java.awt.*;
import static java.awt.GraphicsEnvironment.isHeadless;
import java.awt.image.BufferedImage;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class GraphicsUtilities {

    /**
     * Retrieves a new translucent compatible image of the specified width and
     * height. That is, the returned {@code BufferedImage} is compatible with
     * the graphics hardware. If the method is called in a headless environment,
     * then the returned {@code BufferedImage} will be compatible with the
     * source image.
     *
     * @param width  the width of the new image
     * @param height the height of the new image
     *
     * @return a new translucent compatible {@code BufferedImage} of the
     *         specified width and height
     */
    public static BufferedImage createCompatibleTranslucentImage(int width,
            int height) {
        return isHeadless()
                ? new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB)
                : getGraphicsConfiguration().createCompatibleImage(width, height, Transparency.TRANSLUCENT);
    }

    private static GraphicsConfiguration getGraphicsConfiguration() {
        return GraphicsEnvironment.getLocalGraphicsEnvironment()
                .getDefaultScreenDevice().getDefaultConfiguration();
    }

    private GraphicsUtilities() {

    }

}
