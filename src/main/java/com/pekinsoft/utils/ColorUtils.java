/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   AppUtils
 *  Class      :   ColorUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 2, 2022
 *  Modified   :   Dec 2, 2022
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 2, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import java.awt.Color;

/**
 * The {@code ColorUtils} class is derived from an example I found online for
 * finding the difference between colors. The author is named Martin Mustin and
 * the code is released under the GNU Lesser General Public License Version 3.
 *
 * These methods are useful in custom {@link javax.swing.table.TableModel
 * TableModels} when altering background colors of rows, to make sure that the
 * UI foreground text is visible and not difficult to see.
 *
 * For example, if the background color of a table row is set to: null {@snippet lang="java":
 * Color(0.940f, 0.940f, 1.0f)  // Extremely light blue
 * }
 * If the UI Manager has the foreground text in {@code TextField}s as a very
 * light color, due to the overall theme being a dark one (such as FlatLaf
 * Dark), then the text in this row would be difficult to see, at best, and not
 * visible at all at worst.
 *
 * Using these methods, one could make sure that a very light foreground color
 * is not used for the text in a table row that is using a very light
 * background. So, by testing the UI Manager's foreground color for a
 * {@code TextField}, one could set the foreground of their table row
 * accordingly, by using the UI Manager's foreground color if it is a dark
 * color, or an alternative color, such as {@code Color.BLACK} if it is not.
 * <p>
 * This class also contains 132 color constants that match the colors listed on
 * the <a href="https://www.w3schools.com/tags/ref_colornames.asp">W3Schools
 * HTML Color Names</a> page. Not all of those colors have been turned into
 * constants in this class, as the {@code Color} class has the basic colors,
 * such as {@code Color.WHITE}, {@code Color.BLACK}, {@code Color.DARK_GREY},
 * etc. The colors which the {@code Color} class already has constant for were
 * not repeated in this class' constants. However, this class did take into
 * account the two spellings of grey/gray and created two constant for each
 * grey/gray color, i.e. {@code DIM_GREY} and {@code DIM_GRAY}.
 *
 * @author Martin Mustin (copyrighted 2008-2011)
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.3
 * @since 1.0
 */
public class ColorUtils {

    private ColorUtils() {
        /*
         * Not instantiable.
         */
    }

    /**
     * Check if a color is more dark than light. Useful if an entity of this
     * color is labeled: use white or lighter color on a "dark" color and a
     * black or darker color on a "light" color.
     *
     * @param r red value of the color to check
     * @param g green value of the color to check
     * @param b blue value of the color to check
     *
     * @return {@code true} if the specified color is a "dark" color;
     *         {@code false} if it is a "light" color
     */
    public static boolean isDarkColor(double r, double g, double b) {
        double white = colorDistance(r, g, b, 1.0, 1.0, 1.0);
        double black = colorDistance(r, g, b, 0.0, 0.0, 0.0);
        return black < white;
    }

    /**
     * Check if the specified color is more dark than light. Useful if an entity
     * of this color is labeled: use white or lighter color on a "dark" color
     * and black or darker color on a "light" color.
     *
     * @param color2Check the {@link java.awt.Color Color} to check
     *
     * @return {@code true} if the specified color is a "dark" color;
     *         {@code false} if it is a "light" color
     */
    public static boolean isDarkColor(Color color2Check) {
        float r = color2Check.getRed() / 255.0f;
        float g = color2Check.getGreen() / 255.0f;
        float b = color2Check.getBlue() / 255.0f;
        return isDarkColor(r, g, b);
    }

    /**
     * Calculates the "distance" between two colors.
     *
     * @param color1 the first {@link java.awt.Color Color} to compare
     * @param color2 the second `Color` to compare
     *
     * @return the "distance" between the two specified `Color`s
     */
    public static double colorDistance(Color color1, Color color2) {
        float[] rgb1 = new float[3];
        float[] rgb2 = new float[3];
        color1.getColorComponents(rgb1);
        color2.getColorComponents(rgb2);
        return colorDistance(rgb1[0], rgb1[1], rgb1[2], rgb2[0], rgb2[1], rgb2[2]);
    }

    /**
     * Calculates the "distance" between the two colors.
     *
     * @param color1 the first color to compare
     * @param color2 the second color to compare
     *
     * @return the "distance" between the two specified colors
     */
    public static double colorDistance(double[] color1, double[] color2) {
        return colorDistance(color1[0], color1[1], color1[2], color2[0],
                color2[1], color2[2]);
    }

    /**
     * Calculates the "distance" between the two colors. The RGB entries are
     * taken to be coordinates in a 3D space (0.0 - 1.0], and this method
     * returns the distance between the coordinates for the first and second
     * colors.
     *
     * @param r1 red value of the first color
     * @param g1 green value of the first color
     * @param b1 blue value of the first color
     * @param r2 red value of the second color
     * @param g2 green value of the second color
     * @param b2 blue value of the second color
     *
     * @return the "distance" between the two colors
     */
    public static double colorDistance(double r1, double g1, double b1, double r2,
            double g2, double b2) {
        double r = r2 - r1;
        double g = g2 - g1;
        double b = b2 - b1;
        return Math.sqrt(r * r + g * g + b * b);
    }

    public static final Color ALICE_BLUE = new Color(240, 248, 255);
    public static final Color ANTIQUE_WHITE = new Color(250, 235, 215);
    public static final Color AQUA = new Color(0, 255, 255);
    public static final Color AQUAMARINE = new Color(127, 255, 212);
    public static final Color AZURE = new Color(240, 255, 255);
    public static final Color BEIGE = new Color(245, 245, 220);
    public static final Color BISQUE = new Color(255, 228, 196);
    public static final Color BLANCHED_ALMOND = new Color(255, 235, 205);
    public static final Color BLUE_VIOLET = new Color(138, 43, 226);
    public static final Color BROWN = new Color(165, 42, 42);
    public static final Color BURLYWOOD = new Color(222, 184, 135);
    public static final Color CADET_BLUE = new Color(95, 158, 160);
    public static final Color CHARTREUSE = new Color(127, 255, 0);
    public static final Color CHOCOLATE = new Color(210, 105, 30);
    public static final Color CORAL = new Color(255, 127, 80);
    public static final Color CORNFLOWER_BLUE = new Color(100, 149, 237);
    public static final Color CORNSILK = new Color(255, 248, 220);
    public static final Color CRIMSON = new Color(220, 20, 60);
    public static final Color DARK_BLUE = new Color(0, 0, 139);
    public static final Color DARK_CYAN = new Color(0, 139, 139);
    public static final Color DARK_GOLDENROD = new Color(184, 134, 11);
    public static final Color DARK_GREEN = new Color(0, 100, 0);
    public static final Color DARK_KHAKI = new Color(189, 183, 107);
    public static final Color DARK_MAGENTA = new Color(139, 0, 139);
    public static final Color DARK_OLIVE_GREEN = new Color(85, 107, 47);
    public static final Color DARK_ORANGE = new Color(255, 140, 0);
    public static final Color DARK_ORCHID = new Color(153, 50, 204);
    public static final Color DARK_RED = new Color(139, 0, 0);
    public static final Color DARK_SALMON = new Color(233, 150, 122);
    public static final Color DARK_SEA_GREEN = new Color(143, 188, 143);
    public static final Color DARK_SLATE_BLUE = new Color(72, 61, 139);
    public static final Color DARK_SLATE_GREY = new Color(47, 79, 79);
    public static final Color DARK_SLATE_GRAY = DARK_SLATE_GREY;
    public static final Color DARK_TURQUOISE = new Color(0, 206, 209);
    public static final Color DARK_VIOLET = new Color(148, 0, 211);
    public static final Color DEEP_PINK = new Color(255, 20, 147);
    public static final Color DEEP_SKY_BLUE = new Color(0, 191, 255);
    public static final Color DIM_GREY = new Color(105, 105, 105);
    public static final Color DIM_GRAY = DIM_GREY;
    public static final Color DODGER_BLUE = new Color(30, 144, 255);
    public static final Color FIREBRICK = new Color(178, 34, 34);
    public static final Color FLORAL_WHITE = new Color(255, 250, 240);
    public static final Color FOREST_GREEN = new Color(34, 139, 34);
    public static final Color FUSCHIA = new Color(255, 0, 255);
    public static final Color GAINSBORO = new Color(220, 220, 220);
    public static final Color GHOST_WHITE = new Color(248, 248, 255);
    public static final Color GOLD = new Color(255, 215, 0);
    public static final Color GOLDENROD = new Color(218, 165, 32);
    public static final Color GREEN_YELLOW = new Color(173, 255, 47);
    public static final Color HONEYDEW = new Color(240, 255, 240);
    public static final Color HOT_PINK = new Color(255, 105, 180);
    public static final Color INDIAN_RED = new Color(205, 92, 92);
    public static final Color INDIGO = new Color(75, 0, 130);
    public static final Color IVORY = new Color(255, 255, 240);
    public static final Color KHAKI = new Color(240, 230, 140);
    public static final Color LAVENDAR = new Color(230, 230, 250);
    public static final Color LAVENDAR_BLUSH = new Color(255, 240, 245);
    public static final Color LAWN_GREEN = new Color(124, 252, 0);
    public static final Color LEMON_CHIFFON = new Color(255, 250, 205);
    public static final Color LIGHT_BLUE = new Color(173, 216, 230);
    public static final Color LIGHT_CORAL = new Color(240, 128, 128);
    public static final Color LIGHT_CYAN = new Color(224, 255, 255);
    public static final Color LIGHT_GOLDENROD_YELLOW = new Color(250, 250, 210);
    public static final Color LIGHT_GREEN = new Color(144, 238, 144);
    public static final Color LIGHT_PINK = new Color(255, 182, 193);
    public static final Color LIGHT_SALMON = new Color(255, 160, 122);
    public static final Color LIGHT_SEA_GREEN = new Color(32, 178, 170);
    public static final Color LIGHT_SKY_BLUE = new Color(135, 206, 250);
    public static final Color LIGHT_SLATE_GREY = new Color(119, 136, 153);
    public static final Color LIGHT_SLATE_GRAY = LIGHT_SLATE_GREY;
    public static final Color LIGHT_STEEL_BLUE = new Color(176, 196, 222);
    public static final Color LIGHT_YELLOW = new Color(255, 255, 224);
    public static final Color LIME = new Color(0, 255, 0);
    public static final Color LIME_GREEN = new Color(50, 205, 50);
    public static final Color LINEN = new Color(250, 240, 230);
    public static final Color MAROON = new Color(128, 0, 0);
    public static final Color MEDIUM_AQUAMARINE = new Color(102, 205, 170);
    public static final Color MEDIUM_BLUE = new Color(0, 0, 205);
    public static final Color MEDIUM_ORCHID = new Color(186, 85, 211);
    public static final Color MEDIUM_PURPLE = new Color(147, 112, 219);
    public static final Color MEDIUM_SEA_GREEN = new Color(60, 179, 113);
    public static final Color MEDIUM_SLATE_BLUE = new Color(123, 104, 238);
    public static final Color MEDIUM_SPRING_GREEN = new Color(0, 250, 154);
    public static final Color MEDIUM_TURQUOISE = new Color(72, 209, 204);
    public static final Color MEDIUM_VIOLET_RED = new Color(199, 21, 133);
    public static final Color MIDNIGHT_BLUE = new Color(25, 25, 112);
    public static final Color MINT_CREAM = new Color(245, 255, 250);
    public static final Color MISTY_ROSE = new Color(255, 228, 225);
    public static final Color MOCCASIN = new Color(255, 228, 181);
    public static final Color NAVAJO_WHITE = new Color(255, 222, 173);
    public static final Color NAVY = new Color(0, 0, 128);
    public static final Color OLD_LACE = new Color(253, 245, 230);
    public static final Color OLIVE = new Color(128, 128, 0);
    public static final Color OLIVE_DRAB = new Color(107, 142, 35);
    public static final Color ORANGE_RED = new Color(255, 69, 0);
    public static final Color ORCHID = new Color(218, 112, 214);
    public static final Color PALE_GOLDENROD = new Color(238, 232, 170);
    public static final Color PALE_GREEN = new Color(152, 251, 152);
    public static final Color PALE_TURQUOISE = new Color(175, 238, 238);
    public static final Color PALE_VIOLET_RED = new Color(219, 112, 147);
    public static final Color PAPAYA_WHIP = new Color(255, 239, 213);
    public static final Color PEACH_PUFF = new Color(255, 218, 185);
    public static final Color PERU = new Color(205, 133, 63);
    public static final Color PINK = new Color(255, 192, 203);
    public static final Color PLUM = new Color(221, 160, 221);
    public static final Color POWDER_BLUE = new Color(176, 224, 230);
    public static final Color PURPLE = new Color(128, 0, 128);
    public static final Color REBECCA_PURPLE = new Color(102, 51, 153);
    public static final Color ROSY_BROWN = new Color(188, 143, 143);
    public static final Color ROYAL_BLUE = new Color(65, 105, 225);
    public static final Color SADDLE_BROWN = new Color(139, 69, 19);
    public static final Color SALMON = new Color(250, 128, 114);
    public static final Color SANDY_BROWN = new Color(244, 164, 96);
    public static final Color SEA_GREEN = new Color(46, 139, 87);
    public static final Color SEA_SHELL = new Color(255, 245, 238);
    public static final Color SIENNA = new Color(160, 82, 45);
    public static final Color SILVER = new Color(192, 192, 192);
    public static final Color SKY_BLUE = new Color(135, 206, 235);
    public static final Color SLATE_BLUE = new Color(106, 90, 205);
    public static final Color SLATE_GREY = new Color(112, 128, 144);
    public static final Color SLATE_GRAY = SLATE_GREY;
    public static final Color SNOW = new Color(255, 250, 250);
    public static final Color SPRING_GREEN = new Color(0, 255, 127);
    public static final Color STEEL_BLUE = new Color(70, 130, 180);
    public static final Color TAN = new Color(210, 180, 140);
    public static final Color TEAL = new Color(0, 128, 128);
    public static final Color THISTLE = new Color(216, 191, 216);
    public static final Color TOMATO = new Color(255, 99, 71);
    public static final Color TURQUOISE = new Color(64, 224, 208);
    public static final Color VIOLET = new Color(238, 130, 238);
    public static final Color WHEAT = new Color(245, 222, 179);
    public static final Color WHITE_SMOKE = new Color(245, 245, 245);
    public static final Color YELLOW_GREEN = new Color(154, 205, 50);

}
