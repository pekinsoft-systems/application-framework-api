/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppUtils
 *  Class      :   MessageBox.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 8, 2020 @ 12:44:04 PM
 *  Modified   :   Mar 8, 2020
 *
 *  Purpose:
 *
 *  Revision History:
 *
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Mar 8, 2020  Sean Carrick        Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

import com.pekinsoft.framework.*;
import java.awt.Component;
import javax.swing.JOptionPane;

/**
 * The `MessageBox` class provides static methods for displaying
 * information to the user.
 *
 * @author Sean Carrick &lt;PekinSOFT at outlook dot com&gt;
 *
 * @version 1.35
 * @since 0.1.0
 */
public class MessageBox {

    public static final int YES_OPTION = JOptionPane.YES_OPTION;
    public static final int NO_OPTION = JOptionPane.NO_OPTION;
    public static final int CANCEL_OPTION = JOptionPane.CANCEL_OPTION;
    public static final int OK_OPTION = JOptionPane.OK_OPTION;
    public static final int CLOSED_OPTION = JOptionPane.CLOSED_OPTION;

    private static final ApplicationContext context;

    static {
        context = Application.getInstance().getContext();
    }

    private MessageBox() { /* No instantiation allowed. */ }

    /**
     * Standardized method of displaying informational messages to the user.
     * 
     * Messages displayed in this manner should not be for errors in the program
     * or warnings of improper usage. Furthermore, questions should not be asked
     * via this method, nor should input be sought. Again, only informational
     * messages should be displayed using this method.
     * 
     * The `MessageBox.showlog(Level.INFO, )` method uses the {@code Application}'s
     * {@code ResourceMap} to retrieve the title for the message box. Therefore,
     * the lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The {@code showlog(Level.INFO, )} method is implemented as:
     * <pre>
     * public static void showlog(Level.INFO, String message, String title) {
     *     JOptionPane.showMessageDialog(
     *             ((SingleFrameApplication) context.getApplication()).getMainFrame(),
     *             message,
     *             context.getResourceMap(MessageBox.class).getString("message.info.title", title),
     *             JOptionPane.INFORMATION_MESSAGE);
     * }
     * </pre>
     *
     * @param parent the parent above which the message should be displayed
     * @param message The message to display to the user.
     * @param title The title of the message box. The `MessageBox`
     * resources already have "`${Application.name}` Information", so only
     * the specifics need to be provided.
     *
     */
    public static void showInfo(Component parent, String message, String title) {
        if (parent == null) {
            parent = context.getApplication().getMainFrame();
        }
        JOptionPane.showMessageDialog(
                parent,
                message,
                context.getResourceMap(MessageBox.class).getString("message.info.title", title),
                JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     * Standardized method of displaying warning messages to the user.
     * 
     * Messages shown in this manner should be used to warn users of a potential
     * problem, such as not providing required data, etc. Simple informational
     * messages should be shown using the showInfo method and errors should be
     * shown using the showError method. Questions and input should be sought
     * using the appropriate methods.
     * 
     * The `MessageBox.showWarning()` method uses the {@code Application}'s
     * {@code ResourceMap} to retrieve the title for the message box. Therefore,
     * the lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The `showWarning()` method is implemented as:
     * <pre>
     * public static void showWarning(String message, String title) {
     *     JOptionPane.showMessageDialog(
     *             ((SingleFrameApplication) context.getApplication()).getMainFrame(),
     *             message,
     *             context.getResourceMap(MessageBox.class)
     *                     .getString("message.warning.title", title),
     *             JOptionPane.INFORMATION_MESSAGE);
     * }
     * </pre>
     *
     * @param message The message to display to the user.
     * @param title The title of the message box.
     *
     */
    public static void showWarning(Component parent, String message, String title) {
        if (parent == null) {
            parent = context.getApplication().getMainFrame();
        }
        JOptionPane.showMessageDialog(
                parent,
                message,
                context.getResourceMap(MessageBox.class)
                        .getString("message.warning.title", title),
                JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Standardized method of displaying errors to the user.
     * 
     * Error messages should be displayed from any `catch()` block where
     * the `try` block has failed. Some critical errors should be followed
     * by a call to the `exit()` method and the program should be
     * terminated. However, that functionality should only be reserved for
     * critical errors, from which the user cannot recover.
     * 
     * The `MessageBox.showError()` method uses the {@code Application}'s
     * {@code ResourceMap} to retrieve the title for the message box. Therefore,
     * the lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The `showError()` method is implemented as:
     * <pre>
     * public static void showError(Exception ex, String title, Application app) {
     *     String msg = context
     *             .getResourceMap(MessageBox.class)
     *             .getString("message.error",
     *                     ex.getLocalizedMessage(),
     *                     ex.getClass().getSimpleName(),
     *                     context.getLocalStorage()
     *                             .getLogDirectory()
     *                             .getAbsolutePath());
     * 
     *     JOptionPane.showMessageDialog(
     *             ((SingleFrameApplication) context.getApplication()).getMainFrame(),
     *             msg,
     *             context.getResourceMap(MessageBox.class)
     *                     .getString("message.error.title", title),
     *             JOptionPane.ERROR_MESSAGE);
     * }
     * </pre>
     *
     * @param ex The exception that was thrown.
     * @param title The title of the message box
     */
    public static void showError(Component parent, Exception ex, String title) {
        if (parent == null) {
            parent = context.getApplication().getMainFrame();
        }
        LocalStorage lst = context.getLocalStorage();
        String msg = context
                .getResourceMap(MessageBox.class)
                .getString("message.error",
                        ex.getLocalizedMessage(),
                        ex.getClass().getSimpleName(),
                        lst.getLogDirectory());

        JOptionPane.showMessageDialog(
                parent,
                msg,
                context.getResourceMap(MessageBox.class)
                        .getString("message.error.title", title),
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Standardized method of displaying a question to the user.
     * 
     * This method should be used to request confirmation from the user for any
     * particular action they may have taken. Mostly, this method should be used
     * to confirm closing a window that has unsaved changes.
     * 
     * The `MessageBox.askQuestion()` method uses the {@code Application}'s
     * {@code ResourceMap} to retrieve the title for the message box. Therefore,
     * the lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The `askQuestion()` method is implemented as:
     * <pre>
     * public static int askQuestion(String question, String title, boolean cancel) {
     *     if (cancel) {
     *         return JOptionPane.showConfirmDialog(
     *                 ((SingleFrameApplication) context.getApplication()).getMainFrame(),
     *                 question,
     *                 context.getResourceMap(MessageBox.class)
     *                         .getString("message.confirm.title", title),
     *                 JOptionPane.YES_NO_CANCEL_OPTION);
     *     }
     *     return JOptionPane.showConfirmDialog(null,
     *             question,
     *             context.getResourceMap(MessageBox.class)
     *                     .getString("message.confirm.title", title),
     *             JOptionPane.YES_NO_OPTION);
     * }
     * </pre>
     *
     * @param question The question that the user needs to answer.
     * @param title The title of the message box.
     * @param cancel Whether to include a cancel button as an option.
     * @return an integer indicating the option selected by the user. This will
     * be one of:
     * 
     * - MessageBox.YES_OPTION
     * - MessagBox.NO_OPTION
     * - MessageBox.CANCEL_OPTION, if cancel button is included
     * 
     */
    public static int askQuestion(Component parent, String question, String title, boolean cancel) {
        if (parent == null) {
            parent = context.getApplication().getMainFrame();
        }
        if (cancel) {
            return JOptionPane.showConfirmDialog(
                    parent,
                    question,
                    context.getResourceMap(MessageBox.class)
                            .getString("message.confirm.title", title),
                    JOptionPane.YES_NO_CANCEL_OPTION);
        }
        return JOptionPane.showConfirmDialog(
                parent,
                question,
                context.getResourceMap(MessageBox.class)
                        .getString("message.confirm.title", title),
                JOptionPane.YES_NO_OPTION);
    }

    /**
     * Standardized method of seeking additional input from the user.
     * 
     * This method should be used to prompt the user for a single piece of input
     * that they may have forgotten to provide in another manner. A good use of
     * this method would be to get the value of a required field that the user
     * did not enter on a form.
     * 
     * The `MessageBox.getInput()` method uses the {@code Application}'s
     * {@code ResourceMap} to retrieve the title for the message box. Therefore,
     * the lead portion of the title is already present by default, so only the
     * message specifics title portion needs to be provided. For example, if the
     * message is telling the user that changes were successfully saved, all
     * that needs to be provided to this message for the title is the value
     * "Save Successful". The `getInput()` method is implemented as:
     * <pre>
     * public static String getInput(String prompt, String title) {
     *     return JOptionPane.showInputDialog(
     *             ((SingleFrameApplication) context.getApplication()).getMainFrame(),
     *             prompt, 
     *             context.getResourceMap(MessageBox.class).getString("message.input.title", title),
     *             JOptionPane.OK_CANCEL_OPTION);
     * }
     * </pre>
     *
     * @param prompt A description of the information the user needs to provide.
     * @param title The title of the message box.
     * @return The input requested of the user.
     */
    public static String getInput(Component parent, String prompt, String title) {
        if (parent == null) {
            parent = context.getApplication().getMainFrame();
        }
        return JOptionPane.showInputDialog(
                parent,
                prompt, 
                context.getResourceMap(MessageBox.class).getString("message.input.title", title),
                JOptionPane.PLAIN_MESSAGE);
    }

}
