/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   Platform_Application_API
 *  Class      :   StringUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 13, 2024
 *  Modified   :   Apr 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.utils;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class StringUtils {

    private StringUtils() {
        // No instantiation allowed.
    }

    /**
     * Escapes characters that may be contained in the specified {@code text}
     * that have special meaning in XML and/or HTML.
     * <p>
     * As more are thought of, add them to this method.
     *
     * @param text the text to have escaped (or tokenized)
     *
     * @return the escaped (tokenized) text
     */
    public static String escapeXml(String text) {
        String s = text == null
                ? ""
                : text.replace("&", "&amp;");
        s = s.replace("<", "&lt;");
        s = s.replace(">", "&gt;");
        return s;
    }

}
