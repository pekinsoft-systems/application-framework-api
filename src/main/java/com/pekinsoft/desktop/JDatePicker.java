package com.pekinsoft.desktop;

import java.awt.*;
import java.awt.event.*;
import java.time.LocalDate;
import java.time.YearMonth;
import javax.swing.*;

/**
 * JDatePicker is a custom Swing component that allows users to select a date.
 * It features a text field displaying the selected date, and a dropdown button
 * to show a calendar for selecting a new date.
 */
public class JDatePicker extends JComponent {

    private LocalDate selectedDate;
    private YearMonth displayedMonth;
    private final CalendarPanel calendarPanel;
    private final JDialog popup;
    private final JTextField editor;
    private final JButton arrowButton;

    /**
     * Constructs a JDatePicker component initialized to the current date.
     */
    public JDatePicker() {
        this.selectedDate = LocalDate.now();
        this.displayedMonth = YearMonth.from(selectedDate);

        setLayout(new BorderLayout());

        editor = new JTextField(selectedDate.toString());
        editor.setEditable(false);
        add(editor, BorderLayout.CENTER);

        arrowButton = new JButton("\u25BC"); // Down arrow symbol
        arrowButton.setMargin(new Insets(2, 2, 2, 2));
        arrowButton.setFocusable(false);
        add(arrowButton, BorderLayout.EAST);

        // Create a popup dialog for the calendar
        popup = new JDialog((Frame) null, "Select Date", false);
        popup.setUndecorated(true);
        popup.setLayout(new BorderLayout());
        popup.setAlwaysOnTop(true);
        popup.setFocusableWindowState(false);

        calendarPanel = new CalendarPanel();
        popup.add(calendarPanel);

        // Show or hide Popup when button is clicked
        arrowButton.addActionListener(e -> {
            if (popup.isVisible()) {
                popup.setVisible(false);
            } else if (isShowing()) { // Ensure the component is visible before showing the popup
                SwingUtilities.invokeLater(() -> {
                    calendarPanel.updateCalendar(displayedMonth); // Ensure the calendar is updated before showing
                    try {
                        Point location = getLocationOnScreen();
                        popup.setLocation(location.x, location.y + getHeight());
                        popup.pack();
                        popup.setVisible(true);
                        calendarPanel.revalidate();
                        calendarPanel.repaint(); // Force repaint to fix white-out issue
                        System.out.println("Popup shown with updated calendar.");
                    } catch (IllegalComponentStateException ex) {
                        System.err.println("Component not fully visible: " + ex
                                .getMessage());
                    }
                });
            }
        });

        // Hide popup when clicking outside the calendar, but not when clicking the arrow button
        Toolkit.getDefaultToolkit().addAWTEventListener(event -> {
            if (event instanceof MouseEvent) {
                MouseEvent mouseEvent = (MouseEvent) event;
                Component clickedComponent = SwingUtilities
                        .getDeepestComponentAt(mouseEvent.getComponent(),
                                mouseEvent.getX(), mouseEvent.getY());
                if (popup.isVisible() && clickedComponent != null && !SwingUtilities
                        .isDescendingFrom(clickedComponent, popup) && clickedComponent != this && clickedComponent != arrowButton) {
                    popup.setVisible(false);
                }
            }
        }, AWTEvent.MOUSE_EVENT_MASK);
    }

    /**
     * CalendarPanel is an inner class that provides a graphical representation
     * of a month view calendar,
     * allowing users to select a specific date.
     */
    private class CalendarPanel extends JPanel {

        private final JComboBox<Integer> yearSelector;
        private final JComboBox<String> monthSelector = new JComboBox<>(
                new String[]{
                    "January", "February", "March", "April", "May", "June",
                    "July", "August", "September", "October", "November",
                    "December"
                });
        private final JPanel daysPanel;

        /**
         * Constructs a CalendarPanel initialized to the currently displayed
         * month and year.
         */
        public CalendarPanel() {
            setLayout(new BorderLayout());

            // Control Panel for Month and Year
            JPanel controlPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

            yearSelector = new JComboBox<>();
            for (int i = displayedMonth.getYear() + 10; i >= displayedMonth
                    .getYear() - 60; i--) {
                yearSelector.addItem(i);
            }
            yearSelector.setSelectedItem(displayedMonth.getYear());
            yearSelector.addActionListener(e -> updateCalendar(YearMonth.of(
                    (Integer) yearSelector.getSelectedItem(), monthSelector
                    .getSelectedIndex() + 1)));
            controlPanel.add(yearSelector);

            monthSelector.setSelectedIndex(displayedMonth.getMonthValue() - 1);
            monthSelector.addActionListener(e -> updateCalendar(YearMonth.of(
                    (Integer) yearSelector.getSelectedItem(), monthSelector
                    .getSelectedIndex() + 1)));
            controlPanel.add(monthSelector);

            add(controlPanel, BorderLayout.NORTH);

            // Days Panel
            daysPanel = new JPanel(new GridLayout(0, 7));
            daysPanel.setPreferredSize(new Dimension(300, 150)); // Reduced size for better appearance
            add(daysPanel, BorderLayout.CENTER);

            updateCalendar(displayedMonth);
        }

        /**
         * Updates the calendar view to the specified YearMonth.
         *
         * @param newMonth the YearMonth to display in the calendar.
         */
        public void updateCalendar(YearMonth newMonth) {
            displayedMonth = newMonth;
            daysPanel.removeAll();

            // Add day labels
            String[] days = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
            for (String day : days) {
                JLabel dayLabel = new JLabel(day, SwingConstants.CENTER);
                dayLabel.setFont(dayLabel.getFont().deriveFont(Font.BOLD));
                daysPanel.add(dayLabel);
            }

            // Get the first day of the month and the number of days
            LocalDate firstOfMonth = displayedMonth.atDay(1);
            int dayOfWeekValue = firstOfMonth.getDayOfWeek().getValue() % 7; // Adjusting for Sunday start
            int lengthOfMonth = displayedMonth.lengthOfMonth();

            // Fill empty days before start of month
            for (int i = 0; i < dayOfWeekValue; i++) {
                daysPanel.add(new JLabel(""));
            }

            // Fill the days of the month
            for (int day = 1; day <= lengthOfMonth; day++) {
                JButton dayButton = new JButton(String.valueOf(day));
                dayButton.setMargin(new Insets(1, 1, 1, 1));
                dayButton.setFocusable(false); // Suppress focus border
                if (displayedMonth.atDay(day).equals(LocalDate.now())) {
                    dayButton.setBackground(Color.CYAN); // Highlight current day
                }
                dayButton.addActionListener(new DateSelectionListener(day));
                daysPanel.add(dayButton);
            }

            daysPanel.revalidate();
            daysPanel.repaint();
            System.out.println("Calendar updated: " + displayedMonth);
        }
    }

    /**
     * DateSelectionListener is an ActionListener for selecting a date from the
     * calendar.
     */
    private class DateSelectionListener implements ActionListener {

        private final int day;

        /**
         * Constructs a DateSelectionListener for the specified day of the
         * month.
         *
         * @param day the day of the month that this listener represents.
         */
        public DateSelectionListener(int day) {
            this.day = day;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            selectedDate = displayedMonth.atDay(day);
            editor.setText(selectedDate.toString());
            popup.setVisible(false);
            System.out.println("Date selected: " + selectedDate);
        }
    }

    /**
     * Gets the currently selected date.
     *
     * @return the currently selected LocalDate.
     */
    public LocalDate getSelectedDate() {
        return selectedDate;
    }

    /**
     * Sets the selected date for the date picker.
     * If the provided date is null, it defaults to the current date.
     *
     * @param date the LocalDate to set as the selected date.
     */
    public void setSelectedDate(LocalDate date) {
        if (date == null) {
            date = LocalDate.now();
        }
        this.selectedDate = date;
        this.displayedMonth = YearMonth.from(date);
        editor.setText(date.toString());
    }

    /**
     * Main method for testing the JDatePicker component.
     *
     * @param args command line arguments.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("JDatePicker");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLayout(new FlowLayout());
            frame.add(new JDatePicker());
            frame.pack();
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        });
    }
}
