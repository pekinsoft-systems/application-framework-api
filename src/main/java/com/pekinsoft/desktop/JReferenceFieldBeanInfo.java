/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   CustomComponents
 *  Class      :   JReferenceFieldBeanInfo.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 13, 2024
 *  Modified   :   Nov 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import java.beans.*;

public class JReferenceFieldBeanInfo extends SimpleBeanInfo {

    @Override
    public BeanDescriptor getBeanDescriptor() {
        return new BeanDescriptor(JReferenceField.class);
    }

    @Override
    public PropertyDescriptor[] getPropertyDescriptors() {
        try {
            PropertyDescriptor background = new PropertyDescriptor("background",
                    JReferenceField.class);
            PropertyDescriptor foreground = new PropertyDescriptor("foreground",
                    JReferenceField.class);
            PropertyDescriptor columns = new PropertyDescriptor("columns",
                    JReferenceField.class);
            PropertyDescriptor text = new PropertyDescriptor("text",
                    JReferenceField.class);

            return new PropertyDescriptor[]{background, foreground, columns,
                text};
        } catch (IntrospectionException e) {
            e.printStackTrace();
            return null;
        }
    }
}
