/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   JSR-296.SwingAppApplication
 *  Class      :   OptionsPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Sep 5, 2022
 *  Modified   :   Sep 5, 2022
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Sep 5, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import com.pekinsoft.api.SaveCookie;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import java.text.MessageFormat;
import java.util.Comparator;
import javax.swing.JPanel;

/**
 * The {@code OptionsPanel} class provides a method by which each
 * module may provide a panel into the {@code Application}'s options dialog, if
 * one is provided.
 * <p>
 * This class implements the {@link SaveCookie} interface to allow for the
 * implementing panel to save its data in the way that makes sense for it.
 * <p>
 * The methods in this class have default implementations that return required
 * values from the {@link ResourceMap}. These values are defined as follows:
 * <dl>
 * <dt><strong>{@code options.title}</strong></dt>
 * <dd>This is the title that will be displayed on the tab for this
 * {@code OptionsPanel}.</dd>
 * <dt><strong>{@code options.category}</strong></dt>
 * <dd>This is the category of the <em>Options</em> dialog in which this
 * {@code OptionsPanel} will be installed.<br><br>This value should use the
 * <em>string variable substitution</em> available within the
 * {@code ResourceMap}, because these categories are defined by the
 * {@link Application Application's} {@code ResourceMap}.</dd>
 * <dt><strong>{@code options.index}</strong></dt>
 * <dd>This is the index position of this {@code OptionsPanel} within its
 * desired category.</dd>
 * </dl>
 * <p>
 * The only method that is required to be implemented by the subclass is the
 * {@link SaveCookie#save() save} method to handle the user's options settings
 * in the way that makes sense for the {@code OptionsPanel} and application.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 1.0
 */
public abstract class OptionsPanel extends JPanel
        implements SaveCookie, Comparator<OptionsPanel> {

    protected OptionsPanel(ApplicationContext context) {
        this.context = context;
        this.logger = context.getLogger(getClass());
    }

    /**
     * The title that will be displayed on the generated tab in the Options
     * dialog.
     * <p>
     * The default implementation simply returns the {@link ResourceMap} value
     * for the key {@code options.title}.
     *
     * @return the tab title
     */
    public String tabTitle() {
        return getResourceMap().getString("options.title");
    }

    /**
     * The category into which the {@code OptionsPanel} would like to be
     * installed. This method returns a string value, which may be parsed into
     * an enumeration constant name by the containing {@link Application}. This
     * allows for the {@code Application} to decide what options categories are
     * available to its plugins, without this interface defining categories that
     * make no sense in a specific context.
     * <p>
     * The default implementation simply returns the {@link ResourceMap} value
     * for the key {@code options.category}. It is best to use <em>string
     * variable substitution</em> for this resource, since the
     * {@link Application} will have these categories defined.
     *
     * @return the name of the category into which the panel should be installed
     */
    public String category() {
        return getResourceMap().getString("options.category");
    }

    /**
     * Retrieves this {@code OptionsPanel}'s preferred location within its
     * desired {@link #category() category}.
     * <p>
     * The default implementation simply returns the {@link ResourceMap} value
     * for the key {@code options.index}.
     *
     * @return the desired index position
     */
    public int categoryIndex() {
        return getResourceMap().getInteger("options.index");
    }

    @Override
    public final int compare(OptionsPanel o1, OptionsPanel o2) {
        return Integer.compare(o1.categoryIndex(), o2.categoryIndex());
    }

    /**
     * Retrieves the {@link ApplicationContext} in which this
     * {@code OptionsPanel}
     * was constructed.
     *
     * @return the context
     */
    protected ApplicationContext getContext() {
        return context;
    }

    /**
     * Retrieves the {@link Application} in which this {@code OptionsPanel} is
     * being used.
     *
     * @return the application
     */
    protected Application getApplication() {
        return getContext().getApplication();
    }

    /**
     * Lazily loads the {@link ResourceMap} for this {@code OptionsPanel}. The
     * returned resource map is a chain of resource maps including the
     * {@code OptionsPanel} super-class'.
     *
     * @return the resource map chain
     */
    protected ResourceMap getResourceMap() {
        return getContext().getResourceMap(getClass(), OptionsPanel.class);
    }

    /**
     * A convenience method allowing subclasses to log messages to the logger
     * that is created for this class.
     *
     * @param level  the {@link Level} at which the message should be logged
     * @param format the {@link MessageFormat#format} string for the message
     * @param args   the arguments to the format string (may be empty, but not
     *               {@code null}
     */
    protected void log(Level level, String format, Object... args) {
        logger.log(level, format, args);
    }

    /**
     * A convenience method allowing subclasses to log error messages to the
     * logger that is created for this class.
     *
     * @param level  the {@link Level} at which the message should be logged
     * @param format the {@link MessageFormat#format} string for the message
     * @param cause  the {@link Exception} that was caught
     * @param args   the arguments to the format string (may be empty, but not
     *               {@code null}
     */
    protected void log(Level level, String format, Throwable cause,
            Object... args) {
        logger.log(level, format, cause, args);
    }

    private final ApplicationContext context;
    private final Logger logger;

}
