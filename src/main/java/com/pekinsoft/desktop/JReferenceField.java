/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   CustomComponents
 *  Class      :   JReferenceField.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 13, 2024
 *  Modified   :   Nov 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;

/**
 * A custom Swing component that combines a {@link JTextField} with a
 * {@link JTable} in a popup for selecting reference data.
 * This component allows users to type in a field and see a filtered list of
 * possible matches from a table.
 * The table can be populated with custom data, and the filtering logic can be
 * customized.
 *
 * @param <T> The type of data that this reference field will handle.
 */
public class JReferenceField<T> extends JComponent {

    private JTextField textField;
    private JPanel panel;
    private JTable table;
    private JWindow popup;
    private DefaultTableModel tableModel;
    private List<T> data;
    private Function<T, String> filterFieldExtractor;
    private Function<T, String> textFieldValueExtractor;
    private String[] columnNames;
    private Function<T, Object[]> rowValueExtractor;

    /**
     * Constructs a new {@code JReferenceField} with no initial data.
     */
    public JReferenceField() {
        setLayout(new BorderLayout());
        textField = new JTextField();
        add(textField, BorderLayout.CENTER);

        // Set up the table and panel
        table = new JTable() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; // Make table cells non-editable
            }
        };
        panel = new JPanel(new BorderLayout());
        panel.add(new JScrollPane(table), BorderLayout.CENTER);
        popup = new JWindow();
        popup.setAlwaysOnTop(true);
        popup.setType(Window.Type.POPUP);
        popup.setFocusableWindowState(false);
        popup.getContentPane().add(panel);
        popup.setSize(new Dimension(400, table.getRowHeight() * 12));

        // Add key listener for CTRL+SPACE and ENTER
        textField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_SPACE && e.isControlDown()) {
                    togglePopup();
                } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    acceptSelectedValue();
                }
            }
        });

        // Add document listener for filtering
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                filterData();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filterData();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filterData();
            }
        });

        // Add mouse listener to table for selection
        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    acceptSelectedValue();
                }
            }
        });
    }

    /**
     * Sets the data for the {@link JTable} and the behavior for filtering and
     * displaying values.
     *
     * @param data                    The list of data items to be displayed in
     *                                the table.
     * @param columnNames             The names of the columns to be displayed
     *                                in the table.
     * @param rowValueExtractor       A function that extracts the row values
     *                                from a data item.
     * @param filterFieldExtractor    A function that extracts the field to be
     *                                used for filtering from a data item.
     * @param textFieldValueExtractor A function that extracts the value to be
     *                                set in the text field from a data item.
     */
    public void setData(List<T> data, String[] columnNames,
            Function<T, Object[]> rowValueExtractor,
            Function<T, String> filterFieldExtractor,
            Function<T, String> textFieldValueExtractor) {
        this.data = data;
        this.columnNames = columnNames;
        this.rowValueExtractor = rowValueExtractor;
        this.filterFieldExtractor = filterFieldExtractor;
        this.textFieldValueExtractor = textFieldValueExtractor;
        this.tableModel = new DefaultTableModel(columnNames, 0);
        table.setModel(tableModel);
        filterData(); // Apply filtering when data is set
    }

    /**
     * Toggles the visibility of the popup window containing the {@link JTable}.
     */
    private void togglePopup() {
        if (popup.isVisible()) {
            popup.setVisible(false);
        } else {
            showPopup();
        }
    }

    /**
     * Shows the popup window with the filtered data.
     */
    private void showPopup() {
        filterData(); // Ensure data is filtered based on current text
        Point location = textField.getLocationOnScreen();
        popup.setLocation(location.x, location.y + textField.getHeight());
        popup.setVisible(true);
        SwingUtilities.invokeLater(() -> textField.requestFocusInWindow());
    }

    /**
     * Refreshes the table data with the current list of items.
     */
    private void refreshTable() {
        if (tableModel == null) {
            return;
        }
        // Clear the table model data
        tableModel.setRowCount(0);
        if (data != null) {
            for (T item : data) {
                tableModel.addRow(rowValueExtractor.apply(item));
            }
        }
    }

    /**
     * Filters the data based on the current text in the {@link JTextField}.
     */
    private void filterData() {
        if (data == null || filterFieldExtractor == null) {
            return;
        }

        String filterText = textField.getText().toLowerCase();
        List<T> filteredData = data.stream()
                .filter(item -> filterFieldExtractor.apply(item).toLowerCase()
                .startsWith(filterText))
                .collect(Collectors.toList());

        tableModel.setRowCount(0);
        for (T item : filteredData) {
            tableModel.addRow(rowValueExtractor.apply(item));
        }

        // Adjust panel size
        popup.setSize(new Dimension(400, Math.min(
                table.getRowHeight() * (filteredData.size() + 1), table
                .getRowHeight() * 12)));
        if (!filteredData.isEmpty()) {
            table.setRowSelectionInterval(0, 0);
        }
    }

    /**
     * Accepts the currently selected value from the {@link JTable} and sets it
     * in the {@link JTextField}.
     */
    private void acceptSelectedValue() {
        int selectedRow = table.getSelectedRow();
        if (selectedRow != -1) {
            String value = textFieldValueExtractor.apply(data.get(selectedRow));
            textField.setText(value);
            popup.setVisible(false);
            SwingUtilities.invokeLater(() -> textField.requestFocusInWindow());
        }
    }

    // Expose JTextField properties
    /**
     * Sets the background color of the {@link JTextField}.
     *
     * @param color The background color to set.
     */
    @Override
    public void setBackground(Color color) {
        textField.setBackground(color);
    }

    /**
     * Gets the background color of the {@link JTextField}.
     *
     * @return The background color.
     */
    @Override
    public Color getBackground() {
        return textField.getBackground();
    }

    /**
     * Sets the foreground color of the {@link JTextField}.
     *
     * @param color The foreground color to set.
     */
    @Override
    public void setForeground(Color color) {
        textField.setForeground(color);
    }

    /**
     * Gets the foreground color of the {@link JTextField}.
     *
     * @return The foreground color.
     */
    @Override
    public Color getForeground() {
        return textField.getForeground();
    }

    /**
     * Sets the number of columns for the {@link JTextField}.
     *
     * @param columns The number of columns to set.
     */
    public void setColumns(int columns) {
        textField.setColumns(columns);
    }

    /**
     * Gets the number of columns for the {@link JTextField}.
     *
     * @return The number of columns.
     */
    public int getColumns() {
        return textField.getColumns();
    }

    /**
     * Sets the text of the {@link JTextField}.
     *
     * @param text The text to set.
     */
    public void setText(String text) {
        textField.setText(text);
        filterData(); // Apply filtering when text is set programmatically
    }

    /**
     * Gets the text of the {@link JTextField}.
     *
     * @return The current text.
     */
    public String getText() {
        return textField.getText();
    }

    /**
     * Main method for testing the {@code JReferenceField} component.
     *
     * @param args Command line arguments.
     */
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("JReferenceField Test");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setLayout(new FlowLayout());

            JReferenceField<Customer> referenceField = new JReferenceField<>();
            referenceField.setData(
                    List.of(
                            new Customer(1, "Acme Corp", "123 Main St",
                                    "Springfield", "IL", "62701"),
                            new Customer(2, "Globex Inc", "456 Elm St",
                                    "Shelbyville", "IL", "62565"),
                            new Customer(3, "Initech", "789 Oak St",
                                    "Capital City", "IL", "62703"),
                            new Customer(4, "Umbrella Corp", "101 Maple Ave",
                                    "Raccoon City", "WI", "53901"),
                            new Customer(5, "Wayne Enterprises", "111 Gotham Rd",
                                    "Gotham", "NY", "10001"),
                            new Customer(6, "Stark Industries", "222 Iron St",
                                    "Malibu", "CA", "90265"),
                            new Customer(7, "Pied Piper", "333 Startup Ln",
                                    "Palo Alto", "CA", "94301"),
                            new Customer(8, "Vandelay Industries",
                                    "444 Import Rd", "New York", "NY", "10010"),
                            new Customer(9, "Hooli", "555 Tech Ave",
                                    "San Francisco", "CA", "94105"),
                            new Customer(10, "Dunder Mifflin", "666 Paper St",
                                    "Scranton", "PA", "18503")
                    ),
                    new String[]{"ID", "Company", "Street", "City", "State",
                        "Zip Code"},
                    customer -> new Object[]{customer.getId(), customer
                        .getCompany(), customer.getStreet(), customer.getCity(),
                        customer.getState(), customer.getZipCode()},
                    Customer::getState,
                    Customer::getCompany
            );
            referenceField.setColumns(20);

            frame.add(referenceField);
            frame.setSize(800, 400);
            frame.setVisible(true);
        });
    }

    private static class Customer {

        private final int id;
        private final String company;
        private final String street;
        private final String city;
        private final String state;
        private final String zipCode;

        public Customer(int id, String company, String street, String city,
                String state, String zipCode) {
            this.id = id;
            this.company = company;
            this.street = street;
            this.city = city;
            this.state = state;
            this.zipCode = zipCode;
        }

        @Override
        public String toString() {
            return company + " - " + city + ", " + state;
        }

        public int getId() {
            return id;
        }

        public String getCompany() {
            return company;
        }

        public String getStreet() {
            return street;
        }

        public String getCity() {
            return city;
        }

        public String getState() {
            return state;
        }

        public String getZipCode() {
            return zipCode;
        }

    }

}
