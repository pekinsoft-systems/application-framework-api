/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   DockingView.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 28, 2024
 *  Modified   :   Oct 28, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 28, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import com.pekinsoft.api.*;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Level;
import java.awt.*;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.io.*;
import java.lang.reflect.*;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.List;
import java.util.*;
import javax.swing.*;

/**
 * The {@code DockingWindow} class is an implementation of the
 * {@link MainView} class for {@link Application Applications} that need a
 * simple docking framework for displaying sub-windows.
 * <p>
 * The docking capabilities of this window are not complete. In other words,
 * while {@link Dockable Dockables} may be docked into four different docking
 * areas, these are not customizable locations. Also, while the docks may be
 * closed, they may not be iconified, pinned, nor floated. These are features
 * that may be added to the framework in the future.
 * <p>
 * The current docking framework is very basic and functional. To dock a window,
 * it must be a {@code Dockable} instance, and it must have been registered with
 * the {@link DockingManager}. Once registered, the {@code Dockable} may be
 * docked and undocked as needed. Each {@code Dockable} is given a tab component
 * with a close button at the time it is docked, and it is docked into the
 * docking location that it specifies. The docking location <em>cannot be
 * changed</em> at runtime.
 * <p>
 * As each {@code Dockable} is docked, its {@link Dockable#onDock() onDock}
 * method is called, it has its prior session state restored, and it has its
 * components injected via the {@code Dockable}'s {@link ResourceMap}.
 * <p>
 * As each {@code Dockable} is <em>un</em>docked, its
 * {@link Dockable#onUndock() onUndock} method is called, and it has is current
 * session state saved.
 * <p>
 * When a {@code Dockable} is
 * {@link #deregister(java.lang.String) deregistered}, the {@code Dockable}'s
 * {@link Dockable#close() close} method is called.
 * <p>
 * The {@code DockingWindow} has overridden both the {@code MainView}'s
 * {@link MainView#show(javax.swing.JComponent) show(JComponent)} and
 * {@link MainView#close(javax.swing.JComponent) close(JComponent)} methods to
 * deal with {@code Dockable}s.
 * <p>
 * If a {@code Dockable} is passed to {@code show(JComponent)}, then the docking
 * registry is checked to see if the {@code Dockable} has already bee
 * registered. If not, it is registered at that time. If the {@code Dockable} is
 * in the registry (even if just registered), it is then docked into its desired
 * docking location. However, if the {@link JComponent} passed to the
 * {@code show} method <em>is not</em> a {@code Dockable}, it is treated the
 * same way as in the {@code MainView}'s method.
 * <p>
 * If a {@code Dockable} is passed to {@code close(JComponent)}, the docking
 * registry is checked to see if the component has been registered. If not, no
 * exception is thrown and no action is taken. If it is registered, then it is
 * undocked. However, if the specified component <em>is not</em> an instance of
 * {@code Dockable}, it is treated the same way as in the {@code MainView}'s
 * method.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class DockingView extends MainView implements DockingManager {

    /**
     * Constructs a new {@code DockingWindow} for an {@link Application}, with
     * the given {@link ApplicationContext}.
     * <p>
     * This constructor is only callable by subclasses.
     *
     * @param context the application context
     */
    protected DockingView(ApplicationContext context) {
        super(context);

        dockRegistry = new HashMap<>();
        openDocks = new ArrayList<>();

        initComponents();
    }

    @Override
    public void register(Dockable dock) {
        if (dockRegistry.containsKey(dock.getDockId())) {
            return;
        }

        dockRegistry.put(dock.getDockId(), dock);
    }

    @Override
    public void deregister(String dockId) {
        if (!dockRegistry.containsKey(dockId)) {
            return;
        }

        Dockable dock = dockRegistry.remove(dockId);
        dock.close();
    }

    @Override
    public boolean isRegistered(String dockId) {
        return dockRegistry.containsKey(dockId);
    }

    @Override
    public void dock(String dockId) {
        if (dockId == null || dockId.isBlank() || dockId.isEmpty()) {
            throw new NullPointerException("The dockId cannot be blank, empty, "
                    + "or null.");
        }
        if (!dockRegistry.containsKey(dockId)) {
            throw new DockRegistrationException(dockId + " has not been "
                    + "registered with this DockingManager.");
        }

        Dockable dock = dockRegistry.get(dockId);
        openDocks.add(dock);
        dock.onDock();
        restore(dock);

        switch (dock.getDockingLocation().toUpperCase()) {
            case "CENTER" ->
                addCenterDock(dock);
            case "EAST" ->
                addEastDock(dock);
            case "SOUTH" ->
                addSouthDock(dock);
            case "WEST" ->
                addWestDock(dock);
            default ->
                throw new UnsupportedDockingLocationException(
                        dock.getDockingLocation() + " is not known by this "
                        + "DockingManager");
        }
    }

    @Override
    public void undock(String dockId) {
        if (dockId == null || dockId.isBlank() || dockId.isEmpty()) {
            throw new NullPointerException("The dockId cannot be blank, empty, "
                    + "or null.");
        }
        if (!dockRegistry.containsKey(dockId)) {
            throw new DockRegistrationException(dockId + " has not been "
                    + "registered with this DockingManager.");
        }

        Dockable dock = dockRegistry.get(dockId);
        openDocks.remove(dock);
        dock.onUndock();
        save(dock);

        int index = -1;
        switch (dock.getDockingLocation().toUpperCase()) {
            case "CENTER" -> {
                index = centerDocks.indexOfComponent(dock);
                if (index != -1) {
                    centerDocks.remove(dock);
                }
            }
            case "EAST" -> {
                index = eastDocks.indexOfComponent(dock);
                if (index != -1) {
                    eastDocks.remove(dock);
                }
            }
            case "SOUTH" -> {
                index = southDocks.indexOfComponent(dock);
                if (index != -1) {
                    southDocks.remove(dock);
                }
            }
            case "WEST" -> {
                index = westDocks.indexOfComponent(dock);
                if (index != -1) {
                    westDocks.remove(dock);
                }
            }
            default ->
                throw new DockRegistrationException(dockId + " has not been "
                        + "registered with this DockingManager.");
        }
    }

    /**
     * The default implementation for this method checks to see if the specified
     * {@code component} is an instance of {@link Dockable}.
     * <p>
     * If it is, then the docking registry is checked to see if it has been
     * registered. If not, the {@code Dockable} is registered, and then it is
     * docked. If it has been registered, it is simply docked.
     * <p>
     * If it is not an instance of {@code Dockable}, this it is set as this
     * window's main component.
     * <p>
     * If the specified {@code component} is {@code null}, no exception is
     * thrown, and no action is taken.
     *
     * @param component the component to show
     */
    @Override
    public void show(JComponent component) {
        if (component == null) {
            return;
        }
        if (component instanceof Dockable dock) {
            if (!dockRegistry.containsKey(dock.getDockId())) {
                dockRegistry.put(dock.getDockId(), dock);
            }
            dock(dock.getDockId());
        } else {
            super.show(component);
        }
    }

    /**
     * The default implementation for this method checks to see if the specified
     * {@code component} is an instance of {@link Dockable}.
     * <p>
     * If it is, then the docking registry is checked to see if it has been
     * registered. If not, no exception is thrown nor action taken. If it has
     * been, then the {@code component} is undocked.
     * <p>
     * If it is not an instance of {@code Dockable}, then it is removed as this
     * window's main component, and the default main component is restored.
     * <p>
     * If the specified {@code component} is {@code null}, no exception is
     * thrown, and no action is taken.
     *
     * @param component
     */
    @Override
    public void close(JComponent component) {
        if (component == null) {
            return;
        }
        if (component instanceof Dockable dock) {
            if (!dockRegistry.containsKey(dock.getDockId())) {
                return;
            }
            undock(dock.getDockId());
        } else {
            super.close(component);
        }
    }

    @Override
    public void preload() {
        String fileName = null;
        try {
            Path filePath = getContext().getLocalStorage().getConfigDirectory();
            fileName = Path.of(filePath.toString(), openDocksFile).toString();

            BufferedReader in = new BufferedReader(getContext()
                    .getLocalStorage()
                    .openFileReader(StorageLocations.CONFIG_DIR,
                            openDocksFile));

            String line = null;
            while ((line = in.readLine()) != null) {
                Class<?> winClass = Class.forName(line);
                Method getInstance = winClass.getDeclaredMethod("getInstance");
                Object winObj = null;
                if (getInstance != null) {
                    winObj = getInstance.invoke(winClass);
                } else {
                    Constructor ctor = winClass.getDeclaredConstructor();
                    if (!ctor.canAccess(null)) {
                        ctor.setAccessible(true);
                    }
                    winObj = winClass.cast(ctor.newInstance());
                }

                if (winObj != null) {
                    switch (winObj) {
                        case JFrame frame ->
                            show(frame);
                        case JComponent component ->
                            show(component);
                        default -> {
                        }
                    }
                }
            }

            in.close();

            // Once the file has been read in and the BufferedReader has been
            //+ closed, delete the file on disk.
            getContext().getLocalStorage().deleteFile(
                    StorageLocations.CONFIG_DIR, openDocksFile);
        } catch (IOException
                | ClassNotFoundException
                | IllegalAccessException
                | IllegalArgumentException
                | InstantiationException
                | NoSuchMethodException
                | SecurityException
                | InvocationTargetException e) {
            String msgTmplt = "Unable to restore previously open windows from "
                    + "file \"{0}\": [{1}] {2}";
            log(Level.WARNING, msgTmplt, fileName, e.getClass().getSimpleName(),
                    e.getMessage());

            StatusDisplayer.getDefault().setMessage(MessageFormat.format(
                    msgTmplt, fileName, e.getClass().getSimpleName(),
                    e.getMessage()), Level.WARNING);
        } finally {
            super.preload();
        }
    }

    @Override
    protected void writeOpenWindowsFile() {
        if (!openDocks.isEmpty()) {
            Path filePath = null;
            String nameAndPath = null;

            try {
                filePath = getContext().getLocalStorage().getConfigDirectory();
                nameAndPath = Path.of(filePath.toString(), openDocksFile)
                        .toString();

                BufferedWriter out = new BufferedWriter(
                        getContext().getLocalStorage().openFileWriter(
                                StorageLocations.CONFIG_DIR, openDocksFile,
                                false));

                for (Window window : openWindows) {
                    out.write(window.getClass().getName() + "\n");
                }

                out.flush();
                out.close();
            } catch (IOException e) {
                String msgTmplt = "Unable to save open windows to \"{0}\": [{1}] "
                        + "{2}";
                log(Level.WARNING, msgTmplt, nameAndPath,
                        e.getClass().getSimpleName(), e.getMessage());

                StatusDisplayer.getDefault().setMessage(MessageFormat.format(
                        msgTmplt, nameAndPath, e.getClass().getSimpleName(),
                        e.getMessage()), Level.WARNING);
            }
        }

        super.writeOpenWindowsFile();
    }

    private void addCenterDock(Dockable dock) {
        restore(dock);

        centerDocks.addTab(dock.getTitle(), dock.getIcon(), dock);
        int index = centerDocks.indexOfComponent(dock);
        centerDocks.setTabComponentAt(index, new ButtonTabComponent(centerDocks,
                dock));
        centerDocks.setSelectedIndex(index);

        configureDocking();
    }

    private void addEastDock(Dockable dock) {
        restore(dock);

        eastDocks.addTab(dock.getTitle(), dock.getIcon(), dock);
        int index = eastDocks.indexOfComponent(dock);
        eastDocks.setTabComponentAt(index, new ButtonTabComponent(eastDocks,
                dock, eastRoot));
        eastDocks.setSelectedIndex(index);

        configureDocking();
    }

    private void addSouthDock(Dockable dock) {
        restore(dock);

        southDocks.addTab(dock.getTitle(), dock.getIcon(), dock);
        int index = southDocks.indexOfComponent(dock);
        southDocks.setTabComponentAt(index, new ButtonTabComponent(southDocks,
                dock, southRoot));
        southDocks.setSelectedIndex(index);

        configureDocking();
    }

    private void addWestDock(Dockable dock) {
        restore(dock);

        westDocks.addTab(dock.getTitle(), dock.getIcon(), dock);
        int index = westDocks.indexOfComponent(dock);
        westDocks.setTabComponentAt(index, new ButtonTabComponent(westDocks,
                dock, westRoot));
        westDocks.setSelectedIndex(index);

        configureDocking();
    }

    private void configureDocking() {
        setupEastDock();
        setupSouthDock();
        setupWestDocks();

        assembleSplitPanes();
    }

    private void setupEastDock() {
        if (eastDocks.isVisible()) {
            if (eastSplit == null) {
                eastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                        centerDocks, eastDocks);
                eastSplit.setResizeWeight(0.8d);
            }
        } else {
            eastSplit = null;
        }
    }

    private void setupSouthDock() {
        if (southDocks.isVisible()) {
            if (southSplit == null) {
                southSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                        centerDocks, southDocks);
                southSplit.setResizeWeight(0.8);
            }

            if (eastSplit != null) {
                southSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT,
                        eastSplit, southDocks);
            }
        } else {
            southSplit = null;
        }
    }

    private void setupWestDocks() {
        if (westDocks.isVisible()) {
            if (westSplit == null) {
                westSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                        westDocks, centerDocks);
                westSplit.setResizeWeight(0.2);
            }

            if (southSplit != null) {
                westSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                        westDocks, southSplit);
            } else if (eastSplit != null) {
                westSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                        westDocks, eastSplit);
            }
        }
    }

    private void assembleSplitPanes() {
        JComponent newComponent = null;

        // Determine which panes are visible and assemble them hierarchically.
        if (eastSplit != null && southSplit != null && westSplit != null) {
            newComponent = westSplit;
        } else if (eastSplit != null && southSplit != null) {
            newComponent = southSplit;
        } else if (westSplit != null && southSplit != null) {
            newComponent = westSplit;
        } else if (westSplit != null && eastSplit != null) {
            newComponent = westSplit;
        } else if (westSplit != null) {
            newComponent = westSplit;
        } else if (eastSplit != null) {
            newComponent = eastSplit;
        } else if (southSplit != null) {
            newComponent = southSplit;
        } else {
            newComponent = centerDocks;
        }

        setComponent(newComponent);
    }

    private void initComponents() {
        component = new JPanel(new BorderLayout());
        component.setName("component");

        eastSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        eastSplit.setName("eastSplit");

        southSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        southSplit.setName("southSplit");

        westSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        westSplit.setName("westSplit");

        // centerDocks will be visible regardless of whether it contains any
        //+ open tabs, so we do not add the visiblityListener to it.
        centerDocks = new JTabbedPane(JTabbedPane.TOP,
                JTabbedPane.SCROLL_TAB_LAYOUT);
        centerDocks.setName("centerDocks");

        // The rest of the docking tabbed panes will get the visibilityListener.
        eastDocks = new JTabbedPane(JTabbedPane.TOP,
                JTabbedPane.SCROLL_TAB_LAYOUT);
        eastDocks.setName("eastDocks");
        eastDocks.addContainerListener(visibilityListener);
        eastDocks.setVisible(false);

        eastSplit.setRightComponent(eastDocks);

        southDocks = new JTabbedPane(JTabbedPane.BOTTOM,
                JTabbedPane.SCROLL_TAB_LAYOUT);
        southDocks.setName("southDocks");
        southDocks.addContainerListener(visibilityListener);
        southDocks.setVisible(false);

        southSplit.setBottomComponent(southDocks);

        westDocks = new JTabbedPane(JTabbedPane.TOP,
                JTabbedPane.SCROLL_TAB_LAYOUT);
        westDocks.setName("westDocks");
        westDocks.addContainerListener(visibilityListener);
        westDocks.setVisible(false);

        eastRoot = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
        eastRoot.setName("eastRoot");
        eastRoot.addContainerListener(visibilityListener);
        eastRoot.setVisible(false);
        southRoot = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
        southRoot.setName("soutRoot");
        southRoot.addContainerListener(visibilityListener);
        southRoot.setVisible(false);
        westRoot = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
        westRoot.setName("westRoot");
        westRoot.addContainerListener(visibilityListener);
        westRoot.setVisible(false);

        westSplit.setLeftComponent(westDocks);

        component.add(westRoot, BorderLayout.WEST);
        component.add(southRoot, BorderLayout.SOUTH);
        component.add(eastRoot, BorderLayout.EAST);
        component.add(centerDocks, BorderLayout.CENTER);

        setComponent(component);
    }

    private static final String openDocksFile = "openDocks.cfg";
    protected final Map<String, Dockable> dockRegistry;
    protected final List<Dockable> openDocks;
    private final ContainerListener visibilityListener
            = new DockVisibilityListener();

    // Primary component for the DockingView.
    private JPanel component;

    // Docking tabs
    private JTabbedPane centerDocks;
    private JTabbedPane eastDocks;
    private JTabbedPane southDocks;
    private JTabbedPane westDocks;

    // Iconify roots
    private JPanel eastRoot;
    private JPanel southRoot;
    private JPanel westRoot;

    // Docking splits
    private JSplitPane eastSplit;
    private JSplitPane southSplit;
    private JSplitPane westSplit;

    private class DockVisibilityListener implements ContainerListener {

        @Override
        public void componentAdded(ContainerEvent e) {
            e.getContainer().setVisible(true);
        }

        @Override
        public void componentRemoved(ContainerEvent e) {
            Container container = e.getContainer();
            if (container != null) {
                int tabCount = 0;
                if (container instanceof JTabbedPane tabbedPane) {
                    tabCount = tabbedPane.getTabCount();
                } else {
                    tabCount = container.getComponentCount();
                }

                container.setVisible(container.getComponentCount() == 0
                        || tabCount == 0);
            }
        }

    }

}
