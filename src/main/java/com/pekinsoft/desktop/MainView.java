/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   MainView.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 28, 2024
 *  Modified   :   Oct 28, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 28, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import com.pekinsoft.api.StatusDisplayer;
import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.security.LogonManager;
import com.pekinsoft.utils.DebugLogger;
import com.pekinsoft.utils.PreferenceKeys;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.lang.reflect.*;
import java.nio.file.Path;
import java.text.MessageFormat;
import java.util.ArrayList;
import javax.swing.*;

/**
 * The {@code MainView} class is a subclass of the {@link FrameView} class that
 * provides default implementation for the {@link WindowManager} interface.
 * Further, the {@code MainView} provides for automatically logging out of a
 * server to which the {@link Application} might be connected, and protecting
 * any data displayed by covering the content area of the main window with an
 * mostly opaque glass pane until such time as the user logs in again.
 * <p>
 * Applications should use this class to display windows and dialogs in order to
 * make sure that they are properly initialized, including component property
 * injection from the component's {@link ResourceMap}, as well as having any
 * session state restored from the last application run.
 * <p>
 * Applications should use this class to close windows and dialogs, as well, so
 * that session state may be saved when appropriate. Windows are stored for
 * searching and saving open windows, so when they are closed through this
 * class, they will be removed from the tracking system for open windows. This
 * tracking system allows the application to restore any open windows when the
 * application is executed again.
 * <p>
 * There are only two methods that are abstract in this class:
 * {@link #preload() preload}, which is used to restore any windows that
 * remained open the last time the application exited, and
 * {@link #performLogin() performLogin} which is to allow the user to log back
 * into any server on which their login had timed out. This system is in place
 * specifically for the protection of data from unauthorized users.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class MainView extends FrameView implements WindowManager {

    private static final int MINUTES_IN_MILLIS = 60000;

    /**
     * Constructs a new {@code MainWindow} for an {@link Application}, with the
     * given {@link ApplicationContext}.
     * <p>
     * This constructor is only callable by subclasses.
     *
     * @param context the application context
     */
    protected MainView(ApplicationContext context) {
        super(context);
        logger = context.getLogger(getClass());

        openWindows = new ArrayList<>();

        // Retrieve the login timeout from the SYSTEM settings, with a default
        //+ value of 5 minutes
        int timeout = context.getPreferences(ApplicationContext.SYSTEM)
                .getInt(PreferenceKeys.PREF_LOGIN_TIMEOUT, 0) * MINUTES_IN_MILLIS;
        if (timeout > 0) {
            logoutTimer = new Timer(timeout, (ActionEvent evt) -> {
                DebugLogger.debug(logger, "Login Timeout: Performing logout");
                performLogout();
            });
        }
        logoutTimer.setRepeats(false);

        initComponents();

        getFrame().addWindowListener(new LogoutTimerStarter());
    }

    @Override
    public void showMainFrame() {
        logger.info("Starting showMainFrame.");
        initRootPaneContainer(getFrame());

        if (getContext().getPreferences(ApplicationContext.SYSTEM)
                .getBoolean(PreferenceKeys.PREF_REQUIRE_LOGON, false)) {
            getMainFrame().setTitle(getResourceMap()
                    .getString("main.frame.title"));
        } else {
            getMainFrame().setTitle(getResourceMap()
                    .getString("main.frame.unsecure.title"));
        }
        getFrame().setVisible(true);
        logger.info("Finishing showMainFrame.");
    }

    @Override
    public void show(JComponent component) {
        logger.info("Starting show(JComponent).");
        if (component == null) {
            return;
        }
        getContext().getResourceMap(component.getClass())
                .injectComponents(component);
        restore(component);
        setComponent(component);
        logger.info("Finishing show(JComponent).");
    }

    @Override
    public void show(JFrame frame) {
        logger.info("Starting show(JFrame).");
        if (frame == null) {
            return;
        }
        getContext().getResourceMap(frame.getClass())
                .injectComponents(frame);
        restore(frame);
        openWindows.add(frame);

        frame.setVisible(true);
        logger.info("Finishing show(JFrame).");
    }

    @Override
    public void show(JDialog dialog) {
        logger.info("Starting show(JDialog).");
        getContext().getResourceMap(dialog.getClass())
                .injectComponents(dialog);
        dialog.setLocationRelativeTo(getFrame());
        restore(dialog);

        // We do not save dialogs to the open windows listing.
        dialog.setVisible(true);
        logger.info("Finishing show(JDialog).");
    }

    @Override
    public void closeMainFrame() {
        logger.info("Starting closeMainFrame.");
        // Save which windows, if any are open.
        writeOpenWindowsFile();
        // Save session state for the main window.
        save(getFrame());
        // Hide and dispose the frame.
        getFrame().setVisible(false);
        logger.info("Finishing closeMainFrame.");
        getFrame().dispose();
    }

    @Override
    public void close(JComponent component) {
        logger.info("Starting close(JComponent).");
        save(component);
        setComponent(this.component);
        logger.info("Finishing close(JComponent).");
    }

    @Override
    public void close(JFrame frame) {
        logger.info("Starting close(JFrame).");
        save(frame);
        openWindows.remove(frame);
        frame.setVisible(false);
        frame.dispose();
        logger.info("Finishing close(JFrame).");
    }

    @Override
    public void close(JDialog dialog) {
        logger.info("Starting show(JDialog).");
        save(dialog);
        dialog.setVisible(false);
        // We do not dispose of a dialog because we do not know if the method
        //+ that showed it is done with it.
        logger.info("Finished close(JDialog).");
    }

    @Override
    public Container find(String name) {
        logger.info(String.format("Starting find for \"%s\"", name));
        for (Window window : openWindows) {
            if (name.equals(window.getName())) {
                return window;
            } else if (window instanceof Frame frame) {
                if (name.equals(frame.getTitle())) {
                    logger.info(String.format("Finishing find with %s", window));
                    return frame;
                }
            }
        }
        logger.info("Finishing find with null.");
        return null;
    }

    @Override
    public JFrame getMainFrame() {
        logger.info("Retrieving mainFrame");
        return getFrame();
    }

    @Override
    public void showToolBar(String name) {
        logger.info("Starting showToolBar.");
        if (name == null || name.isBlank() || name.isEmpty()) {
            return;
        }

        for (JToolBar toolBar : getToolBars()) {
            if (name.equals(toolBar.getName())) {
                toolBar.setVisible(true);
                getContext().getPreferences(ApplicationContext.USER)
                        .putBoolean(name + ".visible", true);
            }
        }
        logger.info("Finished showToolBar.");
    }

    @Override
    public void hideToolBar(String name) {
        logger.info("Starting hideToolBar.");
        if (name == null || name.isBlank() || name.isEmpty()) {
            return;
        }

        for (JToolBar toolBar : getToolBars()) {
            if (name.equals(toolBar.getName())) {
                toolBar.setVisible(false);
                getContext().getPreferences(ApplicationContext.USER)
                        .putBoolean(name + ".visible", false);
            }
        }
        logger.info("Finished hideToolBar.");
    }

    @Override
    public void showButtonText(boolean showText) {
        logger.info("Starting showButtonText.");
        getContext().getPreferences(ApplicationContext.USER)
                .putBoolean(PreferenceKeys.PREF_HIDE_ACTION_TEXT, !showText);
        for (JToolBar toolBar : getToolBars()) {
            toolBar.putClientProperty("hideActionText", !showText);
            for (Component c : toolBar.getComponents()) {
                if (c instanceof AbstractButton button) {
                    button.putClientProperty("hideActionText", !showText);
                }
            }
        }
        logger.info("Finished showButtonText.");
    }

    @Override
    public void preload() {
        logger.info("Starting preload.");

        String fileName = null;
        try {
            Path filePath = getContext().getLocalStorage().getConfigDirectory();
            fileName = Path.of(filePath.toString(), openWindowsFile).toString();
            DebugLogger.debug(logger, "Reading file \"%s\" to preload windows.",
                    fileName);

            BufferedReader in = new BufferedReader(getContext()
                    .getLocalStorage()
                    .openFileReader(StorageLocations.CONFIG_DIR,
                            openWindowsFile));

            String line = null;
            while ((line = in.readLine()) != null) {
                Class<?> winClass = Class.forName(line);
                Method getInstance = winClass.getDeclaredMethod("getInstance");
                Object winObj = null;
                if (getInstance != null) {
                    winObj = getInstance.invoke(winClass);
                } else {
                    Constructor ctor = winClass.getDeclaredConstructor();
                    if (!ctor.canAccess(null)) {
                        ctor.setAccessible(true);
                    }
                    winObj = winClass.cast(ctor.newInstance());
                }

                if (winObj != null) {
                    switch (winObj) {
                        case JFrame frame -> {
                            DebugLogger.debug(logger, "winObj is a JFrame");
                            show(frame);
                        }
                        case JComponent comp -> {
                            DebugLogger.debug(logger, "winObj is a JComponent");
                            show(comp);
                        }
                        default -> {
                        }
                    }
                }
            }

            in.close();

            // Once the file has been read in and the BufferedReader has been
            //+ closed, delete the file on disk.
            getContext().getLocalStorage().deleteFile(
                    StorageLocations.CONFIG_DIR, openWindowsFile);
        } catch (IOException
                | ClassNotFoundException
                | IllegalAccessException
                | IllegalArgumentException
                | InstantiationException
                | NoSuchMethodException
                | SecurityException
                | InvocationTargetException e) {
            String msgTmplt = "Unable to restore previously open windows from "
                    + "file \"{0}\": [{1}] {2}";
            log(Level.WARNING, msgTmplt, fileName, e.getClass().getSimpleName(),
                    e.getMessage());

            StatusDisplayer.getDefault().setMessage(MessageFormat.format(
                    msgTmplt, fileName, e.getClass().getSimpleName(),
                    e.getMessage()), Level.WARNING);
        }

        logger.info("Finished preload.");
    }

    @Override
    public void setStatusBar(JComponent statusBar) {
        logger.info("Starting setStatusBar.");
        statusBar.addMouseListener(mouseTimerListener);
        statusBar.addKeyListener(keyTimerListener);

        super.setStatusBar(statusBar);
        logger.info("Finishing setStatusBar.");
    }

    @Override
    public void setToolBar(JToolBar toolBar) {
        logger.info("Starting setToolBar.");
        toolBar.addMouseListener(mouseTimerListener);
        toolBar.addKeyListener(keyTimerListener);
        super.setToolBar(toolBar);
        logger.info("Finishing setToolBar.");
    }

    @Override
    public void setToolBars(java.util.List<JToolBar> toolBars) {
        logger.info("Starting setToolBars.");
        for (JToolBar tb : toolBars) {
            tb.addMouseListener(mouseTimerListener);
            tb.addKeyListener(keyTimerListener);
            logger.info("Finishing setToolBars.");
        }

        super.setToolBars(toolBars);
    }

    @Override
    public void removeToolBar(JToolBar toolBar) {
        logger.info("Starting removeToolBar.");
        toolBar.removeMouseListener(mouseTimerListener);
        toolBar.removeKeyListener(keyTimerListener);
        super.removeToolBar(toolBar);
        logger.info("Finishinging removeToolBar.");
    }

    @Override
    public void addToolBar(JToolBar toolBar) {
        logger.info("Starting addToolBar.");
        toolBar.addMouseListener(mouseTimerListener);
        toolBar.addKeyListener(keyTimerListener);
        super.addToolBar(toolBar);
        logger.info("Finishing addToolBar.");
    }

    @Override
    public void setMenuBar(JMenuBar menuBar) {
        logger.info("Starting setMenuBar.");
        menuBar.addMouseListener(mouseTimerListener);
        menuBar.addKeyListener(keyTimerListener);
        super.setMenuBar(menuBar);
        logger.info("Finishing setMenuBar.");
    }

    @Override
    public void setComponent(JComponent component) {
        logger.info("Starting setComponent.");
        component.addMouseListener(mouseTimerListener);
        component.addKeyListener(keyTimerListener);
        super.setComponent(component);
        logger.info("Finishing setComponent.");
    }

    /**
     * Method called from the {@link MouseAdapter} and/or {@link KeyAdapter} to
     * allow the user to log back in after a period of inactivity has locked the
     * window.
     */
    protected void performLogin() {
        boolean loggedOn = LogonManager.getInstance().logon();
        if (loggedOn) {
            getRootPane().getGlassPane().setVisible(false);
        }
    }

    protected void startLogoutTimer() {
        if (getContext().getPreferences(ApplicationContext.SYSTEM)
                .getBoolean(PreferenceKeys.PREF_REQUIRE_LOGON, false)) {
            DebugLogger.debug(logger, "Starting the logout timer.");
            logoutTimer.restart();
        }
    }

    protected void resetLogoutTimer() {
        if (getContext().getPreferences(ApplicationContext.SYSTEM)
                .getBoolean(PreferenceKeys.PREF_AUTO_LOGOUT, false)) {
            DebugLogger.debug(logger, "Restarting the logout timer.");
            logoutTimer.restart();
        }
    }

    /**
     * Restores the last saved session state for the specified
     * {@code component}.
     *
     * @param component the component for which session state is to be restored
     */
    protected void restore(Component component) {
        getContext().getResourceMap(component.getClass())
                .injectComponents(component);

        String fileName = sessionFileName(component);
        try {
            getContext().getSessionStorage().restore(component, fileName);
        } catch (IOException e) {
            log(Level.WARNING, "Unable to restore session for {0} from file "
                    + "\"{1}\"", component.getName(), fileName);
        }
    }

    /**
     * Saves the session state for the specified {@code component}.
     *
     * @param component the component for which session state is to be saved
     */
    protected void save(Component component) {
        String fileName = sessionFileName(component);
        try {
            getContext().getSessionStorage().save(component, fileName);
        } catch (IOException e) {
            log(Level.WARNING, "Unable to restore session for {0} from file "
                    + "\"{1}\"", component.getName(), fileName);
        }
    }

    /**
     * Retrieves a file name for the session state file for the specified
     * {@link Component}. If the component is {@code null}, {@code null} is
     * returned.
     *
     * @param c the component for which a session state file name is needed
     *
     * @return the session state file name
     */
    protected String sessionFileName(Component c) {
        if (c == null) {
            return null;
        }

        return c.getName() + ".session.xml";
    }

    /**
     * Convenience method for subclasses to perform logging using the
     * {@link java.util.logging.Logger Logger} obtained from the
     * {@link ApplicationContext} for this class.
     *
     * @param level the {@link java.util.logging.Level Level} at which the
     *              message should be logged
     * @param msg   the message to be logged
     * @param args  any arguments to the message or a {@link Throwable}
     */
    protected void log(Level level, String msg, Object... args) {
        logger.log(level, msg, args);
    }

    protected void writeOpenWindowsFile() {
        if (!openWindows.isEmpty()) {
            Path filePath = null;
            String nameAndPath = null;

            try {
                filePath = getContext().getLocalStorage().getConfigDirectory();
                nameAndPath = Path.of(filePath.toString(), openWindowsFile)
                        .toString();

                BufferedWriter out = new BufferedWriter(
                        getContext().getLocalStorage().openFileWriter(
                                StorageLocations.CONFIG_DIR, openWindowsFile,
                                false));

                for (Window window : openWindows) {
                    out.write(window.getClass().getName() + "\n");
                }

                out.flush();
                out.close();
            } catch (IOException e) {
                String msgTmplt = "Unable to save open windows to \"{0}\": [{1}] "
                        + "{2}";
                log(Level.WARNING, msgTmplt, nameAndPath,
                        e.getClass().getSimpleName(), e.getMessage());

                StatusDisplayer.getDefault().setMessage(MessageFormat.format(
                        msgTmplt, nameAndPath, e.getClass().getSimpleName(),
                        e.getMessage()), Level.WARNING);
            }
        }
    }

    /**
     * Initializes all of the properties of the specified {@link
     * javax.swing.RootPaneContainer RootPaneContainer} {@code c}. This method
     * attempts to add various listeners to the container to handle proper
     * shutdown and session state saving behind the scenes, as well as to
     * attempt to restore the previous session state, if any.
     *
     * @param c the {@code RootPaneContainer} to initialize
     */
    protected final void initRootPaneContainer(RootPaneContainer c) {
        JComponent rootPane = c.getRootPane();
        // These initializations are only done once
        Object k = "SingleFrameApplication.initRootPaneContainer";
        if (rootPane.getClientProperty(k) != null) {
            return;
        }
        rootPane.putClientProperty(k, Boolean.TRUE);
        // Inject resources
        Container root = rootPane.getParent();
        if (root instanceof Component component) {
            getResourceMap().injectComponents(root);
        }
        // If this is the mainFrame, then close == exit
        JFrame mf = getMainFrame();
        if (c == mf) {
            mf.addWindowListener(new MainFrameListener());
            mf.addHierarchyListener(new SecondaryWindowListener());
            mf.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        } else if (root instanceof Component window) { // close == save session state
            window.addHierarchyListener(
                    new SecondaryWindowListener());
        }
        // If this is a JFrame monitor "normal" (not maximized) bounds
        if (root instanceof JFrame) {
            root.addComponentListener(new Application.FrameBoundsListener());
        }
        // If the window's bounds don't appear to have been set, do it
        if (root != null) {
            switch (root) {
                case Window window -> {
                    if (!root.isValid() || (root.getWidth() == 0) || (root
                            .getHeight() == 0)) {
                        window.pack();

                        if (window.getWidth() <= 80 || window.getHeight() <= 80) {
                            Dimension dim = (Dimension) getContext()
                                    .getResourceMap()
                                    .getObject("Application.defaultWindowSize",
                                            Dimension.class);
                            if (dim == null) {
                                ResourceMap rm = getContext().getResourceMap(
                                        root
                                                .getClass());
                                dim = (Dimension) rm.getObject(
                                        "window.defaultSize",
                                        Dimension.class);
                                if (dim == null) {
                                    dim = new Dimension(1024, 768);
                                }
                            }

                            window.setSize(dim);
                        }
                    }
                    if (!window.isLocationByPlatform() && (root.getX() == 0) && (root
                            .getY() == 0)) {
                        Component owner = window.getOwner();
                        if (owner == null) {
                            owner = (window != mf) ? mf : null;
                        }
                        window.setLocationRelativeTo(owner);  // center the window
                    }
                }
                case JInternalFrame window -> {
                    if (!window.isValid() || (window.getWidth() == 0) || (window
                            .getHeight() == 0)) {
                        window.pack();
                    }
                }
                default -> {
                }
            }
        }
        // Restore session state
        if (root instanceof Component component) {
            String filename = sessionFileName(component);
            if (filename != null) {
                try {
                    getContext().getSessionStorage().restore(root, filename);
                } catch (IOException e) {
                    String msg = String.format("couldn't restore sesssion [%s]",
                            filename);
                    logger.log(Level.WARNING, msg, e);
                }
            }
        }
    }

    private void performLogout() {
        opaqueGlassPane.addMouseListener(new GpMouseListener());
        opaqueGlassPane.addKeyListener(new GpKeyListener());
        getRootPane().setGlassPane(opaqueGlassPane);
        getRootPane().getGlassPane().setVisible(true);
    }

    private void initComponents() {
        statusBar = StatusBar.getInstance();
        statusBar.setName("statusBar");

        component = new JPanel();
        component.setName("component");
        component.setLayout(new BorderLayout());

        setStatusBar(statusBar);
        setToolBars(toolBars);
        setComponent(component);

        if (getRootPane() != null) {
            getRootPane().addMouseListener(mouseTimerListener);
            getRootPane().addKeyListener(keyTimerListener);
        }

        if (getComponent() != null) {
            getComponent().addMouseListener(mouseTimerListener);
            getComponent().addMouseListener(mouseTimerListener);
        }

        if (getFrame() != null) {
            getFrame().addMouseListener(mouseTimerListener);
            getFrame().addMouseListener(mouseTimerListener);
        }

        if (getStatusBar() != null) {
            getStatusBar().addMouseListener(mouseTimerListener);
            getStatusBar().addKeyListener(keyTimerListener);
        }

        if (getMenuBar() != null) {
            getMenuBar().addMouseListener(mouseTimerListener);
            getMenuBar().addKeyListener(keyTimerListener);
        }
    }

    private static final String openWindowsFile = "openWindows.cfg";
    protected final java.util.List<Window> openWindows;

    private final Logger logger;
    private final MouseListener mouseTimerListener = new MouseTimerListener();
    private final KeyListener keyTimerListener = new KeyTimerListener();
    private final JComponent opaqueGlassPane = new SemiOpaqueGlassPane();
    private java.util.List<JToolBar> toolBars = new ArrayList<>();
    private javax.swing.Timer logoutTimer;

    private JComponent toolBarsPanel = new JPanel(new FlowLayout(
            FlowLayout.LEADING));

    private JMenuBar menuBar = null;
    private JComponent component = null;
    private JComponent statusBar = null;

    private final class LogoutTimerStarter extends WindowAdapter {

        @Override
        public void windowOpened(WindowEvent e) {
            resetLogoutTimer();
        }

    }

    private final class MouseTimerListener extends MouseAdapter {

        @Override
        public void mouseMoved(MouseEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void mouseExited(MouseEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            resetLogoutTimer();
        }

    }

    private final class KeyTimerListener extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void keyPressed(KeyEvent e) {
            resetLogoutTimer();
        }

        @Override
        public void keyTyped(KeyEvent e) {
            resetLogoutTimer();
        }

    }

    private final class SemiOpaqueGlassPane extends JComponent {

        public SemiOpaqueGlassPane() {
            setSize(MainView.this.getRootPane().getSize());
        }

        @Override
        public void paint(Graphics g) {
            super.paintComponent(g);

            Graphics2D g2 = (Graphics2D) g.create();

            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER,
                    0.8f));

            g2.setColor(getBackground());
            g2.fillRect(0, 0, getWidth(), getHeight());

            String text = getResourceMap().getString("logged.out.message");
            g2.setFont(g2.getFont().deriveFont(Font.BOLD, 24.0f));
            int fontHeight = g2.getFontMetrics().getHeight();
            int fontWidth = g2.getFontMetrics().stringWidth(text);
            int x = (getWidth() - fontWidth) / 2;
            int y = (getHeight() - fontHeight) / 2;

            g2.setColor(Color.black);
            g2.drawString(text, x, y);

            g2.dispose();
        }

    }

    private final class GpMouseListener extends MouseAdapter {

        @Override
        public void mouseMoved(MouseEvent e) {
            DebugLogger.debug(logger, "Mouse moved");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            DebugLogger.debug(logger, "Mouse dragged");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            DebugLogger.debug(logger, "Mouse wheel moved");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void mouseExited(MouseEvent e) {
            DebugLogger.debug(logger, "Mouse exited");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            DebugLogger.debug(logger, "Mouse entered");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void mouseReleased(MouseEvent e) {
            DebugLogger.debug(logger, "Mouse released");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void mousePressed(MouseEvent e) {
            DebugLogger.debug(logger, "Mouse pressed");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            DebugLogger.debug(logger, "Mouse clicked");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

    }

    private final class GpKeyListener extends KeyAdapter {

        @Override
        public void keyReleased(KeyEvent e) {
            DebugLogger.debug(logger, "Key released");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void keyPressed(KeyEvent e) {
            DebugLogger.debug(logger, "Key pressed");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

        @Override
        public void keyTyped(KeyEvent e) {
            DebugLogger.debug(logger, "Key typed");
            if (logoutTimer.isRunning()) {
                logoutTimer.restart();
            }
            performLogin();
        }

    }

    private class MainFrameListener extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            save(MainView.this.getFrame());
            getApplication().exit(e);
        }
    }

    /*
     * Although it would have been simpler to listen for changes in the
     * secondary window's visibility per either a PropertyChangeEvent on the
     * "visible" property or a change in visibility per ComponentListener,
     * neither listener is notified if the secondary window is disposed.
     * HierarchyEvent.SHOWING_CHANGED does report the change in all cases, so we
     * use that.
     */
    public class SecondaryWindowListener implements HierarchyListener {

        @Override
        public void hierarchyChanged(HierarchyEvent e) {
            if ((e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
                if (e.getSource() instanceof Component secondaryWindow) {
                    if (!secondaryWindow.isShowing()) {
                        try {
                            save(secondaryWindow);
                        } catch (NullPointerException npe) {
                            String msg = String.format("Trapped %s in "
                                    + "SecondaryWindowListener.hierarchyChanged() "
                                    + "on component \"%s\".", npe.getClass()
                                            .getSimpleName(),
                                    e.getComponent().getName());
                            System.err.println(msg);
                        }
                    }

                    getContext().getPreferences(ApplicationContext.USER)
                            .putBoolean(secondaryWindow.getName() + ".visible",
                                    secondaryWindow.isShowing());
                }
            }
        }
    }

    /*
     * In order to properly restore a maximized JFrame, we need to record it's
     * normal (not maximized) bounds. They're recorded under a rootPane client
     * property here, so that they've can be session-saved by
     * WindowProperty#getSessionState().
     */
    public static class FrameBoundsListener implements ComponentListener {

        private void maybeSaveFrameSize(ComponentEvent e) {
            if (e.getComponent() instanceof JFrame f) {
                if ((f.getExtendedState() & Frame.MAXIMIZED_BOTH) == 0) {
                    String clientPropertyKey = "WindowState.normalBounds";
                    f.getRootPane().putClientProperty(clientPropertyKey, f
                            .getBounds());
                }
            }
        }

        @Override
        public void componentResized(ComponentEvent e) {
            maybeSaveFrameSize(e);
        }

        /*
         * BUG: on Windows XP, with JDK6, this method is called once when the
         * frame is a maximized, with x,y=-4 and getExtendedState() == 0.
         */
        @Override
        public void componentMoved(ComponentEvent e) {
            /*
             * maybeSaveFrameSize(e);
             */ }

        @Override
        public void componentHidden(ComponentEvent e) {
        }

        @Override
        public void componentShown(ComponentEvent e) {
        }
    }

}
