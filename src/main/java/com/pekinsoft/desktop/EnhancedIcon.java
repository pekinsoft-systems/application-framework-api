/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   EnhancedIcon.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 31, 2024
 *  Modified   :   Oct 31, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 31, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import java.awt.*;
import javax.swing.Icon;
import javax.swing.JLabel;

public class EnhancedIcon implements Icon {

    public enum Layout {
        HORIZONTAL, VERTICAL
    }

    public enum Rotate {
        NONE, LEFT, RIGHT, UPSIDE_DOWN, CUSTOM
    }

    private final Icon mainIcon;
    private String text;
    private Layout layout;
    private Rotate rotate;
    private double customDegrees;
    private Font font;
    private Color color;
    private int padding;

    public EnhancedIcon(Icon mainIcon, String text, Layout layout, Rotate rotate,
            int padding) {
        this.mainIcon = mainIcon;
        this.text = text;
        this.layout = layout;
        this.rotate = rotate;
        this.padding = padding;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public Font getFont() {
        return this.font;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return this.color;
    }

    public void setCustomDegrees(double degrees) {
        this.customDegrees = degrees;
    }

    public double getCustomDegrees() {
        return this.customDegrees;
    }

    @Override
    public int getIconWidth() {
        int baseWidth = layout == Layout.HORIZONTAL
                        ? mainIcon.getIconWidth() + padding + getTextWidth()
                        : mainIcon.getIconWidth();

        return applyRotationToDimension(baseWidth, getIconHeight());
    }

    @Override
    public int getIconHeight() {
        int baseHeight = layout == Layout.VERTICAL
                         ? mainIcon.getIconHeight() + padding + getTextHeight()
                         : mainIcon.getIconHeight();

        return applyRotationToDimension(getIconWidth(), baseHeight);
    }

    @Override
    public void paintIcon(Component c, Graphics g, int x, int y) {
        Graphics2D g2 = (Graphics2D) g.create();

        if (rotate != Rotate.NONE) {
            applyRotation(g2, x, y);
        }

        mainIcon.paintIcon(c, g2, x, y);
        paintText(c, g2, x, y);

        g2.dispose();
    }

    private void paintText(Component c, Graphics2D g2, int x, int y) {
        if (text == null || text.isEmpty()) {
            return;
        }

        g2.setFont(font != null ? font : c.getFont());
        g2.setColor(color != null ? color : c.getForeground());
        FontMetrics fm = g2.getFontMetrics();

        int textX = layout == Layout.HORIZONTAL ? x + mainIcon.getIconWidth() + padding : x;
        int textY = layout == Layout.VERTICAL
                    ? y + mainIcon.getIconHeight() + padding
                    : y + (mainIcon.getIconHeight() - fm.getHeight()) / 2 + fm
                .getAscent();

        g2.drawString(text, textX, textY);
    }

    private void applyRotation(Graphics2D g2, int x, int y) {
        int centerX = x + getIconWidth() / 2;
        int centerY = y + getIconHeight() / 2;

        switch (rotate) {
            case LEFT ->
                g2.rotate(Math.toRadians(-90), centerX, centerY);
            case RIGHT ->
                g2.rotate(Math.toRadians(90), centerX, centerY);
            case UPSIDE_DOWN ->
                g2.rotate(Math.toRadians(180), centerX, centerY);
            case CUSTOM ->
                g2.rotate(Math.toRadians(customDegrees), centerX, centerY);
        }
    }

    private int getTextWidth() {
        return new JLabel(text).getFontMetrics(getFont()).stringWidth(text);
    }

    private int getTextHeight() {
        return new JLabel(text).getFontMetrics(getFont()).getHeight();
    }

    private int applyRotationToDimension(int width, int height) {
        if (rotate == Rotate.CUSTOM && customDegrees != 0) {
            double radians = Math.toRadians(customDegrees);
            return (int) Math.ceil(Math.abs(Math.cos(radians)) * width + Math
                    .abs(Math.sin(radians)) * height);
        } else if (rotate == Rotate.LEFT || rotate == Rotate.RIGHT) {
            return Math.max(width, height);
        }
        return width;
    }
}
