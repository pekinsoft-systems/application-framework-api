/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   SplashScreen.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 13, 2024
 *  Modified   :   Nov 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import com.pekinsoft.desktop.SplashScreen;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URL;
import java.util.jar.Attributes;
import java.util.jar.Manifest;
import javax.swing.*;

/**
 * SplashScreen provides a splash screen for applications, allowing custom
 * images, messages, and progress indicators.
 * This class is implemented as a singleton to ensure only one splash screen is
 * used throughout the application.
 */
public class SplashScreen {

    private static SplashScreen instance;
    private JWindow splashWindow;
    private Image splashImage;
    private String applicationTitle;
    private String vendor;
    private String copyrightNotice;
    private int totalStartupSteps;
    private int currentStep;
    private Color statusMessageColor;
    private Color progressBarColor;
    private String currentStartupMessage;

    /**
     * Private constructor to enforce the Singleton pattern.
     * Initializes the splash screen, sets default properties, and prepares the
     * UI components.
     */
    private SplashScreen() {
        // Load the splash screen image
        loadSplashImage();

        // Set default properties
        this.applicationTitle = "My Application";
        this.vendor = "My Company";
        this.copyrightNotice = "© 2024 My Company. All rights reserved.";
        this.currentStep = 0;
        this.statusMessageColor = Color.WHITE;
        this.progressBarColor = Color.GREEN;

        // Set up the splash screen
        splashWindow = new JWindow();
        splashWindow.setSize(400, 300);
        splashWindow.setLocationRelativeTo(null);

        // Create a panel to draw custom content
        JPanel contentPanel = new JPanel() {
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                Graphics2D g2d = (Graphics2D) g;
                g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                        RenderingHints.VALUE_ANTIALIAS_ON);

                if (splashImage != null) {
                    g2d.drawImage(splashImage, 0, 0, getWidth(), getHeight(),
                            this);
                } else {
                    g2d.setColor(new Color(70, 130, 180));
                    g2d.fillRect(0, 0, getWidth(), getHeight());
                }

                g2d.setColor(Color.WHITE);
                g2d.setFont(new Font("SansSerif", Font.BOLD, 24));
                g2d.drawString(applicationTitle, 20, 40);

                g2d.setFont(new Font("SansSerif", Font.PLAIN, 16));
                g2d.drawString(vendor, 20, 70);

                g2d.setFont(new Font("SansSerif", Font.ITALIC, 12));
                g2d.drawString(copyrightNotice, 20, getHeight() - 20);

                if (currentStartupMessage != null && !currentStartupMessage
                        .isEmpty()) {
                    g2d.setColor(statusMessageColor);
                    g2d.setFont(new Font("SansSerif", Font.PLAIN, 14));
                    g2d.drawString(currentStartupMessage, 20, getHeight() - 80);
                }

                g2d.setColor(progressBarColor);
                int progressBarWidth = totalStartupSteps > 0 ? (int) ((getWidth() - 40) * ((double) currentStep / totalStartupSteps)) : 0;
                g2d.fillRect(20, getHeight() - 50, progressBarWidth, 20);
                g2d.setColor(Color.BLACK);
                g2d.drawRect(20, getHeight() - 50, getWidth() - 40, 20);
            }
        };

        contentPanel.setLayout(new BorderLayout());
        splashWindow.add(contentPanel);

        // Optional: Add a dismiss button
        JButton dismissButton = new JButton("Dismiss");
        dismissButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dismiss();
            }
        });
        contentPanel.add(dismissButton, BorderLayout.SOUTH);
    }

    /**
     * Returns the singleton instance of the SplashScreen.
     *
     * @return the singleton instance of SplashScreen
     */
    public static SplashScreen getInstance() {
        if (instance == null) {
            instance = new SplashScreen();
        }
        return instance;
    }

    /**
     * Loads the splash image from the manifest file or the build directory.
     * If the image is not found in the manifest, a fallback path is used.
     */
    private void loadSplashImage() {
        try {
            URL manifestUrl = getClass().getClassLoader().getResource(
                    "META-INF/MANIFEST.MF");
            if (manifestUrl != null) {
                Manifest manifest = new Manifest(manifestUrl.openStream());
                Attributes attributes = manifest.getMainAttributes();
                String splashImagePath = attributes.getValue(
                        "SplashScreen-Image");
                if (splashImagePath != null) {
                    URL imageUrl = getClass().getClassLoader().getResource(
                            splashImagePath);
                    if (imageUrl != null) {
                        if ("jar".equals(imageUrl.getProtocol())) {
                            splashImage = Toolkit.getDefaultToolkit().getImage(
                                    imageUrl);
                        } else if ("file".equals(imageUrl.getProtocol())) {
                            splashImage = Toolkit.getDefaultToolkit().getImage(
                                    imageUrl);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Fallback to locating the image in the build directory if not found in JAR
        if (splashImage == null) {
            URL imageUrl = getClass().getResource("/resources/splash.png"); // Example path for IDE builds
            if (imageUrl != null) {
                splashImage = Toolkit.getDefaultToolkit().getImage(imageUrl);
            }
        }
    }

    /**
     * Displays the splash screen in a separate thread to prevent blocking the
     * main thread.
     */
    public void show() {
        new Thread(() -> {
            splashWindow.setAlwaysOnTop(true);
            splashWindow.setVisible(true);
        }).start();
    }

    /**
     * Makes the splash screen thread sleep for the specified amount of time.
     * This method is mainly intended for testing purposes to slow down the
     * splash screen updates.
     *
     * @param milliseconds the amount of time, in milliseconds, for the splash
     *                     screen to sleep
     */
    public void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            e.printStackTrace();
        }
    }

    /**
     * Dismisses the splash screen, hiding and disposing of the window.
     */
    public void dismiss() {
        SwingUtilities.invokeLater(() -> {
            splashWindow.setVisible(false);
            splashWindow.dispose();
        });
    }

    /**
     * Updates the current progress step, message, and repaints the splash
     * screen.
     * This method ensures that the UI update occurs on the Event Dispatch
     * Thread.
     *
     * @param step    the current step of the startup process
     * @param message the message to display for the current step
     */
    public void updateStep(int step, String message) {
        if (step >= 0 && step <= totalStartupSteps) {
            this.currentStep = step;
            this.currentStartupMessage = message;
            SwingUtilities.invokeLater(() -> splashWindow.repaint());
        }
    }

    public void setImage(Image image) {
        splashImage = image;
        SwingUtilities.invokeLater(() -> splashWindow.repaint());
    }

    /**
     * Sets the total number of startup steps for the progress bar.
     *
     * @param totalSteps the total number of startup steps
     */
    public void setTotalStartupSteps(int totalSteps) {
        this.totalStartupSteps = totalSteps;
    }

    /**
     * Sets the application title to be displayed on the splash screen.
     *
     * @param applicationTitle the title of the application
     */
    public void setApplicationTitle(String applicationTitle) {
        this.applicationTitle = applicationTitle;
    }

    /**
     * Sets the vendor name to be displayed on the splash screen.
     *
     * @param vendor the name of the vendor
     */
    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    /**
     * Sets the copyright notice to be displayed on the splash screen.
     *
     * @param copyrightNotice the copyright notice
     */
    public void setCopyrightNotice(String copyrightNotice) {
        this.copyrightNotice = copyrightNotice;
    }

    /**
     * Sets the color of the status messages displayed on the splash screen.
     *
     * @param statusMessageColor the color of the status messages
     */
    public void setStatusMessageColor(Color statusMessageColor) {
        this.statusMessageColor = statusMessageColor;
    }

    /**
     * Sets the color of the progress bar displayed on the splash screen.
     *
     * @param progressBarColor the color of the progress bar
     */
    public void setProgressBarColor(Color progressBarColor) {
        this.progressBarColor = progressBarColor;
    }
}
