/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   DockingNotificationsPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 28, 2024
 *  Modified   :   Oct 28, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 28, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop.notifications.support;

import com.pekinsoft.desktop.Dockable;
import com.pekinsoft.framework.Application;
import javax.swing.table.TableModel;

/**
 *
 * @author Sean Carrick
 */
public class DockingNotificationsPanel extends Dockable {

    private static Dockable instance;

    public static Dockable getInstance() {
        if (instance == null) {
            instance = new DockingNotificationsPanel();
        }
        return instance;
    }

    /**
     * Creates new form DockingNotificationsPanel
     */
    private DockingNotificationsPanel() {
        super(Application.getInstance().getContext());

        initComponents();

        getContext().addPropertyChangeListener(
                (NotificationsTableModel) notificationsTableModel);
        getApplication().addPropertyChangeListener(
                (NotificationsTableModel) notificationsTableModel);
    }

    @Override
    public void onDock() {

    }

    @Override
    public void onUndock() {

    }

    @Override
    public void close() {

    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        notifierSplit = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        notificationsTable = new javax.swing.JTable();
        holderLabel = new javax.swing.JLabel();

        setLayout(new java.awt.BorderLayout());

        notifierSplit.setDividerLocation(500);
        notifierSplit.setResizeWeight(0.5);
        notifierSplit.setName("notifierSplit"); // NOI18N

        jScrollPane1.setName("jScrollPane1"); // NOI18N

        notificationsTable.setModel(notificationsTableModel);
        notificationsTable.setColumnModel(new NotificationsTableColumnModel(
            notificationsTable.getFontMetrics(notificationsTable.getFont())));
    notificationsTable.setName("notificationsTable"); // NOI18N
    jScrollPane1.setViewportView(notificationsTable);

    notifierSplit.setLeftComponent(jScrollPane1);

    holderLabel.setFont(new java.awt.Font("Dialog", 1, 11)); // NOI18N
    holderLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
    holderLabel.setText("jLabel1");
    holderLabel.setName("holderLabel"); // NOI18N
    notifierSplit.setRightComponent(holderLabel);

    add(notifierSplit, java.awt.BorderLayout.CENTER);
    getResourceMap().injectComponents(this);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel holderLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable notificationsTable;
    private javax.swing.JSplitPane notifierSplit;
    // End of variables declaration//GEN-END:variables

    private final TableModel notificationsTableModel = new NotificationsTableModel();

}
