/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-management-api
 *  Class      :   NotificationsTableModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 25, 2024
 *  Modified   :   Jun 25, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 25, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.desktop.notifications.support;

import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.framework.AbstractBean;
import com.pekinsoft.utils.PropertyKeys;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class NotificationsTableModel extends DefaultTableModel
        implements PropertyChangeListener, PropertyKeys {
    
    public NotificationsTableModel () {
        notifications = new ArrayList<>();
        
        ServiceLoader.load(AbstractBean.class).findFirst().get()
                .addPropertyChangeListener(this);
    }
    
    public ErrorInfo getSelectedErrorInfo(int row) {
        if (notifications.isEmpty()) {
            return null;
        }
        
        return notifications.get(row);
    }

    @Override
    public Object getValueAt(int row, int column) {
        ErrorInfo info = notifications.get(row);
        
        return switch (column) {
            case 0 -> info.getCategory();
            case 1 -> info.getTitle();
            case 2 -> info.getBasicErrorMessage();
            case 3 -> info.getLevel();
            default -> null;
        };
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        return notifications == null
                ? 0
                : notifications.size();
    }

    @Override
    public void removeRow(int row) {
        if (notifications != null) {
            if (row < notifications.size()) {
                ServiceLoader.load(AbstractBean.class).findFirst().get()
                        .firePropertyChange(KEY_REMOVE_NOTIFICATION, null, 
                        notifications.get(row));
                notifications.remove(row);
            }
        }
        
        fireTableDataChanged();
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        if (pce.getPropertyName() != null) switch (pce.getPropertyName()) {
            case KEY_ADD_NOTIFICATION -> {
                Object obj = pce.getNewValue();
                if (obj instanceof ErrorInfo ei) {
                    if (!notifications.contains(ei)) {
                        notifications.add(ei);
                    }
                }
            }
            case KEY_REMOVE_NOTIFICATION -> {
                Object obj = pce.getNewValue();
                if (obj instanceof ErrorInfo ei) {
                    if (notifications.contains(ei)) {
                        notifications.remove(ei);
                    }
                }
            }
        }
        fireTableDataChanged();
    }
    
    private final List<ErrorInfo> notifications;

}
