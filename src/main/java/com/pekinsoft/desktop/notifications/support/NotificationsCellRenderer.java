/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-management-api
 *  Class      :   NotificationsCellRenderer.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 25, 2024
 *  Modified   :   Jun 25, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 25, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop.notifications.support;

import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.utils.PreferenceKeys;
import java.awt.Component;
import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class NotificationsCellRenderer extends DefaultTableCellRenderer
        implements PreferenceKeys {

    public NotificationsCellRenderer() {

    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel label = (JLabel) super.getTableCellRendererComponent(table,
                value, isSelected, hasFocus, row, column);

        if (column == 3) {
            label.setHorizontalAlignment(JLabel.CENTER);
        }

        NotificationsTableModel tm = (NotificationsTableModel) table.getModel();
        ErrorInfo info = tm.getSelectedErrorInfo(row);

        if (isSelected) {
            label.setForeground(info.getLevel().asColor());
            label.setBackground(UIManager.getColor("Table.selectionBackground"));
        } else {
            label.setForeground(UIManager.getColor("Table.foreground"));
            label.setBackground(info.getLevel().asColor());
        }

        switch (column) {
            case 0 ->
                label.setText(info.getCategory());
            case 1 ->
                label.setText(info.getTitle());
            case 2 ->
                label.setText(info.getBasicErrorMessage());
            case 3 ->
                label.setText(info.getLevel().getName());
        }

        return label;
    }

}
