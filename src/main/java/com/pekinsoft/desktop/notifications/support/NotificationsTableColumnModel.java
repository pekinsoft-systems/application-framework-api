/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   application-management-api
 *  Class      :   NotificationsTableColumnModel.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 25, 2024
 *  Modified   :   Jun 25, 2024
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 25, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.desktop.notifications.support;

import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ResourceMap;
import java.awt.FontMetrics;
import java.util.Objects;
import javax.swing.table.DefaultTableColumnModel;
import javax.swing.table.TableColumn;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class NotificationsTableColumnModel extends DefaultTableColumnModel {
    
    public NotificationsTableColumnModel (FontMetrics fm) {
        Objects.requireNonNull(fm, "The FontMetrics cannot be null.");
        resourceMap = Application.getInstance().getContext().getResourceMap(getClass());
        
        int digit = fm.stringWidth("0");
        int alpha = fm.stringWidth("W");
        
        addColumn(createColumn(0, 10 * alpha, fm, true, 
                resourceMap.getString("col0.text")));
        addColumn(createColumn(1, 20 * alpha, fm, true,
                resourceMap.getString("col1.text")));
        addColumn(createColumn(2, 50 * alpha, fm, true,
                resourceMap.getString("col2.text")));
        addColumn(createColumn(3, 15 * alpha, fm, true,
                resourceMap.getString("col3.text")));
    }
    
    private TableColumn createColumn(int col, int w, FontMetrics fm, 
            boolean resizable, String text) {
        int textW = fm.stringWidth(text + "   ");
        if (w < textW) {
            w = textW;
        }
        
        TableColumn c = new TableColumn(col);
        c.setCellRenderer(new NotificationsCellRenderer());
        c.setHeaderValue(text);
        c.setPreferredWidth(w);
        if (!resizable) {
            c.setMinWidth(w);
            c.setMaxWidth(w);
        }
        c.setResizable(resizable);
        
        return c;
    }
    
    private final ResourceMap resourceMap;

}
