/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Dockable.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import com.pekinsoft.api.DockingManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import java.text.MessageFormat;
import javax.swing.Icon;
import javax.swing.JPanel;

/**
 * The {@code Dockable} abstract class defines methods to allow for a
 * {@code javax.swing.JPanel JPanel}, or other
 * {@link java.awt.Container Container}, to be used in some sort of docking
 * framework within the {@link DockingManager main window} of an
 * {@link Application}.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 2.4
 * @since 2.0
 */
public abstract class Dockable extends JPanel {

    protected Dockable(ApplicationContext context) {
        this.context = context;
        logger = context.getLogger(getClass());
    }

    /**
     * Retrieves the {@code Dockable}'s unique identifier string for use within
     * the docking framework.
     * <p>
     * This value should uniquely identify this {@code Dockable} from the others
     * contained within the framework. By default, this method simply returns
     * the {@code Dockable}'s title property value.
     *
     * @return the {@code Dockable}'s unique identifier string
     */
    public String getDockId() {
        return getTitle();
    }

    /**
     * Retrieves the title to be displayed in the {@code Dockable}'s tab within
     * the docking framework in which it is used.
     * <p>
     * This value should be pulled from the
     * {@link com.pekinsoft.framework.ResourceMap ResourceMap} for the class, so
     * that it may be easily internationalized.
     *
     * @return the {@code Dockable}'s title
     */
    public String getTitle() {
        return getResourceMap().getString("dock.title");
    }

    /**
     * Retrieves the {@link javax.swing.Icon Icon} that is displayed in the
     * {@code Dockable}'s tab within the docking framework in which it is used.
     * <p>
     * This value should be pulled from the
     * {@link com.pekinsoft.framework.ResourceMap ResourceMap} for the class, so
     * that it may be easily localized.
     *
     * @return the {@code Dockable}'s icon
     */
    public Icon getIcon() {
        return getResourceMap().getIcon("dock.icon");
    }

    /**
     * A string value that names the location in which the {@code Dockable}
     * should be installed within the docking framework in which it is used.
     * <p>
     * This version of {@code Dockable} has updated this method to return a
     * string, so that each
     * {@link com.pekinsoft.framework.AppAction Application's} docking framework
     * may define its own docking locations. If the {@code Application}'s
     * docking framework provides an enumeration of docking location values, it
     * may parse the returned string value into one of them, or throw an error
     * if no such location exists.
     *
     * @return the location in which the {@code Dockable} should be installed as
     *         a string value
     */
    public String getDockingLocation() {
        return getResourceMap().getString("dock.location");
    }

    /**
     * Provides a means for the {@code Dockable} to take some actions whenever
     * it is docked into the {@link DockingManager}.
     * <p>
     * Such actions could be adding toolbars, menus, or menu items into the
     * {@link Application Application's} global actions system.
     */
    public abstract void onDock();

    /**
     * Provides a means for the {@code Dockable} to take some actions whenever
     * it is undocked from the {@link DockingManager}.
     * <p>
     * Such actions could include removing added toolbars, menus, or menu items
     * from the {@link Application Application's} global actions system.
     */
    public abstract void onUndock();

    /**
     * Provides a means by which the {@code Dockable} may perform any cleanup
     * that it requires, once it is no longer needed by the {@link Application}
     * or {@link DockingManager}.
     */
    public abstract void close();

    /**
     * Retrieves the {@link ApplicationContext} in which this {@code Dockable}
     * was constructed.
     *
     * @return the context
     */
    protected ApplicationContext getContext() {
        return context;
    }

    /**
     * Retrieves the {@link Application} in which this {@code Dockable} is being
     * used.
     *
     * @return the application
     */
    protected Application getApplication() {
        return getContext().getApplication();
    }

    /**
     * Lazily loads the {@link ResourceMap} for this {@code Dockable}. The
     * returned resource map is a chain of resource maps including the
     * {@code Dockable} super-class'.
     *
     * @return the resource map chain
     */
    protected ResourceMap getResourceMap() {
        return getContext().getResourceMap(getClass(), Dockable.class);
    }

    /**
     * A convenience method allowing subclasses to log messages to the logger
     * that is created for this class.
     *
     * @param level  the {@link Level} at which the message should be logged
     * @param format the {@link MessageFormat#format} string for the message
     * @param args   the arguments to the format string (may be empty, but not
     *               {@code null}
     */
    protected void log(Level level, String format, Object... args) {
        logger.log(level, format, args);
    }

    /**
     * A convenience method allowing subclasses to log error messages to the
     * logger that is created for this class.
     *
     * @param level  the {@link Level} at which the message should be logged
     * @param format the {@link MessageFormat#format} string for the message
     * @param cause  the {@link Exception} that was caught
     * @param args   the arguments to the format string (may be empty, but not
     *               {@code null}
     */
    protected void log(Level level, String format, Throwable cause,
            Object... args) {
        logger.log(level, format, cause, args);
    }

    private final ApplicationContext context;
    private final Logger logger;

}
