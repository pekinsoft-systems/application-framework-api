/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-management-api
 *  Class      :   ButtonTabComponent.java
 *  Author     :   Sean Carrick
 *  Created    :   Jun 23, 2024
 *  Modified   :   Jun 23, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jun 23, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import com.pekinsoft.api.DockingManager;
import com.pekinsoft.utils.ColorUtils;
import java.awt.*;
import java.awt.event.*;
import java.util.Objects;
import javax.swing.*;
import javax.swing.plaf.basic.BasicButtonUI;

public class ButtonTabComponent extends JPanel {

    private final JTabbedPane pane;
    private final Dockable dock;
    private final JPanel iconifyRoot;  // Optional root for iconification

    public ButtonTabComponent(final JTabbedPane pane, final Dockable dock) {
        this(pane, dock, null);
    }

    public ButtonTabComponent(final JTabbedPane pane, final Dockable dock,
            final JPanel iconifyRoot) {
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        this.pane = Objects.requireNonNull(pane, "JTabbedPane cannot be null.");
        this.dock = Objects.requireNonNull(dock, "Dockable cannot be null.");
        this.iconifyRoot = iconifyRoot;

        initComponents();
    }

    private void initComponents() {
        setOpaque(false);
        JLabel label = createTabLabel();
        add(label);
        label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 5));

        JButton closeButton = new TabButton();
        add(closeButton);

        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
    }

    private JLabel createTabLabel() {
        return new JLabel() {
            @Override
            public Icon getIcon() {
                int i = pane.indexOfTabComponent(ButtonTabComponent.this);
                return i != -1 ? pane.getIconAt(i) : dock.getIcon();
            }

            @Override
            public String getText() {
                int i = pane.indexOfTabComponent(ButtonTabComponent.this);
                return i != -1 ? pane.getTitleAt(i) : dock.getTitle();
            }
        };
    }

    public Dockable getDockable() {
        return dock;
    }

    public JTabbedPane getTabbedPane() {
        return pane;
    }

    private class TabButton extends JButton implements ActionListener {

        public TabButton() {
            configureButton();
        }

        private void configureButton() {
            int size = 17;
            setPreferredSize(new Dimension(size, size));
            setToolTipText("Close or minimize this Dock");
            setUI(new BasicButtonUI());
            setContentAreaFilled(false);
            setFocusable(false);
            setBorder(BorderFactory.createEtchedBorder());
            setBorderPainted(false);
            setRolloverEnabled(true);
            addActionListener(this);
            addMouseListener(buttonMouseListener);
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            DockingManager mgr = DockingManager.getDefault();
            int i = pane.indexOfTabComponent(ButtonTabComponent.this);

            if (iconifyRoot != null && iconifyRoot.isVisible()) {
                iconifyDock();
            } else if (i != -1) {
                mgr.undock(dock.getDockId());
                pane.remove(ButtonTabComponent.this);
            }
        }

        private void iconifyDock() {
            // Create a minimized icon with text and add it to the root panel
            EnhancedIcon minimizedIcon = new EnhancedIcon(dock.getIcon(), dock
                    .getTitle(),
                    EnhancedIcon.Layout.HORIZONTAL, EnhancedIcon.Rotate.NONE, 5);
            minimizedIcon.setFont(getFont().deriveFont(Font.PLAIN, 12));
            minimizedIcon.setColor(Color.GRAY);

            JButton iconifiedButton = new JButton(minimizedIcon);
            iconifiedButton.addActionListener(e -> restoreDock());
            iconifyRoot.add(iconifiedButton);
            iconifyRoot.revalidate();
            iconifyRoot.repaint();

            pane.remove(ButtonTabComponent.this);  // Remove from tab pane but keep dockable data
        }

        private void restoreDock() {
            pane.addTab(dock.getTitle(), dock.getIcon(), dock);
            iconifyRoot.remove(this);
            iconifyRoot.revalidate();
            iconifyRoot.repaint();
        }

        @Override
        public void updateUI() {
            // Prevent UI updates for custom UI
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g.create();

            if (getModel().isPressed()) {
                g2.translate(1, 1);
            }

            g2.setStroke(new BasicStroke(2));
            g2.setColor(
                    getModel().isRollover() ? ColorUtils.CRIMSON : Color.BLACK);

            int delta = 6;
            g2.drawLine(delta, delta, getWidth() - delta - 1,
                    getHeight() - delta - 1);
            g2.drawLine(getWidth() - delta - 1, delta, delta,
                    getHeight() - delta - 1);
            g2.dispose();
        }
    }

    private static final MouseListener buttonMouseListener = new MouseAdapter() {
        @Override
        public void mouseEntered(MouseEvent e) {
            if (e.getComponent() instanceof AbstractButton button) {
                button.setBorderPainted(true);
            }
        }

        @Override
        public void mouseExited(MouseEvent e) {
            if (e.getComponent() instanceof AbstractButton button) {
                button.setBorderPainted(false);
            }
        }
    };
}
