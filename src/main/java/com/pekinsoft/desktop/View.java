/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   View.java
 *  Author     :   Sean Carrick
 *  Created    :   Jul 14, 2024
 *  Modified   :   Jul 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jul 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import com.pekinsoft.desktop.View;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Logger;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.*;
import javax.swing.*;

/**
 * A View encapsulates a top-level Application GUI component, like a JFrame or
 * an Applet, and its main GUI elements: a menu bar, tool bar, component, and a
 * status bar. All of the elements are optional (although a View without a main
 * component would be unusual). Views have a `JRootPane`, which is the
 * root component for all of the Swing Window types as well as JApplet. Setting
 * a View property, like `menuBar` or `toolBar`, just adds a
 * component to the rootPane in a way that's defined by the View subclass. By
 * default the View elements are arranged in a conventional way:
 * <p>
 * - `menuBar` - becomes the rootPane's JMenuBar
 * - `toolBar` - added to {@code BorderLayout.NORTH} of the rootPane's
 * contentPane
 * - {@code Component} - added to `BorderLayout.CENTER` of the
 * rootPane's contentPane
 * - `StatusBar` - added to {@code BorderLayout.SOUTH} of the
 * rootPane's contentPane
 * <p>
 * <p>
 * To show or hide a View you call the corresponding Application methods. Here's
 * a simple example: {@snippet lang="java":
 * class MyApplication extends SingleFrameApplication {
 *     @Override protected void startup() {
 *         View view = getMainView();
 *         view.setComponent(createMainComponent());
 *         view.setMenuBar(createMenuBar());
 *         show(view);
 * }
 * }
 * }
 * <p>
 * The advantage of Views over just configuring a JFrame or JApplet directly, is
 * that a View is more easily moved to an alternative top level container, like
 * a docking Application.
 *
 * @see JRootPane
 *
 *
 */
public class View extends AbstractBean implements PropertyChangeListener {

    private final Logger logger;
    private final ApplicationContext context;
    private ResourceMap resourceMap = null;
    private JRootPane rootPane = null;
    private JComponent component = null;
    private JMenuBar menuBar = null;
    private List<JToolBar> toolBars = new ArrayList<>();
    protected JComponent toolBarsPanel = new JPanel(new FlowLayout(
            FlowLayout.LEADING));
    private JComponent statusBar = null;

    /**
     * Construct an empty View object for the specified Application.
     *
     * @param context the ApplicationContext responsible for showing/hiding this
     *                View
     *
     */
    public View(ApplicationContext context) {
        Objects
                .requireNonNull(context,
                        "The ApplicationContext cannot be null.");
        this.context = context;
        logger = context.getLogger(View.class.getName());
        getContext().addPropertyChangeListener("toolBars", this);
    }

    /**
     * The {@code Application} that's responsible for showing/hiding this View.
     *
     * @return the Application that owns this View
     *
     * @see #getContext
     */
    public final Application getApplication() {
        return context.getApplication();
    }

    /**
     * The {@code ApplicationContext} for the {@code Application} that's
     * responsible for
     * showing/hiding this View. This method
     * is just shorthand for `getApplication().getContext()`.
     *
     * @return the Application that owns this View
     *
     * @see #getApplication
     */
    public final ApplicationContext getContext() {
        return context;
    }

    /**
     * The {@code ResourceMap} for this View. This method is just shorthand for
     * `getContext().getResourceMap(getClass(), View.class)`.
     *
     * @return The {@code ResourceMap} for this View
     *
     * @see #getContext
     */
    public ResourceMap getResourceMap() {
        if (resourceMap == null) {
            resourceMap = getContext().getResourceMap(getClass(), View.class);
        }
        return resourceMap;
    }

    /**
     * The `JRootPane` for this View. All of the components for this View
     * must be added to its rootPane. Most applications will do so by setting
     * the View's {@code Component}, `menuBar`, `toolBar`, and
     * `StatusBar` properties.
     *
     * @return The {@code rootPane} for this View
     *
     * @see #setComponent
     * @see #setMenuBar
     * @see #setToolBar
     * @see #setStatusBar
     */
    public JRootPane getRootPane() {
        if (rootPane == null) {
            rootPane = new JRootPane();
            rootPane.setOpaque(true);
        }
        return rootPane;
    }

    private void replaceContentPaneChild(JComponent oldChild,
            JComponent newChild, String constraint) {
        Container contentPane = getRootPane().getContentPane();
        if (oldChild != null) {
            contentPane.remove(oldChild);
        }
        if (newChild != null) {
            contentPane.add(newChild, constraint);

            SwingUtilities.updateComponentTreeUI(contentPane);
            SwingUtilities.updateComponentTreeUI(newChild);

            for (Component c : newChild.getComponents()) {
                SwingUtilities.updateComponentTreeUI(c);
            }

            contentPane.invalidate();
            newChild.invalidate();
        }
    }

    /**
     * The main {JComponent} for this View.
     *
     * @return The {@code Component} for this View
     *
     * @see #setComponent
     */
    public JComponent getComponent() {
        return component;
    }

    /**
     * Set the single main Component for this View. It's added to the
     * `BorderLayout.CENTER` of the rootPane's contentPane. If the
     * component property was already set, the old component is removed first.
     * <p>
     * This is a bound property. The default value is {@code null}.
     *
     * @param component The {@code Component} for this View
     *
     * @see #getComponent
     */
    public void setComponent(JComponent component) {
        JComponent oldValue = this.component;
        this.component = component;
        replaceContentPaneChild(oldValue, this.component, BorderLayout.CENTER);
        firePropertyChange("component", oldValue, this.component);
    }

    /**
     * The main `JMenuBar` for this View.
     *
     * @return The `menuBar` for this View
     *
     * @see #setMenuBar
     */
    public JMenuBar getMenuBar() {
        return menuBar;
    }

    public void setMenuBar(JMenuBar menuBar) {
        JMenuBar oldValue = getMenuBar();
        this.menuBar = menuBar;
        getRootPane().setJMenuBar(menuBar);
        firePropertyChange("menuBar", oldValue, menuBar);
    }

    public List<JToolBar> getToolBars() {
        return toolBars;
    }

    public void addToolBar(JToolBar toolBar) {
//        if (getToolBars() == null) {
//            toolBars = new ArrayList<>();
//            toolBarsPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
//        }
//        getToolBars().add(toolBar);
//        toolBarsPanel.add(toolBar);
//        if (!Arrays.asList(getRootPane().getContentPane().getComponents()).contains(toolBarsPanel)) {
//            getRootPane().getContentPane().add(toolBarsPanel, BorderLayout.NORTH);
//        }
        List<JToolBar> oldValue = getToolBars();
        if (toolBars == null) {
            toolBars = new ArrayList<>();
        }
        toolBars.add(toolBar);
        updateToolBarsPanel(oldValue);
    }

    public void removeToolBar(JToolBar toolBar) {
//        if (toolBars != null && !toolBars.isEmpty() && toolBars.contains(toolBar)) {
//            toolBars.remove(toolBar);
//            toolBarsPanel.remove(toolBar);
//            toolBarsPanel.validate();
//            toolBarsPanel.repaint();
//            getRootPane().validate();
//        }
        List<JToolBar> oldValue = getToolBars();
        if (toolBars != null && !toolBars.isEmpty() && toolBars
                .contains(toolBar)) {
            toolBars.remove(toolBar);
            updateToolBarsPanel(oldValue);
        }
    }

    public void setToolBars(List<JToolBar> toolBars) {
        if (toolBars == null) {
            throw new IllegalArgumentException("null toolbars");
        }
        List<JToolBar> oldValue = getToolBars();
        this.toolBars = new ArrayList<>(toolBars);
        updateToolBarsPanel(oldValue);
    }

    protected void updateToolBarsPanel(List<JToolBar> oldToolBars) {
        List<JToolBar> oldValue = oldToolBars;
        JComponent oldToolBarsPanel = this.toolBarsPanel;
        JComponent newToolBarsPanel = new JPanel(new FlowLayout(
                FlowLayout.LEADING));

        for (JToolBar tb : toolBars) {
            newToolBarsPanel.add(tb);
        }
//        if (this.toolBars.size() == 1) {
//            newToolBarsPanel = toolBars.get(0);
//        } else if (this.toolBars.size() > 1) {
//            newToolBarsPanel = new JPanel(new FlowLayout());
//            for (JComponent toolBar : this.toolBars) {
//                newToolBarsPanel.add(toolBar);
//            }
//        }
        replaceContentPaneChild(oldToolBarsPanel, newToolBarsPanel,
                BorderLayout.NORTH);
        firePropertyChange("toolBars", oldValue, this.toolBars);
    }

    /**
     * Returns the first (primary) {@link javax.swing.JToolBar JToolBar}
     * attached to this {@code View}. If there are multiple toolbars associated
     * with this {@code View}
     *
     * @return
     */
    public JToolBar getToolBar() {
        List<JToolBar> toolBars = getToolBars();
        return (toolBars.isEmpty()) ? null : toolBars.get(0);
    }

    public void setToolBar(JToolBar toolBar) {
        JToolBar oldValue = getToolBar();
        List<JToolBar> toolBars = Collections.emptyList();
        if (toolBar != null) {
            toolBars = Collections.singletonList(toolBar);
        }
        setToolBars(toolBars);
        firePropertyChange("toolBar", oldValue, toolBar);
    }

    public void setToolBar(JToolBar toolBar, boolean update) {
        if (update) {
            setToolBar(toolBar);
        } else {
            if (toolBars == null) {
                toolBars = new ArrayList<>();
            }
            toolBars.add(toolBar);
        }
    }

    public JComponent getStatusBar() {
        return statusBar;
    }

    public void setStatusBar(JComponent statusBar) {
        JComponent oldValue = this.statusBar;
        this.statusBar = statusBar;
        replaceContentPaneChild(oldValue, this.statusBar, BorderLayout.SOUTH);
        firePropertyChange("statusBar", oldValue, this.statusBar);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch (evt.getPropertyName()) {
            case "toolBars":
                toolBarsPanel.validate();
                break;
        }
    }

}
