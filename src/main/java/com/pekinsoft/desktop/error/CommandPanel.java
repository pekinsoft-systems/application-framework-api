/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ErrorPaneInProcess
 *  Class      :   CommandPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 14, 2024
 *  Modified   :   Apr 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop.error;


import com.pekinsoft.desktop.error.ErrorPane;
import java.awt.Component;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 *
 * @author Sean Carrick
 */
class CommandPanel extends javax.swing.JPanel
        implements PropertyChangeListener {

    private final ActionMap actionMap;
    boolean isFatal = false;
    
    /**
     * Creates new form CommandPanel
     */
    public CommandPanel(ActionMap actionMap) {
        if(actionMap == null) {
            throw new NullPointerException("The actionMap cannot be null.");
        }
        
        this.actionMap = actionMap;
        initComponents();
    }
    
    public void detailsVisible(boolean detailsVisible) {
        showDetailsButton.setVisible(!detailsVisible);
        hideDetailsButton.setVisible(detailsVisible);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() != null) {
            switch (evt.getPropertyName()) {
                case ErrorPane.PROP_DETAILS_VISIBLE -> {
                    boolean detailsVisible = (boolean) evt.getNewValue();
                    showDetailsButton.setVisible(!detailsVisible);
                    hideDetailsButton.setVisible(detailsVisible);
                }
                case ErrorPane.PROP_IS_FATAL_ERROR -> {
                    isFatal = (boolean) evt.getNewValue();
                    configureCloseButtons();
                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        closeButton = new JButton();
        reportButton = new JButton();
        hideDetailsButton = new JButton();
        fatalCloseButton = new JButton();
        showDetailsButton = new JButton();

        closeButton.setText("Close/Exit");
        closeButton.setAction(actionMap.get(ErrorPane.NORMAL_CLOSE_ACTION_KEY));
        closeButton.setName("closeButton"); // NOI18N

        reportButton.setText("Submit Report");
        reportButton.setAction(actionMap.get(ErrorPane.REPORT_ACTION_KEY));
        reportButton.setName("reportButton"); // NOI18N

        hideDetailsButton.setText("← Hide Details");
        hideDetailsButton.setAction(actionMap.get(ErrorPane.HIDE_DETAILS_ACTION_KEY));
        hideDetailsButton.setName("hideDetailsButton"); // NOI18N

        fatalCloseButton.setText("Exit Application");
        fatalCloseButton.setAction(actionMap.get(ErrorPane.FATAL_CLOSE_ACTION_KEY));
        fatalCloseButton.setName("fatalCloseButton"); // NOI18N

        showDetailsButton.setText("Show Details →");
        showDetailsButton.setAction(actionMap.get(ErrorPane.SHOW_DETAILS_ACTION_KEY));
        showDetailsButton.setName("showDetailsButton"); // NOI18N

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(Alignment.LEADING)
            .addGroup(Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(reportButton)
                .addPreferredGap(ComponentPlacement.RELATED, 97, Short.MAX_VALUE)
                .addComponent(hideDetailsButton, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(showDetailsButton)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(fatalCloseButton)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(closeButton)
                .addContainerGap())
        );

        layout.linkSize(SwingConstants.HORIZONTAL, new Component[] {closeButton, fatalCloseButton, hideDetailsButton, reportButton, showDetailsButton});

        layout.setVerticalGroup(layout.createParallelGroup(Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(Alignment.BASELINE)
                    .addComponent(closeButton)
                    .addComponent(reportButton)
                    .addComponent(hideDetailsButton)
                    .addComponent(fatalCloseButton)
                    .addComponent(showDetailsButton))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(SwingConstants.VERTICAL, new Component[] {closeButton, fatalCloseButton, hideDetailsButton, reportButton});

    }// </editor-fold>//GEN-END:initComponents

    private void configureCloseButtons() {
        closeButton.setVisible(!isFatal);
        fatalCloseButton.setVisible(isFatal);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton closeButton;
    private JButton fatalCloseButton;
    private JButton hideDetailsButton;
    private JButton reportButton;
    private JButton showDetailsButton;
    // End of variables declaration//GEN-END:variables
}
