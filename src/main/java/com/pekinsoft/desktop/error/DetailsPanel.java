/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   ErrorPaneInProcess
 *  Class      :   DetailsPanel.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 14, 2024
 *  Modified   :   Apr 14, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 14, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop.error;

import com.pekinsoft.desktop.error.ErrorPane;
import com.pekinsoft.framework.ActionManager;
import com.pekinsoft.framework.AppAction;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick
 */
class DetailsPanel extends javax.swing.JPanel 
        implements PropertyChangeListener {
    
    /**
     * Creates new form DetailsPanel
     */
    public DetailsPanel() {
        initComponents();
        
        copyToClipboardButton.setAction(ActionManager.getInstance()
                .getActionMap(getClass(), this).get("copyToClipboard"));
    }
    
    @AppAction
    public void copyToClipboard(ActionEvent evt) {
        details.copy();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() != null) switch (evt.getPropertyName()) {
            case ErrorPane.PROP_ERROR_DETAILS -> {
                String text = (String) evt.getNewValue();
                if (BasicHTML.isHTMLString(text)) {
                    details.setContentType("text/html");
                } else {
                    details.setContentType("text/plain");
                }
                details.setText(text);
                details.setCaretPosition(0);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        spacerPanel = new JPanel();
        hiddenLabel = new JLabel();
        innerDetailsPanel = new JPanel();
        detailsScrollPane = new JScrollPane();
        details = new JEditorPane();
        copyToClipboardButton = new JButton();

        setMaximumSize(new Dimension(700, 400));
        setMinimumSize(new Dimension(250, 100));
        setOpaque(false);
        setPreferredSize(new Dimension(700, 337));
        setLayout(new BorderLayout());

        spacerPanel.setMaximumSize(new Dimension(108, 32767));
        spacerPanel.setMinimumSize(new Dimension(108, 100));
        spacerPanel.setName("spacerPanel"); // NOI18N

        hiddenLabel.setMaximumSize(new Dimension(96, 96));
        hiddenLabel.setMinimumSize(new Dimension(96, 96));
        hiddenLabel.setName("hiddenLabel"); // NOI18N
        hiddenLabel.setPreferredSize(new Dimension(96, 96));

        GroupLayout spacerPanelLayout = new GroupLayout(spacerPanel);
        spacerPanel.setLayout(spacerPanelLayout);
        spacerPanelLayout.setHorizontalGroup(spacerPanelLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(spacerPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(hiddenLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        spacerPanelLayout.setVerticalGroup(spacerPanelLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(spacerPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(hiddenLabel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(235, Short.MAX_VALUE))
        );

        add(spacerPanel, BorderLayout.LINE_START);

        innerDetailsPanel.setName("innerDetailsPanel"); // NOI18N

        detailsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        detailsScrollPane.setMaximumSize(new Dimension(32767, 500));
        detailsScrollPane.setMinimumSize(new Dimension(16, 300));
        detailsScrollPane.setName("detailsScrollPane"); // NOI18N
        detailsScrollPane.setPreferredSize(new Dimension(115, 300));

        details.setEditable(false);
        details.setName("details"); // NOI18N
        details.setTransferHandler(createTransferHandler(details));
        details.setOpaque(false);
        detailsScrollPane.setViewportView(details);

        copyToClipboardButton.setText("Copy to Clipboard");
        copyToClipboardButton.setMaximumSize(new Dimension(136, 25));
        copyToClipboardButton.setMinimumSize(new Dimension(136, 25));
        copyToClipboardButton.setName("copyToClipboardButton"); // NOI18N
        copyToClipboardButton.setPreferredSize(new Dimension(136, 25));

        GroupLayout innerDetailsPanelLayout = new GroupLayout(innerDetailsPanel);
        innerDetailsPanel.setLayout(innerDetailsPanelLayout);
        innerDetailsPanelLayout.setHorizontalGroup(innerDetailsPanelLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(Alignment.TRAILING, innerDetailsPanelLayout.createSequentialGroup()
                .addContainerGap(502, Short.MAX_VALUE)
                .addComponent(copyToClipboardButton, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(detailsScrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        innerDetailsPanelLayout.setVerticalGroup(innerDetailsPanelLayout.createParallelGroup(Alignment.LEADING)
            .addGroup(innerDetailsPanelLayout.createSequentialGroup()
                .addComponent(detailsScrollPane, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(ComponentPlacement.RELATED)
                .addComponent(copyToClipboardButton, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        add(innerDetailsPanel, BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JButton copyToClipboardButton;
    private JEditorPane details;
    private JScrollPane detailsScrollPane;
    private JLabel hiddenLabel;
    private JPanel innerDetailsPanel;
    private JPanel spacerPanel;
    // End of variables declaration//GEN-END:variables

    private TransferHandler createTransferHandler(JTextComponent detailComponent) {
        return new DetailsTransferHandler(detailComponent);
    }
    
    private static final class DetailsTransferHandler extends TransferHandler {
        private JTextComponent details;
        
        private DetailsTransferHandler(JTextComponent detailsComponent) {
            if (detailsComponent == null) {
                throw new NullPointerException("Detail component cannot be "
                        + "null.");
            }
            this.details = detailsComponent;
        }

        @Override
        protected Transferable createTransferable(JComponent c) {
            String text = details.getSelectedText();
            if (text == null || text.isBlank() || text.isEmpty()) {
                details.selectAll();
                text = details.getSelectedText();
                details.select(-1, -1);
            }
            
            return new StringSelection(text);
        }

        @Override
        public int getSourceActions(JComponent c) {
            return TransferHandler.COPY;
        }
    }

}
