/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   PlatformAPI
 *  Class      :   EqualSizeJButton.java
 *  Author     :   Sean Carrick
 *  Created    :   Apr 12, 2024
 *  Modified   :   Apr 12, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Apr 12, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop.error;

import java.awt.Dimension;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.plaf.ComponentUI;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
class EqualSizeJButton extends JButton {
    
    public EqualSizeJButton() {
        this("Button");
    }

    public EqualSizeJButton(String text) {
        super(text);
    }

    public EqualSizeJButton(Action a) {
        super(a);
    }

    private EqualSizeJButton[] group;

    public void setGroup(EqualSizeJButton[] group) {
        this.group = group;
    }

    private Dimension getRealPreferredSize() {
        return super.getPreferredSize();
    }

    /**
     * If the {@code preferredSize} has been set to a non-{@code null} value,
     * this just returns it. If the UI delegate's {@code getPreferredSize}
     * method returns a non-{@code null} value, then return that; otherwise
     * defer to the component's layout manager.
     *
     * @return the value of the {@code preferredSize} property
     *
     * @see #setPreferredSize(java.awt.Dimension)
     * @see ComponentUI
     */
    @Override
    public Dimension getPreferredSize() {
        int width = 0;
        int height = 0;
        for (EqualSizeJButton btn : group) {
            Dimension size = btn.getRealPreferredSize();
            width = Math.max(size.width, width);
            height = Math.max(size.height, height);
        }

        return new Dimension(width, height);
    }

}
