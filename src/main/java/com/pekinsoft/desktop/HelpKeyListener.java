/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   CustomComponents
 *  Class      :   HelpKeyListener.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 13, 2024
 *  Modified   :   Nov 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.desktop;

import java.awt.BorderLayout;
import java.awt.Point;
import java.awt.event.*;
import javax.swing.*;

public class HelpKeyListener extends KeyAdapter {

    private final JComponent parentComponent;
    private final String helpMessage;
    private JWindow helpWindow;

    public HelpKeyListener(JComponent parentComponent, String helpMessage) {
        this.parentComponent = parentComponent;
        this.helpMessage = helpMessage;
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.isShiftDown() && e.getKeyCode() == KeyEvent.VK_F1) {
            showHelpWindow();
        }
    }

    private void showHelpWindow() {
        if (helpWindow != null) {
            return;
        }

        helpWindow = new JWindow();
        helpWindow.getContentPane().setBackground(UIManager.getColor(
                "ToolTip.background"));
        JLabel helpLabel = new JLabel(helpMessage);
        helpLabel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        helpWindow.add(helpLabel);

        helpWindow.pack();

        Point location = parentComponent.getLocationOnScreen();
        location.y += parentComponent.getHeight();
        helpWindow.setLocation(location);

        helpWindow.setVisible(true);

        // Add a mouse listener to close the window when clicked
        helpWindow.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                closeHelpWindow();
            }
        });

        // Add a focus listener to the parent component to close the window when focus is lost
        parentComponent.addFocusListener(new java.awt.event.FocusAdapter() {
            @Override
            public void focusLost(java.awt.event.FocusEvent e) {
                closeHelpWindow();
            }
        });
    }

    private void closeHelpWindow() {
        if (helpWindow != null) {
            helpWindow.setVisible(false);
            helpWindow.dispose();
            helpWindow = null;
        }
    }

    // Example usage
    public static void main(String[] args) {
        JFrame frame = new JFrame("Help KeyListener Example");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 300);

        JTextField textField = new JTextField();
        textField.addKeyListener(new HelpKeyListener(textField,
                "Enter your data here. This field accepts numeric values only."));

        frame.setLayout(new BorderLayout());
        frame.add(textField, BorderLayout.NORTH);

        frame.setVisible(true);
    }
}
