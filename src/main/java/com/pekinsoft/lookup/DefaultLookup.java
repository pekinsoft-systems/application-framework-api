/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   DefaultLookup.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 25, 2024
 *  Modified   :   Oct 25, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 25, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.lookup;

import com.pekinsoft.framework.Application;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class DefaultLookup extends Lookup {

    private static DefaultLookup instance = null;
    private static Logger logger = Application.getInstance()
            .getContext().getLogger(DefaultLookup.class);

    public static Lookup getInstance() {
        if (instance == null) {
            instance = new DefaultLookup();
        }

        return instance;
    }

    public DefaultLookup() {

    }

    @Override
    public <T> T lookup(Class<T> type, boolean refreshCache) {
        List<T> providers = (List<T>) lookupAll(type, refreshCache);
        return providers.isEmpty() ? null : providers.get(0);
    }

    @Override
    public <T> Collection<T> lookupAll(Class<T> type, boolean refreshCache) {
        if (refreshCache || !cache.containsKey(type)) {
            loadProviders(type);
        }
        return (Collection<T>) cache.getOrDefault(type, Collections.emptyList());
    }

    private <T> void loadProviders(Class<T> type) {
        List<Object> providers = new ArrayList<>();
        List<Class<?>> services = registry.get(type);

        if (services != null) {
            for (Class<?> service : services) {
                try {
                    T instantiated = null;
                    try {
                        logger.info("Checking to see if service \"{0}\" has a "
                                + "getInstance method.", service);
                        Method getInstance = service.getDeclaredMethod(
                                "getInstance");
                        if (!getInstance.canAccess(null)) {
                            getInstance.setAccessible(true);
                        }
                        instantiated = type.cast(getInstance.invoke(null));
                        logger.info("Service \"{0}\" retrieved via its "
                                + "getInstance method", service);
                    } catch (NoSuchMethodException tryingConstructor) {
                        logger.info("Service \"{0}\" does not have a "
                                + "getInstance method: Attempting constructor.",
                                service);
                        instantiated = type.cast(service
                                .getDeclaredConstructor()
                                .newInstance());
                        logger.info("Service \"{0}\" initialized via its "
                                + "constructor.", service);
                    }
                    if (instantiated != null) {
                        providers.add(instantiated);
                    }
                } catch (IllegalAccessException
                        | IllegalArgumentException
                        | InstantiationException
                        | NoSuchMethodException
                        | SecurityException
                        | InvocationTargetException e) {
                    logger.error("Could not initialize provider \"{0}\": [{1}] "
                            + "{2}", e, service.getName(),
                            e.getClass().getSimpleName(), e.getMessage());
                }
            }
            cache.put(type, providers);
            logger.log(Level.DEBUG, "Providers registered in the cache: {0}",
                    cache);
        }
    }

}
