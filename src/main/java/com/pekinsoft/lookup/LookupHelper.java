/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LookupAPI
 *  Class      :   LookupHelper.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 17, 2023
 *  Modified   :   Jan 17, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 17, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.lookup;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * A static class that provides helper methods for the {@code Lookup} to use in
 * order to simplify the {@code Lookup}.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class LookupHelper {
    
    private LookupHelper () { /* No instantiation allowed */ }
    
    /**
     * Determines whether the specified class is one built using the <em>Singleton
     * Design Pattern</em> and has a {@code getInstance()} method.
     * 
     * @param cls the class to check
     * @return {@code true} if a {@code getInstance()} method is present; {@code 
     * false} if not
     */
    static boolean isSingleton(Class<?> cls) {
        try {
            Method getInstance = cls.getMethod("getInstance");
            if (getInstance != null) {
                return true;
            }
        } catch (NoSuchMethodException 
                | SecurityException e) {
            return false;
        }
        
        return false;
    }
    
    /**
     * Determines whether the specified class is one built using the <em>Stingleton
     * Design Pattern</em>, but using the updated {@code provider()} method that
     * is required by the {@link java.util.ServiceLoader ServiceLoader} when in
     * a modular application.
     * 
     * @param cls the class to check
     * @return {@code true} if a {@code provider()} method is present; {@code 
     * false} if not
     */
    static boolean isProvider(Class<?> cls) {
        try {
            Method provider = cls.getMethod("provider");
            if (provider != null) {
                return true;
            }
        } catch (NoSuchMethodException
                | SecurityException e) {
            return false;
        }
        
        return false;
    }
    
    /**
     * Retrieves an instance of the specified class using its {@code getInstance()}
     * method. This method assumes that {@link #isSingleton(java.lang.Class) 
     * isSingletion} has been called prior to this one, as no checks are 
     * performed.
     * 
     * @param cls the class on which to invoke the {@code getInstance()} method
     * @return the instance object of the provided class
     */
    static Object getInstance(Class<?> cls) {
        try {
            Method getInstance = cls.getMethod("getInstance");
            if (getInstance != null) {
                if (!getInstance.canAccess(null)) {
                    getInstance.setAccessible(true);
                }
                
                return getInstance.invoke(cls);
            }
        } catch (IllegalAccessException 
                | IllegalArgumentException 
                | NoSuchMethodException 
                | SecurityException 
                | InvocationTargetException e) {
            return null;
        }
        return null;
    }
    
    /**
     * Retrieves an instance of the specified class using its {@code provider()}
     * method. This method assumes that {@link #isProvider(java.lang.Class) 
     * isProvider} has been called prior to this one, as no checks are performed.
     * 
     * @param cls the class on which to invoke the {@code provider()} method
     * @return the instance object of the provided class
     */
    static Object getProvider(Class<?> cls) {
        try {
            Method provider = cls.getMethod("provider");
            if (provider != null) {
                if (!provider.canAccess(null)) {
                    provider.setAccessible(true);
                }
                
                return provider.invoke(cls);
            }
        } catch (IllegalAccessException 
                | IllegalArgumentException 
                | NoSuchMethodException 
                | SecurityException 
                | InvocationTargetException e) {
            return null;
        }
        return null;
    }

}
