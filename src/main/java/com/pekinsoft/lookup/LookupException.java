/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LookupAPI
 *  Class      :   LookupException.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 17, 2023
 *  Modified   :   Jan 17, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 17, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.lookup;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class LookupException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    /**
     * Creates a new instance of {@code LookupException} without detail message.
     */
    public LookupException() {
    }


    /**
     * Constructs an instance of {@code LookupException} with the specified detail message.
     * @param msg the detail message.
     */
    public LookupException(String msg) {
        super(msg);
    }
}
