/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Lookup2.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 25, 2024
 *  Modified   :   Oct 25, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 25, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.lookup;

import com.pekinsoft.framework.Application;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public abstract class Lookup {

    public static Lookup getDefault() {
        return DefaultLookup.getInstance();
    }

    protected static final Map<Class<?>, List<Class<?>>> registry = new ConcurrentHashMap<>();
    protected static final Map<Class<?>, List<Object>> cache = new ConcurrentHashMap<>();

    private static final Logger logger = Application.getInstance()
            .getContext().getLogger(Lookup.class);

    static {
        loadServiceFiles();
    }

    protected static void loadServiceFiles() {
        try {
            // Use ClassLoader to locate all JAR files in the classpath
            Enumeration<URL> resources = ClassLoader.getSystemResources(
                    "META-INF/services");

            while (resources.hasMoreElements()) {
                URL url = resources.nextElement();

                if ("file".equals(url.getProtocol())) {
                    // Directory on the file system (not in a JAR)
                    Path serviceDir = Paths.get(url.toURI());

                    Files.walkFileTree(serviceDir, new SimpleFileVisitor<>() {
                        @Override
                        public FileVisitResult visitFile(Path file,
                                BasicFileAttributes attrs) throws IOException {
                            processServiceFile(file);
                            return FileVisitResult.CONTINUE;
                        }
                    });
                } else {
                    try (FileSystem fs = FileSystems.newFileSystem(url.toURI(),
                            Collections.emptyMap())) {
                        Path serviceDir = fs.getPath("META-INF", "services");

                        Files.walkFileTree(serviceDir,
                                new SimpleFileVisitor<>() {
                            @Override
                            public FileVisitResult visitFile(Path file,
                                    BasicFileAttributes attrs)
                                    throws IOException {
                                processServiceFile(file);

                                return FileVisitResult.CONTINUE;
                            }
                        });
                    } catch (Exception e) {
                        logger.log(Level.WARNING,
                                "Failed to create FileSystem for: "
                                + "{0} - [{1}] {2}", new Object[]{url,
                                    e.getClass().getSimpleName(), e.getMessage()});
                    }
                }
            }
        } catch (IOException | URISyntaxException e) {
            logger.log(Level.WARNING, "Failed to load service files. [{0}] {1}",
                    new Object[]{e.getClass().getSimpleName(), e.getMessage()});
        }
    }

    public Lookup() {

    }

    public abstract <T extends Object> T lookup(Class<T> type,
            boolean refreshCache);

    public abstract <T extends Object> Collection<T> lookupAll(Class<T> type,
            boolean refreshCache);

    public <T extends Object> T lookup(Class<T> type) {
        return lookup(type, false);
    }

    public <T extends Object> Collection<T> lookupAll(Class<T> type) {
        return lookupAll(type, false);
    }

    private static synchronized void processServiceFile(Path file) {
        String serviceName = file.getFileName().toString();

        try {
            Class<?> serviceClass = Class.forName(serviceName);
            List<Class<?>> providers = registry.getOrDefault(serviceClass,
                    new ArrayList<>());

            Files.lines(file).forEach(line -> {
                try {
                    if (!line.startsWith("#")) {
                        providers.add(Class.forName(line.trim()));
                    }
                } catch (ClassNotFoundException e) {
                    logger.log(Level.WARNING, "Failed to create provider for "
                            + "{0}: [{1}] {2}", new Object[]{
                                file.getFileName(), e.getClass().getSimpleName(),
                                e.getMessage()});
                }
            });
            registry.put(serviceClass, providers);
        } catch (IOException | ClassNotFoundException e) {
            logger.log(Level.WARNING, "Failed to create service class: {0} - "
                    + "[{1}] {2}", new Object[]{serviceName,
                        e.getClass().getSimpleName(), e.getMessage()});
        }
    }

}
