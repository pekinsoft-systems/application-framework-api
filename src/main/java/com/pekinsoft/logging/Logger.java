/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Logger.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 12, 2024
 *  Modified   :   Nov 12, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 12, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.logging;

import com.pekinsoft.api.*;
import com.pekinsoft.framework.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.*;
import java.text.MessageFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * The {@code Logger} class is a special logger designed specifically for the
 * <em>Application Framework API</em>. This logger does not attempt to create a
 * log file until the {@link Application} has reached its
 * {@link Application#ready() ready} state, at which time the entire framework
 * has been initialized and configured. This allows the {@code Logger} to create
 * its log file in the
 * {@link LocalStorage#getLogDirectory() application-specific location}.
 * <p>
 * In order to accomplish the above, {@code Logger} implements the
 * {@link PropertyChangeListener } interface and is automatically registered at
 * construction with the {@code Application} class against its
 * &ldquo;ready&rdquo; property. When the ready property changes, the logger
 * creates its log file and flushes its internal message queue to that file, so
 * no log messages are lost.
 * <p>
 * Furthermore, the {@code Logger} implements the {@link SaveCookie} interface
 * and registers itself with the {@code Application} so that its log file may be
 * properly flushed and closed when the application exits. Since the
 * application's global Save action is tied to the registered
 * {@code SaveCookie}s, the loggers are registered in a special registry within
 * the application which is separate from the {@code SaveCookie} registry. This
 * way, invoking the global Save action will not inadvertently close the various
 * loggers that may have been created by the application classes.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Logger extends AbstractBean
        implements PropertyChangeListener, SaveCookie {

    /**
     * Format for non-error messages. Format is:
     * <ol start="0">
     * <li>{@code Level.name()}</li>
     * <li>{@code LocalDateTime.now()}, formatted</li>
     * <li>{@code message}, formatted by {@link MessageFormat#format}</li>
     * </ol>
     */
    private static final String MESSAGE_FORMAT = "[{0}]: {1}\n{2}";
    /**
     * Format for error messages. Format is:
     * <ol start="0">
     * <li>{@code Level.name()}</li>
     * <li>{@code LocalDateTime.now()}, formatted</li>
     * <li>{@code cause.getClass().getSimpleName()}</li>
     * <li>{@code message}, formatted by {@link MessageFormat#format}</li>
     * </ol>
     */
    private static final String ERROR_FORMAT = "[{0}]: {1} :: {2}\n{3}";

    /**
     * Constructs a new {@code Logger} instance using the specified name
     * parameter as the base name of the created log file. This {@code Logger}
     * will be used within the {@link ApplicationContext context} in which it
     * was constructed.
     *
     * @param context the {@code ApplicationContext} in which this
     *                {@code Logger} will be used
     * @param name    the base name for the log file, typically the name of the
     *                class for which the {@code Logger} is constructed
     */
    Logger(ApplicationContext context, String name) {
        this.context = context;
        this.name = name;
        messageQ = new ArrayList<>();
        context.getApplication().addPropertyChangeListener("ready", this);
        context.getApplication().addLogger(this);
    }

    /**
     * Retrieves the current {@link Level} setting for this {@code Logger}.
     * <p>
     * This property controls whether or not a log message is actually logged.
     *
     * @return the current {@code Level}
     */
    public Level getLevel() {
        return level;
    }

    /**
     * Sets the {@link Level} setting for this {@code Logger}.
     * <p>
     * This setting controls whether a message is actually logged.
     * <p>
     * This is a bound property.
     *
     * @param level the new {@code Level} setting
     */
    public void setLevel(Level level) {
        Level old = getLevel();
        this.level = level;
        firePropertyChange("level", old, getLevel());
    }

    /**
     * Determines whether the specified {@link Level} is loggable under this
     * {@code Logger}'s current {@link #getLevel() level setting}.
     *
     * @param level the {@code Level} to test for logability
     *
     * @return {@code true} if the level is loggable; {@code false} if not
     */
    public boolean isLoggable(Level level) {
        return getLevel() != Level.OFF
                && level.getValue() >= getLevel().getValue();
    }

    /**
     * Convenience method for tracing through the code base of an application.
     *
     * @param message the message to log
     */
    public synchronized void trace(String message) {
        log(Level.TRACE, message);
    }

    /**
     * Convenience method for tracing through the code base of an application.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param args   the arguments to the format string
     */
    public synchronized void trace(String format, Object... args) {
        log(Level.TRACE, format, args);
    }

    /**
     * Convenience method for debugging the code base of an application.
     *
     * @param message the message to log
     */
    public synchronized void debug(String message) {
        log(Level.DEBUG, message);
    }

    /**
     * Convenience method for debugging the code base of an application.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param args   the arguments to the format string
     */
    public synchronized void debug(String format, Object... args) {
        log(Level.DEBUG, format, args);
    }

    /**
     * Convenience method for checking the configuration within the code base of
     * an application.
     *
     * @param message the message to log
     */
    public synchronized void config(String message) {
        log(Level.CONFIG, message);
    }

    /**
     * Convenience method for checking the configuration within the code base of
     * an application.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param args   the arguments to the format string
     */
    public synchronized void config(String format, Object... args) {
        log(Level.CONFIG, format, args);
    }

    /**
     * Convenience method for providing information regarding the state of
     * an application.
     *
     * @param message the message to log
     */
    public synchronized void info(String message) {
        log(Level.INFO, message);
    }

    /**
     * Convenience method for providing information regarding the state of
     * an application.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param args   the arguments to the format string
     */
    public synchronized void info(String format, Object... args) {
        log(Level.INFO, format, args);
    }

    /**
     * Convenience method for providing a simple warning of something not quite
     * right within an application.
     *
     * @param message the message to log
     */
    public synchronized void warning(String message) {
        log(Level.WARNING, message);
    }

    /**
     * Convenience method for providing a simple warning of something not quite
     * right within an application.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param args   the arguments to the format string
     */
    public synchronized void warning(String format, Object... args) {
        log(Level.WARNING, format, args);
    }

    /**
     * Convenience method for providing a simple warning of something not quite
     * right within an application, along with an exception for more
     * information.
     *
     * @param message the message to log
     * @param cause   the exception that contains more information
     */
    public synchronized void warning(String message, Throwable cause) {
        log(Level.WARNING, message, cause);
    }

    /**
     * Convenience method for providing a simple warning of something not quite
     * right within an application, along with an exception for more
     * information.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param cause  the exception that contains more information
     * @param args   the arguments to the format string
     */
    public synchronized void warning(String format, Throwable cause,
            Object... args) {
        log(Level.WARNING, format, cause, args);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * wrong within an application.
     *
     * @param message the message to log
     */
    public synchronized void error(String message) {
        log(Level.ERROR, message);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * wrong within an application.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param args   the arguments to the format string
     */
    public synchronized void error(String format, Object... args) {
        log(Level.ERROR, format, args);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * wrong within an application, along with an exception for more
     * information.
     *
     * @param message the message to log
     * @param cause   the exception that contains more information
     */
    public synchronized void error(String message, Throwable cause) {
        log(Level.ERROR, message, cause);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * wrong within an application, along with an exception for more
     * information.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param cause  the exception that contains more information
     * @param args   the arguments to the format string
     */
    public synchronized void error(String format, Throwable cause,
            Object... args) {
        log(Level.ERROR, format, cause, args);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * terribly wrong within an application, along with an exception for more
     * information.
     *
     * @param message the message to log
     * @param cause   the exception that contains more information
     */
    public synchronized void critical(String message, Throwable cause) {
        log(Level.CRITICAL, message, cause);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * terribly wrong within an application, along with an exception for more
     * information.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param cause  the exception that contains more information
     * @param args   the arguments to the format string
     */
    public synchronized void critical(String format, Throwable cause,
            Object... args) {
        log(Level.CRITICAL, format, cause, args);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * catastrophically wrong within an application, along with an exception for
     * more information.
     *
     * @param message the message to log
     * @param cause   the exception that contains more information
     */
    public synchronized void fatal(String message, Throwable cause) {
        log(Level.FATAL, message, cause);
    }

    /**
     * Convenience method for providing a simple error of something gone
     * catastrophically wrong within an application, along with an exception for
     * more information.
     *
     * @param format the {@link MessageFormat#format(String) format string} for
     *               the message to log
     * @param cause  the exception that contains more information
     * @param args   the arguments to the format string
     */
    public synchronized void fatal(String format, Throwable cause,
            Object... args) {
        log(Level.FATAL, format, cause, args);
    }

    /**
     * A method by which a simple log message may be logged.
     *
     * @param level   the {@link Level} at which the message should be logged
     * @param message the simple message to log
     */
    public synchronized void log(Level level, String message) {
        logMessage(level, formatMessage(level, message, new Object[0]));
    }

    /**
     * A method by which a complex message may be logged. This method uses the
     * {@link MessageFormat#format(Object) MessageFormat} formatting to parse in
     * the specified <em>var-args</em> list of arguments to the message format
     * string.
     *
     * @param level  the {@link Level} at which the message should be logged
     * @param format the {@code MessageFormat} format string of the message
     * @param args   the arguments to the format string
     */
    public synchronized void log(Level level, String format, Object... args) {
        logMessage(level, formatMessage(level, format, args));
    }

    /**
     * A method by which a simple log message may be logged, along with a
     * trapped exception.
     *
     * @param level   the {@link Level} at which the message should be logged
     * @param message the simple message to log
     * @param cause   the exception that was trapped
     */
    public synchronized void log(Level level, String message, Throwable cause) {
        logMessage(level, formatErrorMessage(level, message, cause,
                new Object[0]));
    }

    /**
     * A method by which a complex message may be logged. This method uses the
     * {@link MessageFormat#format(Object) MessageFormat} formatting to parse in
     * the specified <em>var-args</em> list of arguments to the message format
     * string. This version also accepts an exception that has been caught to
     * provide more information.
     *
     * @param level  the {@link Level} at which the message should be logged
     * @param format the {@code MessageFormat} format string of the message
     * @param args   the arguments to the format string
     * @param cause  the exception that was trapped
     */
    public synchronized void log(Level level, String format, Throwable cause,
            Object... args) {
        logMessage(level, formatErrorMessage(level, format, cause, args));
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName() != null
                && "ready".equals(evt.getPropertyName())) {
            appReady = (boolean) evt.getNewValue();
            configureLogger();
        }
    }

    @Override
    public boolean save() {
        try {
            out.flush();
            out.close();
            return true;
        } catch (IOException e) {
            throw new UncheckedIOException("Unable to flush and close log file "
                    + "for " + name, e);
        }
    }

    boolean isAppReady() {
        return appReady;
    }

    private String formatMessage(Level level, String format, Object... args) {
        timestamp = LocalDateTime.now();
        String message = null;
        if (args == null) {
            message = MessageFormat.format(MESSAGE_FORMAT, level.getName(),
                    dtf.format(timestamp), message);
        } else {
            message = MessageFormat.format(MESSAGE_FORMAT, level.getName(),
                    dtf.format(timestamp),
                    MessageFormat.format(format, args));
        }
        return message;
    }

    private String formatErrorMessage(Level level, String format,
            Throwable cause, Object... args) {
        timestamp = LocalDateTime.now();
        String message = null;
        StringBuilder stackTrace = new StringBuilder();
        Throwable parent = cause;
        while (parent != null) {
            stackTrace.append("[").append(parent.getClass().getSimpleName());
            stackTrace.append("] ").append(parent.getMessage());

            for (StackTraceElement el : parent.getStackTrace()) {
                stackTrace.append("\n\t").append(el.toString());
            }

            parent = parent.getCause();
        }

        if (args == null) {
            message = MessageFormat.format(ERROR_FORMAT,
                    cause.getClass().getSimpleName(), timestamp, format);
        } else {
            message = MessageFormat.format(ERROR_FORMAT, level.getName(),
                    timestamp, cause.getClass().getSimpleName(), message);
        }

        message += stackTrace.toString();

        return message;
    }

    private synchronized void logMessage(Level level, String message) {
        // Log to the console regardless.
        if (level.getValue() < Level.WARNING.getValue()) {
            System.out.println(message);
        } else {
            System.err.println(message);
        }

        if (isAppReady()) {
            writeLog(message);
        } else {
            messageQ.add(message);
        }
    }

    private void writeLog(String message) {
        if (!message.endsWith(System.lineSeparator())) {
            message += System.lineSeparator();
        }
        if (out != null) {
            try {
                out.write(message);
                out.flush();
            } catch (IOException e) {
                Exceptions.getDefault().print(e, "Unable to write message: "
                        + message);
            }
        }
    }

    private void flushQ() {
        if (!messageQ.isEmpty()) {
            for (String message : messageQ) {
                writeLog(message);
            }
            messageQ.clear();
        }
    }

    private synchronized void configureLogger() {
        if (out != null) {
            return;
        }
        try {
            out = context.getLocalStorage().openFileWriter(
                    StorageLocations.LOG_DIR, name + ".log", false);
            flushQ();
        } catch (IOException e) {
            System.err.println(
                    "[" + Level.ERROR.getName() + "] Logger.configureLogger: "
                    + e.getClass().getSimpleName() + " - " + e.getMessage());
        }
    }

    private final ApplicationContext context;
    private final List<String> messageQ;
    private final String name;
    private final DateTimeFormatter dtf = DateTimeFormatter
            .ofPattern("EEE, dd LLL YYYY kk:mm:ss.SSS");

    private Level level = Level.INFO;
    private LocalDateTime timestamp;

    private BufferedWriter out;
    private boolean appReady = false;
    private boolean qFlushed = false;

}
