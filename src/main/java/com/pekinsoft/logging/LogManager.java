/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   LogManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 12, 2024
 *  Modified   :   Nov 12, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 12, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.logging;

import com.pekinsoft.framework.ApplicationContext;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class LogManager {

    public static Logger getLogger(ApplicationContext context, Class<?> clazz) {
        return getLogger(context, clazz.getName());
    }

    public static Logger getLogger(ApplicationContext context, String name) {
        return registry.computeIfAbsent(name, l -> new Logger(context, name));
    }

    private LogManager() {

    }

    private static final Map<String, Logger> registry = new HashMap<>();

}
