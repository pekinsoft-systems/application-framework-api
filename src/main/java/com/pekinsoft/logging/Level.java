/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Level.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 12, 2024
 *  Modified   :   Nov 12, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 12, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.logging;

import com.pekinsoft.utils.ColorUtils;
import java.awt.Color;
import java.awt.Font;
import javax.swing.UIManager;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Level {

    /**
     * This is a special {@code Level} for use with the
     * {@link com.pekinsoft.api.StatusDisplayer StatusDisplayer} for messages
     * that should have no decoration on them.
     * <p>
     * If this level is used for a
     * {@link Logger#log(Level, String) log message},
     * it will never be logged, as this level has a value of
     * {@code Integer.MIN_VALUE}, and {@link #ALL} has the value of
     * {@code Integer.MIN_VALUE + 1}.
     */
    public static final Level NORMAL = new Level("NORMAL", Integer.MIN_VALUE);
    /**
     * Used to flag to the {@link Logger} that all messages should be logged.
     * However, even when the logger has its level set to {@code ALL}, at no
     * time will a {@link #NORMAL} message be sent to the log output, nor the
     * console. {@code NORMAL} is a special level reserved for use by the
     * {@link com.pekinsoft.api.StatusDisplayer StatusDisplayer}. This
     * {@code Level} has the value of {@code Integer.MIN_VALUE + 1}.
     */
    public static final Level ALL = new Level("ALL", Integer.MIN_VALUE + 1);
    /**
     * Used to trace the entire process of the application through its
     * codebase. Typically used for introducing the beginning of method
     * execution and the completion of the method. Used also for tracking the
     * branching and iteration logic throughout the code. This {@code Level} has
     * a value of 0.
     */
    public static final Level TRACE = new Level("TRACE", 0);
    /**
     * Used for lower-level messaging to further assist developers when
     * debugging the code. This {@code Level} has a value of 100.
     */
    public static final Level DEBUG = new Level("DEBUG", 100);
    /**
     * Used for configuration messages within the code to assist developers in
     * debugging an application. This {@code Level} has a value of 200.
     */
    public static final Level CONFIG = new Level("CONFIG", 200);
    /**
     * Used for informational messages only. Typically, these types of messages
     * are not logged very often, but are displayed to the user via the
     * application's {@link com.pekinsoft.api.StatusDisplayer StatusDisplayer}.
     * This {@code Level} has the value of 300.
     */
    public static final Level INFO = new Level("INFO", 300);
    /**
     * Used for warning messages. Warnings could be indicators of potential bugs
     * in the application, or lack of training for the users. This {@code Level}
     * has the value of 400.
     */
    public static final Level WARNING = new Level("WARNING", 400);
    /**
     * Used for errors in the application. These could be application- or
     * user-caused, but are not critical in nature and should never cause the
     * application to terminate. This {@code Level} has the value of 500.
     */
    public static final Level ERROR = new Level("ERROR", 500);
    /**
     * Used for more extreme errors, but those that are not quite
     * {@link #FATAL}. This {@code Level} has the value of 600.
     */
    public static final Level CRITICAL = new Level("CRITICAL", 600);
    /**
     * A {@code Level} to be used for logging messages just prior to the
     * application being terminated due to extreme errors. The {@code Level} has
     * the value of {@code Integer.MAX_VALUE - 1}.
     */
    public static final Level FATAL = new Level("FATAL", Integer.MAX_VALUE - 1);
    /**
     * Turns all logging off. The value of this {@code Level} is
     * {@code Integer.MAX_VALUE}.
     */
    public static final Level OFF = new Level("OFF", Integer.MAX_VALUE);

    public static Level parse(String name) {
        return switch (name.toLowerCase()) {
            case "all" ->
                ALL;
            case "trace" ->
                TRACE;
            case "debug" ->
                DEBUG;
            case "config" ->
                CONFIG;
            case "info" ->
                INFO;
            case "warning" ->
                WARNING;
            case "error" ->
                ERROR;
            case "critical" ->
                CRITICAL;
            case "fatal" ->
                FATAL;
            default ->
                OFF;
        };
    }

    public Level(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }

    public Color asColor() {
        Color color = UIManager.getColor("TextField.foreground");

        switch (getValue()) {
            case 200 ->
                color = ColorUtils.CADET_BLUE;
            case 300 ->
                color = ColorUtils.DARK_BLUE;
            case 400 ->
                color = ColorUtils.CHOCOLATE;
            case 500 ->
                color = ColorUtils.CRIMSON;
            case 600 ->
                color = ColorUtils.FIREBRICK;
            case Integer.MAX_VALUE - 1 ->
                color = ColorUtils.DARK_RED;
            default ->
                UIManager.getColor("Label.foreground");
        }

        return color;
    }

    public Font asFont() {
        Font font = UIManager.getFont("Label.font");

        switch (getValue()) {
            case 200 ->
                font = font.deriveFont(Font.ITALIC);
            case 300, 400, 500, 600 ->
                font = font.deriveFont(Font.BOLD);
            case Integer.MAX_VALUE - 1 ->
                font = font.deriveFont(Font.BOLD + Font.ITALIC);
            default ->
                font = UIManager.getFont("Label.font");
        }

        return font;
    }

    private final String name;
    private final int value;

}
