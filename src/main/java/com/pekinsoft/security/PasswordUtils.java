/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   PasswordUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Oct 28, 2024
 *  Modified   :   Oct 28, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Oct 28, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import com.pekinsoft.framework.Application;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import static com.pekinsoft.utils.PreferenceKeys.PREF_PASSWORD_MIN_LENGTH;
import static com.pekinsoft.utils.PreferenceKeys.PREF_PASSWORD_MIN_NUMBERS;
import static com.pekinsoft.utils.PreferenceKeys.PREF_PASSWORD_MIN_SYMBOLS;
import static com.pekinsoft.utils.PreferenceKeys.PREF_PASSWORD_MIN_UPPER;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.*;
import java.util.prefs.Preferences;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/**
 * A static class that allows for passwords to be secured before storing, and to
 * determine if provided passwords match the stored passwords.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class PasswordUtils {

    private static final Logger logger = Application.getInstance()
            .getContext().getLogger(PasswordUtils.class);
    private static final SecureRandom RAND = new SecureRandom();
    private static final int ITERATIONS = 65536;
    private static final int KEY_LENGTH = 512;
    private static final String ALGORITHM = "PBKDF2WithHmacSHA512";

    private PasswordUtils() {
        /*
         * No instantiation allowed.
         */ }

    /**
     * Method provides a means of generating salt for password hashing to
     * protect the password for hackers and allow for storage in a database.
     * <p>
     * Password-based encryption generates a cryptographic key using a user
     * password as a starting point. We irreversibly convert the password to a
     * fixed-length hash code using a one-way hash function, adding a second
     * random string as &quot;salt&quot;, to prevent hackers from performing
     * *dictionary attacks*, where a list of common passwords are mapped
     * to their hashed outputs -- if the hacker knows the hashing algorithm used
     * and can gain access to the database where the hashcodes are stored, they
     * can use their &quot;dictionary&quot; to map back to the original password
     * and gain access to those accounts.</p>
     * <p>
     * We provide this method to allow the calling application to generate salt.
     * </p>
     *
     * @param length Number of bits of salt to generate.
     *
     * @return Optional&lt;String&gt; of the salt.
     */
    public static Optional<String> generateSalt(final int length) {
        if (length < 1) {
            System.err.println("error in generateSalt :: length must be > 0");
            return Optional.empty();
        }

        byte[] salt = new byte[length];
        RAND.nextBytes(salt);

        return Optional.of(Base64.getEncoder().encodeToString(salt));
    }

    /**
     * This method actually performs the hashing of the provided password, using
     * the provided salt string. With these two ingredients, we will generate a
     * secure hash that can be stored in a database or password file and not be
     * able to be determined what the original password was.
     * <p>
     * For security's sake, we immediately convert the password to a character
     * array and the salt to a byte array. As soon as we are done with the
     * provided strings, we set them up for garbage collection. Likewise, as
     * soon as we are done with the arrays, we place `null` characters in each
     * element, so as to keep the original password a secret.</p>
     *
     * @param password String pulled from a password field on a logon dialog.
     * @param salt     String of randomly generated salt.
     *
     * @return A secure hash of the original password.
     */
    public static Optional<String> hashPassword(String password, String salt) {
        char[] chars = password.toCharArray();
        byte[] bytes = salt.getBytes();

        PBEKeySpec spec = new PBEKeySpec(chars, bytes, ITERATIONS, KEY_LENGTH);

        Arrays.fill(chars, Character.MIN_VALUE);

        try {
            SecretKeyFactory fac = SecretKeyFactory.getInstance(ALGORITHM);
            byte[] securePassword = fac.generateSecret(spec).getEncoded();
            return Optional.of(Base64.getEncoder()
                    .encodeToString(securePassword));

        } catch (NoSuchAlgorithmException
                | InvalidKeySpecException ex) {
            String msg = String.format(
                    "%s: Exception encountered in hashPassword " // NOI18N
                    + "with password=\"%s\" and a salt of \"%s\"", ex, password, // NOI18N
                    salt); // NOI18N
            logger.log(Level.WARNING, msg);
            return Optional.empty();
        } finally {
            spec.clearPassword();
        }
    }

    /**
     * This method allows the calling application to verify that a user entered
     * the correct password, by checking it against a known hash.
     *
     * @param password The entered password.
     * @param key      The hash of the original password that was stored.
     * @param salt     The same random salt as originally used.
     *
     * @return Whether the passwords match or not.
     */
    public static boolean verifyPassword(String password, String key,
            String salt) {
        Optional<String> optEncrypted = hashPassword(password, salt);
        if (!optEncrypted.isPresent()) {
            return false;
        }
        return optEncrypted.get().equals(key);
    }

    /**
     * Determines whether the specified password is valid.
     * <p>
     * This method simply checks that the password meets the minimum
     * requirements as established by the {@link Application}.
     * <p>
     * Returns {@code true} if the password is:
     * <ul><li>greater than or equal to the minimum required length</li>
     * <li>has as many or more uppercase letters as required</li>
     * <li>has as many or more digits as required</li>
     * <li>has as many or more symbols as required</li></ul>
     * <p>
     * The strength of the password may be determined by passing it to the
     * {@link #calculatStrength(char[]) calculateStrength} method.
     * <p>
     * This method securely erases the supplied password.
     *
     * @param password     the password to check
     * @param minLength    the minimum required length
     * @param minUppercase the minimum uppercase characters required
     * @param minNumbers   the minimum numbers required
     * @param minSymbols   the minimum symbols required
     *
     * @return {@code true} if the password meets the minimum requirements;
     *         {@code false} otherwise
     */
    public static boolean isPasswordValid(char[] password, int minLength,
            int minUppercase, int minNumbers, int minSymbols) {
        int u = 0, n = 0, s = 0;
        boolean valid = false;
        String upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String numbers = "0123456789";
        String symbols = "`-=[]\\;',./~!@#$%^&*()_+{}|:\"<>?";

        if (password.length >= minLength) {
            for (char c : password) {
                if (symbols.contains(String.valueOf(c))) {
                    s++;
                } else if (numbers.contains(String.valueOf(c))) {
                    n++;
                } else if (upperCase.contains(String.valueOf(c))) {
                    u++;
                }
            }
        }

        // Securely delete the copy of the password.
        for (int x = 0; x < 7; x++) {
            Arrays.fill(password, '\0');
        }

        return (s >= minSymbols) && (n >= minNumbers) && (u >= minUppercase)
                && (password.length >= minLength);
    }

    /**
     * Convenience method for testing password validity using the
     * {@link Application Application's} stored {@link Preferences} for password
     * rules.
     * <p>
     * This method only tests the validity against the minimum requirements. To
     * test the strength of the password, call
     * {@link #calculatStrength(char[]) }.
     *
     * @param password the password to test for validity
     *
     * @return {@code true} if the password meets all minimum requirements;
     *         {@code false} if not
     */
    public static boolean isPasswordValid(char[] password) {
        Preferences prefs = Application.getInstance().getContext()
                .getPreferences(ApplicationContext.SYSTEM);
        int len = prefs.getInt(PREF_PASSWORD_MIN_LENGTH, -1);
        int dig = prefs.getInt(PREF_PASSWORD_MIN_NUMBERS, -1);
        int upp = prefs.getInt(PREF_PASSWORD_MIN_UPPER, -1);
        int sym = prefs.getInt(PREF_PASSWORD_MIN_SYMBOLS, -1);

        return isPasswordValid(password, len, upp, dig, sym);
    }

    /**
     * Calculates the strength of the user's password based upon the following
     * rules:
     * <ol>
     * <li>For every two letters beyond the minimum required length, the
     * strength increases by 3.</li>
     * <li>For every character type beyond the minimum requirements:
     * <ul><li>Digits: each one increases the strength by 5</li>
     * <li>Uppercase Letters: each one increases the strength by 3</li>
     * <li>Special Characters: each one increases the strength by 8</li></ul>
     * <li>For unique characters:
     * <ul><li>Each unique character increases the strength by 1</li>
     * <li>Non-sequential characters: strength increases by 20, minus the number
     * of sequential groups</li>
     * <li>Non-repeating characters: strength increases by 6, minus the number
     * of repetition groups</li></ul>
     * </ol><p>
     * All passwords that meet the bare minimum requirements have an initial
     * strength value of 20, and increase from there according to the rules
     * above. No password strength will exceed 100. The password strength
     * rating implies the difficulty in the password being cracked.
     * <p>
     * This method securely erases the supplied password.
     *
     * @param passwd       the password to be tested
     * @param minLength    the minimum length for the password
     * @param minDigits    the minimum count of digits required
     * @param minUppercase the minimum count of uppercase letters required
     * @param minSymbols   the minimum count of special characters required
     *
     * @return the strength value for the password
     */
    public static int calculatStrength(char[] passwd, int minLength,
            int minDigits, int minUppercase, int minSymbols) {
        int strength = 20;  // Bare minimum.

        // Rule 1: Beyond minimum length.
        if (passwd.length >= minLength) {
            int xtraLen = passwd.length - minLength;
            if (xtraLen > 0) {
                strength += (xtraLen / 2) * 3;
            }

            // Rule 2: Character types.
            int digits = 0, uppers = 0, symbols = 0;
            for (char ch : passwd) {
                if (Character.isDigit(ch)) {
                    digits++;
                } else if (Character.isUpperCase(ch)) {
                    uppers++;
                } else if (!Character.isLetterOrDigit(ch)) {
                    symbols++;
                }
            }
            strength += (digits - minDigits) * 3; // 3 points per additional digit
            strength += (uppers - minUppercase) * 3; // 3 points per add'l uppercase
            strength += (symbols - minSymbols) * 5;// 5 points per add'l symbols

            // Rule 3: Unique, non-sequential, and non-repeating characters
            Set<Character> uniqueChars = new HashSet<>();
            int sequentialGroups = 0, repetitionGroups = 0;
            boolean isSequential = false, isRepeating = false;
            char prevChar = '\0';

            for (char ch : passwd) {
                uniqueChars.add(ch);

                // Sequential check:
                if (prevChar != '\0' && ch == prevChar + 1) {
                    if (!isSequential) {
                        sequentialGroups++;
                    }
                    isSequential = true;
                } else {
                    isSequential = false;
                }

                // Repetition check:
                if (prevChar != '\0' && ch == prevChar) {
                    if (!isRepeating) {
                        repetitionGroups++;
                    }
                    isRepeating = true;
                } else {
                    isRepeating = false;
                }

                prevChar = ch;
            }
            strength += uniqueChars.size(); // 1 point per unique character
            strength += 10 - sequentialGroups;  // 10 points minus sequential groups
            strength += 3 - repetitionGroups;   // 3 points minus repetition groups

            // Securely delete this copy of the password.
            for (int x = 0; x < 7; x++) {
                Arrays.fill(passwd, '\0');
            }
        }

        // Cap password strength at 100
        strength = Math.min(strength, 100);

        return strength;
    }

    /**
     * Convenience method to calculate the password strength using the stored
     * requirements from the {@link Application Application's}
     * {@link Preferences}.
     *
     * @param passwd the password for which the strength should be calculated
     *
     * @return the password's strength value
     *
     * @see #calculatStrength(char[], int, int, int, int)
     */
    public static int calculatStrength(char[] passwd) {
        Preferences prefs = Application.getInstance().getContext()
                .getPreferences(ApplicationContext.SYSTEM);

        int len = prefs.getInt(PREF_PASSWORD_MIN_LENGTH, -1);
        int dig = prefs.getInt(PREF_PASSWORD_MIN_NUMBERS, -1);
        int upr = prefs.getInt(PREF_PASSWORD_MIN_UPPER, -1);
        int sym = prefs.getInt(PREF_PASSWORD_MIN_SYMBOLS, -1);

        return calculatStrength(passwd, len, dig, upr, sym);
    }

    /**
     * Generates a random length for salting the password hash, which will fall
     * between sixteen (16) and thirty-two (32) bytes for solid security.
     *
     * @return a salt length between 16-32 bytes
     */
    public static int generateRandomSaltLength() {
        return 16 + new Random().nextInt(17);
    }

}
