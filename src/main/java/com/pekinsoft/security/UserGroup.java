/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   apex-platform-api
 *  Class      :   UserGroup.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 3, 2024
 *  Modified   :   Nov 3, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 3, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import com.pekinsoft.framework.Application;
import java.util.*;

/**
 * Represents a group within the application, allowing multiple users to share
 * permissions based on group membership. A {@code UserGroup} contains a unique
 * identifier, group name, and members.
 * <p>
 * Group-based permissions make it easier to manage access for users who share
 * similar responsibilities or access levels within the application.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class UserGroup {

    private static final int MIN_GID = 100;
    private static final int MAX_GID = 999;

    /**
     * Constructs a new {@code UserGroup} with the specified name.
     * <p>
     * This method assigns the next available GID, <strong>G</strong>roup
     * <strong>ID</strong>entification number.
     *
     * @param groupName the name of the group
     */
    public UserGroup(String groupName) {
        assignGid();

        this.groupName = groupName;
    }

    private UserGroup() {

    }

    public int getGid() {
        return gid;
    }

    public String getGroupName() {
        return groupName;
    }

    public void addMember(SystemUser member) {
        if (members == null) {
            members = new HashSet<>();
        }
        if (member == null || members.contains(member)) {
            return;
        }

        members.add(member);
    }

    public void removeMember(SystemUser member) {
        if (member == null || members == null || !members.contains(member)) {
            return;
        }

        members.remove(member);
    }

    public Set<SystemUser> getMembership() {
        return members;
    }

    public boolean isMember(SystemUser user) {
        return members.contains(user);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(groupName).append(":");
        sb.append(gid).append(":");

        int last = members.size() - 1;
        int current = 0;
        for (SystemUser member : members) {
            sb.append(member.getUserName());
            if (current != last) {
                current++;
                sb.append(",");
            }
        }

        return sb.toString();
    }

    List<String> membersList;

    public static UserGroup of(String line) {
        String[] fields = line.split(":", -1);
        UserGroup group = new UserGroup(fields[0]);
        group.gid = Integer.parseInt(fields[1]);

        String[] members = fields[2].split(",");
        group.membersList = Arrays.asList(members);

        return group;
    }

    public static UserGroup getGroup(String groupName) {
        return lookupGid(groupName);
    }

    public static UserGroup getGroup(int gid) {
        return LogonManager.getInstance().findGroup(gid);
    }

    public static List<UserGroup> getAllGroups() {
        return new ArrayList<>(LogonManager.getInstance().getGroups());
    }

    public static UserGroup getNoGroup() {
        UserGroup group = new UserGroup(Application.getInstance().getContext()
                .getResourceMap().getString("Application.id"));
        group.gid = 99;
        group.addMember(SystemUser.getUser(Application.getInstance()
                .getContext().getResourceMap().getString("Application.id")));

        return group;
    }

    private void assignGid() {
        gid = LogonManager.getNextGid();
    }

    private static UserGroup lookupGid(String groupName) {
        return LogonManager.getInstance().findGroup(groupName);
    }

    private int gid;
    private String groupName;
    private Set<SystemUser> members;

}
