/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   LogonManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 4, 2024
 *  Modified   :   Nov 4, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 4, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import com.pekinsoft.api.Exceptions;
import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.security.ui.LogonDialog;
import com.pekinsoft.security.ui.wizard.*;
import com.pekinsoft.utils.PreferenceKeys;
import static com.pekinsoft.utils.PreferenceKeys.PREF_REQUIRE_LOGON;
import com.pekinsoft.wizard.Wizard;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CountDownLatch;

/**
 * The {@code LogonManager} singleton manages logging a user into the
 * {@link Application}.
 * <p>
 * With the user logged in, they will have access to only those portions of the
 * application to which they have been granted access. The access may be based
 * upon their user name, primary user group, or other group memberships. Access
 * is strictly controlled, and the {@code LogonManager} is checked often
 * throughout the application's framework to be sure the current user has the
 * access rights necessary for the application's features.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class LogonManager extends AbstractBean implements PreferenceKeys {

    private static final String PASSWD_FILE = "passwd";
    private static final String GROUP_FILE = "group";
    private static final String SHADOW_FILE = ".shadow";

    private static final Map<String, SystemUser> userCache = new HashMap<>();
    private static final Map<String, UserGroup> groupCache = new HashMap<>();
    private static final Map<String, Shadow> shadowMap = new HashMap<>();

    private static LogonManager instance = null;

    /**
     * Retrieves the singleton instance of the {@code LogonManager}
     *
     * @return the singleton instance
     */
    public static LogonManager getInstance() {
        if (instance == null) {
            instance = new LogonManager(Application.getInstance().getContext());
        }

        return instance;
    }

    /**
     * Performs the logon for the user to gain access into the
     * {@link Application}. This involves displaying a common logon dialog into
     * which the user may enter their application username and password.
     * <p>
     * The password will be verified to be correct for the username, and the
     * user's record will be loaded, which details the user's access rights to
     * the various features and files/directories of the application.
     * <p>
     * This is a bound property for &ldquo;loggedOnUser&rdquo;.
     *
     * @return {@code true} if the user is successfully logged on; {@code false}
     *         if not
     */
    public boolean logon() {
        logger.info("Performing logon");

        SystemUser old = getLoggedOnUser();

        if (context.getPreferences(ApplicationContext.SYSTEM)
                .getBoolean(PREF_REQUIRE_LOGON, false)) {
            logger.config("Application requires users to logon.");

            LogonDialog dlg = LogonDialog.getInstance(context);
            int maxAttempts = context.getPreferences(ApplicationContext.SYSTEM)
                    .getInt(PREF_MAX_LOGON_ATTEMPTS, 3);
            int attempt = 0;

            while (attempt < maxAttempts) {
                dlg.setVisible(true);

                if (dlg.isCancelled()) {
//                    securityLog.info("User cancelled logon. Exiting with "
//                            + "SysExits.EX_NOUSER (67).");
                    Application.getInstance().exit(SysExits.EX_NOUSER);
                }

                SystemUser user = userCache.getOrDefault(dlg.getUsername(),
                        SystemUser.getApplicationOwner());
                char[] password = dlg.getPassword();

                Shadow shadow = shadowMap.get(user.getUserName());

                Optional<String> hash = PasswordUtils.hashPassword(
                        String.copyValueOf(password), shadow.getSalt());

                // Delete the user's password array. Perform secure delete.
                for (int x = 0; x < 7; x++) {
                    Arrays.fill(password, '\0');
                }

                if (hash.get() != null && hash.get().equals(shadow.getHash())) {
                    loggedOnUser = user;
//                    securityLog.info(String.format("User \"%s\" successfully "
//                            + "logged on with %s attempts.", user.getUserName(),
//                            attempt));
                    logger.info(String.format("Logged on user: %s (Primary "
                            + "Group: %s)",
                            user.getUserName(),
                            user.getPrimaryGroup().getGroupName()));
                    attempt = maxAttempts;
                } else if (attempt == maxAttempts - 1) {
//                    securityLog.info(String.format("User \"%s\" has failed to "
//                            + "logon in the maximum attempts of %s",
//                            user.getUserName(), maxAttempts));
                } else {
                    attempt++;
                }
            }
            return true;
        } else {
            loggedOnUser = SystemUser.getApplicationOwner();
            logger.config(String.format("Application does not require users to "
                    + "logon. Using default user \"%s\".",
                    loggedOnUser.getUserName()));
        }

        firePropertyChange("loggedOnUser", old, getLoggedOnUser());

        return false;
    }

    /**
     * Performs the logout for the currently logged in user. Once this method
     * returns, the user is no longer logged into the {@link Application}, and
     * access to the entire application will be denied until a user successfully
     * logs into it again.
     */
    public void logout() {
        logger.info(String.format("Logging out the current user: %s",
                loggedOnUser));
        loggedOnUser = null;
    }

    /**
     * Retrieves the currently logged in user. This object may be used to deny
     * or allow access to {@code Application} features, files, and/or
     * directories.
     *
     * @return the {@link SystemUser} object
     */
    public SystemUser getLoggedOnUser() {
        return loggedOnUser;
    }

    /**
     * Retrieves the application owner {@code SystemUser} account.
     *
     * @return the application owner
     */
    public SystemUser getApplicationOwner() {
        SystemUser appOwner = userCache.get(resourceMap
                .getString("Application.id"));
        return appOwner == null ? SystemUser.getApplicationOwner() : appOwner;
    }

    /**
     * Logs on the application owner {@code SystemUser} account.
     */
    public void logonApplicationOwner() {
        loggedOnUser = SystemUser.getApplicationOwner();
    }

    /**
     * Retrieves the {@link SystemUser} object that has the specified username.
     * If no user account exists with the specified name, {@code null} is
     * returned.
     *
     * @param username the username for the account to find
     *
     * @return the located account or {@code null}
     */
    public SystemUser findUser(String username) {
        return userCache.get(username);
    }

    /**
     * Finds the user that has the specified UID.
     * <p>
     * This method searches through the user cache and checks each user's UID
     * value. If the user has the specified UID, it is returned. If no user has
     * the specified UID, then {@code null} is returned.
     *
     * @param uid the UID of the user to find
     *
     * @return the user with the specified UID or {@code null}
     */
    public SystemUser findUser(int uid) {
        for (SystemUser user : userCache.values()) {
            if (uid == user.getUid()) {
                return user;
            }
        }

        return null;
    }

    /**
     * Retrieves the {@link UserGroup} object that has the specified group name.
     * If no group with that name exists, {@code null} is returned.
     *
     * @param groupName the name of the group to find
     *
     * @return the located group or {@code null}
     */
    public UserGroup findGroup(String groupName) {
        return groupCache.get(groupName);
    }

    /**
     * Finds a {@link UserGroup} based upon its GID.
     * <p>
     * This method searches the group cache and checks each group's GID value.
     * If the group has the specified GID, it is returned. If no group has the
     * specified GID, the {@code null} is returned.
     *
     * @param gid the GID of the group to find
     *
     * @return the group with the specified GID or {@code null}
     */
    public UserGroup findGroup(int gid) {
        for (UserGroup group : groupCache.values()) {
            if (gid == group.getGid()) {
                return group;
            }
        }

        return null;
    }

    /**
     * Retrieves a {@link Set} of all registered {@link UserGroup UserGroups}.
     * The {@code UserGroup}s are registered by being added to the {@code group}
     * file in the application's configuration directory.
     *
     * @return a {@code Set} of all registered {@code UserGroup}s
     */
    public Collection<UserGroup> getGroups() {
        return groupCache.values();
    }

    /**
     * Validates the specified password for the given {@link SystemUser}.
     * <p>
     * This method is deterministic and compares the passwords in the same way
     * as does the {@link #logon() logon} method.
     * <p>
     * This method securely erases the given password.
     *
     * @param user     the {@code SystemUser} whose password is to be compared
     * @param password the password to check
     *
     * @return {@code true} if the given password matches the specified user's;
     *         {@code false} otherwise
     */
    public boolean isPasswordCorrectForUser(SystemUser user, char[] password) {
        Shadow shadow = shadowMap.get(user.getUserName());
        String hash = shadow.getHash();
        String salt = shadow.getSalt();
        String pwd = String.copyValueOf(password);

        for (int x = 0; x < 7; x++) {
            Arrays.fill(password, '\0');
        }

        Optional<String> currentHash = PasswordUtils.hashPassword(pwd, salt);

        if (currentHash.get() != null) {
            return currentHash.get().equals(pwd);
        }

        char[] erased = pwd.toCharArray();
        for (int x = 0; x < 7; x++) {
            Arrays.fill(erased, '\0');
            pwd = String.copyValueOf(erased);
        }

        return false;
    }

    /**
     * Changes the password for the specified {@link SystemUser} to the given
     * {@code password}.
     * <p>
     * If the {@code user} and/or {@code password} are {@code null}, or if the
     * {@code password} has a length of zero, or if the {@code user} does not
     * exist, no exception is thrown and no action is taken.
     *
     * @param user     the {@code SystemUser} for whom the password should be
     *                 changed
     * @param password the new password
     */
    public void changePasswordForUser(SystemUser user, char[] password) {
        Shadow shadow = shadowMap.get(user.getUserName());

        Optional<String> hash = PasswordUtils.hashPassword(String.copyValueOf(
                password), shadow.getSalt());

        for (int x = 0; x < 7; x++) {
            Arrays.fill(password, '\0');
        }

        if (hash.get() != null && !hash.get().isBlank() && !hash.get().isEmpty()) {
            StringBuilder sb = new StringBuilder();
            sb.append(shadow.getUsername()).append(":");
            sb.append(shadow.getSalt()).append(":");
            sb.append(hash).append(":");
            sb.append(shadow.getCreated()).append(":");
            sb.append(ChronoUnit.DAYS.between(LocalDate.EPOCH, LocalDate.now()))
                    .append(":");
            sb.append(shadow.getMustChange()).append(":");
            sb.append(shadow.getWarningPeriod()).append(":");
            sb.append(shadow.getGracePeriod()).append(":");
            sb.append(LocalDate.now().plusDays(shadow.getMustChange()))
                    .append(":");
            sb.append(PasswordUtils.calculatStrength(password));

            Shadow updated = Shadow.of(sb.toString());
            shadowMap.remove(shadow.getUsername());
            shadowMap.put(updated.getUsername(), updated);

            updateSecurityFiles();
        }
    }

    public void setLogger(Logger logger) {
        this.logger = logger;
    }

    String getApplicationOwnerSalt() {
        return shadowMap.get(getApplicationOwner().getUserName()).getSalt();
    }

    /**
     * Retrieves the next User Identifier (UID) that is available for use.
     *
     * @return the next available GID
     */
    static int getNextUid() {
        int nextUid = 0;
        for (String uname : userCache.keySet()) {
            int uid = userCache.get(uname).getUid();
            nextUid = Math.max(nextUid, uid);
        }

        return nextUid;
    }

    /**
     * Retrieves the next Group Identifier (GID) that is available for use.
     *
     * @return the next available GID
     */
    static int getNextGid() {
        int nextGid = 0;
        for (String gname : groupCache.keySet()) {
            int gid = groupCache.get(gname).getGid();
            nextGid = Math.max(nextGid, gid);
        }

        return nextGid;
    }

    /**
     * Constructs the {@code LogonManager} singleton instance.
     *
     * @param context the {@link ApplicationContext} in which this
     *                {@code LogonManager} is being used
     */
    private LogonManager(ApplicationContext context) {
        this.context = context;
        logger = context.getLogger(getClass());
        securityLog = context.getLogger("Security");
        resourceMap = context.getResourceMap(getClass());
        category = resourceMap.getString("category.privacy");
//        initialize();
    }

    private volatile Wizard wizard;
    private volatile boolean defaultSecurity = true;

    public void initialize() {
        // Get the configuration directory for the application.
        Path dir = context.getLocalStorage().getConfigDirectory();
        // Get the primary security file.
        Path passwd = dir.resolve("passwd");

        // Check if the primary security file exists.
        if (!Files.exists(passwd)) {
            // If not, run the Security Setup Wizard because this is probably
            //+ the first time the application has been launched. We are using
            //+ a CountDownLatch to lock the thread until the Wizard has been
            //+ completed or cancelled.
            CountDownLatch latch = new CountDownLatch(1);

            wizard = Wizard.createWizard(context,
                    resourceMap.getString(
                            "security.wizard.title"),
                    new LogonPoliciesFinisher(context),
                    () -> {
                        // Check to see if the Wizard was cancelled.
                        if (wizard.isCancelled()) {
                            // If so, exit the application due to NOPERM
                            context.getApplication().exit(SysExits.EX_NOPERM);
                        } else {
                            // Otherwise, check to see if we are using the
                            //+ default or enhanced security model.
                            if (context
                                    .getPreferences(ApplicationContext.SYSTEM)
                                    .getBoolean(PREF_REQUIRE_LOGON, false)) {
                                // Using the enhanced security model requires
                                //+ users to logon, so restart the application
                                //+ to force a logon.
                                defaultSecurity = false;
                                context.getApplication().exit(
                                        SysExits.EX_RESTART);
                            }
                        }
                        // Don't forget to countdown the latch so the thread
                        //+ will quit and the application can continue.
                        latch.countDown();
                    },
                    new WelcomeStep(context),
                    new RequireLogonStep(context),
                    new PasswordPolicyStep(context),
                    new AdministratorAccountStep(context),
                    new SummaryStep(context));
            Runnable setupSecurity = () -> {
                wizard.showWizard();
            };

            try {
                setupSecurity.run();
                latch.await();
            } catch (InterruptedException stop) {
                Thread.currentThread().interrupt();
            }
        } else {
            // The primary security file exists, so set whether using the
            //+ default or enhanced security model
            defaultSecurity = !context.getPreferences(ApplicationContext.SYSTEM)
                    .getBoolean(PREF_REQUIRE_LOGON, false);
        }

        logger.info("Initializing the LogonManager");

        // Read in the `passwd` file to initialize the userCache
        try (BufferedReader in = new BufferedReader(context.getLocalStorage()
                .openFileReader(StorageLocations.CONFIG_DIR, PASSWD_FILE));) {
            String line;
            while ((line = in.readLine()) != null) {
                SystemUser user = SystemUser.of(line);
                userCache.put(user.getUserName(), user);
            }
        } catch (IOException e) {
            title = "Read passwd Failure";
            basic = "Unable to parse the passwd file";
            detail = basic + ": [" + e.getClass().getSimpleName() + "] "
                    + e.getLocalizedMessage();
            Exceptions.getDefault().handle(createErrorInfo(e, Level.ERROR));
            logger.warning(detail);
        }

        // Read in the `group` file to initialize the groupCache
        try (BufferedReader in = new BufferedReader(context.getLocalStorage()
                .openFileReader(StorageLocations.CONFIG_DIR, GROUP_FILE));) {
            String line;
            while ((line = in.readLine()) != null) {
                UserGroup group = UserGroup.of(line);
                groupCache.put(group.getGroupName(), group);
            }
        } catch (IOException e) {
            title = "Read group Failure";
            basic = "Unable to parse the group file";
            detail = basic + ": [" + e.getClass().getSimpleName() + "] "
                    + e.getLocalizedMessage();
            Exceptions.getDefault().handle(createErrorInfo(e, Level.ERROR));
            logger.warning(detail);
        }

        // Read in the `.shadow` file to initialize the groupCache
        try (BufferedReader in = new BufferedReader(context.getLocalStorage()
                .openFileReader(StorageLocations.CONFIG_DIR, SHADOW_FILE));) {
            String line;
            while ((line = in.readLine()) != null) {
                Shadow shadow = Shadow.of(line);
                shadowMap.put(shadow.getUsername(), shadow);
            }
        } catch (IOException e) {
            title = "Read .shadow Failure";
            basic = "Unable to parse the .shadow file";
            detail = basic + ": [" + e.getClass().getSimpleName() + "] "
                    + e.getLocalizedMessage();
            Exceptions.getDefault().handle(createErrorInfo(e, Level.ERROR));
            logger.warning(detail);
        }

        if (defaultSecurity) {
            loggedOnUser = getApplicationOwner();
        }

        resolveReferences();
        logger.info("LogonManager initialized");
    }

    private void updateSecurityFiles() {
        try (BufferedWriter out = new BufferedWriter(context.getLocalStorage()
                .openFileWriter(StorageLocations.CONFIG_DIR, PASSWD_FILE, false))) {
            for (SystemUser user : userCache.values()) {
                out.write(user.toString());
            }
        } catch (IOException e) {
            title = "Writing passwd Failure";
            basic = "Unable to write the passwd file";
            detail = basic + ": [" + e.getClass().getSimpleName() + "] "
                    + e.getLocalizedMessage();
            Exceptions.getDefault().handle(createErrorInfo(e, Level.ERROR));
            logger.warning(detail);
        }

        try (BufferedWriter out = new BufferedWriter(context.getLocalStorage()
                .openFileWriter(StorageLocations.CONFIG_DIR, GROUP_FILE, false))) {
            for (UserGroup group : groupCache.values()) {
                out.write(group.toString());
            }
        } catch (IOException e) {
            title = "Writing group Failure";
            basic = "Unable to write the group file";
            detail = basic + ": [" + e.getClass().getSimpleName() + "] "
                    + e.getLocalizedMessage();
            Exceptions.getDefault().handle(createErrorInfo(e, Level.ERROR));
            logger.warning(detail);
        }

        try (BufferedWriter out = new BufferedWriter(context.getLocalStorage()
                .openFileWriter(StorageLocations.CONFIG_DIR, SHADOW_FILE, false))) {
            for (Shadow shadow : shadowMap.values()) {
                out.write(shadow.toString());
            }
        } catch (IOException e) {
            title = "Writing .shadow Failure";
            basic = "Unable to write the .shadow file";
            detail = basic + ": [" + e.getClass().getSimpleName() + "] "
                    + e.getLocalizedMessage();
            Exceptions.getDefault().handle(createErrorInfo(e, Level.ERROR));
            logger.warning(detail);
        }
    }

    private void resolveReferences() {
        for (UserGroup group : groupCache.values()) {
            if (group.membersList != null) {
                for (String userName : group.membersList) {
                    SystemUser user = userCache.get(userName);
                    if (user != null) {
                        group.addMember(user);
                    }
                }
            }
        }
    }

    private ErrorInfo createErrorInfo(Throwable cause, Level level) {
        return new ErrorInfo(title, basic, detail, category, cause, level,
                createStateMap());
    }

    private Map<String, String> createStateMap() {
        Map<String, String> state = new HashMap<>();

        state.put("Class", getClass().getName());
        state.put("Application", resourceMap.getString("Application.name"));
        state.put("Application Version", resourceMap.getString(
                "Application.version"));
        state.put("Application Vendor", resourceMap.getString(
                "Application.vendor"));
        if (loggedOnUser != null) {
            state.put("Current User", loggedOnUser.getUserName() + " ("
                    + loggedOnUser.getUid() + ")");
        } else {
            state.put("Current User", "[NOUSER]");
        }

        return state;
    }

    private Logger logger;
    private final Logger securityLog;
    private final ApplicationContext context;
    private final ResourceMap resourceMap;
    private final String category;

    private SystemUser loggedOnUser = null;

    private String title = null;
    private String basic = null;
    private String detail = null;

}
