/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   apex-platform-api
 *  Class      :   SystemUser.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 3, 2024
 *  Modified   :   Nov 3, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 3, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import com.pekinsoft.framework.Application;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a user within the application. Each {@code SystemUser} has a
 * unique identifier, username, and a set of permissions governing their access
 * to application features, files, and directories.
 * <p>
 * The {@code SystemUser} object also provides methods for determining password
 * status (expired, needs warning) and account status (active, disabled).
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class SystemUser {

    private static final int MIN_UID = 1000;
    private static final int MAX_UID = 60000;

    /**
     * Constructs a new {@code SystemUser} instance with the specified username
     * and membership in the {@link UserGroup} with the specified UID as their
     * primary group.
     * <p>
     * This constructor will automatically assign the next available UID,
     * <strong>U</strong>ser <strong>ID</strong>entification number.
     *
     * @param userName   the username to assign
     * @param primaryGid the UID of the primary group
     */
    public SystemUser(String userName, int primaryGid) {
        assignUid();

        this.userName = userName;
        this.primaryGid = primaryGid;
        groups = new HashSet<>();
        assignUid();
    }

    private SystemUser() {
        groups = new HashSet<>();
    }

    /**
     * Retrieves the UID, <strong>U</strong>ser <strong>ID</strong>entification
     * number, for this {@code SystemUser}.
     *
     * @return the UID for this user
     */
    public int getUid() {
        return uid;
    }

    /**
     * Retrieves this {@code SystemUser}'s username.
     *
     * @return the username
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Retrieves the GID, <strong>G</strong>roup <strong>ID</strong>entifation
     * number, for this {@code SystemUser}'s primary {@link UserGroup}.
     *
     * @return the GID of the primary group for this user
     */
    public int getPrimaryGid() {
        return primaryGid;
    }

    /**
     * Retrieves this {@code SystemUser}'s primary group. This is typically a
     * group of the same name as the {@link #getUserName() username}.
     *
     * @return the user's primary group
     */
    public UserGroup getPrimaryGroup() {
        return LogonManager.getInstance().findGroup(getPrimaryGid());
    }

    /**
     * Grants this {@code SystemUser} a membership to the specified
     * {@link UserGroup}.
     *
     * @param group the group to which membership is to be granted
     */
    public void grantGroup(UserGroup group) {
        if (group == null || groups.contains(group)) {
            return;
        }

        groups.add(group);
    }

    /**
     * Revokes this {@code SystemUser}'s membership in the specified
     * {@link UserGroup}.
     *
     * @param group the group for which to revoke membership
     */
    public void revokeGroup(UserGroup group) {
        if (group == null || !groups.contains(group)) {
            return;
        }

        groups.remove(group);
    }

    /**
     * Retrieves a {@link Set} of {@link UserGroup UserGroups} to which this
     * {@code SystemUser} has been assigned.
     *
     * @return a set of all groups of which this user is a member
     */
    public Set<UserGroup> getMembershipGroups() {
        return groups;
    }

    /**
     * Retrieves the date that the current password was created.
     *
     * @return the creation date of the password
     */
    public LocalDate getCreatedDate() {
        return LocalDate.EPOCH.plusDays(shadow.getCreated());
    }

    /**
     * Determines whether the password must be changed now. This is a setting
     * that could be made when the {@code SystemUser} account was created, using
     * a temporary password that must be changed upon first logon. If this
     * method returns true, the value will be updated upon successful password
     * change.
     *
     * @return {@code true} if the password must be changed now; {@code false}
     *         if not
     */
    public boolean mustChangeNow() {
        return shadow.getCanChange() == 0;
    }

    /**
     * Retrieves the date on which the user should start being warned that their
     * password is nearing expiration.
     *
     * @return the warning period start date
     */
    public LocalDate getWarningStartDate() {
        return getExpirationDate().minusDays(shadow.getWarningPeriod());
    }

    /**
     * Retrieves the expiration date for the {@code SystemUser} account.
     *
     * @return the password expiration date
     */
    public LocalDate getExpirationDate() {
        return getCreatedDate().plusDays(shadow.getMustChange());
    }

    /**
     * Determines whether the user's password has expired and needs to be
     * changed.
     *
     * @return {@code true} if password is expired; {@code false} if not
     */
    public boolean isPasswordExpired() {
        LocalDate today = LocalDate.now();
        return today.isEqual(getExpirationDate());
    }

    /**
     * Determines whether the user should be warned that their password will be
     * expiring soon.
     *
     * @return {@code true} if user should be warned; {@code false} if no
     */
    public boolean shouldWarn() {
        LocalDate today = LocalDate.now();
        return today.isEqual(getWarningStartDate())
                || today.isAfter(getWarningStartDate());
    }

    /**
     * Determines whether the grace period after password expiration has ended
     * and the account has become disabled.
     *
     * @return {@code true} if account is disabled; {@code false} if not
     */
    public boolean isAccountDisabled() {
        LocalDate today = LocalDate.now();
        LocalDate graceEnds = getExpirationDate().plusDays(shadow
                .getGracePeriod());
        return today.isEqual(graceEnds) || today.isAfter(graceEnds);
    }

    /**
     * Retrieves the default {@code SystemUser} for the application. This
     * account is typically used for applications that are using the <em>default
     * security model</em>, which does not require users to logon.
     *
     * @return the default user account
     */
    public static SystemUser getApplicationOwner() {
        SystemUser noUser = new SystemUser(Application.getInstance()
                .getContext().getResourceMap().getString("Application.id"), 100);
        noUser.uid = 99;
        noUser.primaryGid = UserGroup.getNoGroup().getGid();
        noUser.realName = "[Default User]";
        return noUser;
    }

    /**
     * {@inheritDoc }
     * <p>
     * This incantation converts the {@code SystemUser} to a string that is
     * suitable for writing to a {@code passwd} file.
     *
     * @return {@inheritDoc }
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(userName).append(":");
        sb.append(uid).append(":");
        sb.append(primaryGid).append(":");
        sb.append(realName).append(":");
        sb.append(strength);

        return sb.toString();
    }

    /**
     * Constructs a {@code SystemUser} instance populated from a line of
     * the
     * {@code passwd} file.
     *
     * @param line the line read in from the {@code passwd} file
     *
     * @return a {@code SystemUser} instance
     */
    public static SystemUser of(String line) {
        String[] fields = line.split(":", -1);
        SystemUser user = new SystemUser();
        user.userName = fields[0];
        user.uid = Integer.parseInt(fields[1]);
        user.primaryGid = Integer.parseInt(fields[2]);
        user.realName = fields[3];
        user.strength = Integer.parseInt(fields[4]);
        return user;
    }

    /**
     * Determines whether the password is valid and the user can be logged in.
     *
     * @param password the password to test
     *
     * @return {@code true} if password is correct; {@code false} if not
     */
    public boolean verifyPassword(char[] password) {
        return shadow != null && shadow.verifyPassword(password);
    }

    /**
     * Gets the {@code SystemUser} with the specified username, or {@code null},
     * if no such user exists.
     *
     * @param name the username of the account to get
     *
     * @return the located account or {@code null}
     */
    public static SystemUser getUser(String name) {
        return LogonManager.getInstance().findUser(name);
    }

    /**
     * Gets the {@code SystemUser} with the specified UID, or {@code null} if no
     * such user exists.
     *
     * @param uid the UID of the user to get
     *
     * @return the located user or {@code null}
     */
    public static SystemUser getUser(int uid) {
        return LogonManager.getInstance().findUser(uid);
    }

    private void assignUid() {
        uid = LogonManager.getNextUid();
    }

    private int uid;
    private String userName;
    private String realName;
    private int primaryGid;
    private int strength;
    private final Set<UserGroup> groups;
    private Shadow shadow;

}
