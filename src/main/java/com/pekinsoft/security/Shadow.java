/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   apex-platform-security
 *  Class      :   Shadow.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 3, 2024
 *  Modified   :   Nov 3, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 3, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import java.util.Optional;

/**
 * Provides access to hashed password storage, ensuring password security within
 * the application. The {@code Shadow} class interfaces with a secure storage
 * mechanism (e.g., a shadow file) to retrieve and validate user credentials.
 * <p>
 * This class is package-private, as it is intended for internal security
 * operations within the {@code com.pekinsoft.security} package.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
class Shadow {

    Shadow() {

    }

    public String getUsername() {
        return username;
    }

    public String getSalt() {
        return salt;
    }

    public String getHash() {
        return hash;
    }

    public long getCreated() {
        return created;
    }

    public long getCanChange() {
        return canChange;
    }

    public long getMustChange() {
        return mustChange;
    }

    public int getWarningPeriod() {
        return warningPeriod;
    }

    public int getGracePeriod() {
        return gracePeriod;
    }

    public long getExpires() {
        return expires;
    }

    boolean verifyPassword(char[] password) {
        Optional<String> testHash = PasswordUtils.hashPassword(String.valueOf(
                password), salt);
        if (testHash.get() == null) {
            return false;
        }

        return testHash.get().equals(hash);
    }

    public static Shadow of(String line) {
        String[] fields = line.split(":", -1);
        Shadow shadow = new Shadow();

        shadow.username = fields[0];
        shadow.salt = fields[1];
        shadow.hash = fields[2];
        shadow.created = Integer.parseInt(fields[3]);
        shadow.canChange = Integer.parseInt(fields[4]);
        shadow.mustChange = Integer.parseInt(fields[5]);
        shadow.warningPeriod = Integer.parseInt(fields[6]);
        shadow.gracePeriod = Integer.parseInt(fields[7]);
        shadow.expires = Long.parseLong(fields[8]);

        return shadow;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(username).append(":");
        sb.append(salt).append(":");
        sb.append(hash).append(":");
        sb.append(created).append(":");
        sb.append(canChange).append(":");
        sb.append(mustChange).append(":");
        sb.append(warningPeriod).append(":");
        sb.append(gracePeriod).append(":");
        sb.append(expires);

        return sb.toString();
    }

    private String username;
    private String salt;
    private String hash;
    private long created;
    private long canChange;
    private long mustChange;
    private int warningPeriod;
    private int gracePeriod;
    private long expires;

}
