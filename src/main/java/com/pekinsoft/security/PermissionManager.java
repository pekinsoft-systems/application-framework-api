/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   PermissionManager.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 7, 2024
 *  Modified   :   Nov 7, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 7, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import java.util.*;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class PermissionManager {

    private static PermissionManager instance = null;

    public static PermissionManager getInstance() {
        if (instance == null) {
            instance = new PermissionManager();
        }

        return instance;
    }

    /**
     * Grants permission to a specific user.
     *
     * @param user       the {@link SystemUser} to whom the permission should be
     *                   granted
     * @param permission the {@link Permission} to grant
     */
    public void grantPermission(SystemUser user, Permission permission) {
        userPermissions.computeIfAbsent(user, k -> new HashSet<>())
                .add(permission);
    }

    /**
     * Grants permission to a user group.
     *
     * @param group      the {@link UserGroup} to which the permission should be
     *                   granted
     * @param permission the {@link Permission} to grant
     */
    public void grantPermission(UserGroup group, Permission permission) {
        groupPermissions.computeIfAbsent(group, k -> new HashSet<>())
                .add(permission);
    }

    /**
     * Determines whether the {@link SystemUser user} has the specified
     * {@link Permission permission}.
     * <p>
     * This method first checks if the user has the specified permission, and if
     * not, the checks for the permission in the user's assigned
     * {@link UserGroup groups}.
     *
     * @param user       the user for whom the permission is to be checked
     * @param permission the permission for which to check
     *
     * @return {@code true} if the user has the permission on their own or as
     *         part of a group; {@code false} if no permission
     */
    public boolean hasPermission(SystemUser user, Permission permission) {
        // Check user-specific permissions first.
        Set<Permission> permissions = userPermissions.get(user);
        if (permissions != null && permissions.contains(permission)) {
            return true;
        }

        // Check for group permissions next.
        for (UserGroup group : user.getMembershipGroups()) {
            Set<Permission> groupPerms = groupPermissions.get(group);
            if (groupPerms != null && groupPerms.contains(permission)) {
                return true;
            }
        }

        // FALLBACK: no permission granted to the user nor any of their assigned
        //+ groups.
        return false;
    }

    /**
     * Revokes a {@link Permission} for a specific {@link SystemUser user}.
     *
     * @param user       the user from whom the permission is to be revoked
     * @param permission the permission to revoke
     */
    public void revokePermission(SystemUser user, Permission permission) {
        Set<Permission> permissions = userPermissions.get(user);
        if (permissions != null) {
            permissions.remove(permission);
        }
    }

    /**
     * Revokes a {@link Permission} for the entire {@link UserGroup group}.
     *
     * @param group      the group for which the permission is to be revoked
     * @param permission the permission to revoke
     */
    public void revokePermission(UserGroup group, Permission permission) {
        Set<Permission> permissions = groupPermissions.get(group);
        if (permissions != null) {
            permissions.remove(permission);
        }
    }

    /**
     * Retrieves all {@link Permission permissions} assigned to the specified
     * {@link SystemUser user}, including their assigned {@link UserGroup group}
     * permissions.
     *
     * @param user the user for whom to retrieve all permissions
     *
     * @return a {@link Set} of all permissions for this user and their groups
     */
    public Set<Permission> getAllPermissions(SystemUser user) {
        Set<Permission> allPermissions = new HashSet<>();
        Set<Permission> userPerms = userPermissions.get(user);

        if (userPerms != null) {
            allPermissions.addAll(userPerms);
        }

        for (UserGroup group : user.getMembershipGroups()) {
            Set<Permission> groupPerms = groupPermissions.get(group);
            if (groupPerms != null) {
                allPermissions.addAll(groupPerms);
            }
        }

        return allPermissions;
    }

    private PermissionManager() {

    }

    private final Map<SystemUser, Set<Permission>> userPermissions = new HashMap<>();
    private final Map<UserGroup, Set<Permission>> groupPermissions = new HashMap<>();

}
