/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   AuditInfo.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 8, 2024
 *  Modified   :   Nov 8, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 8, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import java.time.Instant;
import java.util.Objects;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class AuditInfo {

    private AuditInfo(Builder builder) {
        this.user = Objects.requireNonNull(builder.user, "user");
        this.timestamp = Objects.requireNonNullElseGet(builder.timestamp,
                Instant::now);
        this.permission = builder.permission;
        this.detailMessage = builder.detailMessage;
    }

    public SystemUser getUser() {
        return user;
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public Permission getPermission() {
        return permission;
    }

    public String getDetailMessage() {
        return detailMessage;
    }

    private final SystemUser user;
    private final Instant timestamp;
    private final Permission permission;
    private final String detailMessage;

    public static class Builder {

        private final SystemUser user;
        private Instant timestamp;
        private Permission permission;
        private String detailMessage;

        public Builder(SystemUser user) {
            this.user = user;
        }

        public Builder timestamp(Instant timestamp) {
            this.timestamp = timestamp;
            return this;
        }

        public Builder permission(Permission permission) {
            this.permission = permission;
            return this;
        }

        public Builder detailMessage(String detailMessage) {
            this.detailMessage = detailMessage;
            return this;
        }

        public AuditInfo build() {
            return new AuditInfo(this);
        }

    }

    public String toString() {
        return String.format("%s,%s,%s,%s,%s}", user.getUserName(),
                timestamp, permission.getName(), permission.getType(),
                detailMessage);
    }

    static AuditInfo parse(String line) {
        String[] fields = line.split(",");
        return new Builder(LogonManager.getInstance().findUser(fields[0]))
                .timestamp(Instant.parse(fields[1]))
                .permission(new Permission(fields[2],
                        Permission.Type.valueOf(fields[3])))
                .detailMessage(fields[4])
                .build();
    }

}
