/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   WelcomeStep.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 12, 2024
 *  Modified   :   Nov 12, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 12, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui.wizard;

import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.wizard.WizardStep;
import java.awt.Dimension;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author Sean Carrick
 */
public class WelcomeStep extends WizardStep implements ValueKeys {

    /**
     * Creates new form WelcomeStep
     */
    public WelcomeStep(ApplicationContext context) {
        super(context);
        initComponents();
    }

    @Override
    public void onEnter() {
        helpEditorPane.setCaretPosition(0);

        setForwardNavigationMode(MODE_CAN_CONTINUE);

        putDataEntry(KEY_ALLOW_STORAGE, false);
        putDataEntry(KEY_AUTO_LOGOUT, true);
        putDataEntry(KEY_CHG_FIRST_LOGON, -1);
        putDataEntry(KEY_CONFIRM, new char[0]);
        putDataEntry(KEY_GRACE_PERIOD, 7);
        putDataEntry(KEY_LOGIN_TIMEOUT, 5);
        putDataEntry(KEY_MAX_LOGON_ATTEMPTS, 3);
        putDataEntry(KEY_MIN_DIGITS, -1);
        putDataEntry(KEY_MIN_LENGTH, 8);
        putDataEntry(KEY_MIN_SYMBOLS, -1);
        putDataEntry(KEY_MIN_UPPERCASE, -1);
        putDataEntry(KEY_MUST_CHANGE, 99999);
        putDataEntry(KEY_PASSWORD, new char[0]);
        putDataEntry(KEY_REAL_NAME, "");
        putDataEntry(KEY_REQUIRE_LOGON, false);
        putDataEntry(KEY_USER_NAME, "");
        putDataEntry(KEY_WARN_PERIOD, 7);
    }

    @Override
    public void onExit() {

    }

    @Override
    protected String validateStep() {
        setForwardNavigationMode(MODE_CAN_CONTINUE);
        return null;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        helpScrollPane = new javax.swing.JScrollPane();
        helpEditorPane = new javax.swing.JEditorPane();

        setPreferredSize(new java.awt.Dimension(600, 400));

        helpScrollPane.setName("helpScrollPane"); // NOI18N

        helpEditorPane.setEditable(false);
        helpEditorPane.setContentType("text/html"); // NOI18N
        helpEditorPane.setEditorKit(new HTMLEditorKit());
        helpEditorPane.setName("helpEditorPane"); // NOI18N
        helpScrollPane.setViewportView(helpEditorPane);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(helpScrollPane,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        588, Short.MAX_VALUE)
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(helpScrollPane,
                                        javax.swing.GroupLayout.DEFAULT_SIZE,
                                        388, Short.MAX_VALUE)
                                .addContainerGap())
        );

        setPreferredSize(new Dimension(600, 400));
        setMaximumSize(getPreferredSize());

        getResourceMap().injectComponents(this);
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane helpEditorPane;
    private javax.swing.JScrollPane helpScrollPane;
    // End of variables declaration//GEN-END:variables
}
