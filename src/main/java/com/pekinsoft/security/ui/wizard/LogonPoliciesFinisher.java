/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   LogonPoliciesFinisher.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 7, 2024
 *  Modified   :   Nov 7, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 7, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui.wizard;

import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Logger;
import com.pekinsoft.security.*;
import com.pekinsoft.utils.DebugLogger;
import com.pekinsoft.utils.PreferenceKeys;
import com.pekinsoft.wizard.WizardFinisher;
import java.io.BufferedWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.Optional;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

/**
 * The {@code LogonPoliciesFinisher} is an implementation of the
 * {@link WizardFinisher} interface for setting the logon and password policies
 * for the {@link com.pekinsoft.framework.Application Application}.
 * <p>
 * This finisher takes the {@link java.util.Map dataMap's} values and creates
 * the required security subsystem files. If the settings dictate that users are
 * required to logon, this finisher will then force the created administrator
 * user to logon in order to establish the security subsystem's control over the
 * application and its files and directories.
 * <p>
 * If the settings do not dictate the requirement for users to logon, then the
 * default security model will be used, which will still create the required
 * security files, but with a default user and group, which will not require a
 * password to be entered. In this case, all users will be using the same user
 * account, and will have read/write/delete access to all files and directories
 * that are under the control of the application.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class LogonPoliciesFinisher
        implements WizardFinisher, PreferenceKeys, ValueKeys {

    public LogonPoliciesFinisher(ApplicationContext context) {
        this.context = context;
        logger = context.getLogger(LogonPoliciesFinisher.class);
    }

    /**
     * {@inheritDoc }
     * <p>
     * This implementation could possibly throw a {@link SecurityModelException}
     * wrapped into a {@link Runtime} exception if anything goes wrong while
     * installing the chosen security model.
     *
     * @param dataMap {@inheritDoc }
     *
     * @return {@inheritDoc }
     */
    @Override
    public BackgroundTask<?, ?> finish(Map<String, Object> dataMap) {
        DebugLogger.trace(logger, "Starting finish");

        DebugLogger.debug(logger,
                "Storing the dataMap values to the application's "
                + "preferences.");
        Preferences prefs = context.getPreferences(ApplicationContext.SYSTEM);

        prefs.putBoolean(PREF_REQUIRE_LOGON,
                (boolean) dataMap.get(KEY_REQUIRE_LOGON));
        prefs.putInt(PREF_PASSWORD_MIN_LENGTH,
                (int) dataMap.get(KEY_MIN_LENGTH));
        prefs.putInt(PREF_PASSWORD_MIN_NUMBERS,
                (int) dataMap.get(KEY_MIN_DIGITS));
        prefs.putInt(PREF_PASSWORD_MIN_UPPER,
                (int) dataMap.get(KEY_MIN_UPPERCASE));
        prefs.putInt(PREF_PASSWORD_MIN_SYMBOLS,
                (int) dataMap.get(KEY_MIN_SYMBOLS));
        prefs.putInt(PREF_PASSWORD_AGING_CHANGE_FIRST,
                (int) dataMap.get(KEY_CHG_FIRST_LOGON));
        prefs.putInt(PREF_PASSWORD_AGING_MUST_CHANGE,
                (int) dataMap.get(KEY_MUST_CHANGE));
        prefs.putInt(PREF_PASSWORD_AGING_WARN_PERIOD,
                (int) dataMap.get(KEY_WARN_PERIOD));
        prefs.putInt(PREF_PASSWORD_AGING_GRACE_PERIOD,
                (int) dataMap.get(KEY_GRACE_PERIOD));
        prefs.putBoolean(PREF_PASSWORD_ALLOW_STORAGE,
                (boolean) dataMap.get(KEY_ALLOW_STORAGE));
        prefs.putBoolean(PREF_AUTO_LOGOUT,
                (boolean) dataMap.get(KEY_AUTO_LOGOUT));
        prefs.putInt(PREF_LOGIN_TIMEOUT,
                (int) dataMap.get(KEY_LOGIN_TIMEOUT));
        prefs.putInt(PREF_MAX_LOGON_ATTEMPTS,
                (int) dataMap.get(KEY_MAX_LOGON_ATTEMPTS));
        try {
            prefs.flush();
        } catch (BackingStoreException e) {
            logger.warning(String.format("Unable to flush Preferences: [%s] %s",
                    e.getClass().getSimpleName(), e.getMessage()));
        } catch (Exception e) {
            logger.warning(String.format("Unable to flush Preferences: [%s] %s",
                    e.getClass().getSimpleName(), e.getMessage()));
        }

        if (!(boolean) dataMap.get(KEY_REQUIRE_LOGON)) {
            setupDefaultSecurity();
            DebugLogger.trace(logger, "Finishing LogonPoliciesFinisher.finish "
                    + "with the default security model.");
            return null;
        }

        setupEnhancedSecurity(dataMap);
        DebugLogger.trace(logger, "Finishing LogonPoliciesFinisher.finish with "
                + "the enhanced security model.");
        return null;
    }

    private void setupDefaultSecurity() {
        DebugLogger.trace(logger, "Starting setupDefaultSecurity");

        try {
            LocalStorage lst = context.getLocalStorage();

            BufferedWriter out = new BufferedWriter(lst.openFileWriter(
                    StorageLocations.CONFIG_DIR, "passwd", false));
            String defaultUser = context.getResourceMap()
                    .getString("Application.id");
            String contents = defaultUser + ":99:99:Default User:0\n";
            out.write(contents);
            out.flush();
            out.close();

            out = new BufferedWriter(lst.openFileWriter(
                    StorageLocations.CONFIG_DIR, "group", false));
            contents = defaultUser + ":99:" + defaultUser + "\n";
            out.write(contents);
            contents = "adm:100:" + defaultUser + "\n";
            out.write(contents);
            out.flush();
            out.close();

            out = new BufferedWriter(lst.openFileWriter(
                    StorageLocations.CONFIG_DIR, ".shadow", false));
            contents = defaultUser + ":::0:-1:-1:-1:-1:99999\n";
            out.write(contents);
            out.flush();
            out.close();
        } catch (IOException e) {
            String msg = String.format("Unable to install default security "
                    + "model: [%s] %s", e.getClass().getSimpleName(),
                    e.getMessage());
            logger.critical(msg, e);
            throw new RuntimeException(new DefaultSecurityModelException(msg, e));
        }

        DebugLogger.trace(logger, "Finished setupDefaultSecurity successfully");
    }

    private void setupEnhancedSecurity(Map<String, Object> dataMap) {
        DebugLogger.trace(logger, "Starting setupEnhancedSecurity");
        try {
            LocalStorage lst = context.getLocalStorage();

            DebugLogger.debug(logger, "Creating the passwd file in %s",
                    lst.getConfigDirectory());
            BufferedWriter out = new BufferedWriter(lst.openFileWriter(
                    StorageLocations.CONFIG_DIR, "passwd", false));

            String defaultUser = context.getResourceMap()
                    .getString("Application.id");
            StringBuilder contents = new StringBuilder();
            contents.append(defaultUser).append(":");
            contents.append(99).append(":");
            contents.append(99).append(":");
            contents.append("Application (required)").append(":0\n");
            contents.append(dataMap.get(KEY_USER_NAME)).append(":");
            contents.append(1000).append(":");
            contents.append(1000).append(":");
            contents.append(dataMap.get(KEY_REAL_NAME)).append(":");

            int strength = PasswordUtils.calculatStrength((char[]) dataMap.get(
                    KEY_PASSWORD));
            contents.append(strength).append("\n");
            DebugLogger.debug(logger, "Writing the following to the passwd "
                    + "file %s", contents.toString());
            out.write(contents.toString());
            out.flush();
            out.close();

            DebugLogger.debug(logger, "Creating the group file in %s",
                    lst.getConfigDirectory());
            out = new BufferedWriter(lst.openFileWriter(
                    StorageLocations.CONFIG_DIR, "group", false));
            contents = new StringBuilder();
            contents.append("adm:100:").append(defaultUser).append(",");
            contents.append(dataMap.get(KEY_USER_NAME)).append("\n");
            contents.append(dataMap.get(KEY_USER_NAME)).append(":1000:");
            contents.append(dataMap.get(KEY_USER_NAME)).append("\n");
            DebugLogger.debug(logger, "Writing the following to the group "
                    + "file %s", contents.toString());
            out.write(contents.toString());
            out.flush();
            out.close();

            DebugLogger.debug(logger, "Creating the .shadow file in %s",
                    lst.getConfigDirectory());
            out = new BufferedWriter(lst.openFileWriter(
                    StorageLocations.CONFIG_DIR, ".shadow", false));
            contents = new StringBuilder();
            contents.append(defaultUser).append(":::0:-1:-1:-1:-1:99999\n");
            contents.append(dataMap.get(KEY_USER_NAME)).append(":");
            Optional< String> salt = PasswordUtils.generateSalt(
                    PasswordUtils.generateRandomSaltLength());
            if (salt.get() == null) {
                throw new EnhancedSecurityModelException("Could not generate "
                        + "the salt for the password hash");
            }
            contents.append(salt.get()).append(":");
            Optional<String> hash = PasswordUtils.hashPassword(
                    String.valueOf((char[]) dataMap.get(KEY_PASSWORD)),
                    salt.get());
            if (hash.get() == null) {
                throw new EnhancedSecurityModelException(String.format(
                        "Could not hash the password using the salt \"%s\".",
                        salt.get()));
            }
            contents.append(hash.get()).append(":");
            contents.append(LocalDate.now().toEpochDay()).append(":");
            contents.append(dataMap.get(KEY_CHG_FIRST_LOGON)).append(":");
            contents.append(dataMap.get(KEY_MUST_CHANGE)).append(":");
            contents.append(dataMap.get(KEY_WARN_PERIOD)).append(":");
            contents.append(dataMap.get(KEY_GRACE_PERIOD)).append(":");
            Integer mustChange = (Integer) dataMap.get(KEY_MUST_CHANGE);
            long changeDate = ChronoUnit.DAYS.between(LocalDate.EPOCH,
                    LocalDate.now()) + mustChange.longValue();
            contents.append(changeDate).append("\n");
            DebugLogger.debug(logger, "Writing the following to the .shadow "
                    + "file %s", contents.toString());
            out.write(contents.toString());
            out.flush();
            out.close();
        } catch (EnhancedSecurityModelException | IOException e) {
            if (e instanceof EnhancedSecurityModelException) {
                // Convert to RuntimeException and throw as is.
                throw new RuntimeException(e);
            }
            String msg = String.format("Unable to install enhanced security "
                    + "model: [%s] %s", e.getClass().getSimpleName(),
                    e.getMessage());
            logger.critical(msg, e);
            throw new RuntimeException(
                    new EnhancedSecurityModelException(msg, e));
        }
        DebugLogger.trace(logger, "Finished setupEnhancedSecurity");
    }

    private final ApplicationContext context;
    private final Logger logger;

}
