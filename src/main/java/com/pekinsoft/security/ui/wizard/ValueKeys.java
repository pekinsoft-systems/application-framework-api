/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   ValueKeys.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 8, 2024
 *  Modified   :   Nov 8, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 8, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui.wizard;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
interface ValueKeys {

    // Require users to logon
    static final String KEY_REQUIRE_LOGON = "require.logon";

    // Password policies
    static final String KEY_MIN_LENGTH = "minimum.length";
    static final String KEY_MIN_UPPERCASE = "minimum.uppercase";
    static final String KEY_MIN_DIGITS = "minimum.digits";
    static final String KEY_MIN_SYMBOLS = "minimum.symbols";
    static final String KEY_ALLOW_STORAGE = "allow.secure.storage";
    static final String KEY_MAX_LOGON_ATTEMPTS = "maximum.logon.attempts";

    // Password aging
    static final String KEY_CHG_FIRST_LOGON = "must.change.at.first.logon";
    static final String KEY_MUST_CHANGE = "must.change.by";
    static final String KEY_WARN_PERIOD = "warning.period.begins";
    static final String KEY_GRACE_PERIOD = "grace.period.until";
    static final String KEY_AUTO_LOGOUT = "perform.auto.logout";
    static final String KEY_LOGIN_TIMEOUT = "perform.auto.logout.after";

    // Adminstrator account
    static final String KEY_REAL_NAME = "administrator.real.name";
    static final String KEY_USER_NAME = "administrator.username";
    static final String KEY_PASSWORD = "administrator.password";
    static final String KEY_CONFIRM = "administrator.password.confirmation";
}
