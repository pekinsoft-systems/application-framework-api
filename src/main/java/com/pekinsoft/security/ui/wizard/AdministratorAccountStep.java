/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   AdministratorAccountStep.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 13, 2024
 *  Modified   :   Nov 13, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 13, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui.wizard;

import com.pekinsoft.desktop.support.TextSelectionListener;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.security.PasswordUtils;
import com.pekinsoft.security.ui.support.PasswordStrengthListener;
import com.pekinsoft.security.ui.support.StrengthMeter;
import com.pekinsoft.wizard.WizardStep;
import java.awt.Dimension;
import java.awt.event.FocusListener;
import java.util.Arrays;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.html.HTMLEditorKit;

/**
 *
 * @author Sean Carrick
 */
public class AdministratorAccountStep extends WizardStep implements ValueKeys {

    /**
     * Creates new form AdminstratorAccountStep
     */
    public AdministratorAccountStep(ApplicationContext context) {
        super(context);
        initComponents();
    }

    @Override
    public void onEnter() {
        setForwardNavigationMode(MODE_NONE);
        helpEditorPane.setCaretPosition(0);
        // Initialize the fields for the PasswordStrengthListener.
        if (getDataMap() != null) {
            minLength = (getDataEntry(KEY_MIN_LENGTH) != null)
                        ? (int) getDataEntry(KEY_MIN_LENGTH)
                        : minLength;
            minDigits = (getDataEntry(KEY_MIN_DIGITS) != null)
                        ? (int) getDataEntry(KEY_MIN_DIGITS)
                        : minDigits;
            minUppers = (getDataEntry(KEY_MIN_UPPERCASE) != null)
                        ? (int) getDataEntry(KEY_MIN_UPPERCASE)
                        : minUppers;
            minSymbols = (getDataEntry(KEY_MIN_SYMBOLS) != null)
                         ? (int) getDataEntry(KEY_MIN_SYMBOLS)
                         : minSymbols;
        }
        passwordStrength = new PasswordStrengthListener(getContext(),
                passwordField, strengthMeter, minLength, minDigits, minUppers,
                minSymbols);

        confirmField.addFocusListener(textSelector);
        confirmField.getDocument().addDocumentListener(validator);
        passwordField.addFocusListener(textSelector);
        passwordField.getDocument().addDocumentListener(validator);
        passwordField.getDocument().addDocumentListener(passwordStrength);
        realNameField.addFocusListener(textSelector);
        realNameField.getDocument().addDocumentListener(validator);
        usernameField.addFocusListener(textSelector);
        usernameField.getDocument().addDocumentListener(validator);
    }

    @Override
    public void onExit() {
        confirmField.removeFocusListener(textSelector);
        confirmField.getDocument().removeDocumentListener(validator);
        passwordField.removeFocusListener(textSelector);
        passwordField.getDocument().removeDocumentListener(validator);
        passwordField.getDocument().removeDocumentListener(passwordStrength);
        realNameField.removeFocusListener(textSelector);
        realNameField.getDocument().removeDocumentListener(validator);
        usernameField.removeFocusListener(textSelector);
        usernameField.getDocument().removeDocumentListener(validator);
    }

    @Override
    protected String validateStep() {
        String problem = null;

        int minLen = (getDataMap() != null && getDataEntry(KEY_MIN_LENGTH) != null)
                     ? (int) getDataEntry(KEY_MIN_LENGTH) : -1;
        int minDigits = (getDataMap() != null && getDataEntry(KEY_MIN_DIGITS) != null)
                        ? (int) getDataEntry(KEY_MIN_DIGITS) : -1;
        int minSymbols = (getDataMap() != null && getDataEntry(KEY_MIN_SYMBOLS) != null)
                         ? (int) getDataEntry(KEY_MIN_SYMBOLS) : -1;
        int minUppers = (getDataMap() != null && getDataEntry(KEY_MIN_UPPERCASE) != null)
                        ? (int) getDataEntry(KEY_MIN_UPPERCASE) : -1;

        if (realNameField.getText() == null || realNameField.getText().isEmpty()) {
            problem = getResourceMap().getString("problem.realname.required");
        } else if (usernameField.getText() == null || usernameField.getText()
                .isEmpty()) {
            problem = getResourceMap().getString("problem.username.required");
        } else if (!PasswordUtils.isPasswordValid(passwordField.getPassword(),
                minLen,
                minUppers, minDigits, minSymbols)) {
            problem = getResourceMap().getString("password.not.valid", minLen,
                    minUppers, minDigits, minSymbols);
        } else if (!passwordsMatch()) {
            problem = getResourceMap()
                    .getString("problem.passwords.not.matched");
        }

        if (problem == null) {
            setForwardNavigationMode(MODE_CAN_CONTINUE_OR_FINISH);
            setComplete(true);

            putDataEntry(KEY_REAL_NAME, realNameField.getText());
            putDataEntry(KEY_USER_NAME, usernameField.getText());
            putDataEntry(KEY_PASSWORD, passwordField.getPassword());
            putDataEntry("strength", strengthMeter.getStrength());
        } else {
            setForwardNavigationMode(MODE_NONE);
            setComplete(false);
        }

        return problem;
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        helpScrollPane = new javax.swing.JScrollPane();
        helpEditorPane = new javax.swing.JEditorPane();
        realNameLabel = new javax.swing.JLabel();
        realNameField = new javax.swing.JTextField();
        usernameLabel = new javax.swing.JLabel();
        usernameField = new javax.swing.JTextField();
        passwordLabel = new javax.swing.JLabel();
        passwordField = new javax.swing.JPasswordField();
        strengthMeter = new StrengthMeter();
        confirmLabel = new javax.swing.JLabel();
        confirmField = new javax.swing.JPasswordField();

        helpScrollPane.setName("helpScrollPane"); // NOI18N

        helpEditorPane.setName("helpEditorPane"); // NOI18N
        helpEditorPane.setEditorKit(new HTMLEditorKit());
        helpEditorPane.setEditable(false);
        helpScrollPane.setViewportView(helpEditorPane);

        realNameLabel.setText("full name");
        realNameLabel.setName("realNameLabel"); // NOI18N

        realNameField.setName("realNameField"); // NOI18N

        usernameLabel.setText("username");
        usernameLabel.setName("usernameLabel"); // NOI18N

        usernameField.setName("usernameField"); // NOI18N

        passwordLabel.setText("password");
        passwordLabel.setName("passwordLabel"); // NOI18N

        passwordField.setName("passwordField"); // NOI18N

        strengthMeter.setName("strengthMeter"); // NOI18N

        confirmLabel.setText("confirm");
        confirmLabel.setName("confirmLabel"); // NOI18N

        confirmField.setName("confirmField"); // NOI18N

        strengthMeter.setName("strengthMeter");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(
                                        javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(helpScrollPane)
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.TRAILING)
                                                        .addComponent(
                                                                passwordLabel)
                                                        .addGroup(layout
                                                                .createSequentialGroup()
                                                                .addComponent(
                                                                        realNameLabel)
                                                                .addPreferredGap(
                                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                                .addComponent(
                                                                        realNameField,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                        226,
                                                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                                                .addGap(18, 18,
                                                                        18)
                                                                .addComponent(
                                                                        usernameLabel))
                                                        .addComponent(
                                                                confirmLabel))
                                                .addPreferredGap(
                                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(layout
                                                        .createParallelGroup(
                                                                javax.swing.GroupLayout.Alignment.LEADING,
                                                                false)
                                                        .addComponent(
                                                                strengthMeter,
                                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                                168,
                                                                Short.MAX_VALUE)
                                                        .addComponent(
                                                                usernameField,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                                123,
                                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addComponent(
                                                                passwordField)
                                                        .addComponent(
                                                                confirmField))
                                                .addGap(0, 44, Short.MAX_VALUE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(
                        javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(helpScrollPane,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        187,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(
                                        javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(realNameLabel)
                                        .addComponent(realNameField,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(usernameLabel)
                                        .addComponent(usernameField,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(
                                        javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(passwordLabel)
                                        .addComponent(passwordField,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(strengthMeter,
                                        javax.swing.GroupLayout.PREFERRED_SIZE,
                                        4,
                                        javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(
                                        javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(confirmLabel)
                                        .addComponent(confirmField,
                                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                                javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(92, Short.MAX_VALUE))
        );

        setPreferredSize(new Dimension(600, 400));
        setMaximumSize(getPreferredSize());

        getResourceMap().injectComponents(this);
    }// </editor-fold>//GEN-END:initComponents

    private boolean passwordsMatch() {
        return Arrays.equals(passwordField.getPassword(),
                confirmField.getPassword());
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField confirmField;
    private javax.swing.JLabel confirmLabel;
    private javax.swing.JEditorPane helpEditorPane;
    private javax.swing.JScrollPane helpScrollPane;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JTextField realNameField;
    private javax.swing.JLabel realNameLabel;
    private StrengthMeter strengthMeter;
    private javax.swing.JTextField usernameField;
    private javax.swing.JLabel usernameLabel;
    // End of variables declaration//GEN-END:variables

    private final FocusListener textSelector = new TextSelectionListener();
    private final DocumentListener validator = new ValidationListener();
    private DocumentListener passwordStrength;

    private int minLength = -1;
    private int minDigits = -1;
    private int minUppers = -1;
    private int minSymbols = -1;

    private class ValidationListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            validateAndNotify();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            validateAndNotify();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            validateAndNotify();
        }

    }

}
