/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   PasswordChangeDialog.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 9, 2024
 *  Modified   :   Nov 9, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 9, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui;

import com.pekinsoft.api.WindowManager;
import com.pekinsoft.framework.*;
import com.pekinsoft.security.LogonManager;
import com.pekinsoft.security.PasswordUtils;
import com.pekinsoft.security.ui.support.PasswordStrengthListener;
import com.pekinsoft.security.ui.support.StrengthMeter;
import com.pekinsoft.utils.MessageBox;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class PasswordChangeDialog extends JDialog {

    public PasswordChangeDialog(ApplicationContext context) {
        super(WindowManager.getDefault().getMainFrame(), true);

        this.context = context;
        this.resourceMap = context.getResourceMap(getClass());

        initComponents();
    }

    private boolean isPasswordValid() {
        return passwordValid;
    }

    private void setPasswordValid(boolean passwordValid) {
        boolean old = isPasswordValid();
        this.passwordValid = passwordValid;
        firePropertyChange("passwordValid", old, isPasswordValid());
    }

    @AppAction(enabledProperty = "passwordValid")
    public void changePassword(ActionEvent evt) {
        LogonManager.getInstance().changePasswordForUser(
                LogonManager.getInstance().getLoggedOnUser(),
                passwordField.getPassword());

        dispose();
    }

    @AppAction
    public void doCancel(ActionEvent evt) {
        String message = resourceMap.getString("cancel.confirm.message");
        String title = resourceMap.getString("cancel.confirm.title");
        int option = MessageBox.askQuestion(this, message, title, false);
        if (option == MessageBox.YES_OPTION) {
            dispose();
        }
    }

    private void initComponents() {
        oldPasswordLabel = new JLabel();
        passwordLabel = new JLabel();
        confirmLabel = new JLabel();
        oldPasswordField = new JPasswordField(20);
        passwordField = new JPasswordField(20);
        confirmField = new JPasswordField(20);
        strengthMeter = new StrengthMeter();
        okButton = new JButton();
        cancelButton = new JButton();

        oldPasswordLabel.setName("oldPasswordLabel");
        oldPasswordLabel.setText(resourceMap.getString("oldPasswordLabel.text"));
        passwordLabel.setName("passwordLabel");
        passwordLabel.setText(resourceMap.getString("passwordLabel.text"));
        confirmField.setName("confirmField");
        confirmField.setText(resourceMap.getString("confirmField.text"));
        oldPasswordField.setName("oldPasswordField");
        passwordField.setName("passwordField");
        confirmField.setName("confirmField");
        okButton.setName("okButton");
        okButton.setAction(context.getActionMap(getClass(), this).get(
                "changePassword"));
        cancelButton.setName("cancelButton");
        cancelButton.setAction(context.getActionMap(getClass(), this).get(
                "doCancel"));

        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.insets = new Insets(5, 5, 5, 5);
        gbc.fill = GridBagConstraints.HORIZONTAL;

        gbc.gridx = 0;
        gbc.gridy = 0;
        add(oldPasswordLabel, gbc);

        gbc.gridx = 1;
        add(oldPasswordField, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        add(new JSeparator(SwingConstants.HORIZONTAL), gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        add(passwordLabel, gbc);

        gbc.gridx = 1;
        add(passwordField, gbc);

        gbc.gridy = 3;
        gbc.gridx = 1;
        add(strengthMeter, gbc);

        gbc.gridy = 4;
        gbc.gridx = 0;
        add(confirmLabel, gbc);

        gbc.gridx = 1;
        add(confirmField, gbc);

        gbc.gridy = 5;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        add(new JSeparator(SwingConstants.HORIZONTAL), gbc);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING));
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

        gbc.gridy = 6;
        gbc.gridx = 0;
        gbc.gridwidth = 2;
        add(buttonPanel, gbc);

        passwordField.getDocument().addDocumentListener(
                new PasswordStrengthListener(context, passwordField,
                        strengthMeter));
        confirmField.getDocument().addDocumentListener(
                new ConfirmationListener());

        resourceMap.injectComponents(this);
    }

    private void validatePassword() {
        setPasswordValid(PasswordUtils.isPasswordValid(
                passwordField.getPassword())
                && Arrays.equals(passwordField.getPassword(),
                        confirmField.getPassword()));
    }

    private final ApplicationContext context;
    private final ResourceMap resourceMap;

    private JLabel oldPasswordLabel;
    private JLabel passwordLabel;
    private JLabel confirmLabel;
    private JPasswordField oldPasswordField;
    private JPasswordField passwordField;
    private JPasswordField confirmField;
    private StrengthMeter strengthMeter;
    private JButton okButton;
    private JButton cancelButton;

    private boolean passwordValid = false;

    private class ConfirmationListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent e) {
            validatePassword();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            validatePassword();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            validatePassword();
        }

    }

}
