/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   PasswordStrengthListener.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 9, 2024
 *  Modified   :   Nov 9, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 9, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui.support;

import com.pekinsoft.framework.*;
import com.pekinsoft.security.PasswordUtils;
import com.pekinsoft.utils.PreferenceKeys;
import java.util.prefs.Preferences;
import javax.swing.JPasswordField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class PasswordStrengthListener implements DocumentListener,
        PreferenceKeys {

    /**
     * Constructs a new {@code PasswordStrengthListener} with all required
     * values being supplied.
     *
     * @param context       the {@link ApplicationContext} in which this is
     *                      being used
     * @param passwordField the password field to test the strength of the
     *                      password
     * @param strengthMeter the {@link StrengthMeter} in which to display the
     *                      password strength
     * @param minLength     the minimum required length
     * @param minDigits     the minimum required digits
     * @param minUppers     the minimum required uppercase letters
     * @param minSymbols    the minimum required special characters
     */
    public PasswordStrengthListener(ApplicationContext context,
            JPasswordField passwordField, StrengthMeter strengthMeter,
            int minLength, int minDigits, int minUppers, int minSymbols) {
        this.context = context;
        this.resourceMap = context.getResourceMap(getClass());
        this.passwordField = passwordField;
        this.strengthMeter = strengthMeter;
        this.minDigits = minDigits;
        this.minLength = minLength;
        this.minSymbols = minSymbols;
        this.minUppers = minUppers;
    }

    /**
     * Convenience constructor to create a new {@code PasswordStrengthListener}
     * that uses the minimum value requirements from the
     * {@link Application Application's} stored {@link Preferences}.
     *
     * @param context       the {@link ApplicationContext} in which this is
     *                      being used
     * @param passwordField the password field to test the strength of the
     *                      password
     * @param strengthMeter the {@link StrengthMeter} in which to display the
     *                      password strength
     */
    public PasswordStrengthListener(ApplicationContext context,
            JPasswordField passwordField, StrengthMeter strengthMeter) {
        this(context, passwordField, strengthMeter,
                context.getPreferences(ApplicationContext.SYSTEM)
                        .getInt(PREF_PASSWORD_MIN_LENGTH, -1),
                context.getPreferences(ApplicationContext.SYSTEM)
                        .getInt(PREF_PASSWORD_MIN_NUMBERS, -1),
                context.getPreferences(ApplicationContext.SYSTEM)
                        .getInt(PREF_PASSWORD_MIN_UPPER, -1),
                context.getPreferences(ApplicationContext.SYSTEM)
                        .getInt(PREF_PASSWORD_MIN_SYMBOLS, -1));
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        updateStrength();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateStrength();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        updateStrength();
    }

    private void updateStrength() {
        char[] passwd = passwordField.getPassword();
        int strength = PasswordUtils.calculatStrength(passwd, minLength,
                minDigits, minUppers, minSymbols);
        strengthMeter.setStrength(strength);
    }

    private final ApplicationContext context;
    private final ResourceMap resourceMap;
    private final JPasswordField passwordField;
    private final StrengthMeter strengthMeter;
    private final int minLength;
    private final int minDigits;
    private final int minUppers;
    private final int minSymbols;

}
