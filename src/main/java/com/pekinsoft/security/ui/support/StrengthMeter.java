/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   StrengthMeter.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 9, 2024
 *  Modified   :   Nov 9, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 9, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui.support;

import java.awt.*;
import javax.swing.JComponent;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class StrengthMeter extends JComponent {

    public StrengthMeter() {

    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        // Just to be sure that base is not less than zero, nor greater than 100
        this.base = Math.max(0, Math.min(100, base));
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        // Just to be sure that strength does not exceed 100:
        this.strength = Math.max(0, Math.min(100, strength));
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g.create();

        // Calculate width of filled portion
        int width = (int) (getWidth() * (strength / 100.0));

        // Determine color based on strength
        Color fillColor = calculateStrengthColor(strength);

        // Set color and fill the strength meter
        g2.setColor(fillColor);
        g2.fillRect(0, 0, width, getHeight());

        // Draw border
        g2.setColor(Color.black);
        g2.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(20, 8);
    }

    private Color calculateStrengthColor(int value) {
        if (value < 10) {
            return new Color(139, 0, 0); // dark red
        } else if (value < 45) {
            // Fade from red to yellow
            float ratio = (value - 10) / 35.0f;
            return interpolateColor(new Color(139, 0, 0), Color.yellow, ratio);
        } else if (value < 55) {
            return Color.yellow;    // Solid yellow
        } else if (value < 90) {
            // Fade from yellow to dark green
            float ratio = (value - 55) / 35.0f;
            return interpolateColor(Color.yellow, new Color(0, 100, 0), ratio);
        } else {
            return new Color(0, 100, 0);    // dark green
        }
    }

    private Color interpolateColor(Color start, Color end, float ratio) {
        int red = (int) (start.getRed() * (1 - ratio) + end.getRed() * ratio);
        int green = (int) (start.getGreen() * (1 - ratio) + end.getGreen() * ratio);
        int blue = (int) (start.getBlue() * (1 - ratio) + end.getBlue() * ratio);
        return new Color(red, green, blue);
    }

    private int strength;
    private int base;

}
