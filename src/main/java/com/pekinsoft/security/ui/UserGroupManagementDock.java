/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   UserGroupManagementDock.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 9, 2024
 *  Modified   :   Nov 9, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 9, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security.ui;

import com.pekinsoft.desktop.Dockable;
import com.pekinsoft.framework.ApplicationContext;
import com.pekinsoft.security.SystemUser;
import com.pekinsoft.security.UserGroup;
import java.util.*;

/**
 *
 * @author Sean Carrick
 */
public abstract class UserGroupManagementDock extends Dockable {

    /**
     * Creates new form UserGroupManagementDock
     */
    protected UserGroupManagementDock(ApplicationContext context,
            List<SystemUser> usersList, List<UserGroup> groupsList) {
        super(context);
        this.usersList = usersList;
        this.groupsList = groupsList;

        initComponents();
    }

    /**
     * Adds a {@link SystemUser user} to the user registry.
     * <p>
     * If the specified {@code user} is {@code null} or already in the registry,
     * no exception is thrown and no action is taken.
     * <p>
     * This is a bound property.
     *
     * @param user the user to be added
     */
    protected void addUser(SystemUser user) {
        if (user == null || usersList.contains(user)) {
            return;
        }

        List<SystemUser> old, nue;
        synchronized (usersList) {
            old = Arrays.asList(getUsers());
            this.usersList.add(user);
            nue = Arrays.asList(getUsers());
        }

        firePropertyChange("users", old, nue);
    }

    /**
     * Removes a {@link SystemUser user} from the user registry.
     * <p>
     * If the specified {@code user} is {@code null} or not in the registry, no
     * exception is thrown and no action is taken.
     * <p>
     * This is a bound property.
     *
     * @param user the user to be removed
     */
    protected void removeUser(SystemUser user) {
        if (user == null || !usersList.contains(user)) {
            return;
        }

        List<SystemUser> old, nue;
        synchronized (usersList) {
            old = Arrays.asList(getUsers());
            this.usersList.remove(user);
            nue = Arrays.asList(getUsers());
        }

        firePropertyChange("users", old, nue);
    }

    /**
     * Retrieves an array of all {@link SystemUser users} currently registered.
     *
     * @return all users
     */
    protected SystemUser[] getUsers() {
        return usersList.toArray(SystemUser[]::new);
    }

    /**
     * Adds a {@link UserGroup group} to the registry.
     * <p>
     * If the specified {@code group} is {@code null} or is already registered,
     * no exception is thrown and no action is taken.
     * <p>
     * This is a bound property.
     *
     * @param group the group to be added
     */
    protected void addGroup(UserGroup group) {
        if (group == null || groupsList.contains(group)) {
            return;
        }

        List<UserGroup> old, nue;
        synchronized (groupsList) {
            old = Arrays.asList(getGroups());
            this.groupsList.add(group);
            nue = Arrays.asList(getGroups());
        }

        firePropertyChange("groups", old, nue);
    }

    /**
     * Removes a {@link UserGroup group} from the groups registry.
     * <p>
     * If the specified {@code group} is {@code null} or not in the registry, no
     * exception is thrown and no action is taken.
     * <p>
     * This is a bound property.
     *
     * @param group the group to be removed
     */
    protected void removeGroup(UserGroup group) {
        if (group == null || !groupsList.contains(group)) {
            return;
        }

        List<UserGroup> old, nue;

        synchronized (groupsList) {
            old = Arrays.asList(getGroups());
            this.groupsList.remove(group);
            nue = Arrays.asList(getGroups());
        }

        firePropertyChange("groups", old, nue);
    }

    /**
     * Retrieves an array of all {@link UserGroup groups} currently registered.
     *
     * @return all groups
     */
    protected UserGroup[] getGroups() {
        return groupsList.toArray(UserGroup[]::new);
    }

    /**
     * This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        userPanel = new javax.swing.JPanel();
        userNameLabel = new javax.swing.JLabel();
        usernameField = new javax.swing.JComboBox<>();
        groupPanel = new javax.swing.JPanel();

        userPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("user management"));
        userPanel.setName("userPanel"); // NOI18N

        userNameLabel.setText("username");
        userNameLabel.setName("userNameLabel"); // NOI18N

        List<String> userNames = new ArrayList<>();
        userNames.add(getResourceMap().getString("new.user.text"));
        for (SystemUser user : usersList) {
            userNames.add(user.getUserName());
        }
        usernameField.setModel(new javax.swing.DefaultComboBoxModel<>(userNames.toArray(String[]::new)));
        usernameField.setName("usernameField"); // NOI18N

        javax.swing.GroupLayout userPanelLayout = new javax.swing.GroupLayout(userPanel);
        userPanel.setLayout(userPanelLayout);
        userPanelLayout.setHorizontalGroup(
            userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(userPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(userNameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(usernameField, javax.swing.GroupLayout.PREFERRED_SIZE, 167, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(612, Short.MAX_VALUE))
        );
        userPanelLayout.setVerticalGroup(
            userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(userPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(userPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userNameLabel)
                    .addComponent(usernameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(271, Short.MAX_VALUE))
        );

        groupPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("group management"));
        groupPanel.setName("groupPanel"); // NOI18N

        javax.swing.GroupLayout groupPanelLayout = new javax.swing.GroupLayout(groupPanel);
        groupPanel.setLayout(groupPanelLayout);
        groupPanelLayout.setHorizontalGroup(
            groupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        groupPanelLayout.setVerticalGroup(
            groupPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 252, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(userPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(groupPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(userPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(groupPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel groupPanel;
    private javax.swing.JLabel userNameLabel;
    private javax.swing.JPanel userPanel;
    private javax.swing.JComboBox<String> usernameField;
    // End of variables declaration//GEN-END:variables

    private final List<SystemUser> usersList;
    private final List<UserGroup> groupsList;

}
