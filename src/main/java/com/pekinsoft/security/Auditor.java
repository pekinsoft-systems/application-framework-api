/*
 * Copyright (C) 2024 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * *****************************************************************************
 *  Project    :   application-framework-api
 *  Class      :   Auditor.java
 *  Author     :   Sean Carrick
 *  Created    :   Nov 8, 2024
 *  Modified   :   Nov 8, 2024
 *
 *  Purpose: See class JavaDoc for explanation
 *
 *  Revision History:
 *
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Nov 8, 2024  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package com.pekinsoft.security;

import com.pekinsoft.desktop.error.ErrorInfo;
import com.pekinsoft.framework.*;
import com.pekinsoft.logging.Level;
import com.pekinsoft.logging.Logger;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

/**
 * The {@code Auditor} class provides a means of filing audit logs to a special
 * log file, which is hidden and encrypted. The Audit Log is only viewable by a
 * systems or application administrator via an interface in the application.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class Auditor {

    private static Auditor instance = null;

    public static Auditor getInstance() {
        if (instance == null) {
            try {
                instance = new Auditor();
            } catch (Exception e) {
                logger.log(Level.CRITICAL, "Failed to initialize Auditor", e);
            }
        }

        return instance;
    }

    /**
     * Files an Audit Log entry for a security event.
     *
     * @param auditInfo an {@link ErrorInfo} instance that provides all audit
     *                  information in an encapsulated method
     */
    public void log(AuditInfo auditInfo) {
        try (OutputStream out = Files.newOutputStream(auditLogPath,
                StandardOpenOption.APPEND)) {
            byte[] data = encryptAuditInfo(auditInfo);
            out.write(data);
            out.write(System.lineSeparator().getBytes());
        } catch (IOException | GeneralSecurityException e) {
            logger.log(Level.WARNING, "Failed to log audit entry", e);
        }
    }

    public List<AuditInfo> readLogs() throws IOException {
        List<AuditInfo> logs = new ArrayList<>();

        try (InputStream in = Files.newInputStream(auditLogPath)) {
            CipherInputStream cipherIn = new CipherInputStream(in,
                    initDecryptionCipher());
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            cipherIn, StandardCharsets.UTF_8));

            String line;
            while ((line = reader.readLine()) != null) {
                logs.add(parseAuditInfo(line));
            }
        } catch (GeneralSecurityException e) {
            logger.log(Level.WARNING, "Failed to decrypt audit log", e);
        }

        return logs;
    }

    private Auditor() throws Exception {
        context = Application.getInstance().getContext();
        // Define where the autit log file is stored, for example in the config
        //+ directory.
        this.auditLogPath = context.getLocalStorage().getFilePath(
                StorageLocations.CONFIG_DIR, "audit.log.enc");
        this.cipher = initCipher(); // Initialize cipher for encryption
        ensureLogFileExists();
    }

    private byte[] encryptAuditInfo(AuditInfo auditInfo)
            throws GeneralSecurityException, IOException {
        byte[] data = auditInfo.toString().getBytes();
        return cipher.doFinal(data);
    }

    private Cipher initCipher() throws Exception {
        SecretKey key = new SecretKeySpec(LogonManager.getInstance()
                .getApplicationOwnerSalt().getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher;
    }

    private Cipher initDecryptionCipher() throws GeneralSecurityException {
        SecretKey key = new SecretKeySpec(LogonManager.getInstance()
                .getApplicationOwnerSalt().getBytes(), "AES");
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, key);
        return cipher;
    }

    private void ensureLogFileExists() throws IOException {
        if (Files.notExists(auditLogPath)) {
            Files.createFile(auditLogPath);
            Files.setAttribute(auditLogPath, "dos:hidden", true); // Win-Specific
        }
    }

    private AuditInfo parseAuditInfo(String data) {
        // Implement parsing logic based on AuditInfo's toString format or fields.
        return AuditInfo.parse(data);
    }

    private static final Logger logger = Application.getInstance().getContext()
            .getLogger(Auditor.class);
    private final ApplicationContext context;
    private final Path auditLogPath;
    private final Cipher cipher;

}
