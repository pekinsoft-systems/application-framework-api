/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework
 *  Class      :   ValueIsWithinRangeInclusiveVerifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 23, 2022
 *  Modified   :   Dec 23, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 23, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.verifiers;

import java.awt.Color;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.3
 * @since 1.0
 */
public class ValueIsWithinRangeInclusiveVerifier extends InputVerifier {

    private final JLabel problemLabel;
    private final String errorMessage;
    private final IsAnIntegerVerifier isInteger;
    private Color error = new Color(1.0f, 0.832f, 0.832f);
    private Number min, max;
    
    public ValueIsWithinRangeInclusiveVerifier (Number min, Number max,
            JLabel problemLabel, String errorMessage) {
        isInteger = new IsAnIntegerVerifier(problemLabel, errorMessage);
        this.problemLabel = problemLabel;
        this.errorMessage = errorMessage;
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean verify(JComponent input) {
        boolean valid = isInteger.verify(input);    // Test this first.
        if (valid && input != null) {
            if (input instanceof JTextComponent) {
                String toVerify = ((JTextComponent) input).getText();
                
                int value = Integer.parseInt(toVerify);
                valid = (value >= min.doubleValue()) && (value <= max.doubleValue());
            }
            if (!valid) {
                input.setBackground(error);
            } else {
                Color bg = UIManager.getColor("TextField.background");
                input.setBackground(bg == null ? UIManager.getColor("text") : bg);
            }
        }
        
        return valid;
    }

}
