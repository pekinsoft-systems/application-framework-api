/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework
 *  Class      :   FEINVerifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 21, 2022
 *  Modified   :   Dec 21, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 21, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.verifiers;

import java.awt.Color;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.3
 * @since 1.0
 */
public class FEINVerifier extends InputVerifier {
    
    private final Color error = new Color(1.0f, 0.832f, 0.832f);
    private final JLabel problemLabel;
    private final String errorMessage;
    private final String regex = "^(01|02|03|04|05|06|10|11|12|13|14|15|16|20|"
            + "21|22|23|24|25|26|27|30|32|33|34|35|36|37|38|39|40|41|42|43|44|"
            + "45|46|46|47|48|50|51|52|53|54|55|56|57|58|59|60|61|62|63|64|65|"
            + "66|67|68|71|72|73|74|75|76|77|80|81|82|83|84|85|85|86|86|87|87|"
            + "88|88|90|91|92|92|93|94|95|98|99|)-\\d{7}$";
    private final Pattern pattern;
    private Matcher matcher;
    
    public FEINVerifier (JLabel problemLabel, String errorMessage) {
        this.problemLabel = problemLabel;
        this.errorMessage = errorMessage;
        pattern = Pattern.compile(regex);
    }

    @Override
    public boolean verify(JComponent input) {
        boolean valid = true;   // Assume valid
        
        if (input != null) {
            if (input instanceof JTextComponent) {
                String toVerify = ((JTextComponent) input).getText();
                valid = toVerify.trim().isEmpty();
                if (!valid) {
                    matcher = pattern.matcher(toVerify);
                    valid = matcher.matches();
                } 
            }
            if (!valid) {
                input.setBackground(error);
                if (problemLabel != null && !errorMessage.trim().isEmpty()) {
                    problemLabel.setText(errorMessage);
                }
            } else {
                Color bg = UIManager.getColor("TextField.background");
                input.setBackground(bg == null ? UIManager.getColor("text") : bg);
                if (problemLabel != null) {
                    problemLabel.setText("");
                }
            }
        }
        
        return valid;
    }

}
