/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework
 *  Class      :   USStateCanadianProvinceVerifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 21, 2022
 *  Modified   :   Dec 21, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 21, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.verifiers;

import java.awt.Color;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.3
 * @since 1.0
 */
public class USStateCanadianProvinceVerifier extends InputVerifier {
    
    private final JLabel problemLabel;
    private final String errorMessage;
    private Color error = new Color(1.0f, 0.832f, 0.832f);
    
    public USStateCanadianProvinceVerifier (JLabel problemLabel, String errorMessage) {
        this.problemLabel = problemLabel;
        this.errorMessage = errorMessage;
    }

    @Override
    public boolean verify(JComponent input) {
        boolean valid = true;  // Assume valid
        if (input != null) {
            if (input instanceof JTextComponent) {
                String toVerify = ((JTextComponent) input).getText();
                if (toVerify.trim().isEmpty()) {
                    valid = false;
                }
                if (toVerify.length() < 2 || toVerify.length() > 2) {
                    valid = false;
                }
                if (valid) {
                    valid = isValidStateOrProvince(toVerify);
                }
            }
            if (!valid) {
                input.setBackground(error);
                if (problemLabel != null && !errorMessage.trim().isEmpty()) {
                    problemLabel.setText(errorMessage);
                }
            } else {
                Color bg = UIManager.getColor("TextField.background");
                input.setBackground(bg == null ? UIManager.getColor("text") : bg);
                if (problemLabel != null) {
                    problemLabel.setText("");
                }
            }
        }
        return valid;
    }
    
    private boolean isValidStateOrProvince(String model) {
        for (String p : provinces) {
            if (model.toUpperCase().equals(p)) return true;
        }
        for (String s : states) {
            if (model.toUpperCase().equals(s)) return true;
        }
        
        return false;
    }
    
    private final String[] provinces = new String[] {
        "AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "QC", "SK", "YT"
    };
    private final String[] states = new String[] {
        "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", 
        "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", 
        "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", 
        "OH", "OK", "OR", "PA", "RI", "SC", "SD", "TN", "TX", "UT", "VT", "VA",
        "WA", "WV", "WI", "WY"
    };

}
