/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   AppFramework
 *  Class      :   GreaterThanVerifier.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 22, 2022
 *  Modified   :   Dec 22, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 22, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package com.pekinsoft.verifiers;

import java.awt.Color;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.3
 * @since 1.0
 */
public class GreaterThanVerifier extends InputVerifier {
    
    private Color error = new Color(1.0f, 0.832f, 0.832f);
    private final JLabel problemLabel;
    private final String errorMessage;
    private final Number otherValue;
    
    public GreaterThanVerifier (Number otherValue, JLabel problemLabel, String errorMessage) {
        this.problemLabel = problemLabel;
        this.errorMessage = errorMessage;
        this.otherValue = otherValue;
    }

    @Override
    public boolean verify(JComponent input) {
        boolean valid = true;   // Assume valid
        String toVerify = null;
        if (input != null && input instanceof JTextComponent) {
            toVerify = ((JTextComponent) input).getText();
        }
        if (toVerify == null) {
            valid = false;
        }
        
        if (valid) {
            Number thisValue = null;
            try {
                thisValue = NumberFormat.getNumberInstance(Locale.getDefault()).parse(toVerify);
            } catch (ParseException e) {
                valid = false;
            }

            if (thisValue == null) {
                valid = false;
            }
            
            if (valid) {
                valid = thisValue.doubleValue() > otherValue.doubleValue();
            }
        }
        
        if (!valid) {
            input.setBackground(error);
            if (problemLabel != null && !errorMessage.trim().isEmpty()) {
                problemLabel.setText(errorMessage);
            }
        } else {
            Color bg = UIManager.getColor("TextField.background");
            input.setBackground(bg == null ? UIManager.getColor("text") : bg);
            if (problemLabel != null) {
                problemLabel.setText("");
            }
        }
        
        return valid;
    }

}
