# Application Framework API
This API was created by Pekin<sup>SOFT</sup> Systems to allow for ease of development of modular/pluggable Java J2SE/Swing desktop applications.

This API includes plenty of interfaces and abstract classes for allowing further application development through the use of pluggable modules, which will
also need to rely on this API. Further, this API provides Logging interfaces, Prerferences interfaces, etc., which may be used by applications developed using this API.
