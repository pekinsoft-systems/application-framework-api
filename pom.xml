<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.pekinsoft</groupId>
    <artifactId>application-framework-api</artifactId>
    <version>3.7.5</version>
    <packaging>jar</packaging>
    <name>Java/Swing Desktop Application Platform API</name>
    <description>
        A framework on which quality Java/Swing desktop applications may be
        built. This framework allows for modular application development via its
        Plugin Subsystem, with its primary access via the PluginManager. Various
        desktop components are included allowing for seemless integration with
        the rest of the framework. High-quality resources management is
        provided via the ResourceMap, which includes built-in converters for
        most common Swing types. There is a custom logging system that allows
        loggers to be obtained as soon as needed, but will buffer the messages
        until such time that the framework is ready and an operating-specific
        log file location can be retrieved from the LocalStorage class. A new,
        simplified Wizard API is a part of this framework, allowing developers
        to include Wizards within their UI in a simple, intuitive manner.
        Finally, this version has now introduced a two-model Security Subsystem,
        which allows for the default model which has a single default user
        account, or for requiring all uses to log into the application and
        features (Plugins) can be installed/activated based upon the access
        rights of the user group(s) to which the current user belongs.
    </description>
    <url>https://gitlab.com/pekinsoft-systems/application-framework-api/</url>
    <!-- Licensing information. -->
    <licenses>
        <license>
            <name>GNU Lesser General Public License</name>
            <url>https://www.gnu.org/licenses/lgpl-3.0.en.html</url>
            <distribution>repo</distribution>
            <comments>A copyleft license.</comments>
        </license>
    </licenses>
    <!-- Company Information. -->
    <organization>
        <name>PekinSOFT Systems</name>
        <url>https://gitlab.com/pekinsoft-systems</url>
    </organization>
    <!-- Developer(s) Information. -->
    <developers>
        <developer>
            <id>SC</id>
            <name>Sean Carrick</name>
            <email>sean@pekinsoft.com</email>
            <url>https://gitlab.com/pekinsoft-systems</url>
            <organization>PekinSOFT Systems</organization>
            <organizationUrl>https://gitlab.com/pekinsoft-systems</organizationUrl>
            <roles>
                <role>Manager</role>
                <role>Project Lead</role>
                <role>Developer</role>
            </roles>
            <timezone>-6</timezone>
        </developer>
        <developer>
            <id>JK</id>
            <name>Jiří Kovalský</name>
            <email/>
            <organization>PekinSOFT Systems</organization>
            <organizationUrl>https://gitlab.com/pekinsoft-systems</organizationUrl>
            <roles>
                <role>Manager</role>
                <role>Testing Lead</role>
                <role>Developer</role>
            </roles>
        </developer>
    </developers>
    <!-- Contributor(s) Information -->
    <contributors>
        <contributor>
            <name>Kevin Nathan</name>
            <email>knathan@project54.com</email>
            <roles>
                <role>Developer</role>
                <role>Tester</role>
            </roles>
            <timezone>-7</timezone>
        </contributor>
    </contributors>
    <scm>
        <url>https://gitlab.com/pekinsoft-systems/application-framework-api</url>
        <connection>
            https://gitlab.com/pekinsoft-systems/application-framework-api.git
        </connection>
        <developerConnection>
            https://gitlab.com/pekinsoft-systems/application-framework-api.git
        </developerConnection>
        <tag>HEAD</tag>
    </scm>

    <!--repositories>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/groups/pekinsoft-systems/-/packages</url>
        </repository>
    </repositories -->

    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://s01.oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <!--distributionManagement>
        <repository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/groups/pekinsoft-systems/-/packages</url>
        </repository>
        <snapshotRepository>
            <id>gitlab-maven</id>
            <url>https://gitlab.com/groups/pekinsoft-systems/-/packages</url>
        </snapshotRepository>
    </distributionManagement-->

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.release>21</maven.compiler.release>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.jtattoo</groupId>
            <artifactId>JTattoo</artifactId>
            <version>1.6.13</version>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.13.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <issueManagement>
        <system>GitLab</system>
        <url>https://gitlab.com/pekinsoft-systems/application-framework-api/-/issues</url>
    </issueManagement>

    <reporting>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-changes-plugin</artifactId>
                <version>2.12.1</version>
                <reportSets>
                    <reportSet>
                        <reports>
                            <report>changes-report</report>
                            <report>jira-report</report>
                        </reports>
                    </reportSet>
                </reportSets>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>3.6.0</version>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.7.0</version>
                <configuration>
                    <tags>
                        <tag>
                            <name>status</name>
                            <!-- status tag for all places -->
                            <placement>a</placement>
                            <head>Class Status:</head>
                        </tag>
                    </tags>
                </configuration>
            </plugin>
        </plugins>
    </reporting>
    <profiles>
        <profile>
            <id>reporting</id>
            <reporting>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-changes-plugin</artifactId>
                        <version>2.12.1</version>
                        <configuration>
                            <!-- For JIRA report -->
                            <columnNames>Type,Key,Summary,Assignee</columnNames>
                            <maxEntries>200</maxEntries>
                            <onlyCurrentVersion>true</onlyCurrentVersion>
                            <resolutionIds>Fixed</resolutionIds>
                            <sortColumnNames>Type,Key</sortColumnNames>
                        </configuration>
                        <reportSets>
                            <reportSet>
                                <reports>
                                    <report>jira-report</report>
                                </reports>
                            </reportSet>
                        </reportSets>
                    </plugin>
                </plugins>
            </reporting>
        </profile>
        <profile>
            <id>run-its</id>
            <properties>
                <maven.invoker.failure.ignore>false</maven.invoker.failure.ignore>
            </properties>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-invoker-plugin</artifactId>
                        <version>3.7.0</version>
                        <configuration>
                            <ignoreFailures>${maven.invoker.failure.ignore}</ignoreFailures>
                            <cloneProjectsTo>${project.build.directory}/it</cloneProjectsTo>
                            <!--pomIncludes>
                                <pomInclude>*/pom.xml</pomInclude>
                                <pomInclude>*/parent/pom.xml</pomInclude>
                            </pomIncludes>
                            <pomExcludes>
                                <pomExclude>pom.xml</pomExclude>
                            </pomExcludes-->
                            <localRepositoryPath>${project.build.directory}/local-repo</localRepositoryPath>
                            <goals>
                                <goal>site</goal>
                            </goals>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.sonatype.plugins</groupId>
                    <artifactId>nexus-staging-maven-plugin</artifactId>
                    <version>1.7.0</version>
                    <extensions>true</extensions>
                    <configuration>
                        <serverId>ossrh</serverId>
                        <nexusUrl>https://s01.oss.sonatype.org/</nexusUrl>
                        <autoReleaseAfterClose>true</autoReleaseAfterClose>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <artifactId>maven-site-plugin</artifactId>
                <version>3.12.1</version>
            </plugin>
            <plugin>
                <artifactId>maven-project-info-reports-plugin</artifactId>
                <version>3.6.0</version>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.3.0</version>
                <configuration>
                    <skip>false</skip>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-jar-plugin</artifactId>
                <version>3.4.2</version>
            </plugin>
            <plugin>
                <artifactId>maven-install-plugin</artifactId>
                <version>3.1.2</version>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
                <executions>
                    <execution>
                        <id>simple-command</id>
                        <phase>package</phase>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>3.1.2</version>
            </plugin>
            <!-- Publishing requires sources -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-source-plugin</artifactId>
                <version>3.3.1</version>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>jar-no-fork</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- Publishing requires JavaDocs -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.7.0</version>
                <configuration>
                    <tags>
                        <tag>
                            <name>status</name>
                            <!-- status tag for all places -->
                            <placement>a</placement>
                            <head>Class Status:</head>
                        </tag>
                    </tags>
                </configuration>
                <executions>
                    <execution>
                        <id>attach-sources</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>jar</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <!-- Publishing requires PGP signatures -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-gpg-plugin</artifactId>
                <version>3.2.4</version>
                <executions>
                    <execution>
                        <id>sign-artifacts</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>sign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>