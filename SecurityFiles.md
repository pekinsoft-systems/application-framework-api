# *Application Framework API*
## ***Security API***
In building the Security API, we will be able to limit the features that a
`SystemUser` has access to based upon their primary and additional `UserGroup`s.

The Security API is loosely modelled off of the *Nix security subsystem. We have
three security files that need to be created the first time an application is
launched, and that is handled by a `Wizard` that is contained within the
`com.pekinsoft.security.ui` package.

These security files are discussed in detail in the following sections.

### The ".shadow" File
The `.shadow` file maintains information regarding the `SystemUser`'s password,
as well as some application-wide policies. All of this is set up each time a new
`SystemUser` account is created. The file breaks out as follows:

```
1:2:3:4:5:6:7:8:9
```

1. The user's username. This is the username entered into a logon dialog.
2. The salt. Salt is used to ensure the security of the password's hashing
   function. It is generated when the user account is created.
3. The hash. This is the one-way hash of the user's password.
4. Creation Date. This is the date the password was last changed. This value is
   used by the password aging policies. This value is stored as the number of
   days since the Epoch (January 1, 1970).
5. Forced Change. If this value is set to 0, then the user must change their
   password when they logon using the set password. This is typically used for
   temporary passwords when the account is first created.
6. Must Change. This is the length of time (in days) when the user will need
   to change the password. By adding this value to the creation value, the
   expiration date can be determined.
7. Warning Period. This is the number of days prior to the expiration date that
   the user should start being warned that their password is about to expire.
8. Grace Period. This is the number of days after the expiration date that the
   user's account will remain active. During this period, the user may change
   their password and all values will be reset. If this period expires before
   the user changes their password, then their user account will be deactivated.
   Once deactivated, a user with administrator privileges will need to reactivate
   the account.
9. This is the expiration date for the current password. This is stored as the
   number of days since the Epoch (January 1, 1970), and is figured by adding to
   the creation days the number of days until the password must be changed.

### The "group" File
The `group` file maintains the user groups defined on the system. Every user will
be assigned to a primary group, and may be assigned to additional groups. At
least one user must be assigned to the administration group. The file breaks out
as follows:
```
1:2:3
```

1. The group's name. This is a human-friendly name for the group, such as "adm",
   "accounting", "shop", etc.
2. The group's GID, or **G**roup **ID**entification number. This value uniquely
   identifies the group.
3. A comma-separated list of users assigned to the group. This is a list of
   usernames, not UIDs.

### The "passwd" File
The `passwd` file maintains the user account data, including username, UID, GID,
and user's full name. The file breaks out as follows:
```
1:2:3:4:5
```

1. The username for the account. This value can contain letters, underscores,
   dashes, dots, and digits.
2. The UID **U**ser **ID**entification number, which is like the primary key for
   the `passwd` file.
3. The GID **G**roup **ID**entification number of the user's primary user group.
4. The user's full name. This value can actually be any semi-free-form string,
   but historically is the user's full name or a description, such as "CEO".
5. This is the strength of the current password. The strength is checked by
   applying some rules for calculating how easy or hard the password would be to
   crack.

### Related Classes in the Security API
Each of these files have classes which are related to them, but only two of the
classes are publicly accessible: `SystemUser` and `UserGroup`. These two classes
may be used to allow/prevent accessing portions of a secure application.
Regardless if the application uses only the *default security model*, these
classes are still used for the default user account, which typically has the
username and primary group name that is the application's ID value.

When the application uses the *enhanced security model*, all users must logon
each time they launch the application. Also, if the initial user establishes the
policy of login timeout, if the user is inactive for a set period of time, they
will be logged out of the application and the main application window will be
covered until such time that the user enters their logon credentials again. At
that time, the timeout system is restarted.

When the application is launched for the first time, the *Security Setup Wizard*
is run to allow the initial user to configure security for the application. On
the first data step of the Wizard, the user can decide that users are not
required to logon with the application. If this option is chosen, when the user
clicks on Next, they will be taken to the summary screen or they can immediately
Finish the Wizard. This will enable the default security model and create a
"dummy" user account and group.

If the user choses to leave the option to require logons, they will be able to
move to the Password Policies and Aging step, then they will need to create an
Administrative User Account, which will have unfettered access to all features
of the application. This user account will be able to be used to create other
user accounts for other users of the application. Whenever the user is logged on
as the administrative user, they will need to exercise extreme caution because
they will be able to do things that may break the application.

### LogonManager
The `LogonManager` handles all aspects for loggin onto the application, including
storing the currently logged on user's account information, which can be used by
the application for disallowing features and `Plugin`s from being availble to
the user based upon the user's group membership(s).
